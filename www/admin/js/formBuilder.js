/**
 * 
 */

function SLi_scrollToAnchor(aid){
	//alert(aid);
    var aTag = $("a[name=\'"+ aid +"\']");
    $("html,body").animate({scrollTop: aTag.offset().top},"slow");
}

function SLi_DelConfirm(tmpId) {
	if (confirm("Delete this item?")) {
		var tmpK   = "#"+tmpId;
		var tmpL1  = "#view-"+tmpId;
		var tmpL2  = "#del-"+tmpId;
		var tmpOld = "#old-"+tmpId; // cancella il campo del vecchio (IMP se devo solo cancellare il vecchio)
		$(tmpOld).val("");
		$(tmpK).parent().show();
		$(tmpL1).hide();
		$(tmpL2).hide();
		return true;
	} else {
		return false;
	}
}