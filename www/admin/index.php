<?php
/*
	Project:     Ctrl-Area 
	Version:     5.201301.30
	Author:      Silvio (SLivio) Coco
	Contact:     silvio.coco@gmail.com
	Copyright:   http://www.ctrl-area.org/copyright/
*/

// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -> [CORE - REGOLE, VARIABILI, UTILITY]
/*
	$_GET['m']  >> module_id
	$_GET['op'] >> op relativa al module_id
	$_GET['p']  >> pagina
	'check'     >> Questa stringa identifica un  controllo
	'update'    >> ''     ''      ''         un  aggiornamento (visualizza interfaccia per)
	'set'       >> ''     ''      ''         una modifica (effettua)
	'add'       >> ''     ''      ''         un  inserimento
	'form'      >> ''     ''      ''         la visualizzazione di un form per inserimento / modifica
	GLOBAL      >> Per le funzioni che necessitano l'utilizzo delle variabili di configurazione nelle funzioni:
				GLOBAL $CONF, $aModules, $aModule, $op, $m, $p;
*/


// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -> [CORE_CONF]
require_once('inc.config.php');

$CONF['path_modules'] = 'modules';
$CONF['def_modules']  = ''; // Default modules
$CONF['def_page']     = 20; // Default elements for lists page
$CONF['path_module']  = ''; // Active module path
$CONF['admin_id']     = (isset($_SESSION[$SESSION_ID]['admin'])   && $_SESSION[$SESSION_ID]['admin'] != '' && $_SESSION[$SESSION_ID]['admin'] != 0) ? $_SESSION[$SESSION_ID]['admin']   : 0;
$CONF['admin_m']      = (isset($_SESSION[$SESSION_ID]['modules']) && is_array($_SESSION[$SESSION_ID]['modules']))                                   ? $_SESSION[$SESSION_ID]['modules'] : 0;
$CONF['admin_info']   = (isset($_SESSION[$SESSION_ID]['info'])    && is_array($_SESSION[$SESSION_ID]['info']))                                      ? $_SESSION[$SESSION_ID]['info']    : 0;


$op  = (isset($_REQUEST['op'])   && $_REQUEST['op']  != '')  ? $_REQUEST['op']   : '';
$m   = (isset($_REQUEST['m'])    && $_REQUEST['m']   != '')  ? $_REQUEST['m']    : $CONF['def_modules'];
$p   = (isset($_REQUEST['p'])    && $_REQUEST['p']   != '')  ? $_REQUEST['p']    : 0;
$pop = (isset($_REQUEST['pop'])  && $_REQUEST['pop'] != '')  ? true              : false;
$opp = (isset($_REQUEST['opp'])  && $_REQUEST['opp'] != '')  ? $_REQUEST['opp']  : '';
$app = (isset($_REQUEST['app'])  && $_REQUEST['app'] != '')  ? true              : false;
$res = (isset($_REQUEST['res'])  && $_REQUEST['res'] != '')  ? $_REQUEST['res']  : false; // [+] Version 5.201301
$mod = (isset($_REQUEST['mod'])  && $_REQUEST['mod'] != '')  ? true : false;
$hex = (isset($_REQUEST['hex'])  && $_REQUEST['hex'] != '')  ? $_REQUEST['hex']  : '';

$aModules = GDB__Get_AdminModules($CONF['admin_id'], true);
$aModule  = Array();

if ($m != '') {
	$aModule              = GDB__Get_InfoModule($m);
	$CONF['path_module']  = $CONF['path_modules'].'/'.$aModule['module_dir'].'/';
}

if ($op=='clean-error' || $op=='unset-error' || $op=='clear-error') {
	$_SESSION[$SESSION_ID]['error'] = Array();
}


// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -> [CORE_HEADER]
/* 
	Queste righe permettono di chiamare la funzione header.
	E' necessario assegnare alla variabile GET/POST header l'istruzione da eseguire, ad esempio:
	header=location:index.php
*/
$function_header = (isset($_REQUEST['header']) && $_REQUEST['header'] != '') ? $_REQUEST['header'] : '';
if ($function_header != '') {
	header($function_header);
	exit();
}

if ($app==true) {
	get_App($hex);
	die();
	exit();
}

if($res!=false) { // [+] Version 5.201301
	GHTML__Get_Header_Pop();
	require_once('res/'.$op);
	GHTML__Get_FooterPop();
	GHTML__Get_Debug();
	die();
}

if (($pop==false) && ($mod == false))  {
	GHTML__Get_Header('','','');
	GHTML__Get_Left();
	GHTML__Get_Right();
	GHTML__Get_Debug();
    GHTML__Get_Footer();
}

if ($mod == true) {
	GHTML__Get_Header_Mod();
	GHTML__Get_Right_Mod();
	GHTML__Get_Footer();
}

if ($pop==true) {
	GHTML__Get_Header_Pop();
	GHTML__Get_Right();
	GHTML__Get_Footer_Pop();
	GHTML__Get_Debug();
}

?>