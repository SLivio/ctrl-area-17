<?php
/*
	Project:     Ctrl-Area 
	Version:     7.20170110
	Author:      Silvio (SLivio) Coco
	Contact:     silvio.coco@gmail.com
	Copyright:   http://www.ctrl-area.org/copyright/
*/

// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -> [ CONFIG :: Ctrl-Aera 1 ]
ini_set('post_max_size',           '10M');
ini_set('upload_max_filesize',     '10M');
ini_set('date.timezone',           'Europe/Rome');
error_reporting(E_ALL ^ E_DEPRECATED);
//error_reporting(E_ALL);
session_start();
define('APP_NAME',                 'Ctrl-Area');
define('APP_VERSION',              '7.2017.00');
$SESSION_ID   = md5(session_id().APP_NAME.APP_VERSION);
$COOKIE_NAME  = 'ctrl-area';

if (!isset($_SESSION[$SESSION_ID]) || !is_array($_SESSION[$SESSION_ID])) $_SESSION[$SESSION_ID] = Array();
if (!isset($LOCAL_CONF) || !is_array($LOCAL_CONF))                       $LOCAL_CONF = Array();


// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -> [ CONFIG :: Ctrl-Aera 2 ]
$myHOST = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'localhost';
switch($myHOST) {
	case 'studiozavagnini.it' :
	case 'www.studiozavagnini.it' :
		$HOST  = 'localhost';
		$USER  = 'silvius';
		$PSWD  = 'coco.16';
		$DB    = 'zava_demo';
		define('APPLICATION_URL',    'http://www.studiozavagnini.it/adminz/');
		define('APPLICATION_PATH',   $_SERVER['DOCUMENT_ROOT'].'/htdocs/');
		define('APPLICATION_UPLOAD', '../files/');
		define('NL',                 "\n");
		define('DOMAIN',             'http://www.studiozavagnini.it');
		define('DB_PREFIX',          'aa_');
		define('DEBUG',              '1');
		define('APP_THEME',          'flatie'); // night
		define('INTRANET',           true); // + 2016.12.08
	break;
	case 'ctrl-area' :
		$HOST  = 'localhost';
		$USER  = 'root';
		$PSWD  = 'root';
		$DB    = 'ctrl-area-17';
		define('APPLICATION_URL',    'http://ctrl-area/admin/');
		define('APPLICATION_PATH',   $_SERVER['DOCUMENT_ROOT'].'/');
		define('APPLICATION_UPLOAD', '../upload/');
		define('NL',                 "\n");
		define('DOMAIN',             'http://ctrl-area');
		define('DB_PREFIX',          'aa_');
		define('DEBUG',              '1');
		define('APP_THEME',          'flatie'); // night
		define('INTRANET',           true); // + 2016.12.08
		break;
	case 'localhost' :
	case '127.0.0.1' :
		$HOST  = 'localhost';
		$USER  = 'root';
		$PSWD  = 'root';
		$DB    = 'ctrl-area-17';
		define('APPLICATION_URL',    'http://localhost/GIT_ctrl-area-17/www/admin/');
		define('APPLICATION_PATH',   $_SERVER['DOCUMENT_ROOT'].'/GIT_ctrl-area-17/www/');
		define('APPLICATION_UPLOAD', '../upload/');
		define('NL',                 "\n");
		define('DOMAIN',             'http://localhost');
		define('DB_PREFIX',          'aa_');
		define('DEBUG',              '1');
		define('APP_THEME',          'flatie'); // night
		define('INTRANET',           true); // + 2016.12.08
		break;
}

// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -> [ CONFIG :: Project ]
define('EM_FROM_EMAIL',      'no-reply@ctrl-aera.com');
define('EM_FROM_NAME',       'ctrl-aera');
define('MASTER_EMAIL',       'info@ctrl-aera.com');
define('MASTER_NAME',        'ctrl-aera');
define('NOREPLY_EMAIL',      'no-reply@ctrl-aera.com');
define('HELP_EMAIL',         'help@ctrl-aera.com');



// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -> [ CORE :: Get Librearies ]

// LastUpdate 2014.02.28 [ok.6]
function get_Libraries($path='php_lib') {
	if (phpversion()<5) {
		$dh  = opendir($path);
		while (false !== ($filename = readdir($dh))) {
		    $aFile[] = $filename;
		}
		sort($aFile);
		foreach($aFile as $el) {
			if (trim($el) != '.' && trim($el) != '..' && !is_dir($path.'/'.$el) && $el{0}!='.' && $el{0}!='_')
				require_once($path.'/'.$el);
		}
		include_once($path.'/'.'phpmailer_4/class.phpmailer.php');
	} else {
		$aFile = scandir($path);
		foreach($aFile as $el) {
			if (trim($el) != '.' && trim($el) != '..' && !is_dir($path.'/'.$el) && $el{0}!='.' && $el{0}!='_') require_once($path.'/'.$el);
		}
		include_once($path.'/'.'phpmailer_5/class.phpmailer.php');
	}
}

$PATH_LIB = (isset($PATH_LIB) && $PATH_LIB != '') ? $PATH_LIB : './php_lib/';
get_Libraries($PATH_LIB);
include_once($PATH_LIB.'/ValidForm/class.validform.php');



// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -> [ CORE :: DBConnection & Login ]
$_SESSION[$SESSION_ID]['db'] = DB__Connect($HOST, $DB, $USER, $PSWD);


require_once('inc.login.php');


?>