<?php
/*
	Project:     Ctrl-Area 
	Version:     5.201301.18
	LastUpdate:  2013.04.14
*/



// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -> [ CONFIG :: Ctrl-Aera 1 ]
$DOMAINS_ALLOWED = Array('@ctrl-area.org', '@me.com'); // Users can recovery password and register only with this email domains
$REG_FROM_NAME   = 'Ctrl-Area v16';
$REG_FROM_EMAIL  = 'info@ctrl-area.org';
$REG_REPLY_NAME  = 'no-reply';
$REG_REPLY_EMAIL = 'no-reply@ctrl-area.org';
$em_confirm_subject  = "Ctrl-Area :: Welcome.";
$em_confirm_txt      = "Grazie per esserti registrato.\n\nVai al link: #LINK#\nper confermare la registrazione.\n\nI tuoi dati di accesso sono:\nemail: #EMAIL#\npassword: #PWD# \n\n";
$em_recupero_subject = "Ctrl-Area :: Recupero della password.";
$em_recupero_txt     = "La tua password &egrave; stata reimpostata.\n\nI tuoi nuovi dati di accesso sono:\nemail: #EMAIL#\npassword: #PWD# \n\nVai al link: #LINK#\n\n accedi e modifica la tua password usando l'apposito modulo.\n\n";
$em_html             = nl2br($em_confirm_txt);
define('em_smtp', ''); // [test]

// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -> [ LOGIN :: Var ]
$LOGIN_OP       = (isset($_REQUEST['op'])    && $_REQUEST['op'] != '')    ? $_REQUEST['op']             : '';
$LOGIN_EMAIL    = (isset($_POST['email'])    && $_POST['email'] != '')    ? strtolower($_POST['email']) : '';
$LOGIN_PWD      = (isset($_POST['password']) && $_POST['password'] != '') ? $_POST['password']          : '';
$COOKIE_ENABLE  = 1;


// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -> [ LOGIN :: Functions ]


// LastUpdate 2014.02.28 [dev.6]
function HTML__Login_Head() {
	$TEMPLATE_START = '<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>'.((defined('APP_NAME')) ? APP_NAME : 'Ctrl-Area').'</title>
	<meta name="author" content="SLivio">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
	<link rel="shortcut icon" href="img/favicon.ico">
	<link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
	<link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
	<link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
	<link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
	<link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
	<link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
	<link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/plugins.css">
	<link rel="stylesheet" href="css/main.css">
	'.((defined('APP_THEME')) ? '<link id="theme-link" rel="stylesheet" href="css/themes/'.APP_THEME.'.css">' : '').'
	<link rel="stylesheet" href="css/themes.css">
	<script src="js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
</head>
<body>
	<div id="login-background"><img src="css/themes/'.APP_THEME.'/login_header.jpg" alt="Login Background" class="animation-pulseSlow"></div>
	<div id="login-logo"><img src="css/themes/'.APP_THEME.'/logo.png" alt="'.APP_NAME.'"></div>';
	return $TEMPLATE_START;
}


// LastUpdate 2014.02.28 [ok.6]
function HTML__Login_Foot() {
	return '
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script>!window.jQuery && document.write(unescape(\'%3Cscript src="js/vendor/jquery-1.11.0.min.js"%3E%3C/script%3E\'));</script>

	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/app.js"></script>
	</body>
</html>';
}



// LastUpdate 2014.02.28 [ok.6]
function HTML__Login_FormLogin($error='') {
	GLOBAL $COOKIE_ENABLE, $COOKIE_NAME;
	if ($COOKIE_ENABLE==1 && $COOKIE_NAME != '') {
		$email = (isset($_COOKIE[$COOKIE_NAME]) && $_COOKIE[$COOKIE_NAME]!='') ? $_COOKIE[$COOKIE_NAME] : '';
	} else {
		$email = '';
	}
	?>
	<div id="login-container" class="animation-fadeIn">
		 <!-- Login Title -->
		<div class="login-title text-center">
			<h1><strong><?php echo APP_NAME ?></strong><br><small><strong>Area di amministrazione</strong></small></h1>
		</div>
		<!-- END Login Title -->

		<!-- Login Block -->
		<div class="block remove-margin">
			<!-- Login Form -->
			<form action="?op=LOGIN" method="post" id="form-login" class="form-horizontal form-bordered form-control-borderless">
				<div class="form-group">
					<div class="col-xs-12">
						<div class="input-group">
							<span class="input-group-addon"><i class="gi gi-envelope"></i></span>
							<input type="text" id="login-email" name="email" class="form-control input-lg" placeholder="Email" value="<?php echo $email; ?>">
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12">
						<div class="input-group">
							<span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
							<input type="password" id="login-password" name="password" class="form-control input-lg" placeholder="Password">
						</div>
					</div>
				</div>
				<div class="form-group form-actions">
					<div class="col-xs-4">
						<label class="switch switch-primary" data-toggle="tooltip" title="Ricordami">
							<input type="checkbox" id="login-remember-me" name="login-remember-me" checked>
							<span></span>
						</label>
					</div>
					<div class="col-xs-8 text-right">
						<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Login </button>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12">
						<p class="text-center remove-margin"><small>Password dimenticata?</small> <a href="?op=REQ" id="link-login"><small>Recupera!</small></a></p>
					</div>
				</div>
			</form>
			<!-- END Login Form -->
			
		</div>
		<!-- END Login Block -->
	</div>
	<!-- END Login Container -->	

   	<?php
}



// LastUpdate 2014.02.28 [ok.6]
function HTML__Login_FormPwdimenticata($error='') {
	?>
	<div id="login-container" class="animation-fadeIn">
		 <!-- Login Title -->
		<div class="login-title text-center">
			<h1><strong><?php echo APP_NAME ?></strong><br><small><strong>Area di amministrazione</strong></small></h1>
		</div>
		<!-- END Login Title -->

		<!-- Login Block -->
		<div class="block remove-margin">
			<!-- Login Form -->
			<form action="?op=REQ2" method="post" id="form-login" class="form-horizontal form-bordered form-control-borderless">
				<div class="form-group<?php /* Login error */ if ($error != '')  echo ' has-error'; ?>">
					<div class="col-xs-12">
						<?php /* Login error */ if ($error != '')  echo '<p class="text-center text-danger">'.$error.'</p>'; ?>
						<div class="input-group">
							<span class="input-group-addon"><i class="gi gi-envelope"></i></span>
							<input type="text" id="login-email" name="email" class="form-control input-lg" placeholder="Email" >
						</div>
					</div>
				</div>
				<div class="form-group form-actions">
					<div class="col-xs-12 text-right">
						<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Recupera </button>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-12">
						<p class="text-center remove-margin"><small>Hai gi&agrave; un account?</small> <a href="?ref=REQ" id="link-login"><small>Login</small></a></p>
					</div>
				</div>
			</form>
			<!-- END Login Form -->
			
		</div>
		<!-- END Login Block -->
	</div>
	<!-- END Login Container -->
	<?php
}


// NeedUpdate
function HTML__Login_NonConsentito($error='') {
	?><!--
	<div id="content">
		<fieldset class="f_login">
			<legend>Errore</legend>
			Operazione non consentita.<br />Contattare l'amministratore.
		</fieldset>
	</div>--><?php
}


// NeedUpdate
function HTML__Login_FormRegistra($error='') {
	?><!--
	<div id="content">
		<?php
		if ($error != '') {
			echo '<div class="res_ko">'.$error.'</div>';
		}
		?>
		<form name="login" action="?op=REG2" method="post">
			<fieldset class="f_login">
				<legend>New User:</legend>
				<div>
					<label for="email">Email:</label>
					<input type="text" name="email" id="email" value="" />
				</div>
				<div>
					<label for="password">Password:</label>
					<input type="password" name="password" id="password" value="" />		
				</div>
				<div>
					<label for="security_code"><img src="res/captcha/CaptchaSecurityImages.php?width=70&height=30&characters=5" title="Inserisce il codice Captcha" /></label>
					<input type="text" name="security_code" id="security_code" value="" />		
				</div>
				<div class="dbutton">
					<input type="submit" name="login" value="REGISTRATI" />
				</div>
				<div class="a_recovery">
					<a href="?">Back</a>
				</div>
			</fieldset>
		</form>
	</div>
	--><?php
}




// NeedUpdate
function HTML__Login_OkRegistrato() {
	?>
	<div id="login-container" class="animation-fadeIn">
		 <!-- Login Title -->
		<div class="login-title text-center">
			<h1><i class="gi gi-flash"></i> <strong>Saatchi x Amnesty</strong><br><small><strong>Area di amministrazione</strong></small></h1>
		</div>
		<!-- END Login Title -->

		<!-- Login Block -->
		<div class="block remove-margin">
			<div class="form-group">
				Controlla la casella di posta elettronica e segui le istruzioni che ti abbiamo inviato per email.
				<br /><br />
				GRAZIE!
				</div>
		</div>
		<!-- END Login Block -->
	</div>
	<!-- END Login Container -->
	<?php
}


// NeedUpdate
function HTML__Login_OkRecupera() {
	?>
	<div id="login-container" class="animation-fadeIn">
		 <!-- Login Title -->
		<div class="login-title text-center">
			<h1><i class="gi gi-flash"></i> <strong>Saatchi x Amnesty</strong><br><small><strong>Area di amministrazione</strong></small></h1>
		</div>
		<!-- END Login Title -->

		<!-- Login Block -->
		<div class="block remove-margin">
			<div class="form-group">
				Controlla la casella di posta elettronica e segui le istruzioni che ti abbiamo inviato per email.
				<br /><br />
				GRAZIE!
				</div>
		</div>
		<!-- END Login Block -->
	</div>
	<!-- END Login Container -->
	<?php
}



$CON = $_SESSION[$SESSION_ID]['db'];
// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- -> [ LOGIN :: Engine ]
function MY_SWITCH(){ }
switch($LOGIN_OP) {
	// ----- ----- ----- ----- ----- -> [LOGOUT]
	// LastUpdate 2013.04.14 [test]
	case 'LOGOUT' :
		//GDB__Add_Log(GLOBAL_GetUser('user_id'), GLOBAL_GetUser('country_id'), 'LOGOUT', ''); // LOG
		GDB__Add_Log($CON, '', 'LOGOUT', '');
		DB__Close($CON);
		if (isset($SESSION_ID) && $SESSION_ID!='') $_SESSION[$SESSION_ID] = false;
		else session_destroy();
		echo JS_header('index.php');
		die();
	break;
	// ----- ----- ----- ----- ----- -> [LOGIN]
	case 'LOGIN' :
		$error = '';
		//GDB__Add_Log(0, 0, 'LOGIN_ATTEMPT', $LOGIN_EMAIL); // LOG
		GDB__Add_Log('', 'LOGIN_ATTEMPT', $LOGIN_EMAIL);
		if ($COOKIE_ENABLE==1 && $COOKIE_NAME != '') {
			setcookie($COOKIE_NAME,$LOGIN_EMAIL,time() + (86400 * 7)); // 86400 = 1 day
		}
		if ($LOGIN_EMAIL != '' && $LOGIN_PWD != '') {
			$ut      = cleanMysqlInjection($LOGIN_EMAIL);
			$pwd     = cleanMysqlInjection($LOGIN_PWD);
			$aInfoUt = GDB__Get_Admin('', $ut, $pwd);
			if (count($aInfoUt) >= 2 && isset($aInfoUt['user_enable']) && $aInfoUt['user_enable'] == 1) {
				$_SESSION[$SESSION_ID]['login']       = true;
				$_SESSION[$SESSION_ID]['admin']       = $aInfoUt['user_id'];
				$_SESSION[$SESSION_ID]['admin_email'] = $aInfoUt['user_email'];
				$_SESSION[$SESSION_ID]['info']        = GLOBAL_AdminSetUp($aInfoUt['user_id']);
				$_SESSION[$SESSION_ID]['mda']         = md5($aInfoUt['user_id'].$aInfoUt['user_email']);
				$_SESSION[$SESSION_ID]['modules']     = GDB__Get_AdminModules($aInfoUt['user_id']);
				//GDB__Add_Log(GLOBAL_GetUser('user_id'), GLOBAL_GetUser('country_id'), 'LOGIN_SUCCESS', $ut); // LOG
				GDB__Add_Log('', 'LOGIN_SUCCESS', $ut);
			} else {
				$error = 'The email address or password you provided does not match our records.';
			}
		} else {
			$error = 'The email address or password you provided does not match our records.';
		}
		if ($error!='') {
			echo HTML__Login_Head();
			HTML__Login_FormLogin($error);
			echo HTML__Login_Foot();
			die();
		}
	break;
	//------------------------------------------------------[REG]
	case 'REG' :
		echo HTML__Login_Head();
		HTML__Login_NonConsentito();
		//HTML__Login_FormRegistra();
		echo HTML__Login_Foot();
		die();
	break;
	//------------------------------------------------------[REG2]
	case 'REG2' :
		HTML__Login_NonConsentito();
		die();
	
		if(isset($_SESSION['security_code']) && isset($_POST['security_code'])
		 && $_SESSION['security_code']==$_POST['security_code'] && !empty($_SESSION['security_code'] ) ) {
			$error = '';
			if (checkField_Email($LOGIN_EMAIL)) {
				$domain = trim(strstr($LOGIN_EMAIL, '@'));
				if (in_array($domain, $DOMAINS_ALLOWED)) {
					//
					$count = countRecord(DB_PREFIX.'core_admin', 'user_id', 'WHERE user_email="'.cleanMysqlInjection($LOGIN_EMAIL).'"', 0);
					if ($count==0) {
						// aggiungo record
						$u_id  = DB_Login_Add_Admin($LOGIN_EMAIL, $LOGIN_PWD);
						// mando email
						if ($u_id >= 1) {
							$em_url_conf = APPLICATION_URL.'?op=CONFIRM&ck='.$u_id.'|'.md5($u_id.$LOGIN_EMAIL);
							$em_txt_from = Array('#LINK#', '#EMAIL#',    '#PWD#');
							$em_txt_to   = Array($em_url_conf, $LOGIN_EMAIL, $LOGIN_PWD);
							$em_confirm_txt = str_replace($em_txt_from, $em_txt_to, $em_confirm_txt);
							$aTmp           = explode('@', $LOGIN_EMAIL);
							$em             = EMAIL_Login_Send_NewUser($REG_FROM_NAME, $REG_FROM_EMAIL, $aTmp[0], $LOGIN_EMAIL, $REG_REPLY_NAME, $REG_REPLY_EMAIL, $em_confirm_subject, $em_confirm_txt, nl2br($em_confirm_txt));
							// Ok, registrato
							echo HTML__Login_Head();
							HTML__Login_OkRegistrato();
							echo HTML__Login_Foot();
							die();
						} else {
							$error = 'Errore generico.';
						}
					} else {
						$error = 'Ti sei gi&agrave; iscritto con questo indirizzo email.<br />';
					}
					
				} else {
					$error = 'Registrazione non consentita.';
				}
			} else {
				$error = 'Indirizzo email non valido.';
			}
		} else {
			$error = 'Codice non valido.';
		}
		if ($error != '') {
			echo HTML__Login_Head();
			HTML__Login_FormRegistra($error);
			echo HTML__Login_Foot();
			die();
		}
	break;
	case 'REQ' :
		echo HTML__Login_Head();
		HTML__Login_FormPwdimenticata();
		echo HTML__Login_Foot();
		die();
	break;
	case 'REQ2' :
		// controllo email saatchi > true > manda email ripristino
		//                         > false > form con errore
		if (checkField_Email($LOGIN_EMAIL)) {
			$domain = trim(strstr($LOGIN_EMAIL, '@'));
			if (in_array($domain, $DOMAINS_ALLOWED)) {
				//
				$count = countRecord(DB_PREFIX.'core_admin', 'user_id', 'WHERE user_email="'.cleanMysqlInjection($LOGIN_EMAIL).'"', 0);
				if ($count==1) {
					// aggiungo record
					$aInfoUt = GDB__Get_Admin('', cleanMysqlInjection($LOGIN_EMAIL), '');
					$newPwd  = get_RandomString(6);
					$out     = DB_Set_Pwd($aInfoUt['user_id'],md5($newPwd));
					// mando email
					if ($out > 0) {
						$em_url_conf = APPLICATION_URL;
						$em_txt_from = Array('#LINK#', '#EMAIL#',    '#PWD#');
						$em_txt_to   = Array($em_url_conf, $LOGIN_EMAIL, $newPwd);
						$em_recupero_txt = str_replace($em_txt_from, $em_txt_to, $em_recupero_txt);
						$aTmp            = explode('@', $LOGIN_EMAIL);
						$em              = EMAIL_Login_Send_NewUser($REG_FROM_NAME, $REG_FROM_EMAIL, $aTmp[0], $LOGIN_EMAIL, $REG_REPLY_NAME, $REG_REPLY_EMAIL, $em_recupero_subject, $em_recupero_txt, nl2br($em_recupero_txt));
						// Ok, registrato
						echo HTML__Login_Head();
						HTML__Login_OkRecupera();
						echo HTML__Login_Foot();
						die();
					} else {
						$error = 'Errore generico.';
					}
				} else {
					$error = 'Non sei iscritto con questo indirizzo email.<br />';
				}
					
			} else {
				$error = 'Indirizzo email non valido. <small><br>[er 1]</small>';
			}
		} else {
				$error = 'Indirizzo email non valido. <small><br>[er 2]</small>';
		}
		if ($error != '') {
			echo HTML__Login_Head();
			HTML__Login_FormPwdimenticata($error);
			echo HTML__Login_Foot();
			die();
		}
	break;
	case 'CONFIRM' :
		$ck = (isset($_GET['ck']) && $_GET['ck'] != '') ? $_GET['ck'] : '';
		if (trim($ck) != '') {
			$aTmp = explode('|', $ck);
			$aU   = get_InfoRecord(DB_PREFIX.'core_admin', 'WHERE user_id="'.(int)$aTmp[0].'"', 0);
			if (!empty($aU)) {
				if (isset($aU['user_id']) && $aU['user_id'] != '' && isset($aU['user_email']) && $aU['user_email'] != '') {
					if (md5($aU['user_id'].$aU['user_email'])==trim($aTmp[1])) {
						update_Record(DB_PREFIX.'core_admin', 'user_enable', 1, 'WHERE user_id="'.$aU['user_id'].'"');
						$_SESSION[$SESSION_ID]['login']         = true;
						$_SESSION[$SESSION_ID]['admin']         = $aU['user_id'];
						$_SESSION[$SESSION_ID]['admin_email']   = $aU['user_email'];
						$_SESSION[$SESSION_ID]['info']          = GLOBAL_AdminSetUp($aU['user_id']);
						$_SESSION[$SESSION_ID]['mda']           = md5($aU['user_id'].$aU['user_email']);
						$_SESSION[$SESSION_ID]['modules']       = GDB__Get_AdminModules($aU['user_id']);
					} else {
						$error = 'Errore.';
					}
				} else {
					$error = 'Errore.';
				}
			} else {
				$error = 'Errore.';
			}
		} else {
			$error = 'Errore.';
		}
		if ($error != '') {
			echo HTML__Login_Head();
			HTML__Login_FormRegistra($error);
			echo HTML__Login_Foot();
			die();
		}
	break;
	case '' :
	default :
		if (!isset($_SESSION[$SESSION_ID]['login']) 
		 || $_SESSION[$SESSION_ID]['login']==false
		 || $_SESSION[$SESSION_ID]['login']=='') {
			$_SESSION[$SESSION_ID] = '';
			echo HTML__Login_Head();
			HTML__Login_FormLogin();
			echo HTML__Login_Foot();
			die();
		}
	break;
}


?>