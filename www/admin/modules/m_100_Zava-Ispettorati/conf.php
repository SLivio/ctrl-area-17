<?php /*
  Version:     2017.01.11
  Module:      Zava16.Ispettorati
  Author:      SLivio
*/



//--

$LOCAL_CONF = Array(
    'module_op' => Array(
        Array('id'    => '1',
            'label' => 'Gestione Ispettorati ',
            'op'    => 'op-list'),
        Array('id'    => '1',
            'label' => 'Nuovi Ispettorati ',
            'op'    => 'op-list-new')
    ),
    'module_label' => 'Gestisci Ispettorati',
    'key' => 'm_100_Zava-Ispettorati',
    'menu' => Array(
        'visible' => true
    ),
    'local_js' => 'this.lib.js?'.get_RandomString()
);



?>