/*
  Version:     2017.01.11
  Module:      Zava16.Ispettorati
  Author:      SLivio
*/

function addIspettorato(trId, sendUrl) {
	var k = '#'+trId+' .isp-lbl';
	var dataSend = "id="+$(k).text()+"&app=true"; 
	$.ajax({
		method: "POST",
		url: sendUrl,
		data: dataSend,
		async: true,
		dataType:"json"
	}).done(function(jsn) {
		//$('#'+trId).find(".pratiche").html(html);
		console.log(k+'::'+dataSend);
	});
}


function loadIspettorati(m, trId) {
		
		var dataSend = "lbl="+$('#'+trId).find('.isp-lbl').text()+"&app=true";
		
		
		$.ajax({
			method: "POST",
			url: "index.php?m="+m+"&op=get-pratiche",
			data: dataSend,
			async: true,
			dataType:"json"
		}).done(function(jsn) {
			console.log(typeof(jsn[0]))
			var html = '';
			if (typeof(jsn[0]) != 'undefined') 
				html += '<span class="label label-info">'+jsn[0].pratica_anno+' - '+jsn[0].agenzia_nome+' - '+jsn[0].pratica_codice+'</span><br>';
			if (typeof(jsn[1]) != 'undefined')
				html += '<span class="label label-info">'+jsn[1].pratica_anno+' - '+jsn[1].agenzia_nome+' - '+jsn[1].pratica_codice+'</span><br>';
			if (typeof(jsn[2]) != 'undefined')
				html += '<span class="label label-info">'+jsn[2].pratica_anno+' - '+jsn[2].agenzia_nome+' - '+jsn[2].pratica_codice+'</span><br>';
			
			//console.log(jsn[0].pratica_codice);
			
			
			$('#'+trId).find(".pratiche").html(html);
			//$(tmpBadge).text(jsn.msg);
			//$(tmpBadge).text("asd");
			//totPratiche = totPratiche+parseInt(jsn.msg);
			//$(".tot-pratiche").text(totPratiche);
		});
}






function gotoPage(lnk) {
	document.location.href=lnk;
	//alert(lnk);
}


function elDel(m, id) {
	var dataSend = "m="+m+"&app=true&op=del&&id="+id;
	if (confirm("L'oggetto verrà eliminato. Confermi?")) {
		$.ajax({
			method: "POST",
			url: "index.php",
			data: dataSend,
			dataType:"json"
		}).done(function( msg ) {
			var deleted = "#tr_"+id;
			var actions = "#tr_"+id+" .actions a";
			if(msg.op=='1') {
				$(deleted).addClass("text-danger");
				$(deleted).css("text-decoration", "line-through");
				$(actions).attr("href", "javascript:;");
				$(actions).addClass("text-danger");
			} else {
				alert(msg.msg);
			}
		});
	}
}




function ContaPratichePerIspettorato(m, id) {
	var totIspettorati = $(".list-ispettorati > a").length;
	var totPratiche    = 0;
	
	$(".list-ispettorati > a").each(function() {
		
		var dataSend = "lbl="+$(this).find('h4').text()+"&app=true";
		var tmpBadge = $(this).find('span.badge');
		
		$.ajax({
			method: "POST",
			url: "index.php?m="+m+"&id="+id+"&op=get-tot-pratiche",
			data: dataSend,
			dataType:"json"
		}).done(function(jsn) {
			$(tmpBadge).text(jsn.msg);
			//$(tmpBadge).text("asd");
			totPratiche = totPratiche+parseInt(jsn.msg);
			$(".tot-pratiche").text(totPratiche);
		});
		
	});
}

function loadFormIspettorato(ispettLbl) {
	/*
	$.ajax({
		method: "POST",
		url: "index.php?m="+m+"&id="+id+"&op=get-tot-pratiche",
		data: dataSend,
		dataType:"json"
	}).done(function(jsn) {
		$(tmpBadge).text(jsn.msg);
		//$(tmpBadge).text("asd");
		totPratiche = totPratiche+parseInt(jsn.msg);
		$(".tot-pratiche").text(totPratiche);
	});
	*/
}


$(function() {

	$(".list-ispettorati > a").click(function() {
		var ispettorato = $(this).find('h4').text();
		loadFormIspettorato(ispettorato);
	});
	
	// validazione della compagnia
	$("#formCompagnia").validate({
        errorClass: 'help-block animation-slideDown',
        errorElement: 'div',
        errorPlacement: function(error, e) {
        	//e.parents('.form-group > div').append(error);
        },
        highlight: function(e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.help-block').remove();
        },
        success: function(e) {
            e.closest('.form-group').removeClass('has-success has-error');
            e.closest('.help-block').remove();
        },
        rules: {
        	agenzia_nome: {
                required: true,
                minlength: 3
            }
        },
        messages: { }
    });

});


