<?php /*
  Version:     2017.01.11
  Module:      Zava16.Ispettorati
  Author:      SLivio
*/




//----------------------------------------------------------> [HTML]

// LastUpdate 2016.02.26
function DB__Get_Count_Elements($aF=Array()) {
	GLOBAL $CONF;

	$aOut = Array();

	$qAdd  = (isset($aF['ispettorato_nome']) && $aF['ispettorato_nome']!='')  ? ' AND ispettorato_nome like "%'.$aF['ispettorato_nome'].'%"'  : '';

	$q      = 'SELECT count(ispettorato_id) FROM '.DB_PREFIX.'pratiche_ispettorati WHERE ispettorato_id<>0 '.$qAdd;

	$CON    = GDB__Get_CoreSession();
	$r = mysqli_query($CON, $q) or LOG__Error("DB__Get_Count_Elements[]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		$tot     = mysqli_result($r, mysqli_affected_rows($CON), 0);
		$pageTot = ceil($tot/$CONF['def_page']);
		$aOut    = Array('totElements'=>$tot, 'totPages'=>$pageTot);
	} else {
		$aOut    = Array('totElements'=>$tot, 'totPages'=>1);
	}
	return $aOut;
}



// LastUpdate 2016.12.20
function DB__Get_Count_ElementsNew($aF=Array()) {
	GLOBAL $CONF;

	$aOut = Array();


	$q      = 'SELECT count(distinct pratica_ispettorato) FROM '.DB_PREFIX.'pratiche 
			WHERE pratica_ispettorato  NOT IN (SELECT ispettorato_nome FROM '.DB_PREFIX.'pratiche_ispettorati)';

	$CON    = GDB__Get_CoreSession();
	$r = mysqli_query($CON, $q) or LOG__Error("DB__Get_Count_Elements[]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		$tot     = mysqli_result($r, mysqli_affected_rows($CON), 0);
		$pageTot = ceil($tot/$CONF['def_page']);
		$aOut    = Array('totElements'=>$tot, 'totPages'=>$pageTot);
	} else {
		$aOut    = Array('totElements'=>$tot, 'totPages'=>1);
	}
	return $aOut;
}


function DB__Get_CountPraticheIspettorato($nameIsp='') {
	$q      = 'SELECT count(pratica_id) FROM '.DB_PREFIX.'pratiche WHERE pratica_ispettorato like "%'.$nameIsp.'%"';
	
	$CON    = GDB__Get_CoreSession();
	$r = mysqli_query($CON, $q) or LOG__Error("DB__Get_CountPraticheIspettorato[]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		$tot     = mysqli_result($r, mysqli_affected_rows($CON), 0);
	} else {
		$tot     = 0;
	}
	return $tot;
}

// LastUpdate 2017.11.28
function DB__Get_PraticheAgenzia($agId=0, $limit=10) {
	GLOBAL $CONF;
	$q         = 'SELECT * FROM '.DB_PREFIX.'pratiche WHERE pratica_ispettorato_id='.(int)$agId.' order by pratica_id DESC LIMIT 0,'.(int)$limit;
	$CON       = GDB__Get_CoreSession();
	$aElements = DB__QueryN($CON, $q);

	return $aElements;
}


// LastUpdate 2017.11.28
function DB__Get_PraticheIspettorato($ispLbl, $limit=10) {
	GLOBAL $CONF;
	$q         = 'SELECT '.DB_PREFIX.'pratiche.*,'.DB_PREFIX.'pratiche_agenzie.* 
			FROM '.DB_PREFIX.'pratiche, '.DB_PREFIX.'pratiche_agenzie
					WHERE pratica_ispettorato like "%'.$ispLbl.'%" 
							AND '.DB_PREFIX.'pratiche.pratica_agenzia_id='.DB_PREFIX.'pratiche_agenzie.agenzia_id
							ORDER BY pratica_id DESC LIMIT 0,'.(int)$limit;
	$CON       = GDB__Get_CoreSession();
	$aElements = DB__QueryN($CON, $q);

	return $aElements;
}


// LastUpdate 2017.11.28
function DB__Get_Elements($aF=Array()) {
	GLOBAL $CONF;

	$qLimitStart = ($aF['op_page']<=1) ? 0 : (($aF['op_page']-1)*$CONF['def_page']);
	$qLimitEnd   = $CONF['def_page'];

	$qAdd   = (isset($aF['ispettorato_nome']) && $aF['ispettorato_nome']!='')  ? ' AND ispettorato_nome like "%'.$aF['ispettorato_nome'].'%"'  : '';
	$qOrder = 'ORDER BY ispettorato_id DESC';
	$qLimit = 'LIMIT '.$qLimitStart.','.$qLimitEnd;
	$q      = 'SELECT * FROM '.DB_PREFIX.'pratiche_ispettorati WHERE ispettorato_id<>0 '.$qAdd.' '.$qOrder.' '.$qLimit;
	
	$CON       = GDB__Get_CoreSession();
	$aElements = DB__QueryN($CON, $q);

	return $aElements;
}


// LastUpdate 2017.11.28
function DB__Get_ElementsNew($aF=Array()) {
	GLOBAL $CONF;

	$qLimitStart = ($aF['op_page']<=1) ? 0 : (($aF['op_page']-1)*$CONF['def_page']);
	$qLimitEnd   = $CONF['def_page'];

	$qAdd   = '';
	$qAdd  .= (isset($aF['date_from']) && $aF['date_from']!='')  ? ' AND (date_open >= "'.$aF['date_from'].'"'  : '';
	$qAdd  .= (isset($aF['date_to'])   && $aF['date_to']!='')    ? ' AND date_open <= "'.$aF['date_to'].'")'    : '';
	$qOrder = 'ORDER BY pratica_id DESC';
	$qLimit = 'LIMIT '.$qLimitStart.','.$qLimitEnd;
	$q      = 'SELECT distinct pratica_ispettorato  FROM '.DB_PREFIX.'pratiche 
			WHERE pratica_ispettorato  NOT IN (SELECT ispettorato_nome FROM '.DB_PREFIX.'pratiche_ispettorati)  
			'.$qAdd.' '.$qOrder.' '.$qLimit;

	$CON       = GDB__Get_CoreSession();
	$aElements = DB__QueryN($CON, $q);

	return $aElements;
}


// LastUpdate 2016.02.22
function DB__Get_Element($id=0) {
	if ($id==0) {
		$aR = Array(
			'ispettorato_id' => 0,
			'ispettorato_nome' => '',
			'ispettorato_indirizzo_via' => '',
			'ispettorato_indirizzo_num' => '',
			'ispettorato_indirizzo_cap' => '',
			'ispettorato_indirizzo_citta' => '',
			'ispettorato_indirizzo_prov' => '',
			'ispettorato_telefono' => '',
			'ispettorato_fax' => '',
			'ispettorato_sito' => '',
			'ispettorato_email' => ''
		);
	} else {
		$CON = GDB__Get_CoreSession();
		$aR = Array();
		$aR = DB__Get_infoRecord($CON, DB_PREFIX.'pratiche_ispettorati', ' WHERE ispettorato_id="'.(int)$id.'" ', 0);
	}
	
	return $aR;
}



function DB__Validate_Element($action='set', $aP) {
	if($action=='set') {
		$aP['ispettorato_id'] = (isset($aP['ispettorato_id']) && $aP['ispettorato_id'] != '') ? $aP['ispettorato_id'] : 0;
		if ($aP['ispettorato_id']==0) {
			return false;
		}
	}
	// 
	//$aP['ispettorato_nome'] = (isset($aP['ispettorato_nome']) && $aP['ispettorato_nome'] != '') ? $aP['ispettorato_nome'] : '';
	//if ($aP['ispettorato_nome']=='') return false;
	// Se tutto ok
	return true;
}


// SONO ARRIVATO QUI!!!
function DB__Add_Element($aP=Array()) {

	if(DB__Validate_Element('add', $aP)) {
		$CON = GDB__Get_CoreSession();
		$q   = 'INSERT INTO `'.DB_PREFIX.'pratiche_ispettorati` (`ispettorato_ip_creazione`, `ispettorato_data_creazione`) VALUES ("'.$_SERVER["REMOTE_ADDR"].'", NOW())';
		$r   = mysqli_query($CON, $q) or LOG__Error("DB__AddElement", mysqli_error($CON)."\n".$q);
		$id  = mysqli_insert_id($CON);
		//
		$aP['ispettorato_id'] = $id;
		if (mysqli_error($CON)=='') $OUT = DB__Set_Element($aP);
		else                        $OUT = false;
		if($id==$OUT) return $id;
		else          return false;
	} else {
		return false;
	}
}



// LastUpdate 2016.02.22
function DB__Set_Element($aP=Array()) {
	$CON = GDB__Get_CoreSession();
	//
	if(!DB__Validate_Element('set', $aP)) return false;

	$addQ = Array();
	//if ($aP['ispettorato_nome'] != '')    $addQ[] = ' `ispettorato_nome`="'.MyEscape($aP['ispettorato_nome']).'" ';	$addQ[] = ' `u_policy_1`="'.$aP['u_policy_1'].'" ';
	//$addQ[] = ' `ispettorato_nome`="'.$aP['ispettorato_nome'].'" ';
	$addQ[] = ' `ispettorato_indirizzo_via`="'.$aP['ispettorato_indirizzo_via'].'" ';
	$addQ[] = ' `ispettorato_indirizzo_num`="'.$aP['ispettorato_indirizzo_num'].'" ';
	$addQ[] = ' `ispettorato_indirizzo_cap`="'.$aP['ispettorato_indirizzo_cap'].'" ';
	$addQ[] = ' `ispettorato_indirizzo_citta`="'.$aP['ispettorato_indirizzo_citta'].'" ';
	$addQ[] = ' `ispettorato_indirizzo_prov`="'.$aP['ispettorato_indirizzo_prov'].'" ';
	$addQ[] = ' `ispettorato_telefono`="'.$aP['ispettorato_telefono'].'" ';
	$addQ[] = ' `ispettorato_fax`="'.$aP['ispettorato_fax'].'" ';
	$addQ[] = ' `ispettorato_sito`="'.$aP['ispettorato_sito'].'" ';
	$addQ[] = ' `ispettorato_email`="'.$aP['ispettorato_email'].'" ';
	
	
	//
	$q = 'UPDATE `'.DB_PREFIX.'pratiche_ispettorati` SET '.implode(', ', $addQ).',
	`ispettorato_data_modifica`   = NOW(),
	`ispettorato_ip_modifica`     = "'.$_SERVER["REMOTE_ADDR"]  .'"
	WHERE
	ispettorato_id="'.$aP['ispettorato_id'].'"';

	$r = mysqli_query($CON, $q) or LOG__Error("DB__SetElement", mysqli_error($CON)."\n".$q);
	//
	if (mysqli_error($CON)=='') $OUT = $aP['ispettorato_id'];
	else                        $OUT = false;
	//
	return $OUT;
}


// LastUpdate v7 2014.11.20
function DB__Del_Element($id) {
	$CON = GDB__Get_CoreSession();
	$q = 'DELETE FROM `'.DB_PREFIX.'pratiche_ispettorati` WHERE ispettorato_id = "'.(int)$id.'"';
	$r = mysqli_query($CON, $q) or LOG__Error("DB__Del_Element[]", mysqli_error($CON)."\n".$q);
	if (mysqli_error($CON)=='') $OUT = true;
	else                        $OUT = mysqli_error($CON);
	return true;
}

// LastUpdate 7.2017.12.05
function HTML__Pagination($aFilter=Array(), $aCounts=Array()) {
	GLOBAL $m, $CONF, $op;
	
	$HTMLP = '<form class="pull-right"><select onChange="gotoPage(this.value)">';
	$aTmpFilter = $aFilter;
	unset($aTmpFilter['op_page']);
	unset($aTmpFilter['op_order']);
	
	$query = http_build_query(array_filter($aTmpFilter));
	for($i=1;$i<=$aCounts['totPages'];$i++) {
		$class  =  (isset($aFilter['op_page']) && $aFilter['op_page']==$i) ? ' selected="selected"' : '';
		$link   =  '?m='.$m.'&op='.$op.'&op_page='.$i.'&'.$query.'&order='.$aFilter['op_order'];
		$HTMLP .= '<option '.$class.' value="'.$link.'">'.$i.'</a></li>';
	}
	$HTMLP .= '</select></form>';

	$HTML = '<div class="row">
			<div class="col-sm-6"><span><strong>'.$aCounts['totElements'].'</strong> risultati</span></div>
			<div class="col-sm-6">'.$HTMLP.'</div>
		</div>';
	
	return $HTML;
}


// LastUpdate 7.2017.12.05
function HTML__List($aFilter=Array(), $aElements=Array(), $aCounts=Array()) {
	GLOBAL $m, $CONF, $op;

	$HTML  = '';
	$HTMLP = HTML__Pagination($aFilter, $aCounts);


	$HTML .= '
	<div class="block">
		<div class="block-title">
			<div class="block-options pull-right" data-toggle="tooltip" data-original-title="Search">
				<a class="btn btn-alt btn-sm btn-default" title="" href="?app=true&m='.$m.'&op=op-search"  data-toggle="modal" data-target="#modal"><i class="hi hi-search"></i></a>
			</div>
			<h2>Elenco Ispettorati</h2>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog"><div class="modal-content"></div><!-- /.modal-content --></div>
		</div>

		<div class="table-responsive">
			<table class="table table-striped table-vcenter table-bordered dataTable table-hover">
			<tr><td colspan="7">'.$HTMLP.'</td></tr>
			<thead>
				<tr class="tr_head">
					<th>ID</th>
					<th>Ragione sociale</th>
					<th>Indirizzo</th>
					<th>Contatti</th>
					<th colspan="3" class="w90 text-center">Azioni</th>
				</tr>
			</thead>
			<tbody>';

	foreach($aElements as $el) {
		$id = $el['ispettorato_id'];

		$tr_id  = 'tr_'.$id;
		$a_info = '<a href="?m='.$m.'&op=op-details&id='.$id.'&app=true" title="Preview"  data-toggle="modal" data-target="#modal" >'.GHTML__Get_Ico('zoom-in', 'Vedi').'</a>';
		$a_edit = '<a href="?m='.$m.'&op=op-edit&id='.$id.'" title="Edit"  >'.GHTML__Get_Ico('edit', 'Modifica').'</a>';
		$a_del  = '<a href="javascript:elDel('.$m.', '.$id.')" title="Delete">'.GHTML__Get_Ico('delete', 'Cancella').'</a>';
		$a_del  = GHTML__Get_Ico('delete', '');

		$HTMLcontact  = (($el['ispettorato_telefono']!='') ? '<i class="fa fa-phone" data-toggle="tooltip" title="" data-original-title="Telefono"></i> '.$el['ispettorato_telefono'].'<br>' : '');
		$HTMLcontact .= (($el['ispettorato_fax']!='')      ? '<i class="fa fa-print" data-toggle="tooltip" title="" data-original-title="Fax"></i> '.     $el['ispettorato_fax'].'<br>'      : '');
		$HTMLcontact .= (($el['ispettorato_sito']!='')     ? '<i class="hi hi-globe" data-toggle="tooltip" title="" data-original-title="Sito"></i> '.    $el['ispettorato_sito'].'<br>'     : '');
		$HTMLcontact .= (($el['ispettorato_email']!='')    ? '<i class="hi hi-envelope" data-toggle="tooltip" title="" data-original-title="Email"></i> '.   $el['ispettorato_email'].'<br>'    : '');
		
		
		$HTML .= NL.'<tr id="'.$tr_id.'">
					<td>'.$id.'</td>
					<td>'.$el['ispettorato_nome'].'</td>
					<td>
						'.$el['ispettorato_indirizzo_via'].' '.$el['ispettorato_indirizzo_num'].'<br>
						'.$el['ispettorato_indirizzo_cap'].' '.$el['ispettorato_indirizzo_citta'].' '.$el['ispettorato_indirizzo_prov'].'
					</td>
					<td>
						'.$HTMLcontact.'
					</td>
					<td class="text-center actions">'.$a_info.'</td>
					<td class="text-center actions">'.$a_edit.'</td>
					<td class="text-center actions">'.$a_del.'</td>
				</tr>';
	}
	$HTML .= '</tbody>
			<tfoot><td colspan="7">'.$HTMLP.'</td></tfoot>
			</table>
			<br>
			</div>
		</div>';

	return $HTML;
}

function STR__CleanIspettorato($str='') {
	$str = str_replace(' ', '', $str);
	$str = str_replace('.', '', $str);
	$str = str_replace("'", '', $str);
	$str = str_replace('"', '', $str);
	$a1  = Array('à', 'è', 'é', 'ì', 'ò', 'ù');
	$a2  = Array('a', 'e', 'e', 'i', 'o', 'u');
	$str = str_replace($a1, $a2, $str);
	return $str;
}


// LastUpdate 7.2017.12.05
function HTML__ListNew($aFilter=Array(), $aElements=Array(), $aCounts=Array()) {
	GLOBAL $m, $CONF, $op;

	$HTML  = '';
	$HTMLP = HTML__Pagination($aFilter, $aCounts);


	$HTML .= '
	<div class="block">
		<div class="block-title">
			<div class="block-options pull-right" data-toggle="tooltip" data-original-title="Search">
				<a class="btn btn-alt btn-sm btn-default" title="" href="?app=true&m='.$m.'&op=op-search"  data-toggle="modal" data-target="#modal"><i class="hi hi-search"></i></a>
			</div>
			<h2>Elenco Ispettorati non catalogati</h2>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog"><div class="modal-content"></div><!-- /.modal-content --></div>
		</div>

		<div class="table-responsive">
			<table class="table table-striped table-vcenter table-bordered dataTable table-hover">
			<tr><td colspan="4">'.$HTMLP.'</td></tr>
			<thead>
				<tr class="tr_head">
					<th>ID</th>
					<th>Nome</th>
					<th>Ultime pratiche associate</th>
					<th class="w30 text-center"></th>
				</tr>
			</thead>
			<tbody>';

	foreach($aElements as $el) {
		$id = STR__CleanIspettorato($el['pratica_ispettorato']);//$el['ispettorato_id'];

		$tr_id  = 'tr_'.$id;
		$a_edit = '<a href="javascript:;" onclick=\'addIspettorato("'.$tr_id.'", "?m='.$m.'&op=op-add&lbl='.$id.'");\' title="Edit"  >'.GHTML__Get_Ico('add', 'Aggiungi e gestisci').'</a>';
		

		$HTML .= NL.'<tr class="isp-row" id="'.$tr_id.'">
					<td><script> $(function() { loadIspettorati("'.$m.'", "'.$tr_id.'"); }); </script></td>
					<td class="isp-lbl">'.$el['pratica_ispettorato'].'</td>
					<td class="pratiche"></td>
					<td class="text-center actions">'.$a_edit.'</td>
				</tr>';
	}
	$HTML .= '</tbody>
			<tfoot><td colspan="4">'.$HTMLP.'</td></tfoot>
			</table>
			<br>
			</div>
		</div>';

	return $HTML;
}




// LastUpdate 2016.02.22
function HTML__ModalDetails($title='', $aEl=Array()) {
	GLOBAL $m, $CONF;
	
	$totPratiche = DB__Get_CountPraticheIspettorato($aEl['ispettorato_nome']);
	$aPratiche   = DB__Get_PraticheIspettorato($aEl['ispettorato_nome'], 5);
	$HTMLpa      = '';
	foreach($aPratiche as $prat) {
		$HTMLpa .= '<span class="label label-info">'.$prat['pratica_anno'].' | '.$prat['pratica_agenzia'].' | '.$prat['pratica_codice'].'</span><br>';
	}
	
	$HTML ='<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<title>'.$title.'</title>
	<script>
	$(document).on("hidden.bs.modal", function (e) {
	    $(e.target).removeData("bs.modal").find(".modal-content").empty();
	});
	</script>
</head>
<body>
	<div class="modal-header text-center">
		<h2 class="modal-title"><i class="fa fa-search"></i> '.$title.'</h2>
	</div><!-- /modal-header -->
	<div class="modal-body">
		<div class="table-responsive">
			<table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
            	<!--<thead>
                	<tr>
                    	<th class="text-center">ID</th>
                        <th class="text-center"><i class="gi gi-user"></i></th>
					</tr>
				</thead>-->
				<tbody>
					<tr>
						<td width="40%">Nome ispettorato</td>
						<td width="60%">'.$aEl['ispettorato_nome'].'</td>
					</tr>
					<tr>
						<td>Indirizzo</td>
						<td>'.
							$aEl['ispettorato_indirizzo_via'].' '.$aEl['ispettorato_indirizzo_num'].' <br>'.
							$aEl['ispettorato_indirizzo_cap'].' '.$aEl['ispettorato_indirizzo_citta'].' '.$aEl['ispettorato_indirizzo_prov'].'
						</td>
					</tr>
					<tr>
						<td>Contatti</td>
						<td>
							<i class="fa fa-phone"></i>      '.$aEl['ispettorato_telefono'].'<br>
							<i class="fa fa-print"></i>      '.$aEl['ispettorato_fax'].'<br>
							<i class="fa fa-globe"></i>      '.$aEl['ispettorato_sito'].'<br>
							<i class="fa fa-envelope-o"></i> '.$aEl['ispettorato_email'].'
						</td>
					</tr>
					<tr>
						<td>Pratiche totali associate all\'ispettorato</td>
						<td>'.$totPratiche.'</td>
					</tr>
					<tr>
						<td>Ultime pratiche della compagnia</td>
						<td>'.$HTMLpa.'</td>
					</tr>
				</tbody>
			</table>
		</div><!-- /modal-body -->
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div><!-- /modal-footer -->
</body>
</html>';
	return $HTML;
}


// LastUpdate 2016.02.22
function HTML__ModalSearch($title='', $ar=Array()) {
	GLOBAL $m;
	
	$HTML ='<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<title>'.$title.'</title>
	<script>
	$("body").on("hidden.bs.modal", ".modal", function () {
	    $(this).removeData("bs.modal");
	});
	</script>
	<script src="js/app.js"></script>
</head>
<body>
	<div class="modal-header text-center">
		<h2 class="modal-title"><i class="fa fa-search"></i> '.$title.'</h2>
	</div><!-- /modal-header --><!-- /modal-header -->
	<div class="modal-body">
		<form action="?m='.$m.'" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
		<input type="hidden" name="op"  value="op-list">
			<div class="form-group">
				<label class="col-md-3 control-label" for="ispettorato_nome">Ragione sociale della compagnia</label>
				<div class="col-md-9">
					<input type="text" id="ispettorato_nome" name="ispettorato_nome" class="form-control" placeholder="Es: Google inc">
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Search</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div><!-- /modal-footer -->
		</form>
	</div><!-- /modal-body -->
</body>
</html>';
	return $HTML;
}



// LastUpdate 2016.12.05
function DB__Get_CountIspettorati($name=0) {
	GLOBAL $CONF;

	$out  = 0;
	$q    = 'SELECT count(ispettorato_id) FROM '.DB_PREFIX.'pratica_ispettorati WHERE pratica_ispettorato="'.$name.'"';

	$CON    = GDB__Get_CoreSession();
	$r = mysqli_query($CON, $q) or LOG__Error("DB__Get_CountIspettorati[]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		$out = mysqli_result($r, mysqli_affected_rows($CON), 0);
	}
	return $out;
}


// LastUpdate 2017.12.17
function DB__Get_Ispettorati($compId=0) {
	GLOBAL $CONF;

	$q   = 'SELECT DISTINCT pratica_ispettorato FROM `'.DB_PREFIX.'pratica_ispettorati`
			WHERE `pratica_ispettorato_id` = "'.(int)$compId.'"';
	$CON = GDB__Get_CoreSession();
	$aEl = DB__QueryN($CON, $q);

	return $aEl;
}




function HTML__Form($aEl=Array()) {
	GLOBAL $m;
	
	/*$aCountries = GDB__Get_Countries();
	$HTMLoption = '';
	foreach($aCountries as $el) {
		$selected    = ($el['country_name']==$aEl['u_country']) ? 'selected="selected"' : '';
		$HTMLoption .= '<option value="'.$el['country_name'].'" '.$selected.'>'.$el['country_code'].' - '.$el['country_name'].'</option>';
	}*/
	
	$id = $aEl['ispettorato_id'];
	
	$HTML = '<div class="block full">
		<div class="block-title">
			<h2>Scheda Compagnia</h2>
		</div>
		<form id="formCompagnia" action="?m='.$m.'" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" novalidate="novalidate">
		<input type="hidden" name="op" value="op-set">
		<input type="hidden" name="id" value="'.$id.'">
			<div class="form-validation-message">
				<div class="error"></div>
			</div>
			<div class="form-group">
            	<label class="col-md-2 control-label">ID</label>
                <div class="col-md-10"><p class="form-control-static">'.$id.'</p></div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="ispettorato_nome">Ragione sociale <span class="text-danger">*</span></label>
				<div class="col-md-10"><input type="text" class="form-control id="ispettorato_nome" name="ispettorato_nome"" value="'.$aEl['ispettorato_nome'].'" disabled></div>
			</div>
			
            <div class="form-group">
						
                <div class="row">
                    <div class="col-md-6">
						<label class="col-md-4 control-label" for="ispettorato_indirizzo_via">Indirizzo</label>
						<div class="col-md-8"><input type="text" class="form-control" id="ispettorato_indirizzo_via" name="ispettorato_indirizzo_via" placeholder="Es: Via delle valli" value="'.$aEl['ispettorato_indirizzo_via'].'"></div>
					</div>
					<div class="col-md-6">
						<label class="col-md-4 control-label" for="ispettorato_indirizzo_num">Numero</label>
						<div class="col-md-8"><input type="text" class="form-control" id="ispettorato_indirizzo_num" name="ispettorato_indirizzo_num" placeholder="Es: 7" value="'.$aEl['ispettorato_indirizzo_num'].'"></div>
					</div>
				</div>
								
				<div class="row">
                    <div class="col-md-6">
						<label class="col-md-4 control-label" for="ispettorato_indirizzo_cap">Cap</label>
						<div class="col-md-8"><input type="text" class="form-control" id="ispettorato_indirizzo_cap" name="ispettorato_indirizzo_cap" placeholder="Es: 00100" value="'.$aEl['ispettorato_indirizzo_cap'].'"></div>
					</div>
					<div class="col-md-6">
						<label class="col-md-4 control-label" for="ispettorato_indirizzo_citta">Città</label>
						<div class="col-md-8"><input type="text" class="form-control" id="ispettorato_indirizzo_citta" name="ispettorato_indirizzo_citta" placeholder="Es: Roma" value="'.$aEl['ispettorato_indirizzo_citta'].'"></div>
					</div>
				</div>

				<div class="row">
                    <div class="col-md-6">
						<label class="col-md-4 control-label" for="ispettorato_indirizzo_prov">Provincia</label>
						<div class="col-md-8"><input type="text" class="form-control" id="ispettorato_indirizzo_prov" name="ispettorato_indirizzo_prov" placeholder="Es: RM" value="'.$aEl['ispettorato_indirizzo_prov'].'"></div>
					</div>
					<div class="col-md-6"></div>
				</div>
								
			</div>
								
			 <div class="form-group">
						
                <div class="row">
                    <div class="col-md-6">
						<label class="col-md-4 control-label" for="ispettorato_telefono">Telefono</label>
						<div class="col-md-8"><input type="text" class="form-control" id="ispettorato_telefono" name="ispettorato_telefono" placeholder="Es: 06123456" value="'.$aEl['ispettorato_telefono'].'"></div>
					</div>
					<div class="col-md-6">
						<label class="col-md-4 control-label" for="ispettorato_fax">Fax</label>
						<div class="col-md-8"><input type="text" class="form-control" id="ispettorato_fax" name="ispettorato_fax" placeholder="Es: 06123456" value="'.$aEl['ispettorato_fax'].'"></div>
					</div>
				</div>
								
				<div class="row">
                    <div class="col-md-6">
						<label class="col-md-4 control-label" for="ispettorato_sito">Sito</label>
						<div class="col-md-8"><input type="text" class="form-control" id="ispettorato_sito" name="ispettorato_sito" placeholder="Es: http://www.google.it" value="'.$aEl['ispettorato_sito'].'"></div>
					</div>
					<div class="col-md-6">
						<label class="col-md-4 control-label" for="ispettorato_email">Email</label>
						<div class="col-md-8"><input type="text" class="form-control" id="ispettorato_email" name="ispettorato_email" placeholder="info@dominio.est" value="'.$aEl['ispettorato_email'].'"></div>
					</div>
				</div>
								
			</div>					
								
			
			<div class="form-group form-actions">
				<div class="col-md-6 text-center">
					<button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
				</div>
				<div class="col-md-6 text-center">
					<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
				</div>
			</div>
		</form>
	</div>';
	return $HTML;
}




?>