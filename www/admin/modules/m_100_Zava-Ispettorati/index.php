<?php /*
  Version:     2017.01.11
  Module:      Zava16.Ispettorati
  Author:      SLivio
*/



//--
//----------------------------------------------------------> [CONFIG]
if(file_exists($CONF['path_module'].'this.lib.php')) {
	require_once($CONF['path_module'].'this.lib.php');
}
$id  = (isset($_REQUEST['id'])    && $_REQUEST['id']    != '' && $_REQUEST['id'] != 0)     ? $_REQUEST['id']    : 0;
$opp = (isset($_REQUEST['opp'])   && $_REQUEST['opp']   != '' && $_REQUEST['opp'] != '')   ? $_REQUEST['opp'] : '';
$aP  = $_REQUEST;
$aFilter = Array();
//----------------------------------------------------------> [/CONFIG]



switch($op) {
	case 'op-add'  :
		$lbl = (isset($_REQUEST['lbl'])   && $_REQUEST['lbl']   != '' && $_REQUEST['lbl'] != '')   ? $_REQUEST['lbl'] : '';
		$out = ''; // DB__Get_CountIspettorati($lbl);
		if($out==true) $aR  = Array('op'=>'1', 'msg'=>'Ok', 'random'=>get_RandomString());
		else           $aR  = Array('op'=>'0', 'msg'=>'Ko :: '.$out, 'random'=>get_RandomString());
		$json = json_encode($aR);
		die($json);
	break;
	case 'get-pratiche' :
		$lbl   = (isset($_REQUEST['lbl'])   && $_REQUEST['lbl']   != '' && $_REQUEST['lbl'] != '')   ? $_REQUEST['lbl'] : '';
		$aPrat = DB__Get_PraticheIspettorato($lbl);
		$json  = json_encode($aPrat);
		die($json);
	break;
	case 'get-tot-pratiche' :
		$lbl = (isset($_REQUEST['lbl'])   && $_REQUEST['lbl']   != '' && $_REQUEST['lbl'] != '')   ? $_REQUEST['lbl'] : '';
		$out = DB__Get_CountIspettorati($lbl);
		if($out==true) $aR  = Array('op'=>'1', 'msg'=>$out, 'random'=>get_RandomString());
		else           $aR  = Array('op'=>'0', 'msg'=>'Ko :: '.$out, 'random'=>get_RandomString());
		$json = json_encode($aR);
		die($json);
	break;
	case 'del' :
		$out = DB__Del_Element($id);
		if($out==true) $aR  = Array('op'=>'1', 'msg'=>'Ok', 'random'=>get_RandomString());
		else           $aR  = Array('op'=>'0', 'msg'=>'Ko :: '.$out, 'random'=>get_RandomString());
		$json = json_encode($aR);
		die($json);
	break;
	
	case 'op-details' :
		$aEl= DB__Get_Element($id);
		$HTML = HTML__ModalDetails('Dettagli dell\' ispettorato', $aEl);
		echo $HTML;
	break;
	case 'op-search' :
		$HTML = HTML__ModalSearch('Cerca la compagnia', $aP);
		echo $HTML;
	break;
	case 'op-list-new' :
		$aFilter['op_page']    = (isset($_REQUEST['op_page'])    ? $_REQUEST['op_page']         : 1);
		$aFilter['op_order']   = (isset($_REQUEST['op_order'])   ? $_REQUEST['op_order']        : ' ispettorato_id DESC ');
		$aFilter['ispettorato_nome']  = (isset($_REQUEST['ispettorato_nome'])  ? trim($_REQUEST['ispettorato_nome']) : '');
	
		
		$aCount    = DB__Get_Count_ElementsNew($aFilter);
		$aElements = DB__Get_ElementsNew($aFilter);
		
		$HTML      = GHTML__Get_ModuleHeader('Gestione', 'Ispettorati non catalogati');
		$HTML     .= HTML__ListNew($aFilter, $aElements, $aCount);
		echo $HTML;
	break;
	default :
	case 'op-list' :
		$aFilter['op_page']    = (isset($_REQUEST['op_page'])    ? $_REQUEST['op_page']         : 1);
		$aFilter['op_order']   = (isset($_REQUEST['op_order'])   ? $_REQUEST['op_order']        : ' ispettorato_id DESC ');
		$aFilter['ispettorato_nome']  = (isset($_REQUEST['ispettorato_nome'])  ? trim($_REQUEST['ispettorato_nome']) : '');
	
		
		$aCount    = DB__Get_Count_Elements($aFilter);
		$aElements = DB__Get_Elements($aFilter);
		
		$HTML      = GHTML__Get_ModuleHeader('Gestione', 'Ispettorati');
		$HTML     .= HTML__List($aFilter, $aElements, $aCount);
		echo $HTML;
	break;
	case 'op-new'  :
	case 'op-set'  :
	case 'op-edit' :
		$HTML = GHTML__Get_ModuleHeader('Gestione Ispettorati', 'Modifica ('.$id.')');
		if ($op=='op-set') {
			
			if($id!=0) {
				$aP['ispettorato_id'] = $id;
				$dbId = DB__Set_Element($aP);
				if ($dbId) $HTML .= GHTML__Get_StatusMessage('success', 'Modifica Ispettorati', 'Modifica completata con successo');
			}			
			//$HTML .= HTML__FormIspettorato($aEl);
		} 
		$aEl   = DB__Get_Element($id);
		$HTML .= HTML__Form($aEl);
		
		echo $HTML;
	break;
}


?>