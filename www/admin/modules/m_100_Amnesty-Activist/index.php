<?php /*
  Version:     v7 2016.02.22
  Module:      Amnesty Activist
  Author:      SLivio
*/



//--
//----------------------------------------------------------> [CONFIG]
if(file_exists($CONF['path_module'].'this.lib.php')) {
	require_once($CONF['path_module'].'this.lib.php');
}
$id  = (isset($_REQUEST['id'])    && $_REQUEST['id']    != '' && $_REQUEST['id'] != 0)     ? $_REQUEST['id']    : 0;
$opp = (isset($_REQUEST['opp'])   && $_REQUEST['opp']   != '' && $_REQUEST['opp'] != '')   ? $_REQUEST['opp'] : '';
$aP  = $_REQUEST;
$aFilter = Array();
//----------------------------------------------------------> [/CONFIG]



switch($op) {
	case 'aj-del' :
		$out = DB__Del_Element($id);
		$aR  = Array('op'=>'1', 'msg'=>'Ok', 'random'=>get_RandomString());
		$json = json_encode($aR);
		die($json);
	break;
	case 'aj-vstatus' :
		$aP['v_id']     = $id;
		$aP['v_visible'] = (isset($_REQUEST['v_visible'])  && $_REQUEST['v_visible']  != '') ? (int)$_REQUEST['v_visible'] : 0;
		$out = DB__Set_StatusElement($aP);
		$aR  = Array('op'=>'1', 'msg'=>'Ok', 'random'=>get_RandomString());
		$json = json_encode($aR);
		die($json);
	break;
	case 'op-details' :
		$aEl= DB__Get_Element($id);
		$HTML = HTML__ModalDetails('Activist details', $aEl);
		echo $HTML;
	break;
	default :
	case 'op-list' :
		$aFilter['op_page']   = (isset($_REQUEST['op_page'])   ? $_REQUEST['op_page']         : 1);
		$aFilter['op_order']  = (isset($_REQUEST['op_order'])  ? $_REQUEST['op_order']        : ' u_id DESC ');
		$aFilter['v_user']    = (isset($_REQUEST['v_user'])    ? $_REQUEST['v_user']          : '');
		$aFilter['v_visible'] = (isset($aFilter['v_visible'])  ? $_REQUEST['v_visible']       : '');
		$aFilter['v_hp']      = (isset($aFilter['v_hp'])       ? $_REQUEST['v_hp']            : '');
		
		$totPages  = DB__Get_Count_Elements($aFilter);
		$aElements = DB__Get_Elements($aFilter);
		$HTML = HTML__List($aFilter, $aElements, $totPages);
		echo $HTML;
	break;
	case 'op-form' :
	case 'op-add'  :
	case 'op-edit' :
		$HTML = '';

		if($op=='op-add') {
			$aP['v_id'] = DB__Add_Element($aP);
			if ($aP['v_id']) $HTML .= GHTML__Get_StatusMessage('success', 'User/Video', '...');
			else             $HTML .= GHTML__Get_StatusMessage('error', 'User/Video', '...');
			$aEl   = DB__Get_Element($aP['v_id']);
			$HTML .= HTML__Form($aEl);
			
		} else if ($id==0) {
			// Nuovo utente. Per il momento non lo gestiamo.
			$strEl = 'v_id,v_user,v_title_it,v_title_en,v_title_it,v_description_it,v_description_en,v_yt,v_img,v_data_view,v_visible,v_hp,v_weight';
			$aEl   = fillArrayWithKeys(explode(',', $strEl));
			$HTML .= HTML__Form($aEl, 'op-add');
		} else {
			if($op=='op-edit') {
				$aP['v_id'] = $id;
				$uId = DB__Set_Element($aP);
				if ($uId) $HTML .= GHTML__Get_StatusMessage('success', 'User/Video', '...');
			}
			$aEl   = DB__Get_Element($id);
			$HTML .= HTML__Form($aEl);
		}
		
		echo $HTML;
	break;
}


?>