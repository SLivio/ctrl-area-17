<?php /*
  Version:     v7 2016.02.26
  Module:      Amnesty Activist
  Author:      SLivio
*/




//----------------------------------------------------------> [HTML]

// LastUpdate 2016.02.26
function DB__Get_Count_Elements($aF=Array()) {
	GLOBAL $CONF;
	
	$qAdd  = '';
	$qAdd .= ($aF['v_user']!='')   ? ' AND v_user like "%'.$aF['v_user'].'%"'     : '';
	
	
	$q      = 'SELECT count(v_id) FROM '.DB_PREFIX.'video WHERE v_id<>0 '.$qAdd;
	$CON    = GDB__Get_CoreSession();
	$r = mysqli_query($CON, $q) or LOG__Error("countRecord[]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		$tot     = mysqli_result($r, mysqli_affected_rows($CON), 0);
		$pageTot = round($tot/$CONF['def_page'], 0, PHP_ROUND_HALF_UP);
	} else {
		$pageTot = 1;
	}
	return $pageTot;
}


// LastUpdate 2016.02.26
function DB__Get_Elements($aF=Array()) {
	GLOBAL $CONF;

	$qLimitStart = ($aF['op_page']<=1) ? 0 : (($aF['op_page']-1)*$CONF['def_page']);
	$qLimitEnd   = (($aF['op_page']-1)*$CONF['def_page'])+$CONF['def_page'];

	$qAdd  = '';
	$qAdd .= ($aF['v_user']!='')   ? ' AND v_user like "%'.$aF['v_user'].'%"'     : '';
	
	$qOrder = 'ORDER BY v_weight DESC, v_id DESC';
	$qLimit = 'LIMIT '.$qLimitStart.','.$qLimitEnd;
	$q      = 'SELECT * FROM '.DB_PREFIX.'video WHERE v_id<>0 '.$qAdd.' '.$qOrder.' '.$qLimit;
	
	$CON       = GDB__Get_CoreSession();
	$aElements = DB__QueryN($CON, $q);

	return $aElements;
}



// LastUpdate 2016.02.22
function DB__Get_Element($id=0) {
	$CON = GDB__Get_CoreSession();
	$aR = Array();
	$aR = DB__Get_infoRecord($CON, DB_PREFIX.'video', ' WHERE v_id="'.(int)$id.'" ', 0);
	return $aR;
}




// LastUpdate 2016.02.22
function DB__Set_StatusElement($aP=Array()) {
	$CON = GDB__Get_CoreSession();
	//
	$OUT                 = false;
	$aP['v_id']          = (isset($aP['v_id'])       && $aP['v_id'] != '')        ? $aP['v_id']            : 0;
	$aP['v_visible']      = (isset($aP['v_visible'])   && $aP['v_visible'] != '')    ? (int)$aP['v_visible']   : 0;

	// id utente mancante
	if ($aP['v_id']==0) return false;

	$qAdd = ' `v_visible`="'.$aP['v_visible'].'" ';

	//
	$q = 'UPDATE `'.DB_PREFIX.'video` SET '.$qAdd.' WHERE v_id="'.$aP['v_id'].'"';
	$r = mysqli_query($CON, $q) or LOG__Error("DB__Set", mysqli_error($CON)."\n".$q);

	if (mysqli_error($CON)=='') $OUT = $aP['v_id'];
	else                        $OUT = false;
	//
	return $OUT;
}

// LastUpdate 2016.03.04
function DB__Add_Element($aP=Array()) {
	$CON = GDB__Get_CoreSession();
	//
	$OUT                    = false;
	$aP['v_id']             = (isset($aP['v_id'])             && $aP['v_id'] != '')                   ? $aP['v_id']                   : 0;
	$aP['v_user']           = (isset($aP['v_user'])           && trim($aP['v_user']) != '')           ? trim($aP['v_user'])           : '';
	$aP['v_title_it']       = (isset($aP['v_title_it'])       && trim($aP['v_title_it']) != '')       ? trim($aP['v_title_it'])       : '';
	$aP['v_title_en']       = (isset($aP['v_title_en'])       && trim($aP['v_title_en']) != '')       ? trim($aP['v_title_en'])       : '';
	$aP['v_description_it'] = (isset($aP['v_description_it']) && trim($aP['v_description_it']) != '') ? trim($aP['v_description_it']) : '';
	$aP['v_description_en'] = (isset($aP['v_description_en']) && trim($aP['v_description_en']) != '') ? trim($aP['v_description_en']) : '';
	$aP['v_yt']             = (isset($aP['v_yt'])             && trim($aP['v_yt']) != '')             ? trim($aP['v_yt'])             : '';
	$aP['v_img']            = (isset($aP['v_img'])            && trim($aP['v_img']) != '')            ? trim($aP['v_img'])            : '';
	$aP['v_data_view_d']    = (isset($aP['v_data_view_d'])    && trim($aP['v_data_view_d']) != '')    ? trim($aP['v_data_view_d'])    : '';
	$aP['v_data_view_t']    = (isset($aP['v_data_view_t'])    && trim($aP['v_data_view_t']) != '')    ? trim($aP['v_data_view_t'])    : '';
	$aP['v_visible']        = (isset($aP['v_visible'])        && $aP['v_visible'] != '')              ? (int)$aP['v_visible']         : 0;
	$aP['v_hp']             = (isset($aP['v_hp'])             && $aP['v_hp'] != '')                   ? 1                             : 0;
	$aP['v_weight']         = (isset($aP['v_weight'])         && trim($aP['v_weight']) != '')         ? trim($aP['v_weight'])         : 0;

	$OUT = 0;
	//
	$q = "INSERT INTO `".DB_PREFIX.'video'."` (
		v_id,
		v_user,
		v_title_it,
		v_title_en,
		v_description_it,
		v_description_en,
		v_yt,
		v_img,
		v_data_view,
		v_visible,
		v_hp,
		v_weight
	     ) VALUES (
	     NULL,
	     \"".MyEscape($aP['v_user'])."\",
	     \"".MyEscape($aP['v_title_it'])."\",
	     \"".MyEscape($aP['v_title_en'])."\",
	     \"".MyEscape($aP['v_description_it'])."\",
	     \"".MyEscape($aP['v_description_en'])."\",
	     \"".($aP['v_yt'])."\",
	     \"".($aP['v_img'])."\",
	     \"".($aP['v_data_view_d'].' '.$aP['v_data_view_t'])."\",
	     \"".($aP['v_visible'])."\",
	     \"".($aP['v_hp'])."\",
	     \"".($aP['v_weight'])."\")";
	$r = mysqli_query($CON, $q) or LOG__Error("DB__AddElement", mysqli_error($CON)."\n".$q);
	$OUT = mysqli_insert_id($CON);
	return $OUT;
}

// LastUpdate 2016.02.22
function DB__Set_Element($aP=Array()) {
	$CON = GDB__Get_CoreSession();
	//
	$OUT                    = false;
	$aP['v_id']             = (isset($aP['v_id'])             && $aP['v_id'] != '')                   ? $aP['v_id']                   : 0;
	$aP['v_user']           = (isset($aP['v_user'])           && trim($aP['v_user']) != '')           ? trim($aP['v_user'])           : '';
	$aP['v_title_it']       = (isset($aP['v_title_it'])       && trim($aP['v_title_it']) != '')       ? trim($aP['v_title_it'])       : '';
	$aP['v_title_en']       = (isset($aP['v_title_en'])       && trim($aP['v_title_en']) != '')       ? trim($aP['v_title_en'])       : '';
	$aP['v_description_it'] = (isset($aP['v_description_it']) && trim($aP['v_description_it']) != '') ? trim($aP['v_description_it']) : '';
	$aP['v_description_en'] = (isset($aP['v_description_en']) && trim($aP['v_description_en']) != '') ? trim($aP['v_description_en']) : '';
	$aP['v_yt']             = (isset($aP['v_yt'])             && trim($aP['v_yt']) != '')             ? trim($aP['v_yt'])             : '';
	$aP['v_img']            = (isset($aP['v_img'])            && trim($aP['v_img']) != '')            ? trim($aP['v_img'])            : '';
	$aP['v_data_view_d']    = (isset($aP['v_data_view_d'])    && trim($aP['v_data_view_d']) != '')    ? trim($aP['v_data_view_d'])    : '';
	$aP['v_data_view_t']    = (isset($aP['v_data_view_t'])    && trim($aP['v_data_view_t']) != '')    ? trim($aP['v_data_view_t'])    : '';
	$aP['v_visible']        = (isset($aP['v_visible'])        && $aP['v_visible'] != '')              ? (int)$aP['v_visible']         : 0;
	$aP['v_hp']             = (isset($aP['v_hp'])             && $aP['v_hp'] != '')                   ? 1                             : 0;
	$aP['v_weight']         = (isset($aP['v_weight'])         && trim($aP['v_weight']) != '')         ? trim($aP['v_weight'])         : 0;

	// id utente mancante
	if ($aP['v_id']==0) return false;

	$addQ = Array();
	$addQ[] = ' `v_user`="'.MyEscape($aP['v_user']).'" ';
	$addQ[] = ' `v_title_it`="'.MyEscape($aP['v_title_it']).'" ';
	$addQ[] = ' `v_title_en`="'.MyEscape($aP['v_title_en']).'" ';
	$addQ[] = ' `v_description_it`="'.$aP['v_description_it'].'" ';
	$addQ[] = ' `v_description_en`="'.$aP['v_description_en'].'" ';
	$addQ[] = ' `v_yt`="'.$aP['v_yt'].'" ';
	$addQ[] = ' `v_img`="'.$aP['v_img'].'" ';
	$addQ[] = ' `v_data_view`="'.$aP['v_data_view_d'].' '.$aP['v_data_view_t'].'" ';
	$addQ[] = ' `v_weight`="'.((int)$aP['v_weight']).'" ';
	$addQ[] = ' `v_hp`="'.$aP['v_hp'].'" ';
	$addQ[] = ' `v_visible`="'.$aP['v_visible'].'" ';

	//
	$q = 'UPDATE `'.DB_PREFIX.'video` SET '.implode(', ', $addQ).'
	WHERE v_id="'.$aP['v_id'].'"';

	$r = mysqli_query($CON, $q) or LOG__Error("DB__SetElement", mysqli_error($CON)."\n".$q);
	//
	if (mysqli_error($CON)=='') $OUT = $aP['v_id'];
	else                        $OUT = false;
	//
	return $OUT;
}


// LastUpdate 2016.02.23
function DB__Del_Element($id) {
	$CON = GDB__Get_CoreSession();
	$q = 'DELETE FROM `'.DB_PREFIX.'video` WHERE v_id = "'.(int)$id.'"';
	$r = mysqli_query($CON, $q) or LOG__Error("DB__Del_Element[]", mysqli_error($CON)."\n".$q);
}

// LastUpdate 2016.02.23
function HTML__List($aFilter=Array(), $aElements=Array(), $totPages=1) {
	GLOBAL $m, $CONF, $op;

	$HTML = GHTML__Get_ModuleHeader('Gestione', 'Ricerca / Elenco');
	
	$HTMLP = '<ul class="pagination pagination-sm remove-margin">';
	$aTmpFilter = $aFilter; unset($aTmpFilter['op_page']); $query = http_build_query(array_filter($aTmpFilter));
	for($i=1;$i<=$totPages;$i++) {

		$class =  (isset($aFilter['op_page']) && $aFilter['op_page']==$i) ? ' class="active"' : '';
		$link  =  (isset($aFilter['op_page']) && $aFilter['op_page']==$i) ? 'javascript:;'    : '?m='.$m.'&op='.$op.'&op_page='.$i.'&'.$query;
		//$link  = '?m='.$m.'&op='.$op.'&op_page='.$i.'&'.$query;
		$HTMLP .= '<li '.$class.'><a href="'.$link.'">'.$i.'</a></li>';
	}
	$HTMLP .= '</ul>';
	
	
	$HTML .= '<script src="//cameratag.com/api/v7/js/cameratag.js" type="text/javascript"></script>
	<script>
	function ajStatus(uid, status) {
		dataSend="m='.$m.'&app=true&op=aj-vstatus&id="+uid+"&v_visible="+status;
		$.ajax({
			method: "POST",
			url: "index.php",
			data: dataSend,
			dataType:"json"
		}).done(function( msg ) {
			if(msg.op=="1") {
				var k = "#tr_"+uid;
				var c = (status==0) ? "text-classic" : "";
				    c = (status==1) ? "text-success" : c;
					c = (status==2) ? "text-danger"  : c;
				var counter = $(k).length; 
				$(k).attr("class", c);
			}
		});
	}
	function ajDel(uid) {
		if(confirm("Are you sure?")) {
			dataSend="m='.$m.'&app=true&op=aj-del&id="+uid;
			$.ajax({
				method: "POST",
				url: "index.php",
				data: dataSend,
				dataType:"json"
			}).done(function( msg ) {			
				if(msg.op==1) {
					var k = "#tr_"+uid;
					var c = $(k).children().length;
					$(k).addClass("danger");
					$(k).html("<td colspan=\'"+c+"\' class=\'text-danger text-center\'>"+uid+" deleted</td>")
				}
			});
		}
	}
	</script>
	<div class="block">
		<div class="block-title">
			<div class="block-options pull-right" data-toggle="tooltip" data-original-title="Search">
				<a class="btn btn-alt btn-sm btn-default" title="" href="?app=true&m='.$m.'&op=op-search"  data-toggle="modal" data-target="#modal"><i class="hi hi-search"></i></a>
			</div>
			<h2>List</h2>
		</div>

		<!-- Modal Search -->
		<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog"><div class="modal-content"></div><!-- /.modal-content --></div>
		</div>

		<div class="table-responsive">
			<table class="table table-striped table-vcenter table-bordered dataTable table-hover">
			<tr><td colspan="12"><div class="dataTables_paginate paging_bootstrap">'.$HTMLP.'</div></td></tr>
			<thead>
			<tr class="tr_head">
				<th>ID</th>
				<th>Name</th>
				<th>IT</th>
				<th>EN</th>
				<th>Date</th>
				<th>Weight</th>
				<th>Media</th>
				<th colspan="4" class="w90 text-center">Action</th>
			</tr>
			</thead>
			<tbody>';

	foreach($aElements as $el) {
		$tr_id  = 'tr_'.$el['v_id'];
		$a_view = '<a href="?app=true&m='.$m.'&id='.$el['v_id'].'&op=op-details" title="Details" data-toggle="modal" data-target="#modal">'.GHTML__Get_Ico('zoom+', 'Details').'</a>';
		$a_edit = '<a href="?m='.$m.'&op=op-form&id='.$el['v_id'].'" title="Edit"  >'.GHTML__Get_Ico('edit', 'Edit').'</a>';
		$a_del  = '<a href="javascript:ajDel('.$el['v_id'].')" title="Delete">'.GHTML__Get_Ico('delete', 'Delete').'</a>';
		
		$a_status = '<a href="javascript:ajStatus('.$el['v_id'].', 0)" title="Hide"   class="text-classic">'. GHTML__Get_Ico('private', 'Hide').'</a>
				<br><a href="javascript:ajStatus('.$el['v_id'].', 1)" title="Public" class="text-success">'. GHTML__Get_Ico('public',   'Public').'</a>';

		//<span class="text-success">Success Text</span>
		$class = ($el['v_visible']==0) ? 'class="text-classic"' : ''; 
		$class = ($el['v_visible']==1) ? 'class="text-success"' : $class;
		$class = ($el['v_visible']==2) ? 'class="text-danger"'  : $class;
		
		$HTML .= NL.'<tr id="'.$tr_id.'" '.$class.'>
					<td>'.$el['v_id'].'</td>
					<td>'.$el['v_user'].'</td>
					<td><b>'.$el['v_title_it'].'</b><br>'.$el['v_description_it'].'</td>
					<td><b>'.$el['v_title_en'].'</b><br>'.$el['v_description_en'].'</td>
					<td>'.$el['v_data_view'].'</td>
					<td>'.$el['v_weight'].'</td>
					<td>'.((isset($el['v_yt']) && $el['v_yt']!='')   ? '<i class="fa fa-youtube"></i>' : '').'<br>'.((isset($el['v_img']) && $el['v_img']!='') ? '<i class="fa fa-picture-o"></i>' : '').'</td>
					<td class="text-center">'.$a_view.'</td>
					<td class="text-center">'.$a_edit.'</td>
					<td class="text-center">'.$a_status.'</td>
					<td class="text-center">'.$a_del.'</td>
				</tr>';
	}
	$HTML .= '</tbody>
			<tfoot>
				<td colspan="12"><div class="dataTables_paginate paging_bootstrap">'.$HTMLP.'</div></td>
			</tfoot>
			</table>
			<br>
			</div>
		</div>';
	
	return $HTML;
}



// LastUpdate 2016.02.22
function HTML__ModalDetails($title='', $aEl=Array()) {
	GLOBAL $m;
	$HTML ='<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<title>'.$title.'</title>		
	<script>
	$("body").on("hidden.bs.modal", ".modal", function () {
	    $(this).removeData("bs.modal");
	});
	</script>
</head>
<body>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">'.$title.'</h4>
	</div><!-- /modal-header -->
	<div class="modal-body">
		<div class="row">
			<ul class="fa-ul">
				<li><i class="fa fa-pencil fa-li"></i>Title IT:         '.$aEl['v_title_it'].' </li>
				<li><i class="fa fa-pencil fa-li"></i>Title EN:         '.$aEl['v_title_en'].' </li>
				<li><i class="fa fa-pencil fa-li"></i>Public:           '.$aEl['v_visible'].' </li>
			</ul>
		</div>
		<div class="row text-center">
			'.(($aEl['v_yt']!='') ? '<iframe width="560" height="315" src="https://www.youtube.com/embed/'.$aEl['v_yt'].'" frameborder="0" allowfullscreen></iframe>' : '').'
		</div>
	</div><!-- /modal-body -->
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div><!-- /modal-footer -->
</body>
</html>';
	return $HTML;
}


function HTML__Form($aEl=Array(), $op='op-edit') {
	GLOBAL $m;
	
	
	
	$HTML = '<div class="block full">
		<div class="block-title">
			<h2>Gestione utente</h2>
		</div>
		<form action="?m='.$m.'" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
		<input type="hidden" name="op" value="'.$op.'">
		<input type="hidden" name="id" value="'.$aEl['v_id'].'">
			<div class="form-group">
            	<label class="col-md-3 control-label">ID</label>
                	<div class="col-md-9">
                    	<p class="form-control-static">'.$aEl['v_id'].'</p>
					</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="v_user">Activist name</label>
				<div class="col-md-9"><input type="text" class="form-control" id="v_user" name="v_user" placeholder="Name Surname" value="'.$aEl['v_user'].'"></div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="v_title_it">Title IT</label>
				<div class="col-md-9"><input type="text" class="form-control" id="v_title_it" name="v_title_it" placeholder="Title IT" value="'.$aEl['v_title_it'].'"></div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="v_description_it">Description IT</label>
				<div class="col-md-9"><textarea id="v_description_it" name="v_description_it" class="ckeditor">'.$aEl['v_description_it'].'</textarea></div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="v_title_it">Title EN</label>
				<div class="col-md-9"><input type="text" class="form-control" id="v_title_en" name="v_title_en" placeholder="Title EN" value="'.$aEl['v_title_en'].'"></div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="v_description_it">Description EN</label>
				<div class="col-md-9"><textarea id="v_description_en" name="v_description_en" class="ckeditor">'.$aEl['v_description_en'].'</textarea></div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="v_yt">Youtube Video</label>
				<div class="col-md-9"><input type="text" class="form-control id="v_yt" name="v_yt"" placeholder="v_yt" value="'.$aEl['v_yt'].'"></div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="u_email">Image preview</label>
				<div class="col-md-9"><input type="text" class="form-control id="v_img" name="v_img"" placeholder="v_img" value="'.$aEl['v_img'].'"></div>
			</div>
			<div class="form-group form-inline">
				<label class="col-md-3 control-label" for="u_video">Publication\'s date</label>
				<div class="col-md-4">
					<input type="text" id="v_data_view_d" name="v_data_view_d" class="form-control input-datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="'.substr($aEl['v_data_view'], 0, 10).'">
					<input type="text" id="v_data_view_t" name="v_data_view_t" class="form-control input-timepicker24" value="'.substr($aEl['v_data_view'], 10, 6).'">
				</div>
			</div>
            <div class="form-group">
				<label class="col-md-3 control-label">In HP</label>
                <div class="col-md-9">
                	<label class="checkbox-inline" for="v_hp">
                	<input type="checkbox" id="v_hp" name="v_hp" value="1" '.(($aEl['v_hp']==1) ? 'checked="checked"' : '').'> 
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Status</label>
                <div class="col-md-9">
                	<div class="radio">
                		<label for="v_visible_0">
                			<input type="radio" id="v_visible_0" name="v_visible" value="0" '.(($aEl['v_visible']==0) ? 'checked="checked"' : '').'> Stand by
                		</label>
                	</div>
					<div class="radio">
						<label for="v_visible_1">
							<input type="radio" id="v_visible_1" name="v_visible" value="1" '.(($aEl['v_visible']==1) ? 'checked="checked"' : '').'> Published
						</label>
					</div>
				</div>
			</div>
			<div class="form-group">
					<label class="col-md-3 control-label" for="v_weight">Weight</label>
					<div class="col-md-9"><input type="text" class="form-control" id="v_weight" name="v_weight" placeholder="v_weight" value="'.$aEl['v_weight'].'"></div>
			</div>
			
			<div class="form-group form-actions">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-sm btn-primary" name="save" value="save"><i class="fa fa-angle-right"></i> Submit</button>
					<button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
				</div>
			</div>
		</form>
	</div>
	<script src="js/ckeditor/ckeditor.js"></script>';
	return $HTML;
}




?>