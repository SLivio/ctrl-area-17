<?php /*
  Version:     v7 2016.02.22
  Module:      Amnesty Activist
  Author:      SLivio
*/



//--
$LOCAL_CONF['module_op']    = Array(
	Array('id'    => '1',
		'label' => 'Manage Activist ',
		'op'    => 'op-list'),
		Array('id'    => '1',
				'label' => 'Add Activist ',
				'op'    => 'op-form')
	);
$LOCAL_CONF['module_label']  = 'Gestisci video';
$LOCAL_CONF['key']           = 'm_100_Amnesty-Activist';
$LOCAL_CONF['menu']          = Array('visible' => true);

?>