<?php /*
  Version:     v7 2017.01.10
  Module:      Zava16.Autorita
  Author:      SLivio
*/



//--
//----------------------------------------------------------> [CONFIG]
if(file_exists($CONF['path_module'].'this.lib.php')) {
	require_once($CONF['path_module'].'this.lib.php');
}
$id  = (isset($_REQUEST['id'])    && $_REQUEST['id']    != '' && $_REQUEST['id'] != 0)     ? $_REQUEST['id']    : 0;
$opp = (isset($_REQUEST['opp'])   && $_REQUEST['opp']   != '' && $_REQUEST['opp'] != '')   ? $_REQUEST['opp'] : '';
$aP  = $_REQUEST;
$aFilter = Array();
//----------------------------------------------------------> [/CONFIG]



switch($op) {
	case 'get-tot-pratiche' :
		$lbl = (isset($_REQUEST['lbl'])   && $_REQUEST['lbl']   != '' && $_REQUEST['lbl'] != '')   ? $_REQUEST['lbl'] : '';
		$out = DB__Get_CountIspettorati($lbl);
		if($out==true) $aR  = Array('op'=>'1', 'msg'=>$out, 'random'=>get_RandomString());
		else           $aR  = Array('op'=>'0', 'msg'=>'Ko :: '.$out, 'random'=>get_RandomString());
		$json = json_encode($aR);
		die($json);
	break;
	case 'del' :
		$out = DB__Del_Element($id);
		if($out==true) $aR  = Array('op'=>'1', 'msg'=>'Ok', 'random'=>get_RandomString());
		else           $aR  = Array('op'=>'0', 'msg'=>'Ko :: '.$out, 'random'=>get_RandomString());
		$json = json_encode($aR);
		die($json);
	break;
	case 'op-details' :
		$aEl= DB__Get_Element($id);
		$HTML = HTML__ModalDetails('Dettagli dell\'Autorità', $aEl);
		echo $HTML;
	break;
	case 'op-search' :
		$HTML = HTML__ModalSearch('Cerca l\'Autorità', $aP);
		echo $HTML;
	break;
	default :
	case 'op-list' :
		$aFilter['op_page']    = (isset($_REQUEST['op_page'])    ? $_REQUEST['op_page']         : 1);
		$aFilter['op_order']   = (isset($_REQUEST['op_order'])   ? $_REQUEST['op_order']        : ' autorita_id DESC ');
		$aFilter['autorita_nome']  = (isset($_REQUEST['autorita_nome'])  ? trim($_REQUEST['autorita_nome']) : '');
	
		
		$aCount    = DB__Get_Count_Elements($aFilter);
		$aElements = DB__Get_Elements($aFilter);
		
		$HTML      = GHTML__Get_ModuleHeader('Gestione', 'Autorità');
		$HTML     .= HTML__List($aFilter, $aElements, $aCount);
		echo $HTML;
	break;
	case 'op-new'  :
	case 'op-edit' :
		$HTML = '';
	
		if ($id==0 && $op=='op-new') {
			$aEl   = DB__Get_Element(0);
			$HTML .= GHTML__Get_ModuleHeader('Gestione Autorità', 'Nuova');
			$HTML .= HTML__Form($aEl);
		} else {
			$HTML .= GHTML__Get_ModuleHeader('Gestione Autorità', 'Modifica');
			if($id!=0) {
				$aP['autorita_id'] = $id;
				$dbId = DB__Set_Element($aP);
				if ($dbId) $HTML .= GHTML__Get_StatusMessage('success', 'Modifica autorità', 'Modifica completata con successo');
			} else {
				$id = DB__Add_Element($aP);
				if ($id) $HTML .= GHTML__Get_StatusMessage('success', 'Nuova autorità', 'Inserimento completato con successo.');
			}
			$aEl   = DB__Get_Element($id);
			$HTML .= HTML__Form($aEl);
			$HTML .= HTML__FormIspettorato($aEl);
		}
		
		echo $HTML;
	break;
}


?>