<?php /*
  Version:     v7 2017.01.10
  Module:      Zava16.Autorita
  Author:      SLivio
*/



//--

$LOCAL_CONF = Array(
    'module_op' => Array(
        Array('id'    => '1',
            'label' => 'Elenco Autorità ',
            'op'    => 'op-list'),
        Array('id'  => '2',
            'label' => 'Nuova Autorità',
            'op'    => 'op-new')
    ),
    'module_label' => 'Gestisci Autorità',
    'key' => 'm_100_Zava-Autorita',
    'menu' => Array(
        'visible' => true
    ),
    'local_js' => 'this.lib.js?'.get_RandomString()
);





?>