/*
  Version:     v7 2017.01.10
  Module:      Zava16.Autorita
  Author:      SLivio
*/

function gotoPage(lnk) {
	document.location.href=lnk;
	//alert(lnk);
}


function elDel(m, id) {
	var dataSend = "m="+m+"&app=true&op=del&&id="+id;
	if (confirm("L'oggetto verrà eliminato. Confermi?")) {
		$.ajax({
			method: "POST",
			url: "index.php",
			data: dataSend,
			dataType:"json"
		}).done(function( msg ) {
			var deleted = "#tr_"+id;
			var actions = "#tr_"+id+" .actions a";
			if(msg.op=='1') {
				$(deleted).addClass("text-danger");
				$(deleted).css("text-decoration", "line-through");
				$(actions).attr("href", "javascript:;");
				$(actions).addClass("text-danger");
			} else {
				alert(msg.msg);
			}
		});
	}
}



function loadFormIspettorato(ispettLbl) {
	/*
	$.ajax({
		method: "POST",
		url: "index.php?m="+m+"&id="+id+"&op=get-tot-pratiche",
		data: dataSend,
		dataType:"json"
	}).done(function(jsn) {
		$(tmpBadge).text(jsn.msg);
		//$(tmpBadge).text("asd");
		totPratiche = totPratiche+parseInt(jsn.msg);
		$(".tot-pratiche").text(totPratiche);
	});
	*/
}


$(function() {

	if($('.ckeditor').length>=1) {
		CKEDITOR.replace( 'autorita_note', {
			height: 100,
			width: '100%',
			toolbarGroups: [
				{"name":"basicstyles","groups":["basicstyles"]}
			],
			// Remove the redundant buttons from toolbar groups defined above.
			removeButtons: 'Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
		} );
	}
	
	$(".list-ispettorati > a").click(function() {
		var ispettorato = $(this).find('h4').text();
		loadFormIspettorato(ispettorato);
	});
	
	// validazione della compagnia
	$("#formCompagnia").validate({
        errorClass: 'help-block animation-slideDown',
        errorElement: 'div',
        errorPlacement: function(error, e) {
        	//e.parents('.form-group > div').append(error);
        },
        highlight: function(e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.help-block').remove();
        },
        success: function(e) {
            e.closest('.form-group').removeClass('has-success has-error');
            e.closest('.help-block').remove();
        },
        rules: {
        	agenzia_nome: {
                required: true,
                minlength: 3
            }
        },
        messages: { }
    });

});



