/*
  Version:     2017.01.10
  Module:      Core :: Manage Module
  Author:      SLivio
*/


function AJ_AddModule(m, trClass) {
	var trKey = "."+trClass;
	var module_key   = $(trKey).find( "input[name='module_key']" ).val();
	var module_label = $(trKey).find( "input[name='module_label']" ).val();
	var module_dir   = $(trKey).find( "input[name='module_dir']" ).val();
	
	var dataSend = "app=true&m="+m+"&op=m_add&module_key="+module_key+"&module_label="+module_label+"&module_dir="+module_dir;
	if (confirm("Install module: \""+module_label+"\" ?")) {
		$.ajax({
			method: "POST",
			url: "index.php",
			data: dataSend,
			dataType:"json"
		}).done(function( msg ) {
			if(msg.op==1) {
				$(trKey).addClass("alert-success");
				$(trKey).find("form").remove();
				$(trKey).find(".gi").removeClass("gi-warning_sign").removeClass("text-warning");
				$(trKey).find(".gi").addClass("gi-ok_2").addClass("text-success");
				console.log(trKey);
			} else {
				alert(msg.msg);
			}
		});
	}
}


function AJ_EditRecipient(m, oF) {
	var r_id     = document.forms[oF].nr_id.value;
	var r_name   = document.forms[oF].nr_name.value;
	var r_weight = document.forms[oF].nr_weight.value;
	
	var data_send = 'app=true&m='+m+'&op=nr_edit&id='+r_id+'&nr_name='+r_name+'&nr_weight='+r_weight;
	$.ajax({
		url: "index.php",
		type: "POST",
		data: data_send,
		cache: false,
		dataType: "json",
		success: function(resp){
			if(resp.op==1) {
				$("#nr_tr_"+r_id).removeClass().addClass('msg_ok');
			} else {
				$("#nr_tr_"+r_id).removeClass().addClass('msg_ko');
				alert(resp.msg);
			}
		}
	});
}


function AJ_DelModule(m, mid) {
	if (confirm("Eliminare modulo?")) {
		
		var data_send = 'app=true&m='+m+'&op=m_del&id='+mid;
		$.ajax({
			url: "index.php",
			type: "POST",
			data: data_send,
			cache: false,
			dataType: "json",
			success: function(resp){
				if(resp.op==1) {
					$("#tr_"+mid).html('<td colspan="7" class="error">'+resp.msg+'</td>');
				} else {
					$("#tr_"+mid).removeClass().addClass('msg_ko');
					alert(resp.msg);
				}
			}
		});
	}
}


function Display_AddAdminForm(mid) {
	$tmp = '#tr-add-'+mid;
	$($tmp).toggle();
}