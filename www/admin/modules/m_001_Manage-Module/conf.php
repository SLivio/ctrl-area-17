<?php /*
  Version:     2017.01.10
  Module:      Core :: Manage Module
  Author:      SLivio
*/



//--

$LOCAL_CONF = Array(
    'module_op' => Array(
        Array('id'    => '1',
            'label' => 'Check Modules',
            'op'    => 'm_check'), // controlla 
        Array('id'    => '2',
            'label' => 'Manage Modules ',
            'op'    => 'm_conf'),
        Array('id'    => '3',
            'label' => 'Backup ',
            'op'    => 'm_list'),
        Array('id'    => '4',
            'label' => 'Info ',
            'op'    => 'ctrl-area')
    ),
    'module_label' => 'Core :: Manage Modules',
    'key' => 'core_ManageModule__naqweil123489f7hedfuerhbfasdyu6',
    'menu' => Array(
        'visible' => true
    ),
    'local_widget' => Array(
        0 => Array('op' => 'ctrl-area', 'label' => 'Platform widget', 'class' => 'col-sm-6')
    ),
    'local_js' => 'this.lib.js?'.get_RandomString()
);

?>