<?php /*
  Version:     2017.01.10
  Module:      Core :: Manage Module
  Author:      SLivio
*/



//--
if(file_exists($CONF['path_module'].'this.lib.php')) {
	require_once($CONF['path_module'].'this.lib.php');
}

//----------------------------------------------------------> [LOCAL_CONFIG]

$opp  = (isset($_REQUEST['opp']) && $_REQUEST['opp'] != '') ? $_REQUEST['opp']  : '';
$id   = (isset($_REQUEST['id'])  && $_REQUEST['id']  != '') ? $_REQUEST['id']   : '';
$path = (isset($_REQUEST['path']) && $_REQUEST['path'] != '') ? $_REQUEST['path'] : '';
$module_open   = (isset($_REQUEST['module_open'])   && $_REQUEST['module_open']   != '') ? $_REQUEST['module_open']   : 1;
$module_weight = (isset($_REQUEST['module_weight']) && $_REQUEST['module_weight'] != '') ? $_REQUEST['module_weight'] : 0;
$module_label  = (isset($_REQUEST['module_label'])  && $_REQUEST['module_label']  != '') ? $_REQUEST['module_label']  : '';
$out  = '';

//----------------------------------------------------------> [LOCAL_CORE]
$CON = GDB__Get_CoreSession();

switch($op) {
	case 'm_add' :
		$m_id = DB_New_Module($_REQUEST);
		//$m_id = 1;
		if ($m_id != 0 && $m_id != false) {
			$out .= '{
		  "op"  : "1",
		  "msg" : "The module has been installed correctly. New Module id: '.$m_id.'. ",
		  "random" : "'.get_RandomString().'"
		  }';
		} else {
			$out .= '{
		  "op"  : "0",
		  "msg" : "An error has occurred.",
		  "random" : "'.get_RandomString().'"
		  }';
		}
		die($out);
	break;
	case 'm_del' :
		if ($id != '' && FILE_Del_Module($id)==1) {
			DB_Del_Module($id);
			$out .= '{
		  "op"  : "1",
		  "msg" : "Modulo eliminato :: ",
		  "random" : "'.get_RandomString().'"
		  }';
		} else {
			$out .= '{
		  "op"  : "0",
		  "msg" : "Si sono verificati degli errori.",
		  "random" : "'.get_RandomString().'"
		  }';
		}
		die($out);
	break;
	
	case 'm_check' :
		$HTML = LOCAL_CheckModules();
		echo $HTML;
	break;
	case 'm_conf' :
		if ($opp=='set_opened' && $id!='') {
			$open = ($module_open==0) ? 1 : 0;
			DB_Set_OpenedModule($id,$open);
		}
		if ($opp=='set_weight' && $id!='') {
			DB_Set_WeightModule($id,$module_weight);
		}
		if ($opp=='set_label' && $id!='') {
			DB_Set_LabelModule($id,$module_label);
		}
		if ($opp=='m_disable' && $id!='') {
			$aF         = DB__Get_infoRecord($CON, DB_PREFIX.'core_modules', ' WHERE module_id ="'.$id.'"');
			$tmp_status = ($aF['module_enable']==1) ? 0 : 1;
			DB_Set_ModuleEnable($id, $tmp_status);
		}

		if ($opp=='m_del' && $id!='') {
			$aF  = DB__Get_infoRecord($CON, DB_PREFIX.'core_modules', ' WHERE module_id ="'.$id.'"');
			$path_res  = './modules/'.$aF['module_dir'].'/';
			$path_dest = './modules/__DELETE__'.date('Ymd-His').'__'.$aF['module_dir'].'/';
			if (file_exists($path_res) && is_writable($path_res)) {
				if (rename($path_res, $path_dest)) {
					DB_Del_Module($id);
					HTML_TopOkMessage('Modulo cancellato con successo.');
				}
			} else {
				echo GHTML__Get_StatusMessage('warning', 'Warning', 'The module\'s directory is not writable. Is not possibile delete this module permanently.');
				DB_Del_Module($id);
			}
		}
		if ($opp=='add_admin') {
			$user_id   = (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] != '') ? $_REQUEST['user_id'] : 0;
			if ($user_id != 0 && $id != 0) {
				DB_Add_AdminModule($id, $user_id);
			}
		}
		if ($opp=='add_group') {
			$group_id   = (isset($_REQUEST['group_id']) && $_REQUEST['group_id'] != '') ? $_REQUEST['group_id'] : 0;
			if ($group_id != 0 && $id != 0) {
				DB_Add_GroupModule($id, $group_id);
			}
		}
		echo HTML_ModulesList();
	break;
	case 'm_list' :
		//modules/__DELETE__20110117-184147__
		// Elimnazione
		if ($opp=='m_delete' && $path!='') {
			echo 'Tento eliminazione '.$path;
		// Ripristino
		} else if ($opp=='m_restore' && $path!='') {
			if ($path != '' && file_exists($path) && is_dir($path)) {
				$path_res  = $path;
				//modules/__DELETE__20110117-184147__
				$path_dest = './modules/'.substr($path, 35, 300);
				echo $path_dest;
				if (rename($path_res, $path_dest)) {
					echo HTML_TopOkMessage('Ripristino avvenuto correttamente.');
				}
			}
		}
		// Lista
		echo LOCAL_CheckBackup();
	break;
	case 'ctrl-area' :
		echo LOCAL_CheckThis();
	break;
}


?>