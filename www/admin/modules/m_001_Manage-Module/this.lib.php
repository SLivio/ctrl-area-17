<?php /*
  Version:     2017.01.10
  Module:      Core :: Manage Module
  Author:      SLivio
*/



// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- [Admin]
function LOCAL_DB_Get_AdminInModule($module_id=0, $onlyId=0) {
	$CON = GDB__Get_CoreSession();
	
	$c   = 0;
	$a   = Array();
	$aId = Array(0 => 0);
	$q   = 'SELECT '.DB_PREFIX.'core_admin.* 
			FROM 
			'.DB_PREFIX.'core_admin_modules,'.DB_PREFIX.'core_admin 
			WHERE 
			'.DB_PREFIX.'core_admin.user_id='.DB_PREFIX.'core_admin_modules.user_id AND 
			'.DB_PREFIX.'core_admin_modules.module_id="'.$module_id.'"';
	$r = mysqli_query($CON, $q) or LOG__Error("LOCAL_DB_Get_AdminInModule[1]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		while($rec = mysqli_fetch_array($r)) {
			$a[$c] = $rec;
			$aId[] = $rec['user_id'];
			$c++;
		}
	}
	if ($onlyId!=0) {
		return $aId;
	} else {
		return $a;
	}
}

function LOCAL_DB_Get_AdminAll() {
	$CON = GDB__Get_CoreSession();
	$aR = Array();
	$aR = DB__Get_allRecords($CON, DB_PREFIX.'core_admin', 'WHERE `user_id`<>"" ORDER BY user_id DESC');
	return $aR;
}

function LOCAL_DB_Get_AdminNotInModule($module_id=0) {
	$aReturn   = Array();
	$aAdminIn  = LOCAL_DB_Get_AdminInModule($module_id, 1);
	$aAdminAll = LOCAL_DB_Get_AdminAll($module_id, 1);
	$c        = 0;
	foreach($aAdminAll as $tmp) {
		if (!empty($tmp) && !in_array($tmp['user_id'], $aAdminIn)) {
			$aReturn[$c] = $tmp;
			$c++;
		}
	}
	return $aReturn;
}


// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- [Group]

// Last Update v6 2014.04.24
function LOCAL_DB_Get_GroupsInModule($module_id=0, $onlyId=0) {
	$CON = GDB__Get_CoreSession();
	$c   = 0;
	$a   = Array();
	$aId = Array(0 => 0);
	$q   = 'SELECT '.DB_PREFIX.'core_groups.*
			FROM
			'.DB_PREFIX.'core_groups_modules,'.DB_PREFIX.'core_groups
			WHERE
			'.DB_PREFIX.'core_groups.group_id='.DB_PREFIX.'core_groups_modules.group_id AND
			'.DB_PREFIX.'core_groups_modules.module_id="'.$module_id.'"';
	$r = mysqli_query($CON, $q) or LOG__Error("LOCAL_DB_Get_GroupInModule[1]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		while($rec = mysqli_fetch_array($r)) {
			$a[$c] = $rec;
			$aId[] = $rec['group_id'];
			$c++;
		}
	}
	if ($onlyId!=0) {
		return $aId;
	} else {
		return $a;
	}
}


// Last Update v6 2014.04.24
function LOCAL_DB_Get_GroupsAll() {
	$CON = GDB__Get_CoreSession();
	$aR = Array();
	$aR = DB__Get_allRecords($CON, DB_PREFIX.'core_groups', 'WHERE `group_id`<>0 ORDER BY group_id DESC');
	return $aR;
}


// Last Update v6 2014.04.24
function LOCAL_DB_Get_GroupsNotInModule($module_id=0) {
	$aReturn   = Array();
	$aAdminIn  = LOCAL_DB_Get_GroupsInModule($module_id, 1);
	$aAdminAll = LOCAL_DB_Get_GroupsAll($module_id, 1);
	$c        = 0;
	foreach($aAdminAll as $tmp) {
		if (!empty($tmp) && !in_array($tmp['group_id'], $aAdminIn)) {
			$aReturn[$c] = $tmp;
			$c++;
		}
	}
	return $aReturn;
}


function LOCAL_CheckBackup($recovery_path_m='') {
	GLOBAL $CONF, $aModules, $op, $m;
	//--------------------------------------------------------[CHECK SU FILESYSTEM]
	$HTML = NL.'<div class="contentcontainer">
		<div class="headings">
			<h2>Modules Backup</h2>
		</div>
		<div class="contentbox">
			<table width="100%">
			<thead>
				<tr><th colspan="4">Scanning directory...</th></tr>
			</thead>
			<tbody>';
	$COUNT = 1;
	$aDirModules  = Array(); // elenco dei moduli
	$aDirModules  = PHP_scandir('modules', false, 1);
	//
	$class = 'class="tr_pari"';
	foreach($aDirModules as $dir) {
		// verifica moduli cancellati
		if (substr($dir, 0, 5) != '__DEL') continue;
		//
		$class = ($class=='class="tr_pari"') ? 'class="tr_dispari"' : 'class="tr_pari"';
		$LOCAL_CONF = Array();
		$HTML    .= '<tr '.$class.' id="tr_'.$COUNT.'">';
		$HTML    .= '<td class="w30">['.$COUNT.']</td>';
		$tmp_path = $CONF['path_modules'].'/'.$dir; // il percorso del modulo
		$tmp_conf = $tmp_path.'/conf.php';          // il file config del modulo
		$aTmpM    = Array(); // le info sul modulo
		if(file_exists($tmp_conf)) {
			require($tmp_conf);
			if (isset($LOCAL_CONF) && is_array($LOCAL_CONF)) {

				$HTML .= '<td>Check <em>'.$tmp_path.'</em> >> ';
				$HTML .= (isset($LOCAL_CONF['module_label']) ? $LOCAL_CONF['module_label'] : '...');

				if (isset($LOCAL_CONF['key']) && $LOCAL_CONF['key'] != '') {
					$aM = Array('module_key' => $LOCAL_CONF['key'], 'module_label' => $LOCAL_CONF['module_label'], 'module_dir' => $dir);
					$HTML .= '<br>[Key: '.$LOCAL_CONF['key'].']<br>';
				}
				$HTML .= '</td>';
			}

		}
		$m_add = '<a href="?op='.$op.'&m='.$m.'&path='.$tmp_path.'&opp=m_restore"><img src="'.FILE_Ico('add').'" title="restore this module" /></a>';
		$m_del = '<a href="?op='.$op.'&m='.$m.'&path='.$tmp_path.'&opp=m_delete"><img src="'.FILE_Ico('del').'" title="delete this module" /></a>';
		$HTML .= '<td>'.$m_add.'</td>';
		$HTML .= '<td>'.$m_del.'</td>';
		
		$HTML .= '</tr>';
		$COUNT++;
	}
	$HTML .= '<tr class="tr_head"><td colspan="4">End of scanning.</td></tr>
				</tbody>
			</table>
			<div style="clear: both;"></div>
		</div>
	</div>';
	return $HTML;
}


// LastUpdate 4.201112.21 [ok]
function LOCAL_CheckModules() {
	GLOBAL $CONF, $aModules, $aModule, $op, $m;
	//--------------------------------------------------------[CHECK SU FILESYSTEM]
	$HTML = '
	<div class="content-header">
		<div class="header-section">	
			<script type="text/javascript">
			var aj_path   = "./'.$CONF['path_modules'].'/'.$aModule['module_dir'].'";
			var aj_url    = aj_path+"/ajax.php";
			var aj_return = "";
			
			</script>
			 <h1>
				<i class="fa fa-cog"></i>Check Modules<br><small>Analyze all modules.</small>
			</h1>
		</div>
	</div>
	<div class="block">
		<div class="table-responsive">
			<table class="table table-striped table-vcenter">
			<thead>
				<tr><th colspan="3">Scanning directory...</th></tr>
			</thead>
			<tbody>';
	$COUNT = 1;
	$aDirModules  = Array(); // elenco dei moduli
	$aDirModules  = PHP_scandir('modules', false, 1);
	//
	foreach($aDirModules as $dir) {
		// verifica moduli cancellati
		if (substr($dir, 0, 5) == '__DEL') continue;
		//
		$LOCAL_CONF = Array();
		$HTML    .= '<tr class="#KEY#">';
		$HTML    .= '<td class="w30 ac">['.$COUNT.']</td>';
		$tmp_path = $CONF['path_modules'].'/'.$dir; // il percorso del modulo
		$tmp_conf = $tmp_path.'/conf.php';          // il file config del modulo
		$aTmpM    = Array(); // le info sul modulo
		if(file_exists($tmp_conf)) {
			require($tmp_conf);
			if (isset($LOCAL_CONF) && is_array($LOCAL_CONF)) {

				$HTML .= '<td><strong>Name: <em>'.(isset($LOCAL_CONF['module_label']) ? $LOCAL_CONF['module_label'] : 'Not defined').'</em></strong><br>';
				$HTML .= 'Path: <em>'.$tmp_path.'</em><br>';
				
				// check su variabile di conf e confronto con DB
				if (isset($LOCAL_CONF['key']) && $LOCAL_CONF['key'] != '') {
					$aTmpM = GDB__Get_InfoModule('',$LOCAL_CONF['key']);
					if (count($aTmpM) >= 1) {
						$HTML .= 'Key: <em>'.$LOCAL_CONF['key'].'</em><br>';
						$HTML .= 'Module id: <em>'.$aTmpM['module_id'].'</em><br>';
						if (isset($LOCAL_CONF['local_check']) && is_array($LOCAL_CONF['local_check']) && !empty($LOCAL_CONF['local_check'])) {
							foreach($LOCAL_CONF['local_check'] as $aTmpCheck) {
								if ($aTmpCheck['check']==false) {
									$HTML .= ' <a title="" data-toggle="tooltip" href="javascript:void(0)" data-original-title="'.$aTmpCheck['ko'].'"><i class="gi gi-warning_sign text-danger"></i></a>';
								} else {
									$HTML .= ' <a title="" data-toggle="tooltip" href="javascript:void(0)" data-original-title="'.$aTmpCheck['ok'].'"><i class="gi gi-ok_2"></i></a>';
								}
							}
						}
						$ico   = '<i class="gi gi-ok_2 text-success"></i>';
					} else {
						$HTML .= '<span id="txt_oFmAdd_'.$COUNT.'"><strong>Unknowed key: <em>'.$LOCAL_CONF['key'].'</em> </strong><br></span>';
						$aM    = Array('module_key' => $LOCAL_CONF['key'], 'module_label' => $LOCAL_CONF['module_label'], 'module_dir' => $dir);
						$HTML .= '<form name="oFmAdd_'.$COUNT.'">
							<input type="hidden" name="module_key"   value="'.$LOCAL_CONF['key'].'">
							<input type="hidden" name="module_label" value="'.$LOCAL_CONF['module_label'].'">
							<input type="hidden" name="module_dir"   value="'.$dir.'">
							<a href=\'javascript:AJ_AddModule('.$m.', "'.$LOCAL_CONF['key'].'");\' title="Add this module">
									<i class="gi gi-circle_plus"></i> Click here for install this module </a>
						</form>';
						$ico   = '<i class="gi gi-warning_sign text-warning"></i>';
					}
				} else {
					$HTML .= '<br><b>There are probably errors in this module.</b>';
					$ico   = '<i class="gi gi-warning_sign text-warning"></i>';
				}
				$HTML .= '</td>';
				$HTML .= '<td class="w30 ac">'.$ico.' <!--id="Img2_oFmAdd_'.$COUNT.'"--></td>';
			}
			$HTML = str_replace("#KEY#", $LOCAL_CONF['key'], $HTML);
		}
		$HTML .= '</tr>';
		$COUNT++;
	}
	$HTML .= '</tbody>
	<thead>
		<tr>
			<th colspan="3">Scanning database...</th>
		</tr>
	</thead>
	<tbody>';
	$COUNT = 1;
	//
	$aTmpModules = GDB__Get_AllModules();
	//
	$class = 'class="tr_pari"';
	foreach($aTmpModules as $tmpM) {
		$class = ($class=='class="tr_pari"') ? 'class="tr_dispari"' : 'class="tr_pari"';
		//
		$tmp_path = $CONF['path_modules'].'/'.$tmpM['module_dir'];
		$tmp_conf = $tmp_path.'/conf.php';
		$HTML .= '<tr '.$class.'>';
		$HTML .= '<td>['.$COUNT.']</td>';
		$HTML .= '<td>Check module_id: <em>'.$tmpM['module_id'].'</em> <br>';
		$HTML .= '>> Original name: <em>'.(isset($tmpM['module_label']) ? $tmpM['module_label'] : 'Not defined').'</em><br>';
		$HTML .= 'Check directory: <em>'.$tmp_path.'</em><br>';
		if (file_exists($tmp_conf)) {
			$HTML .= '>> Detected // '.$tmpM['record_date'];
			$ico   = '<i class="gi gi-ok_2 text-success"></i>';
		} else {
			$HTML .= '>> Not detected';
			$ico   = '<i class="gi gi-warning_sign text-warning"></i>';
		}
		$HTML .= '<td class="w30 ac">'.$ico.'</td>
			</tr>';
		$COUNT++;
	}
	$HTML .= '</tbody>
			</table>
			<div style="clear: both;"></div>
		</div>
	</div>
	</div>
	</div>';
	return $HTML;
}


// LastUpdate 4.201112.21 [ok]
function LOCAL_CheckThis() {
	GLOBAL $CONF, $aModules, $op, $m, $HOST;
	// 
	$disk_free    = (int)disk_free_space('/');
	$disk_free_M  = (int)(($disk_free/1024)/1024);
	$HTML = '
	<div class="widget">
		<div class="block">
			<div class="block-title"><h2><strong>Ctrl-Ara :: Info</strong></h2></div>
			<dl class="dl-horizontal">
				<dt>'.((defined('APP_NAME')) ? APP_NAME : 'asd').'</dt>
				<dd>v. '.((defined('APP_VERSION')) ? APP_VERSION : '').'</dd>
				<dt>PHP Version</dt>
				<dd>'.phpversion().'</dd>
				<dt>HTTP_HOST</dt>
				<dd>'.$_SERVER['HTTP_HOST'].'</dd>
				<dt>SERVER_NAME</dt>
				<dd>'.$_SERVER['SERVER_NAME'].'</dd>
				<dt>SERVER_ADDR</dt>
				<dd>'.$_SERVER['SERVER_ADDR'].'</dd>
				<dt>SERVER_HOST</dt>
				<dd>'.$HOST.'</dd>
				<dt>DOCUMENT_ROOT</dt>
				<dd>'.$_SERVER['DOCUMENT_ROOT'].'</dd>
				<dt>SCRIPT_FILENAME</dt>
				<dd>'.$_SERVER['SCRIPT_FILENAME'].'</dd>
				<dt>DISK SPACE</dt>
				<dd>Available Space: '.$disk_free_M.' MB</dd>
				
				<dt>POST MAX SIZE</dt>
				<dd>'.ini_get('post_max_size').'</dd>
				<dt>UPLOAD MAX FILESIZE</dt>
				<dd>'.ini_get('upload_max_filesize').'</dd>
				<dt>MAX EXECUTION TIME</dt>
				<dd>'.ini_get('max_execution_time').'</dd>
				<dt>CLEAR ERROR</dt>
				<dd><a href="?op=clear-error">clear</a></dd>
			</dl>
		</div>
	</div>';
	return $HTML;
}

function recurse_copy($src,$dst) {
	$dir = opendir($src);
	@mkdir($dst);
	while(false !== ( $file = readdir($dir)) ) {
		if (( $file != '.' ) && ( $file != '..' )) {
			if ( is_dir($src . '/' . $file) ) {
				recurse_copy($src . '/' . $file,$dst . '/' . $file);
			}
				else {
				copy($src . '/' . $file,$dst . '/' . $file);
			}
		}
	}
	closedir($dir);
} 


function DB_Del_Module($id=0) {
	$CON = GDB__Get_CoreSession();
	DB__Del_records($CON, DB_PREFIX.'core_modules', 'module_id', $id);
	DB__Del_records($CON, DB_PREFIX.'core_admin_modules', 'module_id', $id);
	DB__Del_records($CON, DB_PREFIX.'core_group_modules', 'module_id', $id);
}


function DB_New_Module($aM) {
	$CON = GDB__Get_CoreSession();
	$OUT = 0;
	if (isset($aM['module_key']) && $aM['module_key'] != '' && isset($aM['module_dir']) && $aM['module_dir'] != '') {
		$aM['module_label'] = (isset($aM['module_label']) && $aM['module_label'] != '') ? $aM['module_label'] : 'tmp_label';
		$q = 'INSERT INTO '.DB_PREFIX.'core_modules  
		      (module_id,
		      module_label,
		      module_dir,
		      module_key,
		      module_weight,
		      record_ip,
		      record_date) 
		      VALUES 
		      (NULL,
		      "'.$aM['module_label'].'",
		      "'.$aM['module_dir'].'",
		      "'.$aM['module_key'].'",
		      0,
		      "'.$_SERVER['REMOTE_ADDR'].'",
		      NOW())';
		$r = mysqli_query($CON, $q) or LOG__Error("DB_New_Module", mysqli_error($CON)."\n".$q);
		$OUT = mysqli_insert_id($CON);
	}
	return $OUT;
}


function DB_Set_OpenedModule($module_id=0,$module_open=0) {
	$CON = GDB__Get_CoreSession();
	$OUT = 0;
	if($module_id != '' && $module_id != 0){
		//	
		$q = "UPDATE `".DB_PREFIX."core_modules` SET 
		     `module_open`  =\"".$module_open."\"
		     WHERE 
		     	module_id=".$module_id;
		$r = mysqli_query($CON, $q) or LOG__Error("DB_Set_OpenedModule", mysqli_error($CON)."\n".$q);
		$OUT = 1;
	}
	return $OUT;
}

function DB_Set_ModuleEnable($module_id=0, $module_enable=0) {
	$CON = GDB__Get_CoreSession();
	$OUT = 0;
	if($module_id != '' && $module_id != 0){
		//	
		$q = "UPDATE `".DB_PREFIX."core_modules` SET 
		     `module_enable`  =\"".$module_enable."\"
		     WHERE 
		     	module_id='".$module_id."'";
		$r = mysqli_query($CON, $q) or LOG__Error("DB_Set_OpenedModule", mysqli_error($CON)."\n".$q);
		$OUT = 1;
	}
	return $OUT;
}



function DB_Set_WeightModule($module_id=0,$module_weight=0) {
	$CON = GDB__Get_CoreSession();
	$OUT = 0;
	if($module_id != '' && $module_id != 0){
		//	
		$q = "UPDATE `".DB_PREFIX."core_modules` SET 
		     `module_weight`  =\"".$module_weight."\"
		     WHERE 
		     	module_id=".$module_id;
		$r = mysqli_query($CON, $q) or LOG__Error("DB_Set_WeightModule", mysqli_error($CON)."\n".$q);
		$OUT = 1;
	}
	return $OUT;
}


function DB_Set_LabelModule($module_id=0,$module_label=0) {
	$CON = GDB__Get_CoreSession();
	$OUT = 0;
	if($module_id != '' && $module_id != 0){
		//	
		$q = "UPDATE `".DB_PREFIX."core_modules` SET 
		     `module_label`  =\"".$module_label."\"
		     WHERE 
		     	module_id=".$module_id;
		$r = mysqli_query($CON, $q) or LOG__Error("DB_Set_LabelModule", mysqli_error($CON)."\n".$q);
		$OUT = 1;
	}
	return $OUT;
}


function DB_Add_AdminModule($module_id=0, $user_id=0) {
	$CON = GDB__Get_CoreSession();
	$OUT = false;
	if ($module_id != 0 && $user_id != 0) {
		$aTmpCheck = Array(); // controllo l'esistenza del record
		$aTmpCheck = DB__Get_infoRecord($CON, DB_PREFIX.'core_admin_modules', ' WHERE user_id="'.$user_id.'" AND module_id="'.$module_id.'" ', 0);
		if (!isset($aTmpCheck['module_id'])) {
			// inserisco record
			$q = "INSERT INTO `".DB_PREFIX.'core_admin_modules'."` (
			     `user_id`,
			     `module_id`,
			     `record_ip`,
			     `record_date`
			     ) VALUES (
			     \"".$user_id."\",
			     \"".$module_id."\",
			     \"".$_SERVER["REMOTE_ADDR"]."\",
			     NOW()
			     )";
			$r = mysqli_query($CON, $q) or LOG__Error("DB_Add_Admin", mysqli_error($CON)."\n".$q);
			$OUT = true;
		}
	}
	return $OUT;
}


function DB_Add_GroupModule($module_id=0, $group_id=0) {
	$CON = GDB__Get_CoreSession();
	$OUT = false;
	if ($module_id != 0 && $group_id != 0) {
		$aTmpCheck = Array(); // controllo l'esistenza del record
		$aTmpCheck = DB__Get_infoRecord($CON, DB_PREFIX.'core_groups_modules', ' WHERE group_id="'.$group_id.'" AND module_id="'.$module_id.'" ', 0);
		if (!isset($aTmpCheck['module_id'])) {
			// inserisco record
			$q = "INSERT INTO `".DB_PREFIX.'core_groups_modules'."` (
			     `group_id`,
			     `module_id`,
			     `record_ip`,
			     `record_date`
			     ) VALUES (
			     \"".$group_id."\",
			     \"".$module_id."\",
			     \"".$_SERVER["REMOTE_ADDR"]."\",
			     NOW()
			     )";
			$r = mysqli_query($CON, $q) or LOG__Error("DB_Add_GroupModule", mysqli_error($CON)."\n".$q);
			$OUT = true;
		}
	}
	return $OUT;
}


// LastUpdate 4.201112.21 [ok]
function HTML_ModulesList() {
	GLOBAL $CONF, $aModules, $aModule, $op, $m, $p;
	//
	$aList = GDB__Get_AllModules();
	//

	$HTML = '
	<div class="content-header">
		<div class="header-section">
  			<h1>Permission manage</h1></div>
  		</div>
		<div class="block">
  			<div class="block-title">
				<form method="POST" action="">
				<table class="table table-striped table-vcenter">
					<thead>
					<tr class="tr_head">
						<th class="w50">ID</th>
						<th>Module Name and path</th>
						<th>Admin Add / Group Add</th>
  						<th class="w120">Weight</th>
						<th class="w60" colspan="3">Action</th>
					</tr>
					</thead>
					<tbody>';
	//
	$class = 'class="tr_pari"';
	
	$totAdmin = GDB__Count_Admin();
	$totGroup = GDB__Count_Group();
	foreach($aList as $el) {
		$class = ($class=='class="tr_pari"') ? 'class="tr_dispari"' : 'class="tr_pari"';
		//
		$tr_id  = 'tr_'.$el['module_id'];
		$ico_lock   = ($el['module_open']==1)   ? 'unlock'     : 'lock';
		$ico_enable = ($el['module_enable']==0) ? 'disconnect' : 'connect';
		$m_lock     = '<a href="?op='.$op.'&m='.$m.'&id='.$el['module_id'].'&opp=set_opened&module_open='.$el['module_open'].'">'.GHTML__Get_Ico($ico_lock, 'Lock / Unlock').'</a>';
		$m_disable  = '<a href="?op='.$op.'&m='.$m.'&id='.$el['module_id'].'&opp=m_disable">'.GHTML__Get_Ico($ico_enable, 'Disable').'</a>';
		$m_del      = '<a href="?op='.$op.'&m='.$m.'&id='.$el['module_id'].'&opp=m_del">'.GHTML__Get_Ico('del', 'Delete').'</a>';
		$HTML .= NL.'';
		$HTML .= NL.'<tr '.$class.' id="'.$tr_id.'">';
			$HTML .= '<td>'.$el['module_id'].'</td>';
			$HTML .= '<td><form name="mw_'.$el['module_id'].'_0" method="post" action="">'.
						pager_getInputHidden('opp', 'set_label').''.
						pager_getInputHidden('id', $el['module_id']).'
						<input type="text" name="module_label" value="'.$el['module_label'].'" class="txt mediumbox" /> 
						<a href="javascript:;" onclick="$(\'form[name=mw_'.$el['module_id'].'_0]\').submit()">'.GHTML__Get_Ico('ok', 'Save', '').'</a>
						<br><span class="smltxt">'.$el['module_dir'].'</span>
					</form>'.'</td>';
			
		// Add an Admin
		if ($el['module_open']==0) $m_ad = GDB__Count_AdminModule($el['module_id']);
		else                       $m_ad = $totAdmin;
		$aNotAd = Array();
		$aNotAd = LOCAL_DB_Get_AdminNotInModule($el['module_id']);
		$HTML .= '<td>User: ';
		$toAdd = count($aNotAd);
		if($toAdd >= 1 && $el['module_open'] != 1) {
			$HTML .= '<form name="add-admin-'.$el['module_id'].'" method="post" action="">'.$m_ad.' / '.$totAdmin.' '.
			pager_getInputHidden('op',  $op).
			pager_getInputHidden('id',  $el['module_id']).
			pager_getInputHidden('opp', 'add_admin').
			pager_getSelect($aNotAd, 'user_id', 'user_id', 'user_email', '').' 
			<a href="javascript:;" onclick="$(\'form[name=add-admin-'.$el['module_id'].']\').submit()">'.GHTML__Get_Ico('ok', 'Save', '').'</a>
			</form>';
		} else {
			$HTML .= $m_ad.' / '.$totAdmin;
		}
		$HTML .= '<hr>';
		// Add a Group
		if ($el['module_open']==0) $m_gr = GDB__Count_GroupModule($el['module_id']);
		else                       $m_gr = $totGroup;
		$aNotGr = Array();
		$aNotGr = LOCAL_DB_Get_GroupsNotInModule($el['module_id']);
		$HTML .= 'Group: ';
		$toAdd = count($aNotGr);
		if($toAdd >= 1 && $el['module_open'] != 1) {
			$HTML .= '<form name="add-group-'.$el['module_id'].'" method="post" action="">'.$m_gr.' / '.$totGroup.' '.
						pager_getInputHidden('op',  $op).
						pager_getInputHidden('id',  $el['module_id']).
						pager_getInputHidden('opp', 'add_group').
						pager_getSelect($aNotGr, 'group_id', 'group_id', 'group_label', '').' 
						<a href="javascript:;" onclick="$(\'form[name=add-group-'.$el['module_id'].']\').submit()">'.GHTML__Get_Ico('ok', 'Save', '').'</a>
					</form>';
		} else {
			$HTML .= $m_gr.' / '.$totGroup;
		}
		$HTML .= '</td>';
		
			$HTML .= '<td><form name="mw_'.$el['module_id'].'_1" method="post" action="">'.
				pager_getInputHidden('opp', 'set_weight').
				pager_getInputHidden('id', $el['module_id']).'
				<input type="text" name="module_weight" value="'.$el['module_weight'].'"  class="txt smallbox" size="3">
				<a href="javascript:;" onclick="$(\'form[name=mw_'.$el['module_id'].'_1]\').submit()">'.GHTML__Get_Ico('ok', 'Save', '').'</a>
			</form></td>';
			$HTML .= '<td class="w30 text-center">'.$m_disable.'</td>';
			$HTML .= '<td class="w30 text-center">'.$m_lock.'</td>';
				$HTML .= '<td class="w30 text-center">'.$m_del.'</td>';
		$HTML .= '</tr>';

	}
	$HTML .= '</tbody></table>
			</div>
		</div>
	</div>';
	//
	return $HTML;
}


?>