<?php
/*
  Version:     3.201110.21
  Module:      Core :: Personal Information
  Author:      SLivio
*/



//--
// LastUpdate 3.201110.21 [OK]
function HTML_Form() {
	GLOBAL $CONF, $aModules, $op, $m;
	$LOCAL_TITLE = 'Change your personal infromation';
	$LOCAL_TITLE = 'Modifica delle informazioni personali';
	$a = GDB__Get_Admin($CONF['admin_id']);
	
	echo'
	<div class="page-content">
		<div class="content-header">
			<div class="header-section"><h2>'.$LOCAL_TITLE.'</h2></div>
		</div>
		<div class="block">';
		$objForm = new ValidForm("personalData", NL);
		$objForm->addField("m",  "", VFORM_HIDDEN, array(), array(), array("hint" => $m));
		$objForm->addField("op", "", VFORM_HIDDEN, array(), array(), array("hint" => $op));
		$objForm->addField("id", "", VFORM_HIDDEN, array(), array(), array("hint" => $CONF['admin_id']));
		// Personal data: name, surname
		$objForm->addField("user_name","Nome: ",VFORM_STRING,
			array("maxLength" => 250),
			array("maxLength" => "Valore inserito troppo lungo. Sono consentiti massimo %s caratteri."),
			array("default" => $a['user_name'])
		);
		$objForm->addField("user_surname","Cognome: ",VFORM_STRING,
			array("maxLength" => 250),
			array("maxLength" => "Valore inserito troppo lungo. Sono consentiti massimo %s caratteri."),
			array("default" => $a['user_surname'])
		);
		$objForm->addField("user_alias","Alias: ",VFORM_STRING,
			array("maxLength" => 250),
			array("maxLength" => "Valore inserito troppo lungo. Sono consentiti massimo %s caratteri."),
			array("default" => $a['user_alias'])
		);
		// Login data: email, password
		$objForm->addParagraph("Se si desidera modificare la password, spuntare la checkbox di seguito.", "<br />");
		$objPwd = $objForm->addArea("Modifica le informazioni di login", true, "login", false);
		//*** There is also built-in support for password fields
		$objPwd->addField("user_email", "Indirizzo email:", VFORM_EMAIL, 
			array(
				"maxLength" => 255, 
				"required"  => true,
				"disabled" => true
			), 
			array(
				"maxLength" => "Valore inserito troppo lungo. Sono consentiti massimo %s caratteri.", 
				"required"  => "Campo obbligatorio.", 
				"type"      => "Utilizzare il formato name@domain.com"
			),
			array(
				"default"   => $a['user_email'],
				"readonly"  => "readonly"
			)
		);
		$objPwd->addField("user_password_1","Password", VFORM_PASSWORD,
			array("required" => true),
			array("required" => "Campo obbligatorio")
		);
		$objPwd->addField("user_password_2","Repeat password", VFORM_PASSWORD,
			array("required" => true),
			array(
				"type"     => "This is not a valid e-mail address!",
				"required" => "Inserire nuovamente la password"
			)
		);
		//
		//$objForm->addJSEvent("submit","function(){ if(objForm.validate()){ alert('Client side validation is done. We now continue to validate the data serverside.');} }");
		//*** Setting the main alert.
		$objForm->setMainAlert("Si sono verificati degli errori. Controllare i campi segnalati in rosso e provare nuovamente.");
		//*** As this method already states, it sets the submit button's label.
		$objForm->setSubmitLabel("Invia");
		//*** Handling the form data.
		
		if($objForm->isSubmitted() && $objForm->isValid()){
			$objFormOk  = DB_Set_Admin($_REQUEST); // Field editing
			$strOutput  = $objForm->toHtml();      // Form rendering
			if ($objFormOk!=false) {              // Error message for editing problems
				$strOutput  .= $objForm->removeMainError(); 
				echo GHTML__Get_StatusMessage('success', 'Ok', 'Aggiornamento avvenuto con successo');
			}
		} else {
			$strOutput = $objForm->toHtml(); // Form rendering
		}
		echo $strOutput;
	echo '</div>';
}


// LastUpdate 2017.01.10
function DB__Validate_Element($action='set', $aP) {
	GLOBAL $CONF;
	
	if (!isset($CONF) || !isset($CONF['admin_id']) || $CONF['admin_id']==0) return false;
	if (isset($aP['user_password_1']) && ($aP['user_password_1'] != $aP['user_password_2'])) return false; 
	
	// Se tutto ok
	return true;
}

// LastUpdate 2017.01.10
function DB_Set_Admin($aP=Array()) {
	GLOBAL $CONF, $SESSION_ID;
	
	$CON = GDB__Get_CoreSession();
	//
	if(!DB__Validate_Element('set', $aP)) return false;

	$aP['user_name']       = (isset($aP['user_name'])       && trim($aP['user_name']) != '')       ? $aP['user_name']       : '';
	$aP['user_surname']    = (isset($aP['user_surname'])    && trim($aP['user_surname']) != '')    ? $aP['user_surname']    : '';
	//$aP['user_email']      = (isset($aP['user_email'])      && trim($aP['user_email']) != '')      ? $aP['user_email']      : '';
	$aP['user_alias']      = (isset($aP['user_alias'])      && trim($aP['user_alias']) != '')      ? $aP['user_alias']      : '';
	$aP['user_password_1'] = (isset($aP['user_password_1']) && trim($aP['user_password_1']) != '') ? $aP['user_password_1'] : '';
	$aP['user_password_2'] = (isset($aP['user_password_2']) && trim($aP['user_password_2']) != '') ? $aP['user_password_2'] : '';
	$OUT                   = false;
	
	// Modifica dei dati
	$addQ = Array();
	if (trim($aP['user_name']) != '')       $addQ[] = ' `user_name`="'.$aP['user_name'].'" ';
	if (trim($aP['user_surname']) != '')    $addQ[] = ' `user_surname`="'.$aP['user_surname'].'" ';
	//if (trim($aP['user_email']) != '')      $addQ[] = ' `user_email`="'.$aP['user_email'].'" ';
	if (trim($aP['user_alias']) != '')      $addQ[] = ' `user_alias`="'.$aP['user_alias'].'" ';
	if (trim($aP['user_password_1']) != '') $addQ[] = ' `user_password`="'.md5($aP['user_password_1']).'" ';
	
	//
	$q = 'UPDATE `'.DB_PREFIX.'core_admin` SET '.implode(', ', $addQ).',
	`record_date_modify`   = NOW(),
	`record_ip_modify`     = "'.$_SERVER["REMOTE_ADDR"]  .'"
	WHERE
	user_id="'.$CONF['admin_id'].'"';

	$r = mysqli_query($CON, $q) or LOG__Error("DB_Set_Admin", mysqli_error($CON)."\n".$q);
	//
	if (mysqli_error($CON)=='') {
		$_SESSION[$SESSION_ID]['admin_alias'] = $aP['user_alias'];
		$OUT = $CONF['admin_id'];
	}
	else                        $OUT = false;
	//
	return $OUT;
}







?>