<?php /*
Version:     v7 2017.01.10
Module:      Zava16.Compagnie
Author:      SLivio
*/




//----------------------------------------------------------> [HTML]

// LastUpdate 2016.02.26
function DB__Get_Count_Elements($aF=Array()) {
	GLOBAL $CONF;
	
	$aOut = Array();
	
	$qAdd  = (isset($aF['agenzia_nome']) && $aF['agenzia_nome']!='')  ? ' AND agenzia_nome like "%'.$aF['agenzia_nome'].'%"'  : '';

	$q      = 'SELECT count(agenzia_id) FROM '.DB_PREFIX.'pratiche_agenzie WHERE agenzia_id<>0 '.$qAdd;
	
	$CON    = GDB__Get_CoreSession();
	$r = mysqli_query($CON, $q) or LOG__Error("DB__Get_Count_Elements[]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		$tot     = mysqli_result($r, mysqli_affected_rows($CON), 0);
		$pageTot = ceil($tot/$CONF['def_page']);
		$aOut    = Array('totElements'=>$tot, 'totPages'=>$pageTot);
	} else {
		$aOut    = Array('totElements'=>$tot, 'totPages'=>1);
	}
	return $aOut;
}

function DB__Get_CountPraticheAgenzia($agId=0) {
	$q      = 'SELECT count(pratica_id) FROM '.DB_PREFIX.'pratiche WHERE pratica_agenzia_id='.(int)$agId;
	
	$CON    = GDB__Get_CoreSession();
	$r = mysqli_query($CON, $q) or LOG__Error("DB__Get_CountPraticheAgenzia[]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		$tot     = mysqli_result($r, mysqli_affected_rows($CON), 0);
	} else {
		$tot     = 0;
	}
	return $tot;
}

// LastUpdate 2017.11.28
function DB__Get_PraticheAgenzia($agId=0, $limit=10) {
	GLOBAL $CONF;
	$q         = 'SELECT * FROM '.DB_PREFIX.'pratiche WHERE pratica_agenzia_id='.(int)$agId.' order by pratica_id DESC LIMIT 0,'.(int)$limit;
	$CON       = GDB__Get_CoreSession();
	$aElements = DB__QueryN($CON, $q);

	return $aElements;
}

// LastUpdate 2017.11.28
function DB__Get_Elements($aF=Array()) {
	GLOBAL $CONF;

	$qLimitStart = ($aF['op_page']<=1) ? 0 : (($aF['op_page']-1)*$CONF['def_page']);
	$qLimitEnd   = $CONF['def_page'];

	$qAdd   = ($aF['agenzia_nome']!='')  ? ' AND agenzia_nome like "%'.$aF['agenzia_nome'].'%"'  : '';
	$qOrder = 'ORDER BY agenzia_id DESC';
	$qLimit = 'LIMIT '.$qLimitStart.','.$qLimitEnd;
	$q      = 'SELECT * FROM '.DB_PREFIX.'pratiche_agenzie WHERE agenzia_id<>0 '.$qAdd.' '.$qOrder.' '.$qLimit;
	
	$CON       = GDB__Get_CoreSession();
	$aElements = DB__QueryN($CON, $q);

	return $aElements;
}



// LastUpdate 2016.02.22
function DB__Get_Element($id=0) {
	if ($id==0) {
		$aR = Array(
			'agenzia_id' => 0,
			'agenzia_nome' => '',
			'agenzia_indirizzo_via' => '',
			'agenzia_indirizzo_num' => '',
			'agenzia_indirizzo_cap' => '',
			'agenzia_indirizzo_citta' => '',
			'agenzia_indirizzo_prov' => '',
			'agenzia_telefono' => '',
			'agenzia_fax' => '',
			'agenzia_sito' => '',
			'agenzia_email' => '',
			'agenzia_regexp' => ''
		);
	} else {
		$CON = GDB__Get_CoreSession();
		$aR = Array();
		$aR = DB__Get_infoRecord($CON, DB_PREFIX.'pratiche_agenzie', ' WHERE agenzia_id="'.(int)$id.'" ', 0);
	}
	
	return $aR;
}



function DB__Validate_Element($action='set', $aP) {
	if($action=='set') {
		$aP['agenzia_id'] = (isset($aP['agenzia_id']) && $aP['agenzia_id'] != '') ? $aP['agenzia_id'] : 0;
		if ($aP['agenzia_id']==0) {
			return false;
		}
	}
	// 
	$aP['agenzia_nome'] = (isset($aP['agenzia_nome']) && $aP['agenzia_nome'] != '') ? $aP['agenzia_nome'] : '';
	if ($aP['agenzia_nome']=='') return false;
	// Se tutto ok
	return true;
}


function DB__Add_Element($aP=Array()) {

	if(DB__Validate_Element('add', $aP)) {
		$CON = GDB__Get_CoreSession();
		$q   = 'INSERT INTO `'.DB_PREFIX.'pratiche_agenzie` (`agenzia_ip_creazione`, `agenzia_data_creazione`) VALUES ("'.$_SERVER["REMOTE_ADDR"].'", NOW())';
		$r   = mysqli_query($CON, $q) or LOG__Error("DB__AddElement", mysqli_error($CON)."\n".$q);
		$id  = mysqli_insert_id($CON);
		//
		$aP['agenzia_id'] = $id;
		if (mysqli_error($CON)=='') $OUT = DB__Set_Element($aP);
		else                        $OUT = false;
		if($id==$OUT) return $id;
		else          return false;
	} else {
		return false;
	}
}



// LastUpdate 2016.02.22
function DB__Set_Element($aP=Array()) {
	$CON = GDB__Get_CoreSession();
	//
	if(!DB__Validate_Element('set', $aP)) return false;

	$addQ = Array();
	//if ($aP['agenzia_nome'] != '')    $addQ[] = ' `agenzia_nome`="'.MyEscape($aP['agenzia_nome']).'" ';	$addQ[] = ' `u_policy_1`="'.$aP['u_policy_1'].'" ';
	$addQ[] = ' `agenzia_nome`="'.$aP['agenzia_nome'].'" ';
	$addQ[] = ' `agenzia_indirizzo_via`="'.$aP['agenzia_indirizzo_via'].'" ';
	$addQ[] = ' `agenzia_indirizzo_num`="'.$aP['agenzia_indirizzo_num'].'" ';
	$addQ[] = ' `agenzia_indirizzo_cap`="'.$aP['agenzia_indirizzo_cap'].'" ';
	$addQ[] = ' `agenzia_indirizzo_citta`="'.$aP['agenzia_indirizzo_citta'].'" ';
	$addQ[] = ' `agenzia_indirizzo_prov`="'.$aP['agenzia_indirizzo_prov'].'" ';
	$addQ[] = ' `agenzia_telefono`="'.$aP['agenzia_telefono'].'" ';
	$addQ[] = ' `agenzia_fax`="'.$aP['agenzia_fax'].'" ';
	$addQ[] = ' `agenzia_sito`="'.$aP['agenzia_sito'].'" ';
	$addQ[] = ' `agenzia_email`="'.$aP['agenzia_email'].'" ';
	$addQ[] = ' `agenzia_regexp`="'.$aP['agenzia_regexp'].'" ';
	
	
	//
	$q = 'UPDATE `'.DB_PREFIX.'pratiche_agenzie` SET '.implode(', ', $addQ).',
	`agenzia_data_modifica`   = NOW(),
	`agenzia_ip_modifica`     = "'.$_SERVER["REMOTE_ADDR"]  .'"
	WHERE
	agenzia_id="'.$aP['agenzia_id'].'"';

	$r = mysqli_query($CON, $q) or LOG__Error("DB__SetElement", mysqli_error($CON)."\n".$q);
	//
	if (mysqli_error($CON)=='') $OUT = $aP['agenzia_id'];
	else                        $OUT = false;
	//
	return $OUT;
}


// LastUpdate v7 2014.11.20
function DB__Del_Element($id) {
	$CON = GDB__Get_CoreSession();
	$q = 'DELETE FROM `'.DB_PREFIX.'pratiche_agenzie` WHERE agenzia_id = "'.(int)$id.'"';
	$r = mysqli_query($CON, $q) or LOG__Error("DB__Del_Element[]", mysqli_error($CON)."\n".$q);
	if (mysqli_error($CON)=='') $OUT = true;
	else                        $OUT = mysqli_error($CON);
	return true;
}

// LastUpdate 7.2017.12.05
function HTML__Pagination($aFilter=Array(), $aCounts=Array()) {
	GLOBAL $m, $CONF, $op;
	
	$HTMLP = '<form class="pull-right"><select onChange="gotoPage(this.value)">';
	$aTmpFilter = $aFilter;
	unset($aTmpFilter['op_page']);
	unset($aTmpFilter['op_order']);
	
	$query = http_build_query(array_filter($aTmpFilter));
	for($i=1;$i<=$aCounts['totPages'];$i++) {
		$class  =  (isset($aFilter['op_page']) && $aFilter['op_page']==$i) ? ' selected="selected"' : '';
		$link   =  '?m='.$m.'&op='.$op.'&op_page='.$i.'&'.$query.'&order='.$aFilter['op_order'];
		$HTMLP .= '<option '.$class.' value="'.$link.'">'.$i.'</a></li>';
	}
	$HTMLP .= '</select></form>';

	$HTML = '<div class="row">
			<div class="col-sm-6"><span><strong>'.$aCounts['totElements'].'</strong> risultati</span></div>
			<div class="col-sm-6">'.$HTMLP.'</div>
		</div>';
	
	return $HTML;
}


// LastUpdate 7.2017.12.05
function HTML__List($aFilter=Array(), $aElements=Array(), $aCounts=Array()) {
	GLOBAL $m, $CONF, $op;
	
	$HTML  = '';
	$HTMLP = HTML__Pagination($aFilter, $aCounts);
	
	
	$HTML .= '
	<div class="block">
		<div class="block-title">
			<div class="block-options pull-right" data-toggle="tooltip" data-original-title="Search">
				<a class="btn btn-alt btn-sm btn-default" title="" href="?app=true&m='.$m.'&op=op-search"  data-toggle="modal" data-target="#modal"><i class="hi hi-search"></i></a>
			</div>
			<h2>Elenco Compagnie</h2>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog"><div class="modal-content"></div><!-- /.modal-content --></div>
		</div>

		<div class="table-responsive">
			<table class="table table-striped table-vcenter table-bordered dataTable table-hover">
			<tr><td colspan="7">'.$HTMLP.'</td></tr>
			<thead>
				<tr class="tr_head">
					<th>ID</th>
					<th>Ragione sociale</th>
					<th>Indirizzo</th>
					<th>Contatti</th>
					<th colspan="3" class="w90 text-center">Azioni</th>
				</tr>
			</thead>
			<tbody>';

	foreach($aElements as $el) {
		$id = $el['agenzia_id'];
		
		$tr_id  = 'tr_'.$id;
		$a_info = '<a href="?m='.$m.'&op=op-details&id='.$id.'&app=true" title="Preview"  data-toggle="modal" data-target="#modal" >'.GHTML__Get_Ico('zoom-in', 'Vedi').'</a>';
		$a_edit = '<a href="?m='.$m.'&op=op-edit&id='.$id.'" title="Edit"  >'.GHTML__Get_Ico('edit', 'Modifica').'</a>';
		$a_del  = '<a href="javascript:elDel('.$m.', '.$id.')" title="Delete">'.GHTML__Get_Ico('delete', 'Cancella').'</a>';
		
		$HTMLcontact  = (($el['agenzia_telefono']!='') ? '<i class="fa fa-phone" data-toggle="tooltip" title="" data-original-title="Telefono"></i> '.$el['agenzia_telefono'].'<br>' : '');
		$HTMLcontact .= (($el['agenzia_fax']!='')      ? '<i class="fa fa-print" data-toggle="tooltip" title="" data-original-title="Fax"></i> '.     $el['agenzia_fax'].'<br>'      : '');
		$HTMLcontact .= (($el['agenzia_sito']!='')     ? '<i class="hi hi-globe" data-toggle="tooltip" title="" data-original-title="Sito"></i> '.    $el['agenzia_sito'].'<br>'     : '');
		$HTMLcontact .= (($el['agenzia_email']!='')    ? '<i class="hi hi-envelope" data-toggle="tooltip" title="" data-original-title="Email"></i> '.   $el['agenzia_email'].'<br>'    : '');
		
		
		$HTML .= NL.'<tr id="'.$tr_id.'">
					<td>'.$id.'</td>
					<td>'.$el['agenzia_nome'].'</td>
					<td>
						'.$el['agenzia_indirizzo_via'].' '.$el['agenzia_indirizzo_num'].'<br>
						'.$el['agenzia_indirizzo_cap'].' '.$el['agenzia_indirizzo_citta'].' '.$el['agenzia_indirizzo_prov'].'
					</td>
					<td>
						'.$HTMLcontact.'
					</td>
					<td class="text-center actions">'.$a_info.'</td>
					<td class="text-center actions">'.$a_edit.'</td>
					<td class="text-center actions">'.$a_del.'</td>
				</tr>';
	}
	$HTML .= '</tbody>
			<tfoot><td colspan="7">'.$HTMLP.'</td></tfoot>
			</table>
			<br>
			</div>
		</div>';

	return $HTML;
}




// LastUpdate 2017.01.10
function HTML__ModalDetails($title='', $aEl=Array()) {
	GLOBAL $m, $CONF;
	
	$totPratiche = DB__Get_CountPraticheAgenzia($aEl['agenzia_id']);
	$aPratiche   = DB__Get_PraticheAgenzia($aEl['agenzia_id'], 5);
	$HTMLpa      = '';
	foreach($aPratiche as $prat) {
		$HTMLpa .= '<span class="label label-info">'.$prat['pratica_anno'].' | '.$prat['pratica_codice'].' | '.$prat['pratica_oggetto'].'</span><br>';
	}
	
	$HTML ='<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<title>'.$title.'</title>
	<script>
	$(document).on("hidden.bs.modal", function (e) {
	    $(e.target).removeData("bs.modal").find(".modal-content").empty();
	});
	</script>
</head>
<body>
	<div class="modal-header text-center">
		<h2 class="modal-title"><i class="fa fa-search"></i> '.$title.'</h2>
	</div><!-- /modal-header -->
	<div class="modal-body">
		<div class="table-responsive">
			<table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
            	<!--<thead>
                	<tr>
                    	<th class="text-center">ID</th>
                        <th class="text-center"><i class="gi gi-user"></i></th>
					</tr>
				</thead>-->
				<tbody>
					<tr>
						<td width="40%">Ragione Sociale</td>
						<td width="60%">'.$aEl['agenzia_nome'].'</td>
					</tr>
					<tr>
						<td>Indirizzo</td>
						<td>'.
							$aEl['agenzia_indirizzo_via'].' '.$aEl['agenzia_indirizzo_num'].' <br>'.
							$aEl['agenzia_indirizzo_cap'].' '.$aEl['agenzia_indirizzo_citta'].' '.$aEl['agenzia_indirizzo_prov'].'
						</td>
					</tr>
					<tr>
						<td>Contatti</td>
						<td>
							<i class="fa fa-phone"></i>      '.$aEl['agenzia_telefono'].'<br>
							<i class="fa fa-print"></i>      '.$aEl['agenzia_fax'].'<br>
							<i class="fa fa-globe"></i>      '.$aEl['agenzia_sito'].'<br>
							<i class="fa fa-envelope-o"></i> '.$aEl['agenzia_email'].'
						</td>
					</tr>
					<tr>
						<td>Pratiche totali associate alla compagnia</td>
						<td>'.$totPratiche.'</td>
					</tr>
					<tr>
						<td>Ultime pratiche della compagnia<br>(Anno | Codice | Oggetto)</td>
						<td>'.$HTMLpa.'</td>
					</tr>
				</tbody>
			</table>
		</div><!-- /modal-body -->
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div><!-- /modal-footer -->
</body>
</html>';
	return $HTML;
}


// LastUpdate 2016.02.22
function HTML__ModalSearch($title='', $ar=Array()) {
	GLOBAL $m;
	
	$HTML ='<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<title>'.$title.'</title>
	<script>
	$("body").on("hidden.bs.modal", ".modal", function () {
	    $(this).removeData("bs.modal");
	});
	</script>
	<script src="js/app.js"></script>
</head>
<body>
	<div class="modal-header text-center">
		<h2 class="modal-title"><i class="fa fa-search"></i> '.$title.'</h2>
	</div><!-- /modal-header --><!-- /modal-header -->
	<div class="modal-body">
		<form action="?m='.$m.'" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
		<input type="hidden" name="op"  value="op-list">
			<div class="form-group">
				<label class="col-md-3 control-label" for="agenzia_nome">Ragione sociale della compagnia</label>
				<div class="col-md-9">
					<input type="text" id="agenzia_nome" name="agenzia_nome" class="form-control" placeholder="Es: Google inc">
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Search</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div><!-- /modal-footer -->
		</form>
	</div><!-- /modal-body -->
</body>
</html>';
	return $HTML;
}



// LastUpdate 2016.12.05
function DB__Get_CountIspettorati($compName=0) {
	GLOBAL $CONF;

	$out  = 0;
	$q    = 'SELECT count(pratica_id) FROM '.DB_PREFIX.'pratiche WHERE pratica_ispettorato="'.$compName.'"';

	$CON    = GDB__Get_CoreSession();
	$r = mysqli_query($CON, $q) or LOG__Error("DB__Get_CountAdmin[]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		$out = mysqli_result($r, mysqli_affected_rows($CON), 0);
	}
	return $out;
}


// LastUpdate 2017.12.17
function DB__Get_Ispettorati($compId=0) {
	GLOBAL $CONF;

	$q   = 'SELECT DISTINCT pratica_ispettorato FROM `'.DB_PREFIX.'pratiche`
			WHERE `pratica_agenzia_id` = "'.(int)$compId.'" AND pratica_id<>0; ';
	$CON = GDB__Get_CoreSession();
	$aEl = DB__QueryN($CON, $q);

	return $aEl;
}


// LastUpdate 2017.12.17
function HTML__FormIspettorato($aEl=Array()) {
	GLOBAL $m;
	$id = $aEl['agenzia_id'];
	
	$aIsp = DB__Get_Ispettorati($id);
	
	$c = 0;
	$HTMLisp = '';
	foreach($aIsp as $el) {
		$c++;
		$HTMLisp .= '<a href="javascript:void(0)" class="list-group-item">
				<span class="pull-right ispettorato badge">...</span>
				<h4 class="list-group-item-heading remove-margin">'.$c.'. <span class="ispettorato-nome">'.$el['pratica_ispettorato'].'</span></h4>
			</a>';
	}
	
	// il form di seguito dovrebbe permettere la modifica da questa schermata dell'anagrafica dell'ispettorato:
	// Click sull'elenco a SX -> Caricamento del form a DX 
	// -> Click sul btn del form 
	// -> Salvataggio dei dati e chiusura del form
	// DA COMPLETARE
	$HTMLformIsp = '<div class="block hide">
			<div class="block-title">
				<h2>Aggiorna anagrafica ispettorato</h2>
			</div>
			<div class="form box-ispettorato-sugg">
				<p>Clicca su un ispettorato per modificare la relativa anagrafica.</p>
			</div>
			<div class="form box-ispettorato-sugg">
				<form>
					<fieldset>
						<legend><i class="fa fa-angle-right"></i> Nome ispettorato</legend>
						<div class="form-group">
							<label class="col-md-4 control-label" for="val_username">Ispettorato <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                            	<div class="input-group">
                                	<input type="text" id="val_username" name="val_username" class="form-control" placeholder="Your username..">
                                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
								</div>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>';
	
	$HTML = '<div class="row">
		<div class="col-md-6">
			<div class="block">
				<div class="block-title">
					<h2>Elenco degli ispettorati per questa compagnia</h2>
				</div>
				<div class="list-group remove-margin list-ispettorati">
					'.((count($aIsp)>=1) ? $HTMLisp : 'Nessun ispettorato associato a questa compagnia.').'
				</div>
				<div class="list-group">
					<a href="javascript:void(0)" class="list-group-item">
						<span class="badge tot-pratiche label-primary"></span>
	                    <h4 class="list-group-item-heading"><strong>Pratiche totali</strong> associate ad un ispettorato</h4>
					</a>
					<a href="javascript:void(0)" class="list-group-item">
						<span class="badge tot-ispettorati label-primary"></span>
	                    <h4 class="list-group-item-heading"><strong>Ispettorati totali</strong> associate a questa agenzia</h4>
					</a>
							
					<br>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			'.((count($aIsp)>=1) ? $HTMLformIsp : '').'
		</div>
	</div>
	<script>
		$(function() {
			ContaPratichePerIspettorato("'.$m.'", "'.$id.'");
	});
	</script>';
	return $HTML;
}


function HTML__Form($aEl=Array()) {
	GLOBAL $m;
	
	/*$aCountries = GDB__Get_Countries();
	$HTMLoption = '';
	foreach($aCountries as $el) {
		$selected    = ($el['country_name']==$aEl['u_country']) ? 'selected="selected"' : '';
		$HTMLoption .= '<option value="'.$el['country_name'].'" '.$selected.'>'.$el['country_code'].' - '.$el['country_name'].'</option>';
	}*/
	
	$id = $aEl['agenzia_id'];
	
	$HTML = '<div class="block full">
		<div class="block-title">
			<h2>Scheda Compagnia</h2>
		</div>
		<form id="formCompagnia" action="?m='.$m.'" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" novalidate="novalidate">
		<input type="hidden" name="op" value="op-edit">
		<input type="hidden" name="id" value="'.$id.'">
			<div class="form-validation-message">
				<div class="error"></div>
			</div>
			<div class="form-group">
            	<label class="col-md-2 control-label">ID</label>
                <div class="col-md-10"><p class="form-control-static">'.$id.'</p></div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="agenzia_nome">Ragione sociale <span class="text-danger">*</span></label>
				<div class="col-md-10"><input type="text" class="form-control id="agenzia_nome" name="agenzia_nome"" value="'.$aEl['agenzia_nome'].'" required></div>
			</div>
			
            <div class="form-group">
						
                <div class="row">
                    <div class="col-md-6">
						<label class="col-md-4 control-label" for="agenzia_indirizzo_via">Indirizzo</label>
						<div class="col-md-8"><input type="text" class="form-control" id="agenzia_indirizzo_via" name="agenzia_indirizzo_via" placeholder="Es: Via delle valli" value="'.$aEl['agenzia_indirizzo_via'].'"></div>
					</div>
					<div class="col-md-6">
						<label class="col-md-4 control-label" for="agenzia_indirizzo_num">Numero</label>
						<div class="col-md-8"><input type="text" class="form-control" id="agenzia_indirizzo_num" name="agenzia_indirizzo_num" placeholder="Es: 7" value="'.$aEl['agenzia_indirizzo_num'].'"></div>
					</div>
				</div>
								
				<div class="row">
                    <div class="col-md-6">
						<label class="col-md-4 control-label" for="agenzia_indirizzo_cap">Cap</label>
						<div class="col-md-8"><input type="text" class="form-control" id="agenzia_indirizzo_cap" name="agenzia_indirizzo_cap" placeholder="Es: 00100" value="'.$aEl['agenzia_indirizzo_cap'].'"></div>
					</div>
					<div class="col-md-6">
						<label class="col-md-4 control-label" for="agenzia_indirizzo_citta">Città</label>
						<div class="col-md-8"><input type="text" class="form-control" id="agenzia_indirizzo_citta" name="agenzia_indirizzo_citta" placeholder="Es: Roma" value="'.$aEl['agenzia_indirizzo_citta'].'"></div>
					</div>
				</div>

				<div class="row">
                    <div class="col-md-6">
						<label class="col-md-4 control-label" for="agenzia_indirizzo_prov">Provincia</label>
						<div class="col-md-8"><input type="text" class="form-control" id="agenzia_indirizzo_prov" name="agenzia_indirizzo_prov" placeholder="Es: RM" value="'.$aEl['agenzia_indirizzo_prov'].'"></div>
					</div>
					<div class="col-md-6"></div>
				</div>
								
			</div>
								
			 <div class="form-group">
						
                <div class="row">
                    <div class="col-md-6">
						<label class="col-md-4 control-label" for="agenzia_telefono">Telefono</label>
						<div class="col-md-8"><input type="text" class="form-control" id="agenzia_telefono" name="agenzia_telefono" placeholder="Es: 06123456" value="'.$aEl['agenzia_telefono'].'"></div>
					</div>
					<div class="col-md-6">
						<label class="col-md-4 control-label" for="agenzia_fax">Fax</label>
						<div class="col-md-8"><input type="text" class="form-control" id="agenzia_fax" name="agenzia_fax" placeholder="Es: 06123456" value="'.$aEl['agenzia_fax'].'"></div>
					</div>
				</div>
								
				<div class="row">
                    <div class="col-md-6">
						<label class="col-md-4 control-label" for="agenzia_sito">Sito</label>
						<div class="col-md-8"><input type="text" class="form-control" id="agenzia_sito" name="agenzia_sito" placeholder="Es: http://www.google.it" value="'.$aEl['agenzia_sito'].'"></div>
					</div>
					<div class="col-md-6">
						<label class="col-md-4 control-label" for="agenzia_email">Email</label>
						<div class="col-md-8"><input type="text" class="form-control" id="agenzia_email" name="agenzia_email" placeholder="info@dominio.est" value="'.$aEl['agenzia_email'].'"></div>
					</div>
				</div>
			</div>
								
			<div class="form-group">
				<div class="row">
                    <div class="col-md-6">
						<label class="col-md-4 control-label" for="agenzia_regexp">Validazione</label>
						<div class="col-md-8"><textarea class="form-control" id="agenzia_regexp" name="agenzia_regexp">'.$aEl['agenzia_regexp'].'</textarea></div>
					</div>
					<div class="col-md-6"></div>
				</div>
								
			</div>					
								
			
			<div class="form-group form-actions">
				<div class="col-md-6 text-center">
					<button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
				</div>
				<div class="col-md-6 text-center">
					<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
				</div>
			</div>
		</form>
	</div>';
	return $HTML;
}




?>