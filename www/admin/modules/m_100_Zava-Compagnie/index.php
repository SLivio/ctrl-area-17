<?php /*
Version:     v7 2017.01.10
Module:      Zava16.Autorita
Author:      SLivio
*/



//--
//----------------------------------------------------------> [CONFIG]
if(file_exists($CONF['path_module'].'this.lib.php')) {
	require_once($CONF['path_module'].'this.lib.php');
}
$id  = (isset($_REQUEST['id'])    && $_REQUEST['id']    != '' && $_REQUEST['id'] != 0)     ? $_REQUEST['id']    : 0;
$opp = (isset($_REQUEST['opp'])   && $_REQUEST['opp']   != '' && $_REQUEST['opp'] != '')   ? $_REQUEST['opp'] : '';
$aP  = $_REQUEST;
$aFilter = Array();
//----------------------------------------------------------> [/CONFIG]



switch($op) {
	case 'get-tot-pratiche' :
		$lbl = (isset($_REQUEST['lbl'])   && $_REQUEST['lbl']   != '' && $_REQUEST['lbl'] != '')   ? $_REQUEST['lbl'] : '';
		$out = DB__Get_CountIspettorati($lbl);
		if($out==true) $aR  = Array('op'=>'1', 'msg'=>$out, 'random'=>get_RandomString());
		else           $aR  = Array('op'=>'0', 'msg'=>'Ko :: '.$out, 'random'=>get_RandomString());
		$json = json_encode($aR);
		die($json);
	break;
	case 'del' :
		$out = DB__Del_Element($id);
		if($out==true) $aR  = Array('op'=>'1', 'msg'=>'Ok', 'random'=>get_RandomString());
		else           $aR  = Array('op'=>'0', 'msg'=>'Ko :: '.$out, 'random'=>get_RandomString());
		$json = json_encode($aR);
		die($json);
	break;
	
	case 'aj-udel' :
		$out = DB__Del_Element($id);
		$aR  = Array('op'=>'1', 'msg'=>'Ok', 'random'=>get_RandomString());
		$json = json_encode($aR);
		die($json);
	break;
	case 'aj-ustatus' :
		$aP['u_id']     = $id;
		$aP['u_public'] = (isset($_REQUEST['u_public'])  && $_REQUEST['u_public']  != '') ? (int)$_REQUEST['u_public'] : 0;
		$out = DB__SetStatusElement($aP);
		$aR  = Array('op'=>'1', 'msg'=>'Ok', 'random'=>get_RandomString());
		$json = json_encode($aR);
		die($json);
	break;
	case 'op-details' :
		$aEl= DB__Get_Element($id);
		$HTML = HTML__ModalDetails('Dettagli della compagnia', $aEl);
		echo $HTML;
	break;
	case 'op-search' :
		$HTML = HTML__ModalSearch('Cerca la compagnia', $aP);
		echo $HTML;
	break;
	case 'op-list-new' :
		$aFilter['op_page']   = (isset($_REQUEST['op_page'])   ? $_REQUEST['op_page']         : 1);
		$aFilter['op_order']  = (isset($_REQUEST['op_order'])  ? $_REQUEST['op_order']        : ' u_id DESC ');
		$aFilter['u_country'] = (isset($_REQUEST['u_country']) ? trim($_REQUEST['u_country']) : '');
		$aFilter['u_email']   = (isset($_REQUEST['u_email'])   ? trim($_REQUEST['u_email'])   : '');
		$aFilter['u_surname'] = (isset($_REQUEST['u_surname']) ? trim($_REQUEST['u_surname']) : '');
		$aFilter['u_video']    = (isset($_REQUEST['u_video'])    ? trim($_REQUEST['u_video'])    : '');
		$aFilter['u_video_yt'] = (isset($_REQUEST['u_video_yt']) ? trim($_REQUEST['u_video_yt']) : '');
		$aFilter['u_public'] = '0';

		$totPages  = DB__Get_Count_Elements($aFilter);
		$aElements = DB__Get_Elements($aFilter);
		$HTML = HTML__List($aFilter, $aElements, $totPages);
		echo $HTML;
	break;
	case 'op-list-public' :
		$aFilter['op_page']   = (isset($_REQUEST['op_page'])   ? $_REQUEST['op_page']         : 1);
		$aFilter['op_order']  = (isset($_REQUEST['op_order'])  ? $_REQUEST['op_order']        : ' u_id DESC ');
		$aFilter['u_country'] = (isset($_REQUEST['u_country']) ? trim($_REQUEST['u_country']) : '');
		$aFilter['u_email']   = (isset($_REQUEST['u_email'])   ? trim($_REQUEST['u_email'])   : '');
		$aFilter['u_surname'] = (isset($_REQUEST['u_surname']) ? trim($_REQUEST['u_surname']) : '');
		$aFilter['u_video']    = (isset($_REQUEST['u_video'])    ? trim($_REQUEST['u_video'])    : '');
		$aFilter['u_video_yt'] = (isset($_REQUEST['u_video_yt']) ? trim($_REQUEST['u_video_yt']) : '');
		$aFilter['u_public'] = '1';

		$totPages  = DB__Get_Count_Elements($aFilter);
		$aElements = DB__Get_Elements($aFilter);
		$HTML = HTML__List($aFilter, $aElements, $totPages);
		echo $HTML;
	break;
	default :
	case 'op-list' :
		$aFilter['op_page']    = (isset($_REQUEST['op_page'])    ? $_REQUEST['op_page']         : 1);
		$aFilter['op_order']   = (isset($_REQUEST['op_order'])   ? $_REQUEST['op_order']        : ' agenzia_id DESC ');
		$aFilter['agenzia_nome']  = (isset($_REQUEST['agenzia_nome'])  ? trim($_REQUEST['agenzia_nome']) : '');
	
		
		$aCount    = DB__Get_Count_Elements($aFilter);
		$aElements = DB__Get_Elements($aFilter);
		
		$HTML      = GHTML__Get_ModuleHeader('Gestione', 'Compagnie');
		$HTML     .= HTML__List($aFilter, $aElements, $aCount);
		echo $HTML;
	break;
	case 'op-new'  :
	case 'op-edit' :
		$HTML = '';
	
		if ($id==0 && $op=='op-new') {
			$aEl   = DB__Get_Element(0);
			$HTML .= GHTML__Get_ModuleHeader('Gestione Compagnie', 'Nuova');
			$HTML .= HTML__Form($aEl);
		} else {
			$HTML .= GHTML__Get_ModuleHeader('Gestione Compagnie', 'Modifica');
			if($id!=0) {
				$aP['agenzia_id'] = $id;
				$dbId = DB__Set_Element($aP);
				if ($dbId) $HTML .= GHTML__Get_StatusMessage('success', 'Modifica compagnia', 'Modifica completata con successo');
			} else {
				$id = DB__Add_Element($aP);
				if ($id) $HTML .= GHTML__Get_StatusMessage('success', 'Nuova compagnia', 'Inserimento completato con successo.');
			}
			$aEl   = DB__Get_Element($id);
			$HTML .= HTML__Form($aEl);
			$HTML .= HTML__FormIspettorato($aEl);
		}
		
		echo $HTML;
		
		/*
		$MSG  = '';
		// aggiunta / modifica
		if (isset($aP['btn'])) {
			if(!isset($id) || $id==0) {
				// inserisco il record
				$id = DB__Add($aP);
				if($id) $MSG .= 'News aggiunta correttamente.<br>';
				// se ho selezionaro un file... imposto il nome e lo carico
				if(isset($_FILES['n_image']['name'])) {
					$fNameEst  = get_FileExtension($_FILES['n_image']['name']);
					$fNameNew  = date('Ymd-His').'_'.$id.'.'.$fNameEst;
					$fNameFrom = $_FILES['n_image']['tmp_name'];
					$fnameTo   = $LOCAL_CONF['local_upload'].$fNameNew;
					if(move_uploaded_file($fNameFrom, $fnameTo)) {
						DB__Set_Object_Field(Array('n_image' => $fNameNew, 'n_id' => $id));
						$MSG .= 'File caricato correttamente.<br>';
					} else {
						$MSG .= 'Problemi nel caricamento del file ['.$_FILES['n_image']['error'].'].<br>';
					}
				}
			} else {
				// modifico il record
				DB__Set($aP);
				$MSG .= 'News modificata correttamente.<br>';
				// se ho selezionato un file... imposto il nome e lo carico
				if(isset($_FILES['n_image']['name']) && $_FILES['n_image']['name']!='') {
					$fNameEst  = get_FileExtension($_FILES['n_image']['name']);
					$fNameNew  = date('Ymd-His').'_'.$id.'.'.$fNameEst;
					$fNameFrom = $_FILES['n_image']['tmp_name'];
					$fnameTo   = $LOCAL_CONF['local_upload'].$fNameNew;
					if(move_uploaded_file($fNameFrom, $fnameTo)) {
						DB__Set_Object_Field(Array('n_image' => $fNameNew, 'n_id' => $aP['n_id']));
						$MSG .= 'File caricato correttamente.<br>';
					} else {
						$MSG .= 'Problemi nel caricamento del file ['.$_FILES['n_image']['error'].'].<br>';
					}
					// se ho cancellato il file... lo elimino
				} else if(isset($aP['n_image']) && $aP['n_image']=='') {
					$aObj = DB__Get_Object($aP['n_id']);
					if(file_exists($LOCAL_CONF['local_upload'].$aObj['n_image'])) {
						unlink($LOCAL_CONF['local_upload'].$aObj['n_image']);
						$MSG .= 'File eliminato.<br>';
					}
					DB__Set_Object_Field(Array('n_image' => '', 'n_id' => $aP['n_id']));
				}
			}
		}

		$HTML  = GHTML__Get_ModuleHeader('Gestione News', 'Aggiungi / Modifica');
		if ($MSG!='') $HTML .= GHTML__Get_StatusMessage('success', 'News', $MSG);
		$HTML .= HTML__PageForm($aP);
		echo $HTML;
		*/
	break;
}


?>