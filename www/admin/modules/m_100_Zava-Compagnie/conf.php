<?php /*
  Version:     v7 2016.11.26
  Module:      Zava16.Compagnie
  Author:      SLivio
*/



//--

$LOCAL_CONF = Array(
    'module_op' => Array(
        Array('id'    => '1',
            'label' => 'Elenco Compagnie ',
            'op'    => 'op-list'),
        Array('id'  => '2',
            'label' => 'Nuova Compagnia',
            'op'    => 'op-new')
    ),
    'module_label' => 'Gestisci Compagnie',
    'key' => 'm_100_Zava-Compagnie',
    'menu' => Array(
        'visible' => true
    ),
    'local_js' => 'this.lib.js?'.get_RandomString()
);





?>