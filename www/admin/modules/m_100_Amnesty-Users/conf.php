<?php /*
  Version:     v7 2016.02.22
  Module:      AmnestyUsers
  Author:      SLivio
*/



//--
$LOCAL_CONF['module_op']    = Array(
	Array('id'    => '1',
		'label' => 'Manage all User/Video ',
		'op'    => 'op-list'),
	Array('id'    => '2',
		'label' => 'Manage new User/Video ',
		'op'    => 'op-list-new'),
	Array('id'    => '3',
		'label' => 'Manage approved User/Video ',
		'op'    => 'op-list-public')
	);
$LOCAL_CONF['module_label']  = 'Gestisci utenti/video';
$LOCAL_CONF['key']           = 'm_100_Amnesty-Users';
$LOCAL_CONF['menu']          = Array('visible' => true);

$LOCAL_CONF['remote_upload'] = APPLICATION_URL.'../upload/news/';
$LOCAL_CONF['local_upload']  = '../upload/video/';
$LOCAL_CONF['path_upload']   = 'video/';

//$LOCAL_CONF['local_widget'][0] = Array('op'=>'video-list', 'label'=>'Videos', 'class'=>'col-sm-6');



?>