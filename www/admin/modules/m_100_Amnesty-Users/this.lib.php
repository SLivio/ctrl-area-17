<?php /*
  Version:     v7 2016.02.22
  Module:      AmnestyUsers
  Author:      SLivio
*/




//----------------------------------------------------------> [HTML]

// LastUpdate 2016.02.26
function DB__Get_Count_Elements($aF=Array()) {
	GLOBAL $CONF;
	
	$qAdd  = '';
	$qAdd .= ($aF['u_email']!='')   ? ' AND u_email like "%'.$aF['u_email'].'%"'     : '';
	$qAdd .= ($aF['u_surname']!='') ? ' AND u_surname like "%'.$aF['u_surname'].'%"' : '';
	$qAdd .= ($aF['u_country']!='') ? ' AND u_country like "%'.$aF['u_country'].'%"' : '';
	$qAdd .= ($aF['u_video']!='')    ? ' AND u_video like "%'.$aF['u_video'].'%"'       : '';
	$qAdd .= ($aF['u_video_yt']!='') ? ' AND u_video_yt like "%'.$aF['u_video_yt'].'%"' : '';
	$qAdd .= ($aF['u_public']!='')  ? ' AND u_public = "'.$aF['u_public'].'"'        : '';
	
	$q      = 'SELECT count(u_id) FROM '.DB_PREFIX.'users WHERE u_id<>0 '.$qAdd;
	$CON    = GDB__Get_CoreSession();
	$r = mysqli_query($CON, $q) or LOG__Error("countRecord[]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		$tot     = mysqli_result($r, mysqli_affected_rows($CON), 0);
		$pageTot = round($tot/$CONF['def_page'], 0, PHP_ROUND_HALF_UP);
	} else {
		$pageTot = 1;
	}
	return $pageTot;
}


// LastUpdate 2016.02.22
function DB__Get_Elements($aF=Array()) {
	GLOBAL $CONF;

	$qLimitStart = ($aF['op_page']<=1) ? 0 : (($aF['op_page']-1)*$CONF['def_page']);
	$qLimitEnd   = (($aF['op_page']-1)*$CONF['def_page'])+$CONF['def_page'];

	$qAdd  = '';
	$qAdd .= ($aF['u_email']!='')   ? ' AND u_email like "%'.$aF['u_email'].'%"'     : '';
	$qAdd .= ($aF['u_surname']!='') ? ' AND u_surname like "%'.$aF['u_surname'].'%"' : '';
	$qAdd .= ($aF['u_country']!='') ? ' AND u_country like "%'.$aF['u_country'].'%"' : '';
	$qAdd .= ($aF['u_video']!='')    ? ' AND u_video like "%'.$aF['u_video'].'%"'       : '';
	$qAdd .= ($aF['u_video_yt']!='') ? ' AND u_video_yt like "%'.$aF['u_video_yt'].'%"' : '';
	$qAdd .= ($aF['u_public']!='')  ? ' AND u_public = "'.$aF['u_public'].'"'        : '';
	
	$qOrder = 'ORDER BY u_id DESC';
	$qLimit = 'LIMIT '.$qLimitStart.','.$qLimitEnd;
	$q      = 'SELECT * FROM '.DB_PREFIX.'users WHERE u_id<>0 '.$qAdd.' '.$qOrder.' '.$qLimit;
	
	$CON       = GDB__Get_CoreSession();
	$aElements = DB__QueryN($CON, $q);

	return $aElements;
}

// LastUpdate 2016.02.23
function DB__Get_LogElements($aF=Array()) {
	GLOBAL $CONF;

	$qLimitStart = ($aF['op_page']<=1) ? 0 : ($qLimitStart*$CONF['def_page']);
	$qLimitEnd   = ($qLimitStart*$CONF['def_page'])+$CONF['def_page'];

	$qAdd  = '';
	$qAdd .= (isset($aF['u_country']) && $aF['u_country']!='') ? 'AND u_country like "%'.$aF['u_country'].'%"' : '';
	$qAdd .= (isset($aF['u_public'])  && $aF['u_public']!='')  ? 'AND u_public = "'.$aF['u_public'].'"' : '';
	
	$qOrder = 'ORDER BY ctl_id DESC';
	$qLimit = 'LIMIT '.$qLimitStart.','.$qLimitEnd;
	$q      = 'SELECT * FROM '.DB_PREFIX.'cameratag_log WHERE ctl_id<>0 '.$qAdd.' '.$qOrder.' '.$qLimit;
	$CON       = GDB__Get_CoreSession();
	$aElements = DB__QueryN($CON, $q);

	return $aElements;
}


// LastUpdate 2016.02.22
function DB__Get_Element($id=0) {
	$CON = GDB__Get_CoreSession();
	$aR = Array();
	$aR = DB__Get_infoRecord($CON, DB_PREFIX.'users', ' WHERE u_id="'.(int)$id.'" ', 0);
	return $aR;
}


// LastUpdate 2016.02.22
function GDB__Get_Countries() {
	GLOBAL $CONF;
	$q      = 'SELECT * FROM '.DB_PREFIX.'core_geo_country';
	$CON       = GDB__Get_CoreSession();
	$aElements = DB__QueryN($CON, $q);
	return $aElements;
}


// LastUpdate 2016.02.22
function GDB__Get_Country($id=0) {
	$CON = GDB__Get_CoreSession();
	$aR = Array();
	$aR = DB__Query1($CON, 'SELECT * FROM '.DB_PREFIX.'core_geo_country WHERE country_id="'.(int)$id.'"');
	return $aR;
}

// LastUpdate 2016.02.22
function DB__SetStatusElement($aP=Array()) {
	$CON = GDB__Get_CoreSession();
	//
	$OUT                 = false;
	$aP['u_id']          = (isset($aP['u_id'])       && $aP['u_id'] != '')        ? $aP['u_id']            : 0;
	$aP['u_public']      = (isset($aP['u_public'])   && $aP['u_public'] != '')    ? (int)$aP['u_public']   : 0;

	// id utente mancante
	if ($aP['u_id']==0) return false;

	$qAdd = ' `u_public`="'.$aP['u_public'].'" ';

	//
	$q = 'UPDATE `'.DB_PREFIX.'users` SET '.$qAdd.', `u_info_update_date` = NOW(), `u_info_update_ip`  = "'.$_SERVER["REMOTE_ADDR"]  .'"
	WHERE u_id="'.$aP['u_id'].'"';
	$r = mysqli_query($CON, $q) or LOG__Error("DB__Set", mysqli_error($CON)."\n".$q);

	if (mysqli_error($CON)=='') $OUT = $aP['u_id'];
	else                        $OUT = false;
	//
	return $OUT;
}



// LastUpdate 2016.02.22
function DB__SetElement($aP=Array()) {
	$CON = GDB__Get_CoreSession();
	//
	$OUT                   = false;
	$aP['u_id']            = (isset($aP['u_id'])            && $aP['u_id'] != '')                  ? $aP['u_id']                  : 0;
	$aP['u_name']          = (isset($aP['u_name'])          && trim($aP['u_name']) != '')          ? trim($aP['u_name'])          : '';
	$aP['u_surname']       = (isset($aP['u_surname'])       && trim($aP['u_surname']) != '')       ? trim($aP['u_surname'])       : '';
	$aP['u_borndate']      = (isset($aP['u_borndate'])      && trim($aP['u_borndate']) != '')      ? trim($aP['u_borndate'])      : '';
	$aP['u_country']       = (isset($aP['u_country'])       && trim($aP['u_country']) != '')       ? trim($aP['u_country'])       : '';
	$aP['u_video']         = (isset($aP['u_video'])         && trim($aP['u_video']) != '')         ? trim($aP['u_video'])         : '';
	$aP['u_video_yt']      = (isset($aP['u_video_yt'])      && trim($aP['u_video_yt']) != '')      ? trim($aP['u_video_yt'])      : '';
	$aP['u_video_preview'] = (isset($aP['u_video_preview']) && trim($aP['u_video_preview']) != '') ? trim($aP['u_video_preview']) : '';
	$aP['u_weight']        = (isset($aP['u_weight'])        && trim($aP['u_weight']) != '')        ? trim($aP['u_weight'])        : 0;
	$aP['u_policy_1']      = (isset($aP['u_policy_1'])      && $aP['u_policy_1'] != '')            ? 1                            : 0;
	$aP['u_policy_2']      = (isset($aP['u_policy_2'])      && $aP['u_policy_2'] != '')            ? 1                            : 0;
	$aP['u_public']        = (isset($aP['u_public'])        && $aP['u_public'] != '')              ? (int)$aP['u_public']         : 0;

	// id utente mancante
	if ($aP['u_id']==0) return false;

	$addQ = Array();
	if ($aP['u_name'] != '')          $addQ[] = ' `u_name`="'.MyEscape($aP['u_name']).'" ';
	if ($aP['u_surname'] != '')       $addQ[] = ' `u_surname`="'.MyEscape($aP['u_surname']).'" ';
	if ($aP['u_borndate'] != '')      $addQ[] = ' `u_borndate`="'.MyEscape($aP['u_borndate']).'" ';
	if ($aP['u_country'] != '')       $addQ[] = ' `u_country`="'.$aP['u_country'].'" ';
	if ($aP['u_video'] != '')         $addQ[] = ' `u_video`="'.$aP['u_video'].'" ';
	if ($aP['u_video_yt'] != '')      $addQ[] = ' `u_video_yt`="'.$aP['u_video_yt'].'" ';
	if ($aP['u_video_preview'] != '') $addQ[] = ' `u_video_preview`="'.$aP['u_video_preview'].'" ';
	if ($aP['u_weight'] != '')        $addQ[] = ' `u_weight`="'.$aP['u_weight'].'" ';
	$addQ[] = ' `u_policy_1`="'.$aP['u_policy_1'].'" ';
	$addQ[] = ' `u_policy_2`="'.$aP['u_policy_2'].'" ';
	$addQ[] = ' `u_public`="'.$aP['u_public'].'" ';

	//
	$q = 'UPDATE `'.DB_PREFIX.'users` SET '.implode(', ', $addQ).',
	`u_info_update_date`   = NOW(),
	`u_info_update_ip`     = "'.$_SERVER["REMOTE_ADDR"]  .'"
	WHERE
	u_id="'.$aP['u_id'].'"';

	$r = mysqli_query($CON, $q) or LOG__Error("DB__SetElement", mysqli_error($CON)."\n".$q);
	//
	if (mysqli_error($CON)=='') $OUT = $aP['u_id'];
	else                        $OUT = false;
	//
	return $OUT;
}


// LastUpdate v7 2014.11.20
function DB__Del_Element($id) {
	$CON = GDB__Get_CoreSession();
	$q = 'DELETE FROM `'.DB_PREFIX.'users` WHERE u_id = "'.(int)$id.'"';
	$r = mysqli_query($CON, $q) or LOG__Error("DB__Del_Element[]", mysqli_error($CON)."\n".$q);
}

// LastUpdate 6.2014.04.16
function HTML__List($aFilter=Array(), $aElements=Array(), $totPages=1) {
	GLOBAL $m, $CONF, $op;

	$HTML = GHTML__Get_ModuleHeader('Gestione', 'Ricerca / Elenco');
	
	$HTMLP = '<ul class="pagination pagination-sm remove-margin">';
	$aTmpFilter = $aFilter; unset($aTmpFilter['op_page']); $query = http_build_query(array_filter($aTmpFilter));
	for($i=1;$i<=$totPages;$i++) {

		$class =  (isset($aFilter['op_page']) && $aFilter['op_page']==$i) ? ' class="active"' : '';
		$link  =  (isset($aFilter['op_page']) && $aFilter['op_page']==$i) ? 'javascript:;'    : '?m='.$m.'&op='.$op.'&op_page='.$i.'&'.$query;
		//$link  = '?m='.$m.'&op='.$op.'&op_page='.$i.'&'.$query;
		$HTMLP .= '<li '.$class.'><a href="'.$link.'">'.$i.'</a></li>';
	}
	$HTMLP .= '</ul>';
	
	
	$HTML .= '<script src="//cameratag.com/api/v7/js/cameratag.js" type="text/javascript"></script>
	<script>
	function ajStatus(uid, status) {
		dataSend="m='.$m.'&app=true&op=aj-ustatus&id="+uid+"&u_public="+status;
		$.ajax({
			method: "POST",
			url: "index.php",
			data: dataSend,
			dataType:"json"
		}).done(function( msg ) {
			if(msg.op=="1") {
				var k = "#tr_"+uid;
				var c = (status==0) ? "text-classic" : "";
				    c = (status==1) ? "text-success" : c;
					c = (status==2) ? "text-danger"  : c;
				var counter = $(k).length; 
				$(k).attr("class", c);
			}
		});
	}
	function ajDel(uid) {
		if(confirm("Are you sure?")) {
			dataSend="m='.$m.'&app=true&op=aj-udel&id="+uid;
			$.ajax({
				method: "POST",
				url: "index.php",
				data: dataSend,
				dataType:"json"
			}).done(function( msg ) {			
				if(msg.op==1) {
					var k = "#tr_"+uid;
					var c = $(k).children().length;
					$(k).addClass("danger");
					$(k).html("<td colspan=\'"+c+"\' class=\'text-danger text-center\'>"+uid+" deleted</td>")
				}
			});
		}
	}
	</script>
	<div class="block">
		<div class="block-title">
			<div class="block-options pull-right" data-toggle="tooltip" data-original-title="Search">
				<a class="btn btn-alt btn-sm btn-default" title="" href="?app=true&m='.$m.'&op=op-search"  data-toggle="modal" data-target="#modal"><i class="hi hi-search"></i></a>
			</div>
			<h2>List</h2>
		</div>

		<!-- Modal Search -->
		<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog"><div class="modal-content"></div><!-- /.modal-content --></div>
		</div>

		<div class="table-responsive">
			<table class="table table-striped table-vcenter table-bordered dataTable table-hover">
			<tr><td colspan="14"><div class="dataTables_paginate paging_bootstrap">'.$HTMLP.'</div></td></tr>
			<thead>
			<tr class="tr_head">
				<th>ID</th>
				<th>Name</th>
				<th>Surname</th>
				<th>Born Data</th>
				<th>Email</th>
				<th>Date</th>
				<th>Type</th>
				<th>Public</th>
				<th colspan="2" class="w90 text-center">Video</th>
				<th colspan="3" class="w90 text-center">Action</th>
			</tr>
			</thead>
			<tbody>';

	foreach($aElements as $el) {
		$tr_id  = 'tr_'.$el['u_id'];
		$a_view = '<a href="?app=true&m='.$m.'&id='.$el['u_id'].'&op=op-details" title="Details" data-toggle="modal" data-target="#modal">'.GHTML__Get_Ico('zoom+', 'Details').'</a>';
		$a_edit = '<a href="?m='.$m.'&op=op-form&id='.$el['u_id'].'" title="Edit"  >'.GHTML__Get_Ico('edit', 'Edit').'</a>';
		$a_del  = '<a href="javascript:ajDel('.$el['u_id'].')" title="Delete">'.GHTML__Get_Ico('delete', 'Delete').'</a>';
		
		$a_status = '
				&nbsp;<a href="javascript:ajStatus('.$el['u_id'].', 0)" title="Hide"   class="text-classic">'. GHTML__Get_Ico('private', 'Stand by').'</a>
				&nbsp;<a href="javascript:ajStatus('.$el['u_id'].', 1)" title="Public" class="text-success">'. GHTML__Get_Ico('public',  'Approve this item').'</a>
				&nbsp;<a href="javascript:ajStatus('.$el['u_id'].', 2)" title="Ban"    class="text-danger">'.  GHTML__Get_Ico('ban',     'Ban this item').'</a>';
		$public   = ($el['u_video_preview']!='') ? '<i data-toggle="tooltip" title="PUBLIC" class="gi gi-ok_2"></i>' : '';
		

		$a_video    = ($el['u_video']!='')    ? '<i data-toggle="tooltip" title="CameraTag" class="gi gi-ok_2"></i>' : '<i data-toggle="tooltip" title="CameraTag"  class="gi gi-remove_2"></i>';
		$a_video_yt = ($el['u_video_yt']!='') ? '<i data-toggle="tooltip" title="YouTube" class="gi gi-ok_2"></i>'   : '<i data-toggle="tooltip" title="YouTube" class="gi gi-remove_2"></i>';

		//<span class="text-success">Success Text</span>
		$class = ($el['u_public']==0) ? 'class="text-classic"' : ''; 
		$class = ($el['u_public']==1) ? 'class="text-success"' : $class;
		$class = ($el['u_public']==2) ? 'class="text-danger"'  : $class;
		
		$HTML .= NL.'<tr id="'.$tr_id.'" '.$class.'>
					<td>'.$el['u_id'].'</td>
					<td>'.$el['u_name'].'</td>
					<td>'.$el['u_surname'].'</td>
					<td>'.$el['u_country'].'<br>'.$el['u_borndate'].'</td>
					<td>'.$el['u_email'].'</td>
					<td>'.$el['u_info_date'].'</td>
					<td class="text-center">'.$el['u_class'].'</td>
					<td class="text-center">'.$public.'</td>
					<td class="text-center">'.$a_video.'</td>
					<td class="text-center">'.$a_video_yt.'</td>
					<td class="text-center">'.$a_view.'</td>
					<td class="text-center">'.$a_edit.'</td>
					<td class="text-center">'.$a_status.'</td>
					<td class="text-center">'.$a_del.'</td>
				</tr>';
	}
	$HTML .= '</tbody>
			<tfoot>
				<td colspan="14"><div class="dataTables_paginate paging_bootstrap">'.$HTMLP.'</div></td>
			</tfoot>
			</table>
			<br>
			</div>
		</div>';



	//
	// Visualizzazione della paginazione
	/*
	$HTML_FIND = '';
	if ($PAGE_TOT > 1) {
	$HTML_FIND = '<form name="f_page"><select name="page_number" onChange="location.href=this.value">';
	for($i=0; $i< $PAGE_TOT; $i++) {
	$class = ($i==$p) ? 'selected="selected"' : '';
	$HTML_FIND .= NL.'<option '.$class.' value="?'.http_build_query($aP).'&p='.$i.'">'.(int)($i+1).'</option> ';
	}
	$HTML_FIND .= '</select></form>';
	}
	//
	$HTML_TITLE  = GHTML__Get_ModuleHeader('Gestione News', 'Ricerca / Elenco');
	$HTML_SEARCH = HTML__Search($aP);

	$HTML        = $HTML_TITLE.'
	<div class="block full">
	<div class="block-title">
	<div class="block-options pull-right" data-toggle="tooltip" data-original-title="Search">
	<a class="btn btn-alt btn-sm btn-default" title="" href="javascript:void(0)"  data-toggle="modal" data-target="#search">
	<i class="hi hi-search"></i>
	</a>
	</div>
	<h2>News List</h2>
	</div>

	<!-- Modal Search -->
	<div class="modal fade" id="search" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="myModalLabel">Cerca</h4>
	</div>
	<div class="modal-body">
	'.$HTML_SEARCH.'
	<hr>
	</div>
	</div>
	</div>
	</div>


	<script type="text/javascript">
	var aj_path   = "./'.$CONF['path_modules'].'/'.$aModule['module_dir'].'";
	var aj_url    = aj_path+"/"+"ajax.php";
	var aj_return = "";
	function AJ_Del_Obj(uid) {
	var key = "#tr_"+uid;
	var col = $(key).children("td").length;
	if (confirm("Delete this item?")) {
	$.getJSON(aj_url+"?op=obj_del&id="+uid,"",
	function(data){
	if(data != undefined && data.op==1) {
	$(key).html("<td colspan=\'"+col+"\' class=\'danger\'>"+data.msg+"</td>");
	} else {
	$(key).addClass("error");
	alert("An error has occurred. Please try plater.");
	}
	});
	}
	}
	</script>

	<div class="table-responsive">
	<div id="example-datatable_wrapper" class="dataTables_wrapper" role="grid">
	<table class="table table-striped table-vcenter table-bordered dataTable table-hover">
	<thead>
	<tr class="tr_head">
	<th class="w40">ID</th>
	<th class="text-center">Data
	<a href="?'.http_build_query($aP).'&order=date_asc">'.GHTML__Get_Ico('arrow-up').'</a>
	<a href="?'.http_build_query($aP).'&order=date_desc">'.GHTML__Get_Ico('arrow-down').'</a></th>
	<th>Titolo</th>
	<th>Descrizione</th>
	<th>Data di online</th>
	<th class="text-center">Img</th>
	<th class="text-center">Status</th>
	<th colspan="2" class="w90 text-center">Action</th>
	</tr>
	</thead>
	<tbody>';

	$class = 'class="tr_pari"';
	foreach($aList as $el) {
	$status = ($el['n_enable']==1)     ? GHTML__Get_Ico('public')  : GHTML__Get_Ico('private');
	//
	$tr_id  = 'tr_'.$el['n_id'];
	$a_img  = '';
	if ($el['n_image']!='') {
	$a_img = '<a href="'.$LOCAL_CONF['local_upload'].$el['n_image'].'" data-toggle="lightbox-image" title="View image"><img src="'.$LOCAL_CONF['local_upload'].$el['n_image'].'" class="img-thumbnail" width="90" alt="Details"></a>';
	}
	$a_view = '<a href="?m='.$m.'&op=obj_details&id='.$el['n_id'].'&app=true" class="fancybox.iframe fancyMini" title="View details">'.GHTML__Get_Ico('info').'</a>';
	$a_edit = '<a href="?m='.$m.'&op=obj_form&id='.$el['n_id'].'" title="Edit this item">'.GHTML__Get_Ico('edit', 'Edit').'</a>';
	$a_del  = '<a href="javascript:AJ_Del_Obj('.$el['n_id'].')"  title="Delete this item">'.GHTML__Get_Ico('delete', 'Delete').'</a>';
	//
	$HTML .= NL.'<tr id="'.$tr_id.'">
	<td>'.$el['n_id'].'</td>
	<td class="text-center">'.$el['n_date'].'</td>
	<td>'.$el['n_name'].'</td>
	<td>'.$el['n_description'].'</td>
	<td>'.$el['n_date_from'].' <br> '.$el['n_date_to'].'</td>
	<td class="text-center">'.$a_img.'</td>
	<td class="text-center">'.$status.'</td>
	<td class="text-center">'.$a_edit.'</td>
	<td class="text-center">'.$a_del.'</td>
	</tr>';
	}
	if ($HTML_FIND!='') { $HTML .= NL.'<tr class="tr_find"><td colspan="9">'.$HTML_FIND.'</td></tr>'; }
	$HTML .= '</tbody>
	</table>
	</div>
	</div>
	</div>';
	//
	*
	*/
	return $HTML;
}



// LastUpdate 2016.02.23
function HTML__LogList($aFilter=Array(), $aElements=Array()) {
	GLOBAL $m;

	$HTML = GHTML__Get_ModuleHeader('Gestione', 'Ricerca / Elenco');
	$HTML .= '
	<div class="block">
		<div class="block-title">
			<h2>List</h2>
		</div>
		<div class="table-responsive">
			<table class="table table-striped table-vcenter table-bordered dataTable table-hover">
			<thead>
			<tr class="tr_head">
				<th>ID</th>
				<th>Data</th>
				<th>Date</th>
			</tr>
			</thead>
			<tbody>';

	foreach($aElements as $el) {
		$tr_id  = 'tr_'.$el['ctl_id'];
		$HTML .= NL.'<tr id="'.$tr_id.'"><td>'.$el['ctl_id'].'</td><td>'.$el['ctl_json'].'</td><td>'.$el['ctl_date'].'</td></tr>';
	}
	$HTML .= '</tbody>
			<tfoot>
				<td colspan="3"></td>
			</tfoot>
			</table>
			<br>
			</div>
		</div>';

	return $HTML;
}


// LastUpdate 2016.02.22
function HTML__ModalDetails($title='', $aEl=Array()) {
	GLOBAL $m;
	$HTML ='<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<title>'.$title.'</title>
	<script>
	$("body").on("hidden.bs.modal", ".modal", function () {
	    $(this).removeData("bs.modal");
	});
	</script>
</head>
<body>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">'.$title.'</h4>
	</div><!-- /modal-header -->
	<div class="modal-body">
		<div class="row">
			<ul class="fa-ul">
				<li><i class="fa fa-pencil fa-li"></i>Name:             '.$aEl['u_name'].' </li>
				<li><i class="fa fa-pencil fa-li"></i>Surname:          '.$aEl['u_surname'].' </li>
				<li><i class="fa fa-envelope-o fa-li"></i>Email:        '.$aEl['u_email'].' </li>
				<li><i class="fa fa-globe fa-li"></i>Country:           '.$aEl['u_country'].' </li>
				<li><i class="fa fa-youtube-play fa-li"></i>Video:      '.$aEl['u_video'].' </li>
				<li><i class="fa fa-youtube-play fa-li"></i>Youtube:    '.$aEl['u_video_yt'].' </li>
				<li><i class="fa fa-picture-o fa-li"></i>Image:         '.$aEl['u_video_preview'].' </li>
				<li><i class="fa fa-pencil fa-li"></i>Privacy Policy 1: '.$aEl['u_policy_1'].' </li>
				<li><i class="fa fa-pencil fa-li"></i>Privacy Policy 2: '.$aEl['u_policy_2'].' </li>
				<li><i class="fa fa-pencil fa-li"></i>Public:           '.$aEl['u_public'].' </li>
				<li><i class="fa fa-pencil fa-li"></i>Info IP:          '.$aEl['u_info_ip'].' </li>
				<li><i class="fa fa-pencil fa-li"></i>Info Date:        '.$aEl['u_info_date'].' </li>
			</ul>
		</div>
		<div class="row text-center">
			'.(($aEl['u_video']!='') ? '<iframe src="?m='.$m.'&op=op-player&id='.$aEl['u_id'].'&app=true" width="490" height="280"></iframe>' : '').'
		</div>
	</div><!-- /modal-body -->
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div><!-- /modal-footer -->
</body>
</html>';
	return $HTML;
}


// LastUpdate 2016.02.22
function HTML__ModalSearch($title='', $ar=Array()) {
	GLOBAL $m;
	
	$aCountries = GDB__Get_Countries();
	$HTMLoption = '';
	foreach($aCountries as $el) {
		$selected    = (isset($ar['u_country']) && $el['country_name']==$ar['u_country']) ? 'selected="selected"' : '';
		$HTMLoption .= '<option value="'.$el['country_name'].'" '.$selected.'>'.$el['country_code'].' - '.$el['country_name'].'</option>';
	}
	
	$HTML ='<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<title>'.$title.'</title>
	<script>
	$("body").on("hidden.bs.modal", ".modal", function () {
	    $(this).removeData("bs.modal");
	});
	</script>
	<script src="js/app.js"></script>
</head>
<body>
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">'.$title.'</h4>
	</div><!-- /modal-header -->
	<div class="modal-body">
		<form action="?m='.$m.'" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
		<input type="hidden" name="op"  value="op-list">
		<div class="form-group">
				<label class="col-md-3 control-label" for="u_country">Country</label>
				<div class="col-md-9">
					<select class="select-chosen" id="u_country" name="u_country" data-placeholder="Choose a Country..." style="width: 250px;" class="select-chosen">
                    	<option value="">Choose a Country...</option>
						'.$HTMLoption.'
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="u_surname">Surname</label>
				<div class="col-md-9">
					<input type="text" id="u_surname" name="u_surname" class="form-control" placeholder="Surname">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="u_video">CameraTag Video id</label>
				<div class="col-md-9">
					<input type="text" id="u_video" name="u_video" class="form-control" placeholder="Video id">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="u_video_yt">YouTube Video id</label>
				<div class="col-md-9">
					<input type="text" id="u_video_yt" name="u_video_yt" class="form-control" placeholder="Video id">
				</div>
			</div>
			<!--<div class="form-group">
				<label class="col-md-3 control-label" for="example-textarea-input">DEBUG</label>
                <div class="col-md-9">
                	<textarea rows="6" class="form-control" placeholder="Content..">'.print_r($ar, 1).'</textarea>
                </div>
			</div>-->
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Search</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div><!-- /modal-footer -->
		</form>
	</div><!-- /modal-body -->
</body>
</html>';
	return $HTML;
}



function HTML__Form($aEl=Array()) {
	GLOBAL $m;
	
	$aCountries = GDB__Get_Countries();
	$HTMLoption = '';
	foreach($aCountries as $el) {
		$selected    = ($el['country_name']==$aEl['u_country']) ? 'selected="selected"' : '';
		$HTMLoption .= '<option value="'.$el['country_name'].'" '.$selected.'>'.$el['country_code'].' - '.$el['country_name'].'</option>';
	}
	
	
	
	$HTML = '<div class="block full">
		<div class="block-title">
			<h2>Gestione utente</h2>
		</div>
		<form action="?m='.$m.'" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
		<input type="hidden" name="op" value="op-edit">
		<input type="hidden" name="id" value="'.$aEl['u_id'].'">
			<div class="form-group">
            	<label class="col-md-3 control-label">ID</label>
                	<div class="col-md-9">
                    	<p class="form-control-static">'.$aEl['u_id'].'</p>
					</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="u_name">Name</label>
				<div class="col-md-9"><input type="text" class="form-control" id="u_name" name="u_name" placeholder="Name" value="'.$aEl['u_name'].'"></div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="u_surname">Surname</label>
				<div class="col-md-9"><input type="text" class="form-control" id="u_surname" name="u_surname" placeholder="Surname" value="'.$aEl['u_surname'].'"></div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="u_country">Country</label>
				<div class="col-md-9">
					<select class="select-chosen" id="u_country" name="u_country" data-placeholder="Choose a Country..." style="width: 250px;">
                    	<option value="">Choose a Country...</option>
						'.$HTMLoption.'
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="u_borndate">Born date</label>
				<div class="col-md-9"><input type="text" class="form-control id="u_borndate" name="u_borndate"" placeholder="YYYY-mm-dd" value="'.$aEl['u_borndate'].'"></div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="u_email">Email</label>
				<div class="col-md-9"><input type="text" class="form-control id="u_email" name="u_email"" placeholder="Email" value="'.$aEl['u_email'].'"></div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="u_video">CameraTAG Video</label>
				<div class="col-md-9"><input type="text" class="form-control" id="u_video" name="u_video" placeholder="Video" value="'.$aEl['u_video'].'"></div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="u_video_yt">YouTube Video</label>
				<div class="col-md-9"><input type="text" class="form-control" id="u_video_yt" name="u_video_yt" placeholder="Video YouTube" value="'.$aEl['u_video_yt'].'"></div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="u_video_preview">Video preview</label>
				<div class="col-md-9"><input type="text" class="form-control" id="u_video_preview" name="u_video_preview" placeholder="Image preview" value="'.$aEl['u_video_preview'].'"></div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Privacy Policy 1</label>
                <div class="col-md-9">
                	<label class="checkbox-inline" for="u_policy_1">
                	<input type="checkbox" id="u_policy_1" name="u_policy_1" value="1" '.(($aEl['u_policy_1']==1) ? 'checked="checked"' : '').'> 
					</label>
				</div>
			</div>
            <div class="form-group">
				<label class="col-md-3 control-label">Privacy Policy 2</label>
                <div class="col-md-9">
                	<label class="checkbox-inline" for="u_policy_2">
                	<input type="checkbox" id="u_policy_2" name="u_policy_2" value="1" '.(($aEl['u_policy_2']==1) ? 'checked="checked"' : '').'> 
					</label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Status</label>
                <div class="col-md-9">
                	<div class="radio">
                		<label for="u_public_0">
                			<input type="radio" id="u_public_0" name="u_public" value="0" '.(($aEl['u_public']==0) ? 'checked="checked"' : '').'> Stand by
                		</label>
                	</div>
					<div class="radio">
						<label for="u_public_1">
							<input type="radio" id="u_public_1" name="u_public" value="1" '.(($aEl['u_public']==1) ? 'checked="checked"' : '').'> Approved
						</label>
					</div>
					<div class="radio">
						<label for="u_public_2">
							<input type="radio" id="u_public_2" name="u_public" value="2" '.(($aEl['u_public']==2) ? 'checked="checked"' : '').'> banned
						</label>
					</div>
				</div>
			</div>
			
			<div class="form-group form-actions">
				<div class="col-md-9 col-md-offset-3">
					<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
					<button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
				</div>
			</div>
		</form>
	</div>';
	return $HTML;
}




?>