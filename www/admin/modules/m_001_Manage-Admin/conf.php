<?php /*
  Version:     v7 2016.12
  Module:      Ctrl-Area :: Core :: Manage Admin
  Author:      SLivio
*/



//--

$LOCAL_CONF = Array(
    'module_op' => Array(
        Array('id'    => '1',
            'label' => 'Manage Groups ',
            'op'    => 'g_list'),
        Array('id'    => '2',
            'label' => 'Manage Admin ',
            'op'    => 'ad_list'),
        Array('id'    => '3',
            'label' => 'New Admin ',
            'op'    => 'ad_form'),
        Array('id'    => '4',
            'label' => 'Backup ',
            'op'    => 'bak')
    ),
    'module_label' => 'Core :: Manage Admin',
    'key' => 'core_ManageAdmin__akdjcf2o8347fhedfiuwehiltuerhfaf',
    'menu' => Array(
        'visible' => true
    ),
    'remote_upload' => APPLICATION_URL.'../files/bak/',
    'local_upload' => '../files/bak/',
    'path_upload' => 'bak/',
    'local_widget' => Array(
        0 => Array('op' => 'widget-admin', 'label' => 'Admin widget', 'class' => 'col-sm-6 col-lg-3'),
        1 => Array('op' => 'widget-group', 'label' => 'Group widget', 'class' => 'col-sm-6 col-lg-3')
    ),
    'local_js' => 'this.lib.js?'.get_RandomString()
);




?>