/**
 * 
 */

function AJ_Del_Admin(m, id) {
	var dataSend = "m="+m+"&app=true&op=del&id="+id;
	if (confirm("L'oggetto verrà eliminato. Confermi?")) {
		$.ajax({
			method: "POST",
			url: "index.php",
			data: dataSend,
			dataType:"json"
		}).done(function( msg ) {
			var deleted = "#tr_"+id;
			var actions = "#tr_"+id+" .actions a";
			if(msg.op=='1') {
				$(deleted).addClass("text-danger");
				$(deleted).css("text-decoration", "line-through");
				$(actions).attr("href", "javascript:;");
				$(actions).addClass("text-danger");
			} else {
				alert(msg.msg);
			}
		});
	}
}

function elDel(m, id) {
	var dataSend = "m="+m+"&app=true&op=ad_del&id="+id;
	if (confirm("L'oggetto verrà eliminato. Confermi?")) {
		$.ajax({
			method: "POST",
			url: "index.php",
			data: dataSend,
			dataType:"json"
		}).done(function( msg ) {
			var deleted = "#tr_"+id;
			var actions = "#tr_"+id+" .actions a";
			if(msg.op=='1') {
				$(deleted).addClass("text-danger");
				$(deleted).css("text-decoration", "line-through");
				$(actions).attr("href", "javascript:;");
				$(actions).addClass("text-danger");
			} else {
				alert(msg.msg);
			}
		});
	}
}


function AJ_AddToGroup(m, g_id, uid) {
	var data_send = 'm='+m+'&op=u_add&group_id='+g_id+'&user_id='+uid;
	$.ajax({
	  url: "index.php?app=true",
	  type: "POST",
	  data: data_send,
	  cache: false,
	  dataType: "json",
	  success: function(resp){
	    if(resp.op==1) {
	    	$("#tr_in_"+uid).html('<td colspan="5" class="changed">'+resp.msg+'</td>');
	    } else {
	    	$("#tr_in_"+uid).addClass('deleted');
	    }
	  }
	});
	$('#tr_out_'+uid).appendTo($('#tb_in'));
	$('#tr_out_'+uid).removeClass().addClass('changed');
	$('#td_out_'+uid).html('');
}

function AJ_DelToGroup(m, g_id, uid) {
	var data_send = 'm='+m+'&op=u_del&group_id='+g_id+'&user_id='+uid;
	$.ajax({
		url: "index.php?app=true",
	  type: "POST",
	  data: data_send,
	  cache: false,
	  dataType: "json",
	  success: function(resp){
	    if(resp.op==1) {
	    	$("#tr_out_"+uid).html('<td colspan="5" class="changed">'+resp.msg+'</td>');
	    } else {
	    	$("#tr_out_"+uid).addClass('deleted');
	    }
	  }
	});
	$('#tr_in_'+uid).appendTo($('#tb_out'));
	$('#tr_in_'+uid).removeClass().addClass('changed');
	$('#td_in_'+uid).html('');
}


function AJ_EditGroup(m, oF) {
	var group_id      = document.forms[oF].group_id.value;
	var group_label   = document.forms[oF].group_label.value;
	var group_weight  = document.forms[oF].group_weight.value;
	var el_id         = group_id;

	var data_send = 'm='+m+'&op=g_edit&group_id='+group_id+'&group_label='+group_label+'&group_weight='+group_weight;
	$.ajax({
		url: "index.php?app=true",
	  type: "POST",
	  data: data_send,
	  cache: false,
	  dataType: "json",
	  success: function(resp){
	    if(resp.op==1) {
			$('#g_tr_'+group_id+' > .block').addClass("alert-success");
			setTimeout(function() {
				$('#g_tr_'+group_id+' > .block').removeClass('alert-success');
			}, 2000);
	    } else {
	    	alert(resp.msg);
	    	$('#g_tr_'+group_id+' > .block').addClass("alert-danger");
			setTimeout("$('#g_tr_'+el_id+' > .block').removeClass('alert-danger')", 2000);
			$('#g_tr_'+el_id+' > .block').removeClass('alert-danger')
	    }
	  }
	});
}

function AJ_DelGroup(m, el_id) {
	if (confirm("Eliminare il record?")) {
		var data_send = 'm='+m+'&op=g_del&id='+el_id;
		$.ajax({
		  url: "index.php?app=true",
		  type: "POST",
		  data: data_send,
		  cache: false,
		  dataType: "json",
		  success: function(resp){
		    if(resp.op==1) {
		    	$("#g_tr_"+el_id+' > .block').addClass("alert-danger");
				$("#g_tr_"+el_id+' > .block > .row').html('<p class="text-center">'+resp.msg+'</p>');
				setTimeout(function() {
					$('#g_tr_'+el_id).hide("slow", function() { 
						$('#g_tr_'+el_id).remove();
					});
				}, 2000);
		    } else {
		    	$("#g_tr_"+el_id+' > .block').removeClass("alert-danger").addClass('alert-danger');
		    	alert(resp.msg);
		    }
		  }
		});
	}
}

function openPop_File(m, f_id) {
  	window.open('index.php?app=true&m='+m+'&op=g_export&id='+f_id, 'file_download', 'width=500,height=500');
}


$(function() {
	
	$("#bakBtn").click(function() {
		// disabilito il bottone
		$("#loading").removeClass("hidden");
		// avviso l'utente dell'inizio del backup
		$(".log").append("Inizio del backup: ");
		
		var data_send = "app=true&m="+$("#bakForm input[name=m]").val()
		// ----- [bak admin]
		$(".log").append("inizio backup utenti<br>");
		$.ajax({ 
			url: "index.php?op=bakAdmin", 
			type: "POST", 
			cache: false,
			data: data_send, 
			dataType: "json",
			success: function(resp){
				var t = window.setTimeout(function(){
					$("#loading").addClass("hidden");
					$(".bakList").prepend('<tr><td colspan="2">'+resp.file+'</td></tr>');
				}, 2000);
			}
		});

	});
});


