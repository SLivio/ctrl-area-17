<?php /*
  Version:     v7 2016.12
  Module:      Ctrl-Area :: Core :: Manage Admin
  Author:      SLivio
*/



//--
//----------------------------------------------------------> [CONFIG]
if(file_exists($CONF['path_module'].'this.lib.php')) {
	require_once($CONF['path_module'].'this.lib.php');
}
$id  = (isset($_REQUEST['id'])  && $_REQUEST['id']  != '' && $_REQUEST['id']  != 0)  ? $_REQUEST['id']  : 0;
$op  = (isset($_REQUEST['op'])  && $_REQUEST['op']  != '' && $_REQUEST['op']  != '') ? $_REQUEST['op']  : '';
$opp = (isset($_REQUEST['opp']) && $_REQUEST['opp'] != '' && $_REQUEST['opp'] != '') ? $_REQUEST['opp'] : '';
$aP  = $_REQUEST;
$aFilter = Array();
//----------------------------------------------------------> [/CONFIG]
$out = '';

switch($op) {
	//-----------------------------> [AJAX]
	
	case 'g_edit' :
		$edit = DB_Set_Group($aP);
		if ($edit!=0) $out .= JSON_FAQ('1', 'This item has been modify.');
		else          $out .= JSON_FAQ('0', 'An error has occurred.');
		die($out);
	break;
	case 'g_del' :
		if ($id != '') {
			DB_Del_Group($id);
			$out .= JSON_FAQ('1', 'This item has been deleted.');
		} else {
			$out .= JSON_FAQ('0', 'An error was occurred.');
		}
		die($out);
	break;
	case 'u_del' :
		DB_Del_UserToGroup($aP);
		$out .= JSON_FAQ('1', 'User has ben deleted from group.');
		die($out);
	break;
	case 'u_add' :
		$add = DB_Add_UserToGroup($aP);
		if ($add == 1) $out .= JSON_FAQ('1', 'User has ben added to group.');
		else           $out .= JSON_FAQ('0', 'An error was occurred.');
		die($out);
	break;
	//-----------------------------> [BAK] LastUpdate 2016.12.11
	case 'bak' :
		$HTML  = GHTML__Get_ModuleHeader('Gestione backup', 'Pagina per la gestione dei backup.', 'ENGINE');
		$HTML .= HTML__BakListAndNew();
		echo $HTML;
	break;
	case 'bakAdmin' :
		GLOBAL $HOST, $USER, $PSWD, $DB;
		
		$dump   = '/Applications/MAMP/Library/bin/mysqldump ';
		$bak_path = APPLICATION_PATH.'files/bak/';
		$bak      = 'bak_'.date('Ymd.His').'.sql';
		$exec     =  $dump.' -h'.$HOST.' -u'.$USER.' -p'.$PSWD.' '.$DB.' > '.$bak_path.$bak;
		
		$return = NULL;
		$output = NULL;
		exec($exec, $output, $return);
		
		$aR   = Array('op'=>'1', 'msg'=>'Ok', 'file'=>$bak, 'random'=>get_RandomString());
		$json = json_encode($aR);
		die($json);
	break;
	
	//-----------------------------> [ADMIN] LastUpdate 4.201121.16
	case 'ad_del' :
		if ($id != '') {
			//DB_Del_Admin($id);
			$out .= JSON_FAQ('1', 'This item has been deleted.');
		} else {
			$out .= JSON_FAQ('0', 'An error was occurred.');
		}
		die($out);
	break;
	
	
	//-----------------------------> [GROUP]
	case 'g_list' :
		if ($opp=='g_add') {
			$id = DB_Add_Group($aP);
		}
		echo GHTML__Get_ModuleHeader('Gestione gruppi', 'Pagina per la gestione dei gruppi.', 'USERS');
		echo HTML_GroupList();
	break;
	case 'g_user' :
		HTML_GroupUser($id);
	break;
	case 'g_export' :
		echo PRINT_AdminGroupList($id);
		
	break;
	//-----------------------------> [ADMIN]
	case 'ad-search' :
		$HTML = HTML__Modal_UserSearch('Cerca', $aP);
		echo $HTML;
	break;
	case 'ad_list' :
		$aFilter['op_page']    = (isset($_REQUEST['op_page'])    ? $_REQUEST['op_page']         : 1);
		$aFilter['op_order']   = (isset($_REQUEST['op_order'])   ? $_REQUEST['op_order']        : ' user_id DESC ');
		$aFilter['user_name']    = (isset($_REQUEST['user_name'])    ? trim($_REQUEST['user_name'])    : '');
		$aFilter['user_surname'] = (isset($_REQUEST['user_surname']) ? trim($_REQUEST['user_surname']) : '');
		$aFilter['user_email']   = (isset($_REQUEST['user_email'])   ? trim($_REQUEST['user_email'])   : '');
		$aFilter['user_alias']   = (isset($_REQUEST['user_alias'])   ? trim($_REQUEST['user_alias'])   : '');
		
		//$q_where .= (isset($aP['country_id'])   && trim($aP['country_id']) != '0')  ? ' AND country_id = "'.$aP['country_id'].'"'          : '';
		//$q_where .= (isset($aP['area_id'])      && trim($aP['area_id']) != '0')     ? ' AND area_id    = "'.$aP['area_id'].'"'             : '';
		//$q_where .= (isset($aP['city_id'])      && trim($aP['city_id']) != '0')     ? ' AND city_id    = "'.$aP['city_id'].'"'             : '';
		
		$aCount    = DB__Get_CountUsers($aFilter);
		$aElements = DB__Get_Users($aFilter);
		
		$HTML      = GHTML__Get_ModuleHeader('Gestione Admin', 'Elenco');
		$HTML     .= HTML__UsersList($aFilter, $aElements, $aCount);
		echo $HTML;
		
	break;
	
	
	case 'ad_search_' :
		//$HTML = HTML_AdminSearch($aP);
		//echo $HTML;
	break;
	case 'ad_search' :
	case 'ad_list' :
		//$aTmp = GLOBAL_Get_GlobalQuery();
		$HTML = HTML_AdminList($aP);
		echo $HTML;
	break;
	case 'ad_add' :
	case 'ad_form' :
		HTML_AdminForm($id);
	break;
	case 'ad_conf' :
		if ($opp=='ad_conf_m_add') { 
			$aP['m_add'] = isset($aP['m_add']) ? $aP['m_add'] : '';
			$OUT = DB_Add_AdminModule($aP['m_add'], $id);
			if ($OUT==false) {
				//echo HTML_TopWarning('Si sono verificati degli errori.');
			}
		}
		if ($opp=='ad_conf_m_del') {
			$aP['m_del'] = isset($aP['m_del']) ? $aP['m_del'] : '';
			DB_Del_AdminModule($aP['m_del'], $id);
		}
		if ($opp=='ad_conf_m_set') {
            $aTmp = Array($aP['op_disable'], $aP['module_op_disable']);
            $aTmp = cleanArrayFromEmptyElements($aTmp);
			DB_Set_AdminModule($aP['m_set'], $id, implode(',', $aTmp));
		}
		HTML_AdminConf($id);
	break;
	case 'widget-admin' :
		$HTML = HTML__Widget('admin');
		die($HTML);
	break;
		case 'widget-group' :
		$HTML = HTML__Widget('group');
		die($HTML);
	break;
	
}


?>