<?php
/*
	Project:     Ctrl-Area 
	Module:      Core :: Manage Admin
	Author:      Silvio (SLivio) Coco - silvio.coco@gmail.com
	Version:     6.2014.04.11
*/

/*

//--
$PATH_LIB  = '../../php_lib/';
require_once('../../inc.config.php');
$app = true;
require_once('../../inc.ajax.php');
require_once($CONF['path_module'].'this.lib.php');

//----------------------------------------------------------> [AJ_VAR]

$op   = isset($_REQUEST['op'])       ? $_REQUEST['op']       : '';
$id   = isset($_REQUEST['id'])       ? $_REQUEST['id']       : '';
$aP   = $_REQUEST;

//----------------------------------------------------------> [AJ_CORE]

$out = '';
switch($op) {
	//-----------------------------> [GROUP] LastUpdate 4.201121.16
	case 'g_edit' :
		$edit = DB_Set_Group($aP);
		if ($edit!=0) $out .= JSON_FAQ('1', 'This item has been modify.');
		else          $out .= JSON_FAQ('0', 'An error has occurred.');
	break;
	case 'g_del' :
		if ($id != '') {
			DB_Del_Group($id);
			$out .= JSON_FAQ('1', 'This item has been deleted.');
		} else {
			$out .= JSON_FAQ('0', 'An error was occurred.');
		}
	break;
	case 'u_del' :
		DB_Del_UserToGroup($aP);
		$out .= JSON_FAQ('1', 'User has ben deleted from group.');
	break;
	case 'u_add' :
		$add = DB_Add_UserToGroup($aP);
		if ($add == 1) $out .= JSON_FAQ('1', 'User has ben added to group.');
		else           $out .= JSON_FAQ('0', 'An error was occurred.');
	break;

	//-----------------------------> [ADMIN] LastUpdate 4.201121.16
	case 'ad_del' :
		if ($id != '') {
			DB_Del_Admin($id);
			$out .= JSON_FAQ('1', 'This item has been deleted.');
		} else {
			$out .= JSON_FAQ('0', 'An error was occurred.');
		}
	break;
}

die($out);*/
?>