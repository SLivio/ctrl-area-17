<?php /*
  Version:     v7 2016.12
  Module:      Ctrl-Area :: Core :: Manage Admin
  Author:      SLivio
*/

//--

	// Array for a search form
	$aSearchForm = Array();
	$aSearchForm['form'] = Array(
		'name' => 'test',
		'id'   => 'fid1',
		'method' => 'post',
		'action' => '');
	$aSearchForm['fields'] = Array(
		0 => Array(
			'label' => 'Nome evento',
			'name'  => 'e_name',
			'type'  => 'text'),
		1 => Array(
			'label' => 'Citt&agrave;',
			'name'  => 'e_city',
			'type'  => 'text'),
		2 => Array(
			'label' => 'Data',
			'name'  => 'e_date',
			'type'  => 'date',
			'attribute' => Array('class'=>'ckeditor')),
		3 => Array(
			'label' => 'Invia',
			'name'  => 'btn',
			'type'  => 'submit'
			)
		);


function HTML__BakListAndNew() {
	// yyyy.mm.dd.hh.mm.ss-yyyy.mm.dd.hh.mm.ss | Utenti=1&Pratiche=1&Agenzie=0
	$HTML = '<div class="block full">
		<div class="row">
			<div class="col-md-6">'.HTML__BakList().'</div>
			<div class="col-md-6">'.HTML__BakNew().'</div>
		</div>
	</div>';
	
	return $HTML;
}

function HTML__BakList() {
	GLOBAL $CONF, $bak_path;
	
	$bak_path = APPLICATION_PATH.'files/bak/';
	$aBak     = PHP_scandir($bak_path, 1, 1);
	$HTMLbak  = '';
	
	foreach($aBak as $bak){
		$tmp = filesize($bak_path.$bak);
		if($tmp>1) {
			$tmp = (int)(($tmp/1024)/1024).' M';
		} else {
			$tmp = $tmp.' KB';
		}
		$HTMLbak .= '<tr>
			<td><a href="'.APPLICATION_URL.'/../../files/bak/'.$bak.'" onClick="alert(\'Cliccare con il tasto destro, quindi su Salva.\');return false" target="_blank">'.$bak.'</a></td>
			<td>'.$tmp.'</td>
		</tr>';
	}
	
	// yyyy.mm.dd.hh.mm.ss-yyyy.mm.dd.hh.mm.ss | Utenti=1&Pratiche=1&Agenzie=0
	$HTML = '<div class="block full">
		<div class="block-title">
			<h2>Lista dei backup</h2>
		</div>
		<table id="general-table" class="table table-striped table-vcenter table-responsive">
			<thead>
				<tr>
                    <th>Nome del file</th>
					<th>Dettagli</th>
				</tr>
			</thead>
			<tbody class="bakList">
				'.$HTMLbak.'
			</tbody>
		</table>
	</div>';
	
	return $HTML;
}

function HTML__BakNew() {
	GLOBAL $CONF, $m, $op;
	
	$pathBak   = '';
	$aDirBak   = '';
	$aBakConf  = Array(
			'Utenti'   => Array(DB_PREFIX.'core_admin', DB_PREFIX.'core_admin_groups'),
			'Pratiche' => Array(DB_PREFIX.'pratiche', DB_PREFIX.'pratiche_agenzie', DB_PREFIX.'pratiche_allegati', DB_PREFIX.'pratiche_conducenti', DB_PREFIX.'pratiche_correlate'),
			'Agenzie' => Array(DB_PREFIX.'agenzie')
		);
	
	// 
	
	$HTML = '<div class="block full">
		<div class="block-title">
			<h2>Nuovo Backup</h2>
		</div>
		<form onsubmit="return false" id="bakForm" class="form-horizontal">
			<div class="form-group">
				<input type="hidden" name="m" value="'.$m.'">
				<input type="hidden" name="op" value="bakLaunch">
				<div class="col-xs-12">
					<span class="help-block">Clicca sul bottone in basso per fare un nuovo backup.</span>
				</div>
			</div>
			<div class="form-group form-actions">
				<div class="col-xs-12">
                	<button type="submit" id="bakBtn" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Backup</button>
					
				</div>
			</div>
		</form>
		<i id="loading" class="fa fa-spinner fa-2x fa-spin hidden"></i>
	</div>';
	
	
	return $HTML;
}
	
	
	
	


// LastUpdate 2014.03.02 [test.6]
function HTML__Widget($what='') {
	GLOBAL $SESSION_ID, $op, $m;
	$CON = GDB__Get_CoreSession();
	//
	switch($what) {
		case 'admin' :
			$count = GDB__Count_Admin(1);
			$label = 'Admin totali';
			$link  = 'm='.$m.'&op=ad_list';
		break;
		case 'group' :
			$count = GDB__Count_Group(1);
			$label = 'Gruppi totali';
			$link  = 'm='.$m.'&op=g_list';
		break;
	}
	//
	return '<!-- Widget -->
		<div class="widget">
			<div class="widget-simple">
				<a href="?'.$link.'" class="widget-icon pull-left themed-background-autumn animation-fadeIn">
                	<i class="gi gi-group"></i>
                </a>
                <h3 class="widget-content text-right animation-pullDown">
                	<strong>'.$label.'</strong><br>
                	<small>'.$count.'</small>
				</h3>
			</div>
		</div>
		<!-- END Widget -->';
}

function HTML_GroupUser($id=0) {
	GLOBAL $CONF, $aModules, $aModule, $op, $m, $p;
	
	if ($id==0) return false;
	
	
	$aInfo = GDB__Get_InfoGroup($id);
	$HTML  = GHTML__Get_ModuleHeader('Gestione Gruppo', $aInfo['group_label']);
	//
	// ---- Gestione utenti-gruppi
	//
	$aUin = DB_Get_UserGroup($id);
	$HTML .= '
	<div class="block">
		<div class="block-title">
			<h2>Gestione Utenti/Gruppo</h2>
			<div class="block-options pull-right">
				<a href="?m='.$m.'&op='.$op.'&id='.$id.'" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="" data-original-title="Aggiorna">'.GHTML__Get_Ico('refresh').'</a>
			</div>
		</div>
		<div class="table-responsive">
			<table id="tb_in" class="table table-striped table-vcenter">
			<thead>
			<tr class="tr_head">
				<th colspan="5">Utenti presenti nel gruppo</th>
			</tr>
			<tbody>';
		foreach($aUin as $u) {
			$HTML .= '<tr id="tr_in_'.$u['user_id'].'">
				<td class="col-lg-1 col-md-1 col-sm-1 col-xs-1">'.$u['user_id'].'</td>
				<td class="col-lg-3 col-md-3 col-sm-3 col-xs-3">'.$u['user_name'].'</td>
				<td class="col-lg-3 col-md-3 col-sm-3 col-xs-3">'.$u['user_surname'].'</td>
				<td class="col-lg-4 col-md-4 col-sm-4 col-xs-4">'.$u['user_email'].'</td>
				<td class="col-lg-1 col-md-1 col-sm-1 col-xs-1 text-center" id="td_in_'.$u['user_id'].'"><a href="javascript:;" onClick="AJ_DelToGroup(\''.$m.'\', \''.$aInfo['group_id'].'\', \''.$u['user_id'].'\')">'.GHTML__Get_Ico('-').'</a></td>
			</tr>';
		}
	$aUin  = Array();
	$aUout = DB_Get_UserNotInGroup($id);
		$HTML .= '
			</tbody>
		</table>
		<table id="tb_out" class="table table-striped table-vcenter">
			<thead>
			<tr class="tr_head">
				<th colspan="5">Utenti NON presenti nel gruppo</th>
			</tr>
			</thead>			<tbody>
';
	
	foreach($aUout as $u) {

		$HTML .= '<tr id="tr_out_'.$u['user_id'].'">
				<td class="col-lg-1 col-md-1 col-sm-1 col-xs-1">'.$u['user_id'].'</td>
				<td class="col-lg-3 col-md-3 col-sm-3 col-xs-3">'.$u['user_name'].'</td>
				<td class="col-lg-3 col-md-3 col-sm-3 col-xs-3">'.$u['user_surname'].'</td>
				<td class="col-lg-4 col-md-4 col-sm-4 col-xs-4">'.$u['user_email'].'</td>
				<td class="col-lg-1 col-md-1 col-sm-1 col-xs-1 text-center"  id="td_out_'.$u['user_id'].'"><a href="javascript:;" onClick="AJ_AddToGroup(\''.$m.'\', \''.$aInfo['group_id'].'\', \''.$u['user_id'].'\')">'.GHTML__Get_Ico('+').'</a></td>
		</tr>';
	}
	
	$HTML .= '</tbody>
		</table>
		</div>
	</div>';
	echo $HTML;

}

// LastUpdate 2014.03.04 [test.6]
function HTML_GroupList($id=0) {
	GLOBAL $CONF, $aModules, $aModule, $op, $m, $p;
	//
	$aR      = GDB__Get_Groups();
	
	
	$HTML = '<div class="row">';
	foreach($aR as $el) {
		$class = ($el['group_id']==$id) ? 'class="success"' : '';
		$u_tot = GDB__Count_AdminGroup($el['group_id']);
		
		$HTML .= '
		<div class="col-md-6" id="g_tr_'.$el['group_id'].'">
			<div class="block">
				<div class="block-title">
					<div class="block-options pull-right">'.$el['record_date'].'</div>
					<h2>Gruppo '.$el['group_id'].'</h2>
				</div>
				<div class="row">
				<form name="g_edit_'.$el['group_id'].'" onSubmit="return false" class="form-horizontal">
					<div class="form-group" '.$class.' >
						'.pager_getInputHidden('group_id', $el['group_id']).'
						<div class="col-md-6"><input type="text" name="group_label" value="'.$el['group_label'].'" class="form-control"></div>
						<div class="col-md-2"><input type="text" name="group_weight" value="'.$el['group_weight'].'" class="form-control"></div>
						<div class="col-md-2"><a class="btn btn-sm btn-primary" href="javascript:AJ_EditGroup(\''.$m.'\', \'g_edit_'.$el['group_id'].'\')">'.GHTML__Get_Ico('save').' Salva</a></div>
					</div>
				
					<div class="form-group form-actions">
						<div class="col-sm-12">
							<a class="btn btn-sm btn-primary" href="javascript:openPop_File(\''.$m.'\', '.$el['group_id'].');void(0)">'.GHTML__Get_Ico('download').' Download</a>
							<a class="btn btn-sm btn-primary" href="?m='.$m.'&op=g_user&id='.$el['group_id'].'">'.GHTML__Get_Ico('user-group').' Gestisci ( '.$u_tot.' )</a>
							<a class="btn btn-sm btn-danger" href="javascript:AJ_DelGroup(\''.$m.'\', \''.$el['group_id'].'\')">'.GHTML__Get_Ico('del').' Cancella</a>
						</div>
					</div>	
				</form>
				</div>
            </div>
		</div>';
	}
	echo $HTML.'</div>';

	$HTML = NL.'
	<div class="block">
	<div class="block-title"><h2>Add a group</h2></div>
		<form name="g_add" method="post" action="?m='.$m.'&op='.$op.'">
		<table class="table table-vcenter table-striped">
			<thead>
			<tr class="tr_head">
				<th class="w50">ID</th>
				<th>Group name</th>
				<th class="w50">Weight</th>
				<th class="w50"> </th>
				<th>Date</th>
				<th class="w120 ac">Action</th>
			</tr>
			</thead>
			<tbody>'.
	           pager_getInputHidden('op',  'g_list').
	           pager_getInputHidden('opp', 'g_add').
	           pager_getInputHidden('m',   $m).'';
		$HTML .= '<tr>
			<td>/</td>
			<td><div class="form-group"><div class="col-xs-12"><input type="text" name="group_label" class="form-control" /></div></div></td>
			<td><div class="form-group"><div class="col-xs-3"><input type="text" name="group_weight" class="form-control" /></div></div></td>
				<td> </td>
		<td>'.date('Y-m-d h:i:s').'</td>
		<td>'.pager_getSubmit('btn', 'CREA').'</td>
		</tr>';
	$HTML .= NL.'</table></form>
		</div>
	</div>';
	echo $HTML;
}


function DB_GetCountryList() {
	$a = Array();
	$c = 0;
	$q = "SELECT * FROM ".DB_PREFIX."core_geo_country WHERE country_enable='1' ORDER BY country_name DESC";
	$r = mysqli_query($CON, $q) or LOG__Error("DB_GetCountryList[]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		while($rec = mysqli_fetch_array($r)) {
			$a[$c] = $rec;
			$c++;
		}
	}
	return $a;
}

// LastUpdate v6.2014.04.14
function DB_Get_GeoList($what='', $whatId=0) {
	$CON = GDB__Get_CoreSession();
	$a = Array();
	$c = 0;
	$q = '';
	switch($what) {
		case 'country' :
			$q = "SELECT * FROM ".DB_PREFIX."core_geo_country";
		break;
		case 'area' :
			$condition = ($whatId!=0) ? ' WHERE country_id="'.$whatId.'"' : '';
			$q = "SELECT * FROM ".DB_PREFIX."core_geo_area ".$condition.' ORDER BY area_name ASC';
		break;
		case 'city' :
			$condition = ($whatId!=0) ? ' WHERE area_id="'.$whatId.'"' : '';
			$q = "SELECT * FROM ".DB_PREFIX."core_geo_city ".$condition.' ORDER BY city_name ASC';
		break;

	}
	$r = mysqli_query($CON, $q) or LOG__Error("DB_Get_GeoList[]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		while($rec = mysqli_fetch_array($r)) {
			$a[$c] = $rec;
			$c++;
		}
	}
	return $a;
}



// DEV
function DB_Add_Group($aP) {
	$CON = GDB__Get_CoreSession();
	$aP['group_label']  = isset($aP['group_label'])  ? $aP['group_label']  : '';
	$aP['group_weight'] = isset($aP['group_weight']) ? $aP['group_weight'] : '';
	
	$OUT = 0;
	//
	$q = "INSERT INTO `".DB_PREFIX.'core_groups'."` (
	     `group_id`,
	     `group_label`,
	     `group_weight`,
	     `record_ip`,
	     `record_date`
	     ) VALUES (
	     NULL,
	     \"".$aP['group_label']."\",
	     \"".$aP['group_weight']."\",
	     \"".$_SERVER['REMOTE_ADDR']."\",
		   NOW())";
	$r = mysqli_query($CON, $q) or LOG__Error("DB_Add_Group", mysqli_error($CON)."\n".$q);
	$OUT = mysqli_insert_id($CON);
	return $OUT;
}

function DB_Get_UserGroup($g_id=0) {
	$CON = GDB__Get_CoreSession();
	$a = Array();
	$c = 0;
	$q = "SELECT * FROM ".DB_PREFIX."core_admin_groups,".DB_PREFIX."core_groups,".DB_PREFIX."core_admin
	WHERE ".DB_PREFIX."core_admin_groups.group_id=".DB_PREFIX."core_groups.group_id and
	".DB_PREFIX."core_admin_groups.user_id = ".DB_PREFIX."core_admin.user_id and
	".DB_PREFIX."core_groups.group_id=".$g_id."
	 ORDER BY group_weight DESC";
	$r = mysqli_query($CON, $q) or LOG__Error("DB_GetUser_Group[]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		while($rec = mysqli_fetch_array($r)) {
			$a[$c] = $rec;
			$c++;
		}
	}
	return $a;	
}

function DB_Get_UserNotInGroup($g_id=0) {
	$CON = GDB__Get_CoreSession();
	$aUin = Array();
	$c = 0;
	$q = "SELECT * FROM ".DB_PREFIX."core_admin_groups,".DB_PREFIX."core_groups,".DB_PREFIX."core_admin
	WHERE ".DB_PREFIX."core_admin_groups.group_id=".DB_PREFIX."core_groups.group_id and
	".DB_PREFIX."core_admin_groups.user_id = ".DB_PREFIX."core_admin.user_id and
	".DB_PREFIX."core_groups.group_id=".$g_id."
	 ORDER BY group_weight DESC";
	$r = mysqli_query($CON, $q) or LOG__Error("DB_GetUser_Group[1]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		while($rec = mysqli_fetch_array($r)) {
			$aUin[$c] = $rec['user_id'];
			$c++;
		}
	}
	$aUin[$c] = 0;
	//
	$a = Array();
	$q = "SELECT * FROM ".DB_PREFIX."core_admin
	WHERE ".DB_PREFIX."core_admin.user_id not in (".implode(',', $aUin).")";
	$r = mysqli_query($CON, $q) or LOG__Error("DB_GetUser_Group[2]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		while($rec = mysqli_fetch_array($r)) {
			$a[$c] = $rec;
			$c++;
		}
	}

	return $a;	
}

//----------------------------------------------------------> [AJAX]



// DEV
function DB_Del_Group($id=0) {
	
	$CON = GDB__Get_CoreSession();
	//
	$q = 'DELETE FROM '.DB_PREFIX.'core_groups WHERE group_id="'.(int)$id.'" ';
	$r = mysqli_query($CON, $q) or LOG__Error("DB_Del_Group[1]", mysqli_error($CON)."\n".$q);
	
	$q = 'DELETE FROM '.DB_PREFIX.'core_admin_groups WHERE group_id="'.(int)$id.'" ';
	$r = mysqli_query($CON, $q) or LOG__Error("DB_Del_Group[2]", mysqli_error($CON)."\n".$q);
}

// DEV
function DB_Set_Group($aP) {
	//
	$CON = GDB__Get_CoreSession();
	
	$aP['group_id']     = isset($aP['group_id'])     ? $aP['group_id']     : 0;
	$aP['group_label']  = isset($aP['group_label'])  ? $aP['group_label']  : '';
	$aP['group_weight'] = isset($aP['group_weight']) ? $aP['group_weight'] : 0;
	$aP['group_default_module'] = isset($aP['group_default_module']) ? $aP['group_default_module'] : '';
	//
	$OUT                     = 0;
	if ($aP['group_id']!=0 && $aP['group_label']!='') {
		$q = "UPDATE `".DB_PREFIX.'core_groups'."` SET ".
		     (($aP['group_label'] != '')          ? "`group_label`='".$aP['group_label']."',"    : "").
		     (($aP['group_weight'] != '')         ? "`group_weight`='".$aP['group_weight']."',"  : "").
		     (($aP['group_default_module'] != '') ? "`group_default_module`='".$aP['group_default_module']."',"  : "").
		     "`record_ip_modify`='".$_SERVER['REMOTE_ADDR']."',".
		     "`record_date_modify`=NOW()".
		     " WHERE `group_id`='".$aP['group_id']."'";
		$r = mysqli_query($CON, $q) or LOG__Error("DB_Set_Group[]", mysqli_error($CON)."\n".$q);
		if(mysqli_error($CON) == '') {
			$OUT = $aP['group_id'];
		}
	}
	return $OUT;
}

function DB_Del_UserToGroup($aP) {
	//
	$CON = GDB__Get_CoreSession();
	$aP['group_id'] = isset($aP['group_id']) ? $aP['group_id'] : 0;
	$aP['user_id']  = isset($aP['user_id'])  ? $aP['user_id']  : 0;
	//
	$q = 'DELETE FROM '.DB_PREFIX.'core_admin_groups WHERE group_id="'.$aP['group_id'].'" and user_id="'.$aP['user_id'].'" ';
	$r = mysqli_query($CON, $q) or LOG__Error("DB_Del_UserToGroup[]", mysqli_error($CON)."\n".$q);
}

function DB_Add_UserToGroup($aP) {
	$CON = GDB__Get_CoreSession();
	$aP['group_id']  = isset($aP['group_id'])  ? $aP['group_id']  : 0;
	$aP['user_id']   = isset($aP['user_id'])   ? $aP['user_id']   : 0;
	$OUT = 0;
	if ($aP['user_id']==0 || $aP['group_id']==0) return $OUT;
	//
	$q = "INSERT INTO `".DB_PREFIX.'core_admin_groups'."` (
	     `group_id`,
	     `user_id`,
	     `record_ip`,
	     `record_date`
	     ) VALUES (
	     \"".$aP['group_id']."\",
	     \"".$aP['user_id']."\",
	     \"".$_SERVER['REMOTE_ADDR']."\",
		   NOW())";
	$r = mysqli_query($CON, $q) or LOG__Error("DB_Add_UserToGroup", mysqli_error($CON)."\n".$q);
	//
	if (mysqli_error($CON)=='') $OUT = 1;
	//
	return $OUT;
}

function DB_Del_Admin($id) {
	remove_Record(DB_PREFIX.'core_admin', 'user_id', $id);
	remove_Record(DB_PREFIX.'core_admin_modules', 'user_id', $id);
}



//----------------------------------------------------------> [LOCAL_ADMIN_LIB]


// LastUpdate 7.2017.12.05
function HTML__Pagination($aFilter=Array(), $aCounts=Array()) {
	GLOBAL $m, $CONF, $op;
	
	$HTMLP = '<form class="pull-right"><select onChange="gotoPage(this.value)">';
	$aTmpFilter = $aFilter;
	unset($aTmpFilter['op_page']);
	unset($aTmpFilter['op_order']);
	
	$query = http_build_query(array_filter($aTmpFilter));
	for($i=1;$i<=$aCounts['totPages'];$i++) {
		$class  =  (isset($aFilter['op_page']) && $aFilter['op_page']==$i) ? ' selected="selected"' : '';
		$link   =  '?m='.$m.'&op='.$op.'&op_page='.$i.'&'.$query.'&order='.$aFilter['op_order'];
		$HTMLP .= '<option '.$class.' value="'.$link.'">'.$i.'</a></li>';
	}
	$HTMLP .= '</select></form>';

	$HTML = '<div class="row">
			<div class="col-sm-6"><span><strong>'.$aCounts['totElements'].'</strong> risultati</span></div>
			<div class="col-sm-6">'.$HTMLP.'</div>
		</div>';
	
	return $HTML;
}


// LastUpdate 7.2017.12.05
function HTML__UsersList($aFilter=Array(), $aElements=Array(), $aCounts=Array()) {
	GLOBAL $m, $CONF, $op;
	
	$HTML  = ''; //GHTML__Get_ModuleHeader('Gestione Admin', 'Elenco');
	$HTMLP = HTML__Pagination($aFilter, $aCounts);
	
	
	$HTML .= '
	<div class="block">
		<div class="block-title">
			<div class="block-options pull-right" data-toggle="tooltip" data-original-title="Search">
				<a class="btn btn-alt btn-sm btn-default" title="" href="?app=true&m='.$m.'&op=ad-search"  data-toggle="modal" data-target="#modal"><i class="hi hi-search"></i></a>
			</div>
			<h2>Elenco Utenti</h2>
		</div>
	
		<!-- Modal -->
		<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog"><div class="modal-content"></div><!-- /.modal-content --></div>
		</div>
	
		<div class="table-responsive">
			<table class="table table-striped table-vcenter table-bordered dataTable table-hover">
			<tr><td colspan="7">'.$HTMLP.'</td></tr>
			<thead>
				<tr class="tr_head">
					<th>ID</th>
					<th>Nome e Cognome</th>
					<th>Email e Alias</th>
					<th>Note</th>
					<th colspan="3" class="w90 text-center">Azioni</th>
				</tr>
			</thead>
			<tbody>';	
	
	foreach($aElements as $el) {
		$class2 = ($el['user_enable']==1) ? '' : 'danger';
		//
		$id     = $el['user_id'];
		$a_edit = '<a href="?m='.$m.'&op=ad_form&id='.$id.'" title="Edit admin">'.GHTML__Get_Ico('user-edit', 'Edit').'</a>';
		$a_conf = '<a href="?m='.$m.'&op=ad_conf&id='.$id.'" title="Module admin">'.GHTML__Get_Ico('conf-edit', 'Config').'</a>';
		$a_del  = '<a href="javascript:elDel('.$m.', '.$id.')"  title="Delete admin">'.GHTML__Get_Ico('delete', 'Delete').'</a>';
		//
		$HTML .= NL.'<tr id="tr_'.$id.'">';
			$HTML .= '<td class="'.$class2.'">'.$id.'</td>';
			$HTML .= '<td>'.$el['user_name'].'<br>'.$el['user_surname'].'</td>';
			$HTML .= '<td>'.$el['user_email'].'<br>'.$el['user_alias'].'</td>';
			$HTML .= '<td>'.$el['user_note'].'</td>';
			$HTML .= '<td class="text-center w30">'.$a_edit.'</td>';
			$HTML .= '<td class="text-center w30">'.$a_conf.'</td>';
			$HTML .= '<td class="text-center w30">'.$a_del.'</td>';
		$HTML .= '</tr>';
	}
	$HTML .= '</tbody>
		<tfoot>
			<tr><td colspan="7">'.$HTMLP.'</td></tr>
		</tfoot>
		</table><br>
		</div>
	</div>';
	//
	return $HTML;
}

// LastUpdate 2016.02.22
function HTML__Modal_UserSearch($title='', $ar=Array()) {
	GLOBAL $m;

	$HTML ='<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<title>'.$title.'</title>
	<script>
	$("body").on("hidden.bs.modal", ".modal", function () {
	    $(this).removeData("bs.modal");
	});
	</script>
	<script src="js/app.js"></script>
</head>
<body>
	<div class="modal-header text-center">
		<h2 class="modal-title"><i class="fa fa-search"></i> '.$title.'</h2>
	</div><!-- /modal-header --><!-- /modal-header -->
	<div class="modal-body">
		<form action="?m='.$m.'" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered">
		<input type="hidden" name="op"  value="ad_list">
			<div class="form-group">
				<label class="col-md-3 control-label" for="user_name">Nome</label>
				<div class="col-md-9">
					<input type="text" id="user_name" name="user_name" class="form-control" placeholder="Es: Mario">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label" for="user_surname">Cognome</label>
				<div class="col-md-9">
					<input type="text" id="user_surname" name="user_surname" class="form-control" placeholder="Es: Rossi">
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Search</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div><!-- /modal-footer -->
		</form>
	</div><!-- /modal-body -->
</body>
</html>';
	return $HTML;
}

// DEV
function PRINT_AdminGroupList($id=0) {
	GLOBAL $CONF, $aModules, $aModule, $op, $m, $p;
	//
	$aList  = DB_Get_UserGroup($id);
	//
	$HTML = '<style type="text/css">
	@page { size:210mm 297mm; margin:0mm; page-break-after:284mm; }
	body { font-family:tahoma; }
	table thead { background-color:#ccc; font-size:12px; font-weight:bold; }
	table.tb_list tbody tr td { background-color:#efefef; font-size:12px; }
	</style>
	<table class="tb_list" width="100%">
	<thead>
		<tr><td>id</td><td>Name Surname</td><td>Email</td></tr>
	</thead>
	<tbody>';
	$tmp_c = 0;
	$tmp_t = 0;
	foreach($aList as $el) {
		$HTML .= NL.'<tr><td>'.$el['user_id'].'</td><td>'.$el['user_surname'].' '.$el['user_name'].'</td><td>'.$el['user_email'].'</td></tr>';
		$tmp_t++;
	} 
	$HTML .= '<tr><td colspan="3">Users: '.$tmp_t.'</td></td><tr>
		</tbody>
	</table>
	<script type="text/javascript">
	//self.print();
	</script>';
	//
	return $HTML;
}


//----------------------------------------------------------> [ADMIN]

// LastUpdate 4.201112.21 [ok]
function DB_Add_Admin($aP=Array()) {
	$CON = GDB__Get_CoreSession();
	
	$aP['user_name']       = (isset($aP['user_name'])       && trim($aP['user_name']) != '')       ? $aP['user_name']       : '';
	$aP['user_surname']    = (isset($aP['user_surname'])    && trim($aP['user_surname']) != '')    ? $aP['user_surname']    : '';
	$aP['user_email']      = (isset($aP['user_email'])      && trim($aP['user_email']) != '')      ? $aP['user_email']      : '';
	$aP['user_alias']      = (isset($aP['user_alias'])      && trim($aP['user_alias']) != '')      ? $aP['user_alias']      : '';
	$aP['user_password_1'] = (isset($aP['user_password_1']) && trim($aP['user_password_1']) != '') ? $aP['user_password_1'] : '';
	$aP['user_password_2'] = (isset($aP['user_password_2']) && trim($aP['user_password_2']) != '') ? $aP['user_password_2'] : '';
	$aP['user_enable']     = (isset($aP['user_enable'])     && $aP['user_enable'] != '')           ? 1                      : 0;
	$aP['country_id']      = (isset($aP['country_id'])      && $aP['country_id'] != '')            ? $aP['country_id']      : 0;
	$aP['area_id']         = (isset($aP['area_id'])         && $aP['area_id'] != '')               ? $aP['area_id']         : 0;
	$aP['city_id']         = (isset($aP['city_id'])         && $aP['city_id'] != '')               ? $aP['city_id']         : 0;
	
	$aP['user_note']           = (isset($aP['user_note'])            && trim($aP['user_note']) != '')        ? $aP['user_note']        : '';
	
	$OUT                 = 0;
	//
	if(trim($aP['user_password_1']) != '' && (trim($aP['user_password_1'])==trim($aP['user_password_2']))){
		$aP['user_password'] = md5(trim($aP['user_password_1']));
	} else {
		return 0;
	}
	//
	if ($aP['user_password'] != '' && $aP['user_email'] != '') {
		$q = "INSERT INTO `".DB_PREFIX.'core_admin'."` (
				`user_id`,
				`user_name`,
				`user_surname`,
				`user_email`,
				`user_alias`,
				`user_password`,
				`user_note`,
				`user_enable`,
				`country_id`,
				`area_id`,
				`city_id`,
				`record_date`,
				`record_ip`
		     ) VALUES (
		     NULL,
				\"".$aP['user_name']."\",
				\"".$aP['user_surname']."\",
				\"".$aP['user_email']."\",
				\"".$aP['user_alias']."\",
				\"".$aP['user_password']."\",
		  		\"".$aP['user_note']."\",
				\"".$aP['user_enable']."\",
				\"".$aP['country_id']."\",
				\"".$aP['area_id']."\",
				\"".$aP['city_id']."\",
		     NOW(),
		     \"".$_SERVER["REMOTE_ADDR"]."\"
		     )";
		$r = mysqli_query($CON, $q) or LOG__Error("DB_Add_Admin", mysqli_error($CON)."\n".$q);
		$OUT = mysqli_insert_id($CON);
	}
	return $OUT;

}


// LastUpdate 4.201112.21 [ok]
function DB_Set_Admin($aP=Array()) {
	GLOBAL $CONF, $aModules, $aModule, $op, $m, $p, $SESSION_ID;
	$CON = GDB__Get_CoreSession();
	//
	$aP['user_id']         = (isset($aP['id'])              && $aP['id'] != '')                    ? $aP['id']              : 0;
	$aP['user_name']       = (isset($aP['user_name'])       && trim($aP['user_name']) != '')       ? $aP['user_name']       : '';
	$aP['user_surname']    = (isset($aP['user_surname'])    && trim($aP['user_surname']) != '')    ? $aP['user_surname']    : '';
	$aP['user_email']      = (isset($aP['user_email'])      && trim($aP['user_email']) != '')      ? $aP['user_email']      : '';
	$aP['user_alias']      = (isset($aP['user_alias'])      && trim($aP['user_alias']) != '')      ? $aP['user_alias']      : '';
	$aP['user_enable']     = (isset($aP['user_enable'])     && $aP['user_enable'] != '')           ? 1                      : 0;
	$aP['user_password_1'] = (isset($aP['user_password_1']) && trim($aP['user_password_1']) != '') ? $aP['user_password_1'] : '';
	$aP['user_password_2'] = (isset($aP['user_password_2']) && trim($aP['user_password_2']) != '') ? $aP['user_password_2'] : '';
	$aP['country_id']      = (isset($aP['country_id'])      && $aP['country_id'] != '')            ? $aP['country_id']      : 0;
	$aP['area_id']         = (isset($aP['area_id'])         && $aP['area_id'] != '')               ? $aP['area_id']         : 0;
	$aP['city_id']         = (isset($aP['city_id'])         && $aP['city_id'] != '')               ? $aP['city_id']         : 0;

	$aP['user_note']           = (isset($aP['user_note'])            && trim($aP['user_note']) != '')        ? $aP['user_note']        : '';
	
	$OUT                   = false;
	// Corrispondenza password
	if ($aP['user_password_1'] != $aP['user_password_2']) { 
		return false;
	}
	// id utente mancante
	if ($aP['user_id']==0) {
		return false;
	}
	// Modifica dei dati
	$addQ = Array();
	if (trim($aP['user_name']) != '')       $addQ[] = ' `user_name`="'.$aP['user_name'].'" ';
	if (trim($aP['user_surname']) != '')    $addQ[] = ' `user_surname`="'.$aP['user_surname'].'" ';
	if (trim($aP['user_email']) != '')      $addQ[] = ' `user_email`="'.$aP['user_email'].'" ';
	if (trim($aP['user_alias']) != '')      $addQ[] = ' `user_alias`="'.$aP['user_alias'].'" ';
	if (trim($aP['user_surname']) != '')    $addQ[] = ' `user_surname`="'.$aP['user_surname'].'" ';
	if (trim($aP['user_password_1']) != '') $addQ[] = ' `user_password`="'.md5($aP['user_password_1']).'" ';
	if (isset($aP['country_id']))           $addQ[] = ' `country_id`="'.$aP['country_id'].'" ';
	if (isset($aP['area_id']))              $addQ[] = ' `area_id`="'.$aP['area_id'].'" ';
	if (isset($aP['city_id']))              $addQ[] = ' `city_id`="'.$aP['city_id'].'" ';
	if (isset($aP['user_enable']))          $addQ[] = ' `user_enable`="'.$aP['user_enable'].'" ';
	
	if (trim($aP['user_note']) != '')       $addQ[] = ' `user_note`="'.$aP['user_note'].'" ';
	
	$q = 'UPDATE `'.DB_PREFIX.'core_admin` SET '.implode(', ', $addQ).',
	`record_date_modify`   = NOW(),
	`record_ip_modify`     = "'.$_SERVER["REMOTE_ADDR"]  .'"
	WHERE 
	user_id="'.$aP['user_id'].'"';
	$r = mysqli_query($CON, $q) or LOG__Error("DB_Set_Admin", mysqli_error($CON)."\n".$q);
	if (mysqli_error($CON)=='') {
		$_SESSION[$SESSION_ID]['admin_alias'] = $aP['user_alias'];
		$OUT = $aP['user_id'];
	} else {
		$OUT = false;
	}
	return $OUT;
}



// LastUpdate 4.201112.16 [test]
function HTML_AdminForm($id=0) {
	GLOBAL $CONF, $aModules, $op, $m;
	//
	if ($id==0) {
		$LOCAL_TITLE = 'New User';
		$a = fillArrayWithKeys(Array('user_id', 'user_name', 'user_surname', 'user_alias', 'user_email', 'user_password','user_note', 'country_id', 'area_id', 'city_id'));
		$a['user_enable'] = 0;
		$id       = '0';
	} else {
		$LOCAL_TITLE = 'Edit User';
		$a = GDB__Get_Admin($id);
		$a['user_password'] = '';
	}
    $aCountry = DB_Get_GeoList('country');
	$aArea    = DB_Get_GeoList('area');
	$aCity    = DB_Get_GeoList('city');
    //
	echo GHTML__Get_ModuleHeader($LOCAL_TITLE, 'Gestione utente', 'USER').'
		<div class="block">';
		$objForm = new ValidForm("adminData", NL);
		$objForm->addField("m",        "", VFORM_HIDDEN, array(), array(), array("hint" => $m));
		$objForm->addField("op",       "", VFORM_HIDDEN, array(), array(), array("hint" => $op));
		$objForm->addField("id",       "", VFORM_HIDDEN, array(), array(), array("hint" => $id));
		// Personal data: name, surname
		$objForm->addField("user_name","Name: ",VFORM_STRING,
			array("maxLength" => 250),
			array("maxLength" => "Your last name is too long. Maximum of %s characters allowed."),
			array("default" => $a['user_name'])
		);
		$objForm->addField("user_surname","Surname: ",VFORM_STRING,
			array("maxLength" => 250),
			array("maxLength" => "Your last surname is too long. Maximum of %s characters allowed."),
			array("default" => $a['user_surname'])
		);
		$objForm->addField("user_alias","Alias: ",VFORM_STRING,
				array("maxLength" => 250),
				array("maxLength" => "Your last alias is too long. Maximum of %s characters allowed."),
				array("default" => $a['user_alias'])
		);


		$objForm->addField("user_note","Note: ",VFORM_TEXT,
			array("maxLength" => 300),
			array("maxLength" => "Your last alias is too long. Maximum of %s characters allowed."),
			array("default" => $a['user_note'])
		);

		// country
		$objSelectList = $objForm->addField("country_id", "Country:", VFORM_SELECT_LIST,
			array("required" => true),
			array("required" => "You must select a country."),
			array("default" => $a['country_id'])
		);
		$objSelectList->addField("Select country", "", "");
		foreach($aCountry as $country) {
			$selected = (isset($a['country_id']) && $a['country_id']==$country['country_id']) ? true : false;
			$objSelectList->addField($country['country_name'], $country['country_id'], $selected);
		}

		// area
		$objSelectList = $objForm->addField("area_id", "Area:", VFORM_SELECT_LIST,
			array("required" => true),
			array("required" => "You must select a country."),
			array("default" => $a['area_id'])
		);
		$objSelectList->addField("Area", "", "");
		foreach($aArea as $area) {
			$selected = (isset($a['area_id']) && $a['area_id']==$area['area_id']) ? true : false;
			$objSelectList->addField($area['area_name'], $area['area_id'], $selected);
		}
		
		// city
		$objSelectList = $objForm->addField("city_id", "City:", VFORM_SELECT_LIST,
			array("required" => true),
			array("required" => "You must select a country."),
			array("default" => $a['city_id'])
		);
		$objSelectList->addField("Select city", "", "");
		foreach($aCity as $city) {
			$selected = (isset($a['city_id']) && $a['city_id']==$city['city_id']) ? true : false;
			$objSelectList->addField($city['city_name'], $city['city_id'], $selected);
		}

        $objForm->addField("user_enable", "Enable user:", VFORM_BOOLEAN,
			array(),
			array(),
			array("default" => $a['user_enable'])
		);
		if ($id==0) {
			
			$objForm->addField("user_email", "Email address:", VFORM_EMAIL, 
			array(
				"maxLength" => 255, 
				"required" => TRUE
			), 
			array(
				"maxLength" => "Your input is too long. A maximum of %s characters is OK.", 
				"required" => "This field is required.", 
				"type" => "Use the format name@domain.com"
			),
			array("default" => $a['user_email'])
			);
			$objForm->addField("user_password_1","Password:", VFORM_PASSWORD,
				array(
					"required" => true
				),
				array(
					"type" => "This is not a valid e-mail address!",
					"required" => "Please fill in a password"
				)
			);
			$objForm->addField("user_password_2","Repeat password:", VFORM_PASSWORD,
				array(
					"required" => true
				),
				array(
					"type" => "This is not a valid e-mail address!",
					"required" => "Please retype your password"
				)
			);
		
		
		} else {
			// Login data: email, password
			$objForm->addParagraph("If you want to change password, please fill in the next fields as wel.", "<br />");
			$objPwd = $objForm->addArea("Change login information", true, "login", false);
			//*** There is also built-in support for password fields
			$objPwd->addField("user_email", "Email address:", VFORM_EMAIL, 
			array(
				"maxLength" => 255, 
				"required" => TRUE
			), 
			array(
				"maxLength" => "Your input is too long. A maximum of %s characters is OK.", 
				"required" => "This field is required.", 
				"type" => "Use the format name@domain.com"
			),
			array("default" => $a['user_email'])
			);
			$objPwd->addField("user_password_1","Password:", VFORM_PASSWORD,
				array(
					"required" => true
				),
				array(
					"type" => "This is not a valid e-mail address!",
					"required" => "Please fill in a password"
				)
			);
			$objPwd->addField("user_password_2","Repeat password:", VFORM_PASSWORD,
				array(
					"required" => true
				),
				array(
					"type" => "This is not a valid e-mail address!",
					"required" => "Please retype your password"
				)
			);
		}
		//*** Setting the main alert.
		//$objForm->setMainAlert("One or more errors occurred. Check the marked fields and try again.");
		$objForm->setSubmitLabel("Save");
		//*** Handling the form data.
		if($objForm->isSubmitted() && $objForm->isValid()){
			$strOutput   = '';
			if ($id == 0) {
				$id          = DB_Add_Admin($_REQUEST);
				$strOutput  .= ($id != 0) ? GHTML__Get_StatusMessage('success', 'Success', 'User was been created.') : '';
				$strOutput  .= JS_Goto_EditPage($m,$op,$id);
			} else {
				$id          = DB_Set_Admin($_REQUEST); // Field editing
				$strOutput  .= ($id != 0) ? GHTML__Get_StatusMessage('success', 'Success', 'User information has been updated.') : '';
				$strOutput  .= $objForm->toHtml(); // Form rendering
			}
		} else {
			$strOutput = $objForm->toHtml(); // Form rendering
		}
		echo $strOutput;
	// Link ad configuration user
	if ($id != 0) {
		echo '<table class="table table-vcenter">
		<tfoot>
			<tr><td>'.GHTML__Get_Ico('conf-edit', 'Config').' <a href="?m='.$m.'&op=ad_conf&id='.$id.'" title="Edit User Config">Edit User Config</a></td></tr>
		</tfoot>
		</table>';
	}
	echo '</div>
	</div>';
}


// LastUpdate 3.201111.29 [test]
function HTML_AdminConf($id=0) {
	GLOBAL $CONF, $aModules, $op, $m;
	//
	if ($id==0) {
		// ERRORE
	}
	$a = GDB__Get_Admin($id);
	$aUserModules = GDB__Get_AdminModules($id);
	$aModulesRest = GLOBAL_AdminModules_ArrayDiff($aModules, $aUserModules);
	$LOCAL_TITLE = 'User config :: '.$a['user_email'];
	

	echo '
		'.GHTML__Get_ModuleHeader($LOCAL_TITLE).'
		<div class="block">
			<div class="block-title">
				<h2>Assegnazione nuovi moduli</h2>
			</div>';
		
		if (count($aModulesRest) >= 1) {
			$objForm = new ValidForm("adminModule", NL);
			$objForm->addField("m",   "", VFORM_HIDDEN, array(), array(), array("hint" => $m));
			$objForm->addField("op",  "", VFORM_HIDDEN, array(), array(), array("hint" => $op));
			$objForm->addField("opp", "", VFORM_HIDDEN, array(), array(), array("default" => 'ad_conf_m_add'));
			$objForm->addField("id",  "", VFORM_HIDDEN, array(), array(), array("hint" => $id));
			$objModule = $objForm->addField("m_add","Module:",VFORM_SELECT_LIST,
				array("required"=>true),
				array("required"=>"Select module")
			);
			$objModule->addField("Select module","");
			foreach($aModulesRest as $tmpMod) {
				$objModule->addField($tmpMod['module_label'],$tmpMod['module_id']);
			}
			$objForm->setMainAlert("One or more errors occurred. Check the marked fields and try again.");
			$objForm->setSubmitLabel("Save");
			//*** Handling the form data.
			if($objForm->isSubmitted() && $objForm->isValid()){
				$strOutput  = $objForm->toHtml();      // Form rendering
				$strOutput  .= $objForm->removeMainError(); 
			}  else {
				$strOutput = $objForm->toHtml(); // Form rendering
			}
			echo $strOutput;


		} else {
			echo GHTML__Get_StatusMessage('info', 'Information', 'All modules have been configured for this user.');
		}

	echo '</div>';
	$aUserModules = Array();
	$aModulesRest = Array();
	$aUserModules = GDB__Get_AdminModules($id, false);
	$aModulesRest = GLOBAL_AdminModules_ArrayDiff($aModules, $aUserModules);

	?>
	<div class="block">
		<div class="block-title">
			<h2>Moduli assegnati</h2>
		</div>
		<table class="table table-vcenter">
		<thead>
			<tr class="tr_head">
				<th class="w40 ac">  </th>
				<th class="w200">Moduli Attivi per l'utente</th>
				<th>Filtro sulle operazioni <?php echo GHTML__Get_Ico('info', 'Separare le operazioni con la virgola (,)') ?></th>
				<th class="w30 ac"> - </th>
			</tr>
		</thead>
		<tbody>
			<?php 
			$tmpHtml  = '';
			$class = 'class="tr_pari"';
			foreach($aUserModules as $aTmpM){
				$class = ($class=='class="tr_pari"') ? 'class="tr_dispari"' : 'class="tr_pari"';
				$a_del = '<a href="?m='.$m.'&op=ad_conf&id='.$id.'&opp=ad_conf_m_del&m_del='.$aTmpM['module_id'].'">'.GHTML__Get_Ico('del').'</a>';
				$img_1    = ($aTmpM['module_open']==0) ? GHTML__Get_Ico('lock') : GHTML__Get_Ico('unlock');
				$tmpInfo  = "Id: ".$aTmpM['module_id']." | Weight: ".$aTmpM['module_weight'];
				$tmpHtml .= '<form name=""><tr '.$class.'>';
				$tmpHtml .= '<td>'.$img_1.'</td>';
				$tmpHtml .= '<td>'.GHTML__Get_Ico('info', $tmpInfo).' '.$aTmpM['module_label'].'</td>';
				if ($aTmpM['module_open']==0) {
				$tmpHtml .= '<td>'.
				            pager_getInputHidden('m',     $m).
				            pager_getInputHidden('op',    'ad_conf').
				            pager_getInputHidden('opp',   'ad_conf_m_set').
				            pager_getInputHidden('m_set', $aTmpM['module_id']).
				            pager_getInputHidden('id',    $id).
				            pager_getInputText('module_op_disable', $aTmpM['module_op_disable']);
                            $aTmpMConf = Array(0 => Array('id' => '0', 'op' => '', 'label' => 'select op for this module'));
                            include($CONF['path_modules'].'/'.$aTmpM['module_dir'].'/conf.php');
                            $aTmpMConf = array_merge($aTmpMConf, $LOCAL_CONF['module_op']);
                $tmpHtml .= ' '.pager_getSelect($aTmpMConf, 'op_disable', 'op', 'label', '');
                $tmpHtml .= '<button>'.GHTML__Get_Ico('ok-v').'</button></td>';
				} else {
					$tmpHtml .= '<td></td>';
					$a_del = '';
				}
				$tmpHtml .= '<td class="ac">'.$a_del.'</td>';
				$tmpHtml .= '</tr></form>';
			}
			echo $tmpHtml;
			?>
		</tbody>
		<tfoot>
			<tr><td colspan="7">
	<?php
	if ($id != 0) {
		echo '
		'.GHTML__Get_Ico('conf-edit', 'Config').' <a href="?m='.$m.'&op=ad_form&id='.$id.'" title="Edit user information">Edit user information</a>';
	}
	echo '</td></tr>
		</tfoot>
		</table>
	</div>';
}

function DB_Get_ModuleOp() {
}


function HTML_AdminSearch($aP) {
	GLOBAL $CONF, $aModules, $op, $m;

	$aArea  = DB_Get_GeoList('area');
	$aCity  = DB_Get_GeoList('city');
//
	$objForm = new ValidForm("adminFind", NL);
	$objForm->addField("m",          "", VFORM_HIDDEN, array(), array(), array("hint" => $m));
	$objForm->addField("op",         "", VFORM_HIDDEN, array(), array(), array("hint" => "ad_search"));
	$objForm->addField("country_id", "", VFORM_HIDDEN, array(), array(), array("hint" => "1"));
	// Personal data: name, surname
	$objForm->addField("user_name","Name: ",VFORM_STRING,
		array("maxLength" => 250),
		array("maxLength" => "Your last name is too long. Maximum of %s characters allowed.")
	);
	$objForm->addField("user_surname","Surname: ",VFORM_STRING,
		array("maxLength" => 250),
		array("maxLength" => "Your last surname is too long. Maximum of %s characters allowed.")
	);
	$objForm->addField("user_alias","Alias: ",VFORM_STRING,
		array("maxLength" => 250),
		array("maxLength" => "Your last alias is too long. Maximum of %s characters allowed.")
	);
	$objForm->addField("user_email", "Email address:", VFORM_EMAIL, 
    array("maxLength" => 255,), 
    array("maxLength" => "Your input is too long. A maximum of %s characters is OK.")
	);

	$objCountry = $objForm->addField("area_id","Area",VFORM_SELECT_LIST
			//array("required"=>true),
			//array("required"=>"Select your area")
	);
	$objCountry->addField("Seleziona una regione", '0');
	foreach($aArea as $area) {
		$objCountry->addField($area['area_name'], $area['area_id']);
	}

	$objCountry = $objForm->addField("city_id","City",VFORM_SELECT_LIST
			//array("required"=>true),
			//array("required"=>"Select your city")
	);
	$objCountry->addField("Seleziona una citt&agrave;", '0');
	foreach($aCity as $city) {
		$objCountry->addField($city['city_name'], $city['city_id']);
	}



	$objForm->setSubmitLabel("Search");
	if($objForm->isSubmitted() && $objForm->isValid()){
		$strOutput = $objForm->toHtml(); // Form rendering
		//$strOutput .= '</div>';
		//$strOutput .= HTML_AdminList($_REQUEST);
	} else {
		$strOutput = $objForm->toHtml(); // Form rendering
	}
	return  $strOutput;
}

function __HTML_AdminSearch($aP) {
	GLOBAL $CONF, $aModules, $op, $m;

	$LOCAL_TITLE = 'Find user';
	//
	//echo ''.GHTML__Get_ModuleHeader($LOCAL_TITLE).'
	echo '
		<div class="block">';
	$objForm = new ValidForm("adminFind", NL);
	$objForm->addField("m",  "", VFORM_HIDDEN, array(), array(), array("hint" => $m));
	$objForm->addField("op", "", VFORM_HIDDEN, array(), array(), array("hint" => $op));
	// Personal data: name, surname
	$objForm->addField("user_name","Name: ",VFORM_STRING,
			array("maxLength" => 250),
			array("maxLength" => "Your last name is too long. Maximum of %s characters allowed.")
	);
	$objForm->addField("user_surname","Surname: ",VFORM_STRING,
			array("maxLength" => 250),
			array("maxLength" => "Your last surname is too long. Maximum of %s characters allowed.")
	);
	$objForm->addField("user_alias","Alias: ",VFORM_STRING,
			array("maxLength" => 250),
			array("maxLength" => "Your last alias is too long. Maximum of %s characters allowed.")
	);
	$objForm->addField("user_email", "Email address:", VFORM_EMAIL,
			array("maxLength" => 255),
			array("maxLength" => "Your input is too long. A maximum of %s characters is OK.")
	);
	$objForm->setSubmitLabel("Find");
	if($objForm->isSubmitted() && $objForm->isValid()){
		$strOutput = $objForm->toHtml(); // Form rendering
		$strOutput .= '</div>';
		$strOutput .= HTML_AdminList($_REQUEST);
	} else {
		$strOutput = $objForm->toHtml(); // Form rendering
	}
	echo $strOutput;

	echo '</div>';
}



// LastUpdate 2014.11.20
function DB__Get_AllAdmin($q_where='', $q_order='', $q_limit_start=0, $q_limit_end='') {
	GLOBAL $CONF, $aModules, $op, $m;
	$CON = GDB__Get_CoreSession();
	$q_limit_end = ($q_limit_end=='') ? $CONF['def_page'] : $q_limit_end;
	$q_limit     = 'LIMIT '.$q_limit_start.','.$q_limit_end;
	$q_order     = ($q_order=='') ? 'ORDER BY user_id DESC' : $q_order;
	$aR = Array();
	$aR = DB__Get_allRecords($CON, DB_PREFIX.'core_admin', $q_where.' '.$q_order.' '.$q_limit, 0);
	return $aR;
}


// LastUpdate 2016.12.05
function DB__Get_CountUsers($aF=Array()) {
	GLOBAL $CONF;

	$aOut = Array();
	$qAdd = (isset($aF['user_name']) && $aF['user_name']!='')  ? ' AND user_name like "%'.$aF['user_name'].'%"'  : '';
	$q    = 'SELECT count(user_id) FROM '.DB_PREFIX.'core_admin WHERE user_id<>0 '.$qAdd;

	$CON    = GDB__Get_CoreSession();
	$r = mysqli_query($CON, $q) or LOG__Error("DB__Get_CountAdmin[]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		$tot     = mysqli_result($r, mysqli_affected_rows($CON), 0);
		$pageTot = ceil($tot/$CONF['def_page']);
		$aOut    = Array('totElements'=>$tot, 'totPages'=>$pageTot);
	} else {
		$aOut    = Array('totElements'=>$tot, 'totPages'=>1);
	}
	return $aOut;
}

// LastUpdate 2017.12.05
function DB__Get_Users($aF=Array()) {
	GLOBAL $CONF;

	$qLimitStart = ($aF['op_page']<=1) ? 0 : (($aF['op_page']-1)*$CONF['def_page']);
	$qLimitEnd   = $CONF['def_page'];

	$qAdd   = ($aF['user_name']!='')  ? ' AND user_name like "%'.$aF['user_name'].'%"'  : '';
	$qOrder = 'ORDER BY user_id DESC';
	$qLimit = 'LIMIT '.$qLimitStart.','.$qLimitEnd;
	$q      = 'SELECT * FROM '.DB_PREFIX.'core_admin WHERE user_id<>0 '.$qAdd.' '.$qOrder.' '.$qLimit;

	$CON       = GDB__Get_CoreSession();
	$aElements = DB__QueryN($CON, $q);

	return $aElements;
}





function DB_Add_AdminModule($module_id=0, $user_id=0) {
	$CON = GDB__Get_CoreSession();
	$OUT = false;
	if ($module_id != 0 && $user_id != 0) {
		$aTmpCheck = Array(); // controllo l'esistenza del record
		$aTmpCheck = DB__Get_infoRecord($CON, DB_PREFIX.'core_admin_modules', ' WHERE user_id="'.$user_id.'" AND module_id="'.$module_id.'" AND user_id<>0');
		if (!isset($aTmpCheck['module_id'])) {
			// inserisco record
				$q = "INSERT INTO `".DB_PREFIX.'core_admin_modules'."` (
			     `user_id`,
			     `module_id`,
			     `record_ip`,
			     `record_date`
			     ) VALUES (
			     \"".$user_id."\",
			     \"".$module_id."\",
			     \"".$_SERVER["REMOTE_ADDR"]."\",
			     NOW()
			     )";
			$r = mysqli_query($CON, $q) or LOG__Error("DB_Add_Admin", mysqli_error($CON)."\n".$q);
			//$OUT = mysqli_insert_id($CON);
			//print $q;
			$OUT = true;
			}
	}
	return $OUT;
}

function DB_Del_AdminModule($module_id=0, $user_id=0) {
	$OUT = false;
	if ($module_id != 0 && $user_id != 0) {
		$CON = GDB__Get_CoreSession();
		$q = 'DELETE FROM '.DB_PREFIX.'core_admin_modules WHERE module_id="'.$module_id.'" AND  user_id="'.$user_id.'"';
		$r = mysqli_query($CON, $q) or LOG__Error("DB_Del_AdminModule[]", mysqli_error($CON)."\n".$q);
	}
	return $OUT;
}


function DB_Set_AdminModule($module_id, $user_id, $module_op_disable) {
	$OUT = 0;
	//	
	$CON = GDB__Get_CoreSession();
	$q = 'UPDATE `'.DB_PREFIX.'core_admin_modules` SET 
	     `module_op_disable` ="'.$module_op_disable.'"
	     WHERE 
	     module_id="'.$module_id.'"
	     AND
	     user_id="'.$user_id.'"';
	$r = mysqli_query($CON, $q) or LOG__Error("DB_Set_Admin", mysqli_error($CON)."\n".$q);
	return $OUT;	
}



?>