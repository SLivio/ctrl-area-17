<?php /*
Version:     v7 2017.01.10
Module:      Zava16.PraticheAdmin
Author:      SLivio
*/



//--

$LOCAL_CONF = Array(
    'module_op' => Array(
        Array('id'    => '1',
            'label' => 'Pratiche Aperte',
            'op'    => 'op-list&pratica_status=2'),
        Array('id'  => '2',
            'label' => 'Tutte le Pratiche',
            'op'    => 'op-list'),
        Array('id'  => '3',
            'label' => 'Nuova Pratica',
            'op'    => 'op-new')
    		// http://localhost/GIT_ctrl-area-17/www/admin/index.php?m=14&op=op-list&pratica_status=2
    ),
    'module_label' => 'Gestisci Pratiche',
    'key' => 'm_100_Zava-PraticheAdmin',
    'menu' => Array(
        'visible' => true
    ),
    'local_js' => 'this.lib.js?'.get_RandomString(),
	'local_widget' => Array(
		0 => Array('op' => 'inbox', 'label' => 'Stato delle pratiche', 'class' => 'col-sm-9')
	),
);





?>