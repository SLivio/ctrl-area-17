<?php /*
Version:     v7 2017.01.10
Module:      Zava16.PraticheAdmin
Author:      SLivio
*/




//----------------------------------------------------------> [HTML]

// LastUpdate 2016.02.26
function DB__Get_Count_Elements($aF=Array()) {
	GLOBAL $CONF;
	
	$aOut = Array();
	
	$qAdd  = '';
	$qAdd .= (isset($aF['pratica_codice']) && $aF['pratica_codice']!='')  ? ' AND pratica_codice like "%'.$aF['pratica_codice'].'%"'  : '';
	$qAdd .= (isset($aF['pratica_anno']) && $aF['pratica_anno']!='')  ? ' AND pratica_anno like "%'.$aF['pratica_anno'].'%"'  : '';
	$qAdd .= (isset($aF['pratica_oggetto']) && $aF['pratica_oggetto']!='')  ? ' AND pratica_oggetto like "%'.$aF['pratica_oggetto'].'%"'  : '';
	$qAdd .= (isset($aF['pratica_status']) && $aF['pratica_status']!='')  ? ' AND pratica_status = "'.$aF['pratica_status'].'"'  : '';
	$qAdd .= (isset($aF['agenzia_id']) && $aF['agenzia_id']!='')  ? ' AND pratica_agenzia_id like "%'.$aF['agenzia_id'].'%"'  : '';
	$qAdd .= (isset($aF['collaboratore_nome']) && $aF['collaboratore_nome']!='')  ? ' AND collaboratore_nome like "%'.$aF['collaboratore_nome'].'%"'  : '';
	$qAdd .= (isset($aF['data_sinistro']) && $aF['data_sinistro']!='')  ? ' AND data_sinistro = "'.date("Y-m-d", strtotime($aF['data_sinistro'])).'"'  : '';
	
	// filtro per ricerche basate su tabelle correlate: notifiche, utenti coinvolti, ...
	$aCorrelate = Array(0=>'0'); 
	$qCorrelate = '';
	// filtro con le sole pratiche aperte con alert
	if($aF['filter_alert']==1) {
		$q    = 'SELECT distinct('.DB_PREFIX.'pratiche.pratica_id) FROM '.DB_PREFIX.'pratiche,'.DB_PREFIX.'pratiche_alert
				WHERE '.DB_PREFIX.'pratiche.pratica_id<>0 AND
				'.DB_PREFIX.'pratiche.pratica_id='.DB_PREFIX.'pratiche_alert.pratica_id ';
		$CON    = GDB__Get_CoreSession();
		$aEl    = DB__QueryN($CON, $q);
		if (count($aEl)>=1) {
			foreach($aEl as $key) {
				$aCorrelate[] = $key['pratica_id'];
			}
		}
	}
	// filtro con le sole pratiche aperte con notifiche
	if($aF['filter_notice']==1){
		$q    = 'SELECT distinct('.DB_PREFIX.'pratiche.pratica_id) FROM '.DB_PREFIX.'pratiche,'.DB_PREFIX.'pratiche_notifiche
				WHERE '.DB_PREFIX.'pratiche.pratica_id<>0 AND
				'.DB_PREFIX.'pratiche.pratica_id='.DB_PREFIX.'pratiche_notifiche.pratica_id ';
		$CON    = GDB__Get_CoreSession();
		$aEl    = DB__QueryN($CON, $q);
		if (count($aEl)>=1) {
			foreach($aEl as $key) {
				$aCorrelate[] = $key['pratica_id'];
			}
		}
	}
	// ricerca del conducente o del suo veicolo
	if ($aF['conduce_targa']!='' || $aF['conduce_nome']!='' || $aF['conduce_cognome']!='') {
		$q    = 'SELECT distinct('.DB_PREFIX.'pratiche.pratica_id) FROM '.DB_PREFIX.'pratiche,'.DB_PREFIX.'pratiche_conducenti 
				WHERE '.DB_PREFIX.'pratiche.pratica_id<>0 AND
					'.DB_PREFIX.'pratiche.pratica_id='.DB_PREFIX.'pratiche_conducenti.pratica_id ';
		if ($aF['conduce_targa']!='')   $q .= ' AND conduce_targa = "'.$aF['conduce_targa'].'"';
		if ($aF['conduce_nome']!='')    $q .= ' AND conduce_nome = "'.$aF['conduce_nome'].'"';
		if ($aF['conduce_cognome']!='') $q .= ' AND conduce_cognome = "'.$aF['conduce_cognome'].'"';
		$CON    = GDB__Get_CoreSession();
		$aEl    = DB__QueryN($CON, $q);
		if (count($aEl)>=1) {
			foreach($aEl as $key) {
				$aCorrelate[] = $key['pratica_id'];
			}
		}
	}
	$aCorrelate = array_unique($aCorrelate); // elimino i duplicati
	if(count($aCorrelate)>1){
		$qCorrelate = ' AND pratica_id IN ('.implode($aCorrelate, ',').') ';
	}
	
	$q      = 'SELECT count(pratica_id) FROM '.DB_PREFIX.'pratiche WHERE pratica_id<>0 '.$qAdd.$qCorrelate;
	$CON    = GDB__Get_CoreSession();
	$r = mysqli_query($CON, $q) or LOG__Error("DB__Get_Count_Elements[2]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		$tot     = mysqli_result($r, mysqli_affected_rows($CON), 0);
		$pageTot = ceil($tot/$CONF['def_page']);
		$aOut    = Array('totElements'=>$tot, 'totPages'=>$pageTot);
	} else {
		$aOut    = Array('totElements'=>$tot, 'totPages'=>1);
	}
	return $aOut;
}

// 2010
function DB_get_NumeroRiga($anno=0, $id=0) {
	GLOBAL $CONF;
	
	$tot = 0;
	$CON = GDB__Get_CoreSession();
	if ($id==0 && $anno!=0) {
		$q = 'SELECT count(pratica_id) FROM '.DB_PREFIX.'pratiche WHERE pratica_anno="'.$anno.'"';
		$r = mysqli_query($CON, $q) or LOG__Error("DB_get_NumeroRiga[1]", mysqli_error($CON)."\n".$q);
		if (mysqli_affected_rows($CON) >= 1) {
			$tot = mysqli_result($r, mysqli_affected_rows($CON), 0);
		} else { $tot = 'er1'; }
		$tot = ((int)$tot)+1;
	} else {
		$q      = 'SELECT count(pratica_id) FROM '.DB_PREFIX.'pratiche WHERE pratica_anno="'.$anno.'"  AND pratica_id <= "'.$id.'"';
		$r = mysqli_query($CON, $q) or LOG__Error("DB_get_NumeroRiga[2]", mysqli_error($CON)."\n".$q);
		if (mysqli_affected_rows($CON) >= 1) {
			$tot = mysqli_result($r, mysqli_affected_rows($CON), 0);
		} else { $tot = 'er2'; }
	}
	//$tot = $q;
	return $tot;
}



function DB__Get_CountPraticheAgenzia($agId=0) {
	$q      = 'SELECT count(pratica_id) FROM '.DB_PREFIX.'pratiche WHERE pratica_pratica_id='.(int)$agId;
	
	$CON    = GDB__Get_CoreSession();
	$r = mysqli_query($CON, $q) or LOG__Error("DB__Get_CountPraticheAgenzia[]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		$tot     = mysqli_result($r, mysqli_affected_rows($CON), 0);
	} else {
		$tot     = 0;
	}
	return $tot;
}

// LastUpdate 2017.02.06
function DB__Get_Agenzia($id=0) {
	GLOBAL $CONF;
	$aEl   = Array();
	$q     = 'SELECT * FROM '.DB_PREFIX.'pratiche_agenzie WHERE agenzia_id="'.$id.'"';
	$CON   = GDB__Get_CoreSession();
	$aEl   = DB__Query1($CON, $q);
	return $aEl;
}

// LastUpdate 2017.01.11
function DB__Get_Agenzie() {
	GLOBAL $CONF;
	$aEl   = Array();
	$q     = 'SELECT agenzia_id, agenzia_nome FROM '.DB_PREFIX.'pratiche_agenzie WHERE agenzia_id<>0 ORDER BY agenzia_nome ASC ';
	$CON   = GDB__Get_CoreSession();
	$aEl   = DB__QueryN($CON, $q);
	return $aEl;
}

// LastUpdate 2017.02.14
function DB__Get_Autorita($id=0) {
	GLOBAL $CONF;
	$aEl  = Array();
	$q    = 'SELECT * FROM '.DB_PREFIX.'pratiche_autorita 
			WHERE autorita_id="'.$id.'"';
	$CON  = GDB__Get_CoreSession();
	$aEl  = DB__Query1($CON, $q);
	return $aEl;	
}

// LastUpdate 2017.02.14
function DB__Get_AutoritaN() {
	GLOBAL $CONF;
	$aEl   = Array();
	$q     = 'SELECT autorita_id, autorita_nome FROM '.DB_PREFIX.'pratiche_autorita
			WHERE autorita_id<>0 ORDER BY autorita_nome ASC ';
	$CON   = GDB__Get_CoreSession();
	$aEl   = DB__QueryN($CON, $q);
	return $aEl;
}

// LastUpdate 2017.02.14
function DB__Get_Ispettorato($id=0) {
	GLOBAL $CONF;
	$aEl  = Array();
	$q    = 'SELECT * FROM '.DB_PREFIX.'pratiche_ispettorati
			WHERE ispettorato_id="'.$id.'"';
	$CON  = GDB__Get_CoreSession();
	$aEl  = DB__Query1($CON, $q);
	return $aEl;
}

// LastUpdate 2017.02.14
function DB__Get_Ispettorati() {
	GLOBAL $CONF;
	$aEl   = Array();
	$q     = 'SELECT ispettorato_id, ispettorato_nome FROM '.DB_PREFIX.'pratiche_ispettorati 
			WHERE ispettorato_id<>0 ORDER BY ispettorato_nome ASC ';
	$CON   = GDB__Get_CoreSession();
	$aEl   = DB__QueryN($CON, $q);
	return $aEl;
}

// LastUpdate 2017.02.14
function DB__Get_IspettoratiPerPratica($id=0) {
	GLOBAL $CONF;
	$q   = 'SELECT DISTINCT pratica_ispettorato FROM `'.DB_PREFIX.'pratiche`
			WHERE `pratica_pratica_id` = "'.(int)$id.'" AND pratica_id<>0; ';
	$CON = GDB__Get_CoreSession();
	$aEl = DB__QueryN($CON, $q);
	return $aEl;
}

// LastUpdate 2017.11.28
function DB__Get_Elements($aF=Array()) {
	GLOBAL $CONF;

	$qLimitStart = (!isset($aF['op_page']) || $aF['op_page']<=1) ? 0 : (($aF['op_page']-1)*$CONF['def_page']);
	$qLimitEnd   = $CONF['def_page'];

	$qAdd  = '';
	$qAdd .= (isset($aF['pratica_codice']) && $aF['pratica_codice']!='')  ? ' AND pratica_codice like "%'.$aF['pratica_codice'].'%"'  : '';
	$qAdd .= (isset($aF['pratica_anno']) && $aF['pratica_anno']!='')  ? ' AND pratica_anno like "%'.$aF['pratica_anno'].'%"'  : '';
	$qAdd .= (isset($aF['pratica_oggetto']) && $aF['pratica_oggetto']!='')  ? ' AND pratica_oggetto like "%'.$aF['pratica_oggetto'].'%"'  : '';
	$qAdd .= (isset($aF['pratica_status']) && $aF['pratica_status']!='')  ? ' AND pratica_status = "'.$aF['pratica_status'].'"'  : '';
	$qAdd .= (isset($aF['agenzia_id']) && $aF['agenzia_id']!='')  ? ' AND pratica_agenzia_id like "%'.$aF['agenzia_id'].'%"'  : '';
	$qAdd .= (isset($aF['collaboratore_nome']) && $aF['collaboratore_nome']!='')  ? ' AND collaboratore_nome like "%'.$aF['collaboratore_nome'].'%"'  : '';
	$qAdd .= (isset($aF['data_sinistro']) && $aF['data_sinistro']!='')  ? ' AND data_sinistro = "'.date("Y-m-d", strtotime($aF['data_sinistro'])).'"'  : '';
	
	
	// filtro per ricerche basate su tabelle correlate: notifiche, utenti coinvolti, ...
	$aCorrelate = Array(0=>'0');
	$qCorrelate = '';
	// filtro con le sole pratiche aperte con alert
	if($aF['filter_alert']==1) {
		$q    = 'SELECT distinct('.DB_PREFIX.'pratiche.pratica_id) FROM '.DB_PREFIX.'pratiche,'.DB_PREFIX.'pratiche_alert
				WHERE '.DB_PREFIX.'pratiche.pratica_id<>0 AND
				'.DB_PREFIX.'pratiche.pratica_id='.DB_PREFIX.'pratiche_alert.pratica_id ';
		$CON    = GDB__Get_CoreSession();
		$aEl    = DB__QueryN($CON, $q);
		if (count($aEl)>=1) {
			foreach($aEl as $key) {
				$aCorrelate[] = $key['pratica_id'];
			}
		}
	}
	// filtro con le sole pratiche aperte con notifiche
	if($aF['filter_notice']==1){
		$q    = 'SELECT distinct('.DB_PREFIX.'pratiche.pratica_id) FROM '.DB_PREFIX.'pratiche,'.DB_PREFIX.'pratiche_notifiche
				WHERE '.DB_PREFIX.'pratiche.pratica_id<>0 AND
				'.DB_PREFIX.'pratiche.pratica_id='.DB_PREFIX.'pratiche_notifiche.pratica_id ';
		$CON    = GDB__Get_CoreSession();
		$aEl    = DB__QueryN($CON, $q);
		if (count($aEl)>=1) {
			foreach($aEl as $key) {
				$aCorrelate[] = $key['pratica_id'];
			}
		}
	}
	// ricerca del conducente o del suo veicolo
	if ($aF['conduce_targa']!='' || $aF['conduce_nome']!='' || $aF['conduce_cognome']!='') {
		$q    = 'SELECT distinct('.DB_PREFIX.'pratiche.pratica_id) FROM '.DB_PREFIX.'pratiche,'.DB_PREFIX.'pratiche_conducenti
				WHERE '.DB_PREFIX.'pratiche.pratica_id<>0 AND
					'.DB_PREFIX.'pratiche.pratica_id='.DB_PREFIX.'pratiche_conducenti.pratica_id ';
		if ($aF['conduce_targa']!='')   $q .= ' AND conduce_targa = "'.$aF['conduce_targa'].'"';
		if ($aF['conduce_nome']!='')    $q .= ' AND conduce_nome = "'.$aF['conduce_nome'].'"';
		if ($aF['conduce_cognome']!='') $q .= ' AND conduce_cognome = "'.$aF['conduce_cognome'].'"';
		$CON    = GDB__Get_CoreSession();
		$aEl    = DB__QueryN($CON, $q);
		if (count($aEl)>=1) {
			foreach($aEl as $key) {
				$aCorrelate[] = $key['pratica_id'];
			}
		}
	}
	$aCorrelate = array_unique($aCorrelate); // elimino i duplicati
	if(count($aCorrelate)>1){
		$qCorrelate = ' AND pratica_id IN ('.implode($aCorrelate, ',').') ';
	}
	
	$qOrder = 'ORDER BY pratica_id DESC';
	$qLimit = 'LIMIT '.$qLimitStart.','.$qLimitEnd;
	$q      = 'SELECT * FROM '.DB_PREFIX.'pratiche WHERE pratica_id<>0 '.$qCorrelate.' '.$qAdd.' '.$qOrder.' '.$qLimit;

	$CON       = GDB__Get_CoreSession();
	$aElements = DB__QueryN($CON, $q);

	return $aElements;
}


// LastUpdate 2017.03.21
function DB__Get_SummaryNotice($aIds=Array()) {
	$aIds[] = 0;
	$q = 'SELECT pratica_id, count(pratica_id) as TOT, MAX(notifica_stato) as WHO_NOTICE
	FROM aa_pratiche_notifiche WHERE pratica_id IN ('.implode(',', $aIds).') GROUP BY pratica_id';
	$CON       = GDB__Get_CoreSession();
	$aElements = DB__QueryN($CON, $q);
	return $aElements;
}


// LastUpdate 2017.04.02
function DB__Get_Alert($pId=0) {
	GLOBAL $CONF;

	$q   = 'SELECT * FROM `'.DB_PREFIX.'pratiche_alert`
			WHERE pratica_id="'.(int)$pId.'" ORDER BY alert_id DESC LIMIT 0,1';
	$CON = GDB__Get_CoreSession();
	$aEl = DB__Query1($CON, $q);

	return $aEl;
}

// LastUpdate 2017.04.02
function DB__Get_Notifiche($pId=0) {
	GLOBAL $CONF;

	$q   = 'SELECT * FROM `'.DB_PREFIX.'pratiche_notifiche`
			WHERE pratica_id="'.(int)$pId.'" ORDER BY notifica_id ASC';
	$CON = GDB__Get_CoreSession();
	$aEl = DB__QueryN($CON, $q);

	return $aEl;
}


// LastUpdate 2017.01.11
function DB__Get_Contributors() {
	GLOBAL $CONF;

	$q      = 'SELECT DISTINCT collaboratore_nome
			FROM '.DB_PREFIX.'pratiche
			WHERE pratica_id<>0
					ORDER BY collaboratore_nome ASC ';

	$CON       = GDB__Get_CoreSession();
	$aElements = DB__QueryN($CON, $q);

	return $aElements;
}




// LastUpdate 2016.02.22
function DB__Get_Element($id=0) {
	if ($id==0) {
		$aR = Array(
				'pratica_id' => 0,
				'pratica_anno' => date('Y'),
				'pratica_agenzia' => '',
				'pratica_agenzia_id' => '',
				'pratica_codice' => '',
				'data_open' => date('Y-m-d'),
				//'data_open_it' => date('d-m-Y'),
				'data_close' => '',
				//'data_close_it' => '',
				'data_sinistro' => '',
				//'data_sinistro_it' => '',
				'pratica_status' => '2',
				'pratica_ispettorato' => '',
				'pratica_ispettorato_id' => '',
				'pratica_autorita_id' => '',
				'pratica_oggetto' => '',
				'pratica_indirizzo_via' => '',
				'pratica_indirizzo_num' => '',
				'pratica_indirizzo_cap' => '',
				'pratica_indirizzo_citta' => '',
				'pratica_indirizzo_prov' => '',
				'collaboratore_nome' => '',
				'collaboratore_data_open' => '',
				//'collaboratore_data_open_it' => '',
				'collaboratore_data_close' => '',
				//'collaboratore_data_close_it' => '',
				'pratica_note' => ''
				);
	} else {
		$CON = GDB__Get_CoreSession();
		$aR = Array();
		$aR = DB__Get_infoRecord($CON, DB_PREFIX.'pratiche', ' WHERE pratica_id="'.(int)$id.'" ', 0);

		//$aR['data_open_it']  = date("d-m-Y", strtotime($aR['data_open']));
		//$aR['data_close_it'] = date("d-m-Y", strtotime($aR['data_close']));

	}

	return $aR;
}


function DB__Get_ElementByCode($pCode='') {
	if ($pCode=='') {
		$aR = Array(
				'pratica_id' => 0,
				'pratica_anno' => date('Y'),
				'pratica_agenzia' => '',
				'pratica_agenzia_id' => '',
				'pratica_codice' => ''
				);
	} else {
		$CON = GDB__Get_CoreSession();
		$aR = Array();
		$aR = DB__Get_infoRecord($CON, DB_PREFIX.'pratiche', ' WHERE pratica_codice="'.trim($pCode).'" ', 0);
	}

	return $aR;
}



function DB__Validate_Element($action='set', $aP) {
	if($action=='set') {
		$aP['pratica_id'] = (isset($aP['pratica_id']) && $aP['pratica_id'] != '') ? $aP['pratica_id'] : 0;
		if ($aP['pratica_id']==0) {
			return false;
		}
	}

	//
	$aP['pratica_anno_id'] = (isset($aP['pratica_anno_id']) && $aP['pratica_anno_id'] != '') ? $aP['pratica_anno_id'] : '';
	if ($aP['pratica_anno_id']=='') return false;
	//
	$aP['pratica_codice'] = (isset($aP['pratica_codice']) && $aP['pratica_codice'] != '') ? $aP['pratica_codice'] : '';
	if ($aP['pratica_codice']=='') return false;
	// Se tutto ok
	return true;
}


function DB__Validate_UtenteCoinvolto($action='set', $aP) {

	//LOG__Error("LOG.Set", print_r($aP, 1));
	
	if($action=='set') {
		$aP['conduce_id'] = (isset($aP['conduce_id']) && $aP['conduce_id'] != '') ? $aP['conduce_id'] : 0;
		if ($aP['conduce_id']==0) {
			return false;
		}
	}
	
	if(!isset($aP['pratica_id'])) return false;

	//
	$aP['conduce_nome'] = (isset($aP['conduce_nome']) && $aP['conduce_nome'] != '') ? $aP['conduce_nome'] : '';
	if ($aP['conduce_nome']=='') return false;
	//
	$aP['conduce_cognome'] = (isset($aP['conduce_cognome']) && $aP['conduce_cognome'] != '') ? $aP['conduce_cognome'] : '';
	if ($aP['conduce_cognome']=='') return false;
	// Se tutto ok
	return true;
}


function DB__Validate_File($action='set', $aP) {

	//LOG__Error("LOG.Set", print_r($aP, 1));
	
	if($action=='set') {
		$aP['allegato_id'] = (isset($aP['allegato_id']) && $aP['allegato_id'] != '') ? $aP['allegato_id'] : 0;
		if ($aP['allegato_id']==0) {
			return false;
		}
	}
	
	if(!isset($aP['pratica_id'])) return false;

	//
	//$aP['allegato_file'] = (isset($aP['allegato_file']) && $aP['allegato_file'] != '') ? $aP['allegato_file'] : '';
	//if ($aP['allegato_file']=='') return false;
	// Se tutto ok
	return true;
}


function DB__Add_Element($aEl=Array()) {

	if(DB__Validate_Element('add', $aEl)) {
		$CON = GDB__Get_CoreSession();
		$q   = 'INSERT INTO `'.DB_PREFIX.'pratiche`
				(pratica_anno, pratica_anno_id, pratica_codice, `pratica_ip_creazione`, `pratica_data_creazione`)
				VALUES
				("'.$aEl['pratica_anno'].'", "'.$aEl['pratica_anno_id'].'", "'.$aEl['pratica_codice'].'", "'.$_SERVER["REMOTE_ADDR"].'", NOW())';
		$r   = mysqli_query($CON, $q) or LOG__Error("DB__AddElement", mysqli_error($CON)."\n".$q);
		$id  = mysqli_insert_id($CON);
		//
		$aEl['pratica_id'] = $id;
		if (mysqli_error($CON)=='') $OUT = DB__Set_Element($aEl);
		else                        $OUT = false;
		if($id==$OUT) return $id;
		else          return false;
	} else {
		return false;
	}
}


function DB__Add_UtenteCoinvolto($aEl=Array()) {

	//LOG__Error("LOG.Add", print_r($aEl, 1));
	
	if(DB__Validate_UtenteCoinvolto('add', $aEl)) {
		$CON = GDB__Get_CoreSession();
		$q   = 'INSERT INTO `'.DB_PREFIX.'pratiche_conducenti`
				(pratica_id, conduce_nome, conduce_cognome, conduce_ip_modifica, conduce_data_modifica)
				VALUES
				("'.$aEl['pratica_id'].'", "'.$aEl['conduce_nome'].'", "'.$aEl['conduce_cognome'].'", "'.$_SERVER["REMOTE_ADDR"].'", NOW())';
		$r   = mysqli_query($CON, $q) or LOG__Error("DB__Add_UtenteCoinvolto", mysqli_error($CON)."\n".$q);
		$cid  = mysqli_insert_id($CON);
		//
		$aEl['conduce_id'] = $cid;
		if (mysqli_error($CON)=='') $OUT = DB__Set_UtenteCoinvolto($aEl);
		else                        $OUT = false;
		if($cid==$OUT) return $cid;
		else          return false;
	} else {
		return 'er';
	}
}


function cleanFileName($str='') {
	$str  = strtolower($str);
	$str  = ucwords($str);
	$aIn  = Array(' ','à','è','é','ì','ò','ù','_','-',"'",'"');
	$aOut = Array('', 'a','e','e','i','o','u','', '', '', '');
	$str  = str_replace($aIn, $aOut, $str);
	return $str;
}


function DB__Add_File($aEl=Array(), $aFiles=Array()) {
	GLOBAL $CONF;

	LOG__Error("LOG.Add.aEl", print_r($aEl, 1));
	LOG__Error("LOG.Add.aFi", print_r($aFiles, 1));

	$OUT = false;
	
	if(DB__Validate_File('add', $aEl) && isset($aFiles['allegato_file']['name']) && $aFiles['allegato_file']['name']!='') {
		// nome file
		$path = APPLICATION_UPLOAD.$CONF['upload'];
		$name = date('Ymd_his_').cleanFileName($aFiles['allegato_file']['name']);
		
		//echo mime_content_type($aFiles['allegato_file']['tmp_name']);
		
		if(in_array(mime_content_type($aFiles['allegato_file']['tmp_name']), $CONF['accepted_mimetype'])) {
			if(move_uploaded_file($aFiles['allegato_file']['tmp_name'], $path.'/'.$name)) {
				$CON = GDB__Get_CoreSession();
				$q   = 'INSERT INTO `'.DB_PREFIX.'pratiche_allegati`
				(pratica_id, allegato_file, allegato_descrizione, allegato_weight, allegato_ip_creazione, allegato_data_creazione)
				VALUES
				("'.$aEl['pratica_id'].'", "'.$name.'", "'.$aEl['allegato_descrizione'].'", "'.$aFiles['allegato_file']['size'].'", "'.$_SERVER["REMOTE_ADDR"].'", NOW())';
				$r   = mysqli_query($CON, $q) or LOG__Error("DB__Add_File", mysqli_error($CON)."\n".$q);
				$fid = mysqli_insert_id($CON);
				//
				$aEl['allegato_id'] = $fid;
				if (mysqli_error($CON)=='') $OUT = DB__Set_File($aEl);
				if($fid!=$OUT) $OUT = false;
			}
		}
	}
	return $OUT;
}



// LastUpdate 2017.02.06
function DB__Add_Agenzia($aEl=Array()) {
	$CON = GDB__Get_CoreSession();
	$q   = 'INSERT INTO `'.DB_PREFIX.'pratiche_agenzie`
			(agenzia_nome, `agenzia_ip_creazione`, `agenzia_data_creazione`)
			VALUES
			("'.$aEl['pratica_agenzia'].'", "'.$_SERVER["REMOTE_ADDR"].'", NOW())';
	$r   = mysqli_query($CON, $q) or LOG__Error("DB__Add_Agenzia", mysqli_error($CON)."\n".$q);
	$id  = mysqli_insert_id($CON);
	return $id;
}


// LastUpdate 2017.02.14
function DB__Add_Autorita($aEl=Array()) {
	$CON = GDB__Get_CoreSession();
	$q   = 'INSERT INTO `'.DB_PREFIX.'pratiche_autorita`
			(autorita_nome, `autorita_ip_creazione`, `autorita_data_creazione`)
			VALUES
			("'.$aEl['pratica_autorita'].'", "'.$_SERVER["REMOTE_ADDR"].'", NOW())';
	$r   = mysqli_query($CON, $q) or LOG__Error("DB__Add_Autorita", mysqli_error($CON)."\n".$q);
	$id  = mysqli_insert_id($CON);
	return $id;
}


// LastUpdate 2017.02.14
function DB__Add_Ispettorato($aEl=Array()) {
	$CON = GDB__Get_CoreSession();
	$q   = 'INSERT INTO `'.DB_PREFIX.'pratiche_ispettorati`
			(ispettorato_nome, `ispettorato_ip_creazione`, `ispettorato_data_creazione`)
			VALUES
			("'.$aEl['pratica_ispettorato'].'", "'.$_SERVER["REMOTE_ADDR"].'", NOW())';
	$r   = mysqli_query($CON, $q) or LOG__Error("DB__Add_Ispettorato", mysqli_error($CON)."\n".$q);
	$id  = mysqli_insert_id($CON);
	return $id;
}


// LastUpdate 2017.02.08
function DB__Set_Element($aEl=Array()) {
	$CON = GDB__Get_CoreSession();
	//
	if(!DB__Validate_Element('set', $aEl)) return false;

	// Controllo agenzia.. se non c'è la aggiungo e prendo pratica_agenzia_id
	if(isset($aEl['pratica_agenzia_id']) && $aEl['pratica_agenzia_id']!='') {
		$aAgenzia = DB__Get_Agenzia($aEl['pratica_agenzia_id']);
		if(empty($aAgenzia)) {
			$aEl['pratica_agenzia']    = $aEl['pratica_agenzia_id'];
			$aEl['pratica_agenzia_id'] = DB__Add_Agenzia($aEl);
			$aAgenzia = DB__Get_Agenzia($aEl['pratica_agenzia_id']);
			$aEl['pratica_agenzia'] = $aAgenzia['agenzia_nome'];
		} else {
			$aEl['pratica_agenzia'] = $aAgenzia['agenzia_nome'];
		}
	} else {
		$aEl['pratica_agenzia']    = '';
		$aEl['pratica_agenzia_id'] = '';
	}
	
	// Controllo autorità.. se non c'è la aggiungo e prendo pratica_autorita_id
	if(isset($aEl['pratica_autorita_id']) && $aEl['pratica_autorita_id']!='') {
		$aAutorita = DB__Get_Autorita($aEl['pratica_autorita_id']);
		if(empty($aAutorita)) {
			$aEl['pratica_autorita']    = $aEl['pratica_autorita_id'];
			$aEl['pratica_autorita_id'] = DB__Add_Autorita($aEl);
			$aAutorita = DB__Get_Autorita($aEl['pratica_autorita_id']);
			$aEl['pratica_autorita'] = $aAutorita['autorita_nome'];
		} else {
			$aEl['pratica_autorita'] = $aAutorita['autorita_nome'];
		}
	} else {
		$aEl['pratica_autorita']    = '';
		$aEl['pratica_autorita_id'] = '';
	}
	
	// Controllo ispettorato.. se non c'è la aggiungo e prendo pratica_ispettorato_id
	if(isset($aEl['pratica_ispettorato_id']) && $aEl['pratica_ispettorato_id']!='') {
		$aIspettorato = DB__Get_Ispettorato($aEl['pratica_ispettorato_id']);
		if(empty($aIspettorato)) {
			$aEl['pratica_ispettorato']    = $aEl['pratica_ispettorato_id'];
			$aEl['pratica_ispettorato_id'] = DB__Add_Ispettorato($aEl);
			$aIspettorato = DB__Get_Ispettorato($aEl['pratica_ispettorato_id']);
			$aEl['pratica_ispettorato'] = $aIspettorato['ispettorato_nome'];
		} else {
			$aEl['pratica_ispettorato'] = $aIspettorato['ispettorato_nome'];
		}
	} else {
		$aEl['pratica_ispettorato']    = '';
		$aEl['pratica_ispettorato_id'] = '';
	}

	// date
	$aEl['data_open']  = (isset($aEl['data_open']) && $aEl['data_open']!='')   ? date("Y-m-d", strtotime($aEl['data_open']))  : '';
	$aEl['data_close'] = (isset($aEl['data_close']) && $aEl['data_close']!='') ? date("Y-m-d", strtotime($aEl['data_close'])) : '';
	$aEl['data_sinistro']            = (isset($aEl['data_sinistro'])            && $aEl['data_sinistro']!='')            ? date("Y-m-d", strtotime($aEl['data_sinistro'])) : '';
	$aEl['collaboratore_data_open']  = (isset($aEl['collaboratore_data_open'])  && $aEl['collaboratore_data_open']!='')  ? date("Y-m-d", strtotime($aEl['collaboratore_data_open'])) : '';
	$aEl['collaboratore_data_close'] = (isset($aEl['collaboratore_data_close']) && $aEl['collaboratore_data_close']!='') ? date("Y-m-d", strtotime($aEl['collaboratore_data_close'])) : '';

	$addQ = Array();

	// pratica indirizzo
	$addQ[] = ' `pratica_indirizzo_via`="'.$aEl['pratica_indirizzo_via'].'" ';
	$addQ[] = ' `pratica_indirizzo_num`="'.$aEl['pratica_indirizzo_num'].'" ';
	$addQ[] = ' `pratica_indirizzo_cap`="'.$aEl['pratica_indirizzo_cap'].'" ';
	$addQ[] = ' `pratica_indirizzo_citta`="'.$aEl['pratica_indirizzo_citta'].'" ';
	$addQ[] = ' `pratica_indirizzo_prov`="'.$aEl['pratica_indirizzo_prov'].'" ';

	// info pratica & sinistro
	$addQ[] = ' `pratica_oggetto`="'.$aEl['pratica_oggetto'].'" ';
	$addQ[] = ' `pratica_agenzia`="'.$aEl['pratica_agenzia'].'" ';
	$addQ[] = ' `pratica_agenzia_id`="'.$aEl['pratica_agenzia_id'].'" ';
	$addQ[] = ' `pratica_autorita_id`="'.$aEl['pratica_autorita_id'].'" ';
	$addQ[] = ' `pratica_codice`="'.$aEl['pratica_codice'].'" ';
	$addQ[] = ' `pratica_note`="'.$aEl['pratica_note'].'" ';
	$addQ[] = ' `data_open`="'.$aEl['data_open'].'" ';
	$addQ[] = ' `data_close`="'.$aEl['data_close'].'" ';
	$addQ[] = ' `pratica_status`="'.$aEl['pratica_status'].'" ';
	$addQ[] = ' `pratica_ispettorato`="'.$aEl['pratica_ispettorato'].'" ';
	$addQ[] = ' `pratica_ispettorato_id`="'.$aEl['pratica_ispettorato_id'].'" ';
	$addQ[] = ' `data_sinistro`="'.$aEl['data_sinistro'].'" ';

	// info collaboratore
	$addQ[] = ' `collaboratore_nome`="'.$aEl['collaboratore_nome'].'" ';
	$addQ[] = ' `collaboratore_data_open`="'.$aEl['collaboratore_data_open'].'" ';
	$addQ[] = ' `collaboratore_data_close`="'.$aEl['collaboratore_data_close'].'" ';

	//
	$q = 'UPDATE `'.DB_PREFIX.'pratiche` SET '.implode(', ', $addQ).',
	`pratica_data_modifica`   = NOW(),
	`pratica_ip_modifica`     = "'.$_SERVER["REMOTE_ADDR"]  .'"
	WHERE
	pratica_id="'.$aEl['pratica_id'].'"';

	$r = mysqli_query($CON, $q) or LOG__Error("DB__SetElement", mysqli_error($CON)."\n".$q);
	//
	if (mysqli_error($CON)=='') $OUT = $aEl['pratica_id'];
	else                        $OUT = false;
	//
	return $OUT;
}



// LastUpdate 2017.02.24
function DB__Set_UtenteCoinvolto($aEl=Array()) {
	$CON = GDB__Get_CoreSession();
	//
	if(!DB__Validate_UtenteCoinvolto('set', $aEl)) return false;

	$addQ   = Array();
	$addQ[] = ' `conduce_tipo`="'.$aEl['conduce_tipo'].'" ';
	$addQ[] = ' `conduce_nome`="'.$aEl['conduce_nome'].'" ';
	$addQ[] = ' `conduce_cognome`="'.$aEl['conduce_cognome'].'" ';
	$addQ[] = ' `conduce_intestatario`="'.$aEl['conduce_intestatario'].'" ';
	$addQ[] = ' `conduce_indirizzo`="'.$aEl['conduce_indirizzo'].'" ';
	$addQ[] = ' `conduce_citta`="'.$aEl['conduce_citta'].'" ';
	$addQ[] = ' `conduce_provincia`="'.$aEl['conduce_provincia'].'" ';
	$addQ[] = ' `conduce_telefono_fix`="'.$aEl['conduce_telefono_fix'].'" ';
	$addQ[] = ' `conduce_telefono_cel`="'.$aEl['conduce_telefono_cel'].'" ';
	$addQ[] = ' `conduce_targa`="'.$aEl['conduce_targa'].'" ';
	$addQ[] = ' `conduce_note`="'.txt2html($aEl['conduce_note']).'" ';
	
	//
	$q = 'UPDATE `'.DB_PREFIX.'pratiche_conducenti` SET '.implode(', ', $addQ).',
	`conduce_data_modifica`   = NOW(),
	`conduce_ip_modifica`     = "'.$_SERVER["REMOTE_ADDR"]  .'"
	WHERE
	conduce_id="'.$aEl['conduce_id'].'"';

	$r = mysqli_query($CON, $q) or LOG__Error("DB__SetElement", mysqli_error($CON)."\n".$q);
	//
	if (mysqli_error($CON)=='') $OUT = $aEl['conduce_id'];
	else                        $OUT = false;
	//
	return $OUT;
}


function DB__Get_SetNotice($pid=0, $nid=0, $status=0) {
	$OUT = false;

	if ($pid!=0 && $nid!=0) {
		$q = 'UPDATE `'.DB_PREFIX.'pratiche_notifiche` SET notifica_stato="'.(int)$status.'",
		`notifica_data_modifica`   = NOW(),
		`notifica_ip_modifica`     = "'.$_SERVER["REMOTE_ADDR"]  .'"
		WHERE
		pratica_id="'.(int)$pid.'" AND notifica_id="'.(int)$nid.'"';
		$CON = GDB__Get_CoreSession();
		$r = mysqli_query($CON, $q) or LOG__Error("DB__Get_SetNotice", mysqli_error($CON)."\n".$q);
		//
		if (mysqli_error($CON)=='') $OUT = $status;
	}
	return $OUT;
}



// LastUpdate 2017.02.24
function DB__Set_File($aEl=Array()) {
	$CON = GDB__Get_CoreSession();
	//
	if(!DB__Validate_File('set', $aEl)) return false;

	$addQ   = Array();
	$addQ[] = ' `allegato_descrizione`="'.$aEl['allegato_descrizione'].'" ';
	
	//
	$q = 'UPDATE `'.DB_PREFIX.'pratiche_allegati` SET '.implode(', ', $addQ).',
	`allegato_data_creazione`   = NOW(),
	`allegato_ip_creazione`     = "'.$_SERVER["REMOTE_ADDR"]  .'"
	WHERE
	allegato_id="'.$aEl['allegato_id'].'"';

	$r = mysqli_query($CON, $q) or LOG__Error("DB__SetElement", mysqli_error($CON)."\n".$q);
	//
	if (mysqli_error($CON)=='') $OUT = $aEl['allegato_id'];
	else                        $OUT = false;
	//
	return $OUT;
}


// LastUpdate v7 2014.11.20
function DB__Del_Element($id) {
	$CON = GDB__Get_CoreSession();
	$q = 'DELETE FROM `'.DB_PREFIX.'pratiche` WHERE pratica_id = "'.(int)$id.'"';
	$r = mysqli_query($CON, $q) or LOG__Error("DB__Del_Element[]", mysqli_error($CON)."\n".$q);
	if (mysqli_error($CON)=='') $OUT = true;
	else                        $OUT = mysqli_error($CON);
	return true;
}



// LastUpdate v7 2014.11.20
function DB__Del_UtenteCoinvolto($id) {
	$CON = GDB__Get_CoreSession();
	$q = 'DELETE FROM `'.DB_PREFIX.'pratiche_conducenti` WHERE conduce_id = "'.(int)$id.'"';
	$r = mysqli_query($CON, $q) or LOG__Error("DB__Del_UtenteCoinvolto[]", mysqli_error($CON)."\n".$q);
	if (mysqli_error($CON)=='') $OUT = true;
	else                        $OUT = mysqli_error($CON);
	return true;
}

// LastUpdate v7 2014.11.20
function DB__Del_File($id) {
	$CON = GDB__Get_CoreSession();
	$q = 'DELETE FROM `'.DB_PREFIX.'pratiche_allegati` WHERE allegato_id = "'.(int)$id.'"';
	$r = mysqli_query($CON, $q) or LOG__Error("DB__Del_File[]", mysqli_error($CON)."\n".$q);
	if (mysqli_error($CON)=='') $OUT = true;
	else                        $OUT = mysqli_error($CON);
	return true;
}


// LastUpdate 7.2017.12.05
function HTML__Pagination_Bak($aFilter=Array(), $aCounts=Array()) {
	GLOBAL $m, $CONF, $op;

	$HTMLP = '<form class="pull-right"><select onChange="gotoPage(this.value)">';
	$aTmpFilter = $aFilter;
	unset($aTmpFilter['op_page']);
	unset($aTmpFilter['op_order']);

	$query = http_build_query(array_filter($aTmpFilter));
	for($i=1;$i<=$aCounts['totPages'];$i++) {
		$class  =  (isset($aFilter['op_page']) && $aFilter['op_page']==$i) ? ' selected="selected"' : '';
		$link   =  '?m='.$m.'&op='.$op.'&op_page='.$i.'&'.$query.'&order='.$aFilter['op_order'];
		$HTMLP .= '<option '.$class.' value="'.$link.'">'.$i.'</a></li>';
	}
	$HTMLP .= '</select></form>';

	$HTML = '<div class="row">
			<div class="col-sm-6"><span><strong>'.$aCounts['totElements'].'</strong> risultati</span></div>
			<div class="col-sm-6">'.$HTMLP.'</div>
		</div>';

	return $HTML;
}



// LastUpdate 7.2017.12.05
function HTML__Pagination($aFilter=Array(), $aCounts=Array()) {
	GLOBAL $m, $CONF, $op;

	$aTmpFilter = $aFilter;
	unset($aTmpFilter['op_page']);
	unset($aTmpFilter['op_order']);
	
	$aData1 = array_filter($aTmpFilter);
	$aData2 = Array(
			'm'  => $m,
			'op' => $op,
			'order' => trim($aFilter['op_order'])
		);
	$aData = array_merge($aData1, $aData2);
	
	$rand  = get_RandomString(5);
	$link  = '';
	
	$HTMLP = '<form class="pull-right" id="'.$rand.'" method="get" action="'.$link.'">';

	foreach($aData as $k => $v) {
		$HTMLP .= '<input type="hidden" name="'.$k.'" value="'.$v.'">';
	}
	$HTMLP .= 'Pagina <input type="text" name="op_page" class="page" value="'.((isset($aFilter['op_page']) && $aFilter['op_page']!=0) ? (int)$aFilter['op_page']: 1).'" size="4"> di '.$aCounts['totPages'];


	$HTMLP .= '</form>';

	$HTML = '<div class="row">
			<div class="col-sm-6"><span><strong>'.$aCounts['totElements'].'</strong> risultati</span></div>
			<div class="col-sm-6">'.$HTMLP.'</div>
		</div>';

	return $HTML.$link;
}


// LastUpdate 7.2017.12.05
function HTML__List($aFilter=Array(), $aElements=Array(), $aCounts=Array()) {
	GLOBAL $m, $CONF, $op;
	
	$HTML  = '';
	$HTMLP = HTML__Pagination($aFilter, $aCounts);
	
	
	$HTML .= '
	<div class="block">
		<div class="block-title">
			<div class="block-options pull-right" data-toggle="tooltip" data-original-title="Cerca">
				<a class="btn btn-alt btn-sm btn-default" title="" href="?app=true&m='.$m.'&op=op-search&'.http_build_query($aFilter).'"  data-toggle="modal" data-target="#modal" ><i class="hi hi-search"></i></a>
			</div>
			<div class="block-options pull-right" data-toggle="tooltip" data-original-title="Filtra pratiche con notifiche">
				<a class="btn btn-alt btn-sm btn-default" title="" href="?m='.$m.'&op=op-list&filter_notice=1"><i class="fa fa-envelope-o"></i></a>
			</div>
			<div class="block-options pull-right" data-toggle="tooltip" data-original-title="Filtra pratiche con alert attivi">
				<a class="btn btn-alt btn-sm btn-default" title="" href="?m='.$m.'&op=op-list&filter_alert=1"><i class="fa fa-bullhorn"></i></a>
			</div>
						
			<h2>Elenco Compagnie</h2>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog"><div class="modal-content"></div><!-- /.modal-content --></div>
		</div>

		<div class="table-responsive">
			<table class="table table-striped table-vcenter table-bordered dataTable table-hover">
			<tr><td colspan="9">'.$HTMLP.'</td></tr>
			<thead>
				<tr class="tr_head">
					<th>Anno</th>
					<th>ID</th>
					<th>Oggetto</th>
					<th>Informazioni</th>
					<th colspan="5" class="w90 text-center">Azioni</th>
				</tr>
			</thead>
			<tbody>';

	foreach($aElements as $el) {
		$id = $el['pratica_id'];
		
		$tr_id  = 'tr_'.$id;
		
		if ($el['pratica_status'] == 1) {
			$status = '<i class="fa fa-flag" data-toggle="tooltip" title="" data-original-title="Pratica archiviata"></i>';
		} else if ($el['pratica_status'] == 2) {
			$status = '<i class="fa fa-flag text-success" data-toggle="tooltip" title="" data-original-title="Pratica aperta"></i>';
		} else if ($el['pratica_status'] == 3) {
			$status = '<i class="fa fa-flag text-warning" data-toggle="tooltip" title="" data-original-title="Pratica sollecitata"></i>';
		} else {
			$status = '<i class="fa fa-flag text-muted" data-toggle="tooltip" title="" data-original-title="Pratica annullata"></i>';
		}
		
		$a_info   = '<a href="?m='.$m.'&op=op-details&id='.$id.'&app=true" title="Preview"  data-toggle="modal" data-target="#modal" >'.GHTML__Get_Ico('zoom-in', 'Vedi').'</a>';
		$a_edit   = '<a href="?m='.$m.'&op=op-edit&id='.$id.'" title="Modifica"  >'.GHTML__Get_Ico('edit', 'Modifica').'</a>';
		$a_del    = '<a href="javascript:elDel('.$m.', '.$id.')" title="Delete">'.GHTML__Get_Ico('delete', 'Cancella').'</a>';
		$a_notice = '<a href="?m='.$m.'&op=op-notice&id='.$id.'&app=true" title="Notifiche" data-toggle="modal" data-target="#modal" class="text-muted msg-read-btn notice" id="notice_'.$id.'" data-ctrl-id="'.$id.'" data-ctrl-m="'.$m.'"><i class="hi hi-envelope"></i></a>';
		$HTML .= NL.'<tr id="'.$tr_id.'">
					<td>'.$el['pratica_anno'].'</td>
					<td title="'.$id.'">'.$el['pratica_anno_id'].'</td>
					<td>'.$el['pratica_oggetto'].'</td>
					<td>
						<small>
							Compagnia: '.$el['pratica_agenzia'].'<br>
							Ispettorato: '.$el['pratica_ispettorato'].'<br>
							Protocollo: '.$el['pratica_codice'].'<br>
							Ultimo aggiornamento: '.date("d/m/Y H:i:s", strtotime($el['pratica_data_modifica'])).'
						</small>
						'.(isset($el['agenzia_telefono']) && ($el['agenzia_telefono']!='') ? '<i class="fa fa-phone" data-toggle="tooltip" title="" data-original-title="Telefono"></i> '.$el['agenzia_telefono'].'<br>' : '').'
						'.(isset($el['agenzia_fax']) && ($el['agenzia_fax']!='') ? '<i class="fa fa-print" data-toggle="tooltip" title="" data-original-title="Fax"></i> '.$el['agenzia_fax'].'<br>' : '').'
					</td>
					<td class="text-center actions">'.$status.' '.$el['pratica_status'].'</td>
					<td class="text-center actions">'.$a_notice.'</td>
					<td class="text-center actions">'.$a_info.'</td>
					<td class="text-center actions">'.$a_edit.'</td>
					<td class="text-center actions">'.$a_del.'</td>
				</tr>';
	}
	$HTML .= '</tbody>
			<tfoot><td colspan="9">'.$HTMLP.'</td></tfoot>
			</table>
			<br>
			</div>
		</div>';

	return $HTML;
}

function HTML__ModalDetails_SchedaUtente($aUt=Array()) {
	$HTML = '<table width="100%">';
	if(count($aUt>=1)) {
		$i = 1;
		foreach($aUt as $ut) {
			
			if($i<count($aUt)) $style = 'style="border-bottom:1px solid #efefef; padding-bottom:8px;"';
			else               $style = '';
			
			$HTML .= '
				<tr><td><i class="fa fa-user"></i> '.$ut['conduce_cognome'].' '.$ut['conduce_nome'].' '.((isset($ut['conduce_intestatario']) && $ut['conduce_intestatario']!='') ? '  // Intestatario: '.$ut['conduce_intestatario'] : '').'</td></tr>
				<tr><td><i class="gi gi-car"></i> '.$ut['conduce_targa'].'</td></tr>
				<tr><td>
					'.(($ut['conduce_indirizzo']!='')  ? '<i class="hi hi-home"></i> '.$ut['conduce_indirizzo'] : '').'
					'.(($ut['conduce_citta']!='')      ? ' '.$ut['conduce_citta'] : '').'
					'.(($ut['conduce_provincia']!='')  ? ' '.$ut['conduce_provincia'].'<br>' : '').'
											
					'.(($ut['conduce_telefono_fix']!='') ? '<i class="gi gi-iphone"></i> '.$ut['conduce_telefono_fix'].'<br>' : '').'
					'.(($ut['conduce_telefono_cel']!='') ? '<i class="gi gi-iphone"></i> '.$ut['conduce_telefono_cel'].'<br>' : '').'
				</td></tr>
				<tr><td><div '.$style.'>
					'.(($ut['conduce_note']!='') ? '<i class="hi hi-list-alt"></i> '.$ut['conduce_note'].'<br>' : '').'
				</div></td></tr>';
			$i++;
			
		}
	}
	$HTML .= '</table>';
	return $HTML;
}


// LastUpdate 2017.01.10
function HTML__ModalDetails($title='', $aEl=Array()) {
	GLOBAL $m, $CONF;

	$aPratica     = DB__Get_Element($aEl['pratica_id']);
	$aAgenzia     = DB__Get_Agenzia($aEl['pratica_agenzia_id']);
	$aAutorita    = DB__Get_Autorita($aEl['pratica_autorita_id']);
	$aAssicurati  = DB__Get_UtentiCoinvolti($aEl['pratica_id'], 1);
	$aControparte = DB__Get_UtentiCoinvolti($aEl['pratica_id'], 0);
	$aTestimoni   = DB__Get_UtentiCoinvolti($aEl['pratica_id'], 2);

	$HTMLassicurati  = HTML__ModalDetails_SchedaUtente($aAssicurati);
	$HTMLcontroparte = HTML__ModalDetails_SchedaUtente($aControparte);
	$HTMLtestimoni   = HTML__ModalDetails_SchedaUtente($aTestimoni);

	if ($aPratica['pratica_status'] == 1) {
		$status = '<i class="fa fa-flag" data-toggle="tooltip" title="" data-original-title="Pratica archiviata"></i>';
		$stato  = 'Pratica archiviata';
	} else if ($aPratica['pratica_status'] == 2) {
		$status = '<i class="fa fa-flag text-success" data-toggle="tooltip" title="" data-original-title="Pratica aperta"></i>';
		$stato  = 'Pratica aperta';
	} else if ($aPratica['pratica_status'] == 3) {
		$status = '<i class="fa fa-flag text-warning" data-toggle="tooltip" title="" data-original-title="Pratica sollecitata"></i>';
		$stato  = 'Pratica sollecitata';
	} else {
		$status = '<i class="fa fa-flag text-muted" data-toggle="tooltip" title="" data-original-title="Pratica annullata"></i>';
		$stato  = 'Pratica annullata';
	}

	$HTML ='<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<title>'.$title.'</title>
	<script>
	$(document).on("hidden.bs.modal", function (e) {
	    $(e.target).removeData("bs.modal").find(".modal-content").empty();
	});
	</script>
</head>
<body>
	<div class="modal-header text-center">
		<h2 class="modal-title"><i class="fa fa-search"></i> '.$title.' <strong>'.$aEl['pratica_id'].'</strong></h2>
	</div><!-- /modal-header -->
	<div class="modal-body">
		<div class="table-responsive">
			<div class="block">
				<div class="block-title">
					<h2>Informazioni sulla pratica</h2>
				</div>
				<dl class="dl-horizontal">
					<dt>Anno</dt>
                    <dd>'.$aEl['pratica_anno'].'</dd>

                    <dt>Stato</dt>
                    <dd>'.$stato.' '.$status.'</dd>

                    <dt>Data apertura pratica</dt>
                    <dd>'.(($aEl['data_open'] != '0000-00-00' && $aEl['data_open'] != '') ? date("d/m/Y", strtotime($aEl['data_open'])) : 'N.D.').'</dd>

                    <dt>Data chiusura pratica</dt>
                    <dd>'.(($aEl['data_close'] != '0000-00-00' && $aEl['data_close'] != '') ? date("d/m/Y", strtotime($aEl['data_close'])) : 'N.D.').'</dd>

                    <dt>Protocollo</dt>
                    <dd>'.(($aEl['pratica_codice'] != '') ? $aEl['pratica_codice'] : 'N.D.').'</dd>

				</dl>
			</div>


			<div class="block">
				<div class="block-title">
					<h2>Informazioni sul sinistro</h2>
				</div>
				<dl class="dl-horizontal">
					<dt>Data sinistro</dt>
                    <dd>'.(($aEl['data_sinistro'] != '0000-00-00' && $aEl['data_sinistro'] != '') ? date("d/m/Y", strtotime($aEl['data_sinistro'])) : 'N.D.').'</dd>
			
                    <dt>Compagnia</dt>
                    <dd>'.(isset($aAgenzia['agenzia_nome']) && $aAgenzia['agenzia_nome'] !='' ? $aAgenzia['agenzia_nome'] : 'N.D.').'</dd>

					<dt>Ispettorato</dt>
                    <dd>'.(isset($aIspettorato['ispettorato_nome']) && $aIspettorato['ispettorato_nome'] !='' ? $aIspettorato['ispettorato_nome'] : 'N.D.').'</dd>

                    <dt>Oggetto</dt>
                    <dd>'.(($aEl['pratica_oggetto'] != '') ? $aEl['pratica_oggetto'] : 'N.D.').'</dd>

                    <dt>Autorità</dt>
                    <dd>'.(isset($aAutorita['autorita_nome']) && $aAutorita['autorita_nome'] !='' ? $aAutorita['autorita_nome'] : 'N.D.').'</dd>

				</dl>
			</div>

			<div class="block">
				<div class="block-title">
					<h2>Informazioni sul collaboratore</h2>
				</div>
				<dl class="dl-horizontal">
					<dt>Collaboratore</dt>
                    <dd>'.(($aEl['collaboratore_nome'] == $aEl['collaboratore_nome']) ? $aEl['collaboratore_nome'] : 'N.D.').'</dd>

                    <dt>Data inizio collaboratore</dt>
                    <dd>'.(($aEl['collaboratore_data_open'] != '0000-00-00' && $aEl['collaboratore_data_open'] != '') ? date("d/m/Y", strtotime($aEl['collaboratore_data_open'])) : 'N.D.').'</dd>

                    <dt>Data fine collaboratore</dt>
                    <dd>'.(($aEl['collaboratore_data_close'] != '0000-00-00' && $aEl['collaboratore_data_close'] != '') ? date("d/m/Y", strtotime($aEl['collaboratore_data_close'])) : 'N.D.').'</dd>

				</dl>
			</div>';

	if(count($aAssicurati)>=1) {
		$HTML .= '<table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
				<tbody>
					<tr><th colspan="2" style="background-color: #f9f9f9;">Assicurato/i</th></tr>
					<tr>
						<td width="20%">Info</td>
						<td>'.$HTMLassicurati.'</td>
					</tr>
				</tbody>
			</table>';
	}

	if(count($aControparte)>=1) {
		$HTML .= '<table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
				<tbody>
					<tr><th colspan="2" style="background-color: #f9f9f9;">Controparte</th></tr>
					<tr>
						<td width="20%">Info</td>
						<td>'.$HTMLcontroparte.'</td>
					</tr>
				</tbody>
			</table>';
	}

	if(count($aTestimoni)>=1) {
		$HTML .= '<table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
				<tbody>
					<tr><th colspan="2" style="background-color: #f9f9f9;">Testimoni</th></tr>
					<tr>
						<td width="20%">Info</td>
						<td>'.$HTMLtestimoni.'</td>
					</tr>
				</tbody>
			</table>';
	}

	$HTML .= '
		</div><!-- /modal-body -->
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	</div><!-- /modal-footer -->
</body>
</html>';
	return $HTML;
}


// LastUpdate 2017.02.22
function HTML__ModalUser($id=0, $title='', $aEl=Array()) {
	GLOBAL $m, $CONF;

	$cId                  = $aEl['conduce_id'];
	$pratica_id           = $aEl['pratica_id'];
	$conduce_tipo         = $aEl['conduce_tipo'];
	$conduce_nome         = $aEl['conduce_nome'];
	$conduce_cognome      = $aEl['conduce_cognome'];
	$conduce_intestatario = $aEl['conduce_intestatario'];
	$conduce_indirizzo    = $aEl['conduce_indirizzo'];
	$conduce_citta        = $aEl['conduce_citta'];
	$conduce_provincia    = $aEl['conduce_provincia'];
	$conduce_telefono_fix = $aEl['conduce_telefono_fix'];
	$conduce_telefono_cel = $aEl['conduce_telefono_cel'];
	$conduce_targa        = $aEl['conduce_targa'];
	$conduce_note         = html2txt($aEl['conduce_note']);

	$uStati = get_UserType();
	$HTMLusrtype  = '<select name="conduce_tipo" class="select-chosen">
			<option value="">Seleziona una voce</option>';
	foreach($uStati as $aS) {
		if($cId!=0) $sel = ($conduce_tipo==$aS[0]) ? 'selected="selected"' : '';
		else        $sel = '';
		$HTMLusrtype .= '<option value="'.$aS[0].'" '.$sel.'>'.$aS[1].'</option>';
	}
	$HTMLusrtype .= '</select>';


	$HTML ='<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<title>'.$title.'</title>
	<script>
	$(document).on("hidden.bs.modal", function (e) {
		$(e.target).removeData("bs.modal").find(".modal-content").empty();
	});

		
	$("#submit").click(function(){
    	$.ajax({
            url: "index.php?m='.$m.'&app=true", //this is the submit URL
            type: "POST",
            data: $("#formUtente").serialize(),
            success: function(data){
            	//alert("successfully submitted")
            	$("#cid").attr("value", data.cid);
    
            	var action = $("body").find("form#formPratica").attr("action")+"#tab-coinvolti"
				$("body").find("form#formPratica").attr("action", action);
            	$("body").find("form#formPratica").submit();

            }
        });
	});
	</script>
</head>
<body>
	<div class="modal-header text-center">
		<h2 class="modal-title"><i class="fa fa-search"></i> '.$title.' <strong></strong></h2>
	</div><!-- /modal-header -->

	<form action="?m='.$m.'" method="post" enctype="multipart/form-data" class="form-horizontal" id="formUtente">
		<div class="modal-body">

			<input type="hidden" name="op"  value="op-user-add">
			<input type="hidden" id="cid" name="cid" value="'.$cId.'">
			<input type="hidden" id="id"  name="id" value="'.$id.'">

			<div class="form-group">
				<label class="col-md-4 control-label" for="conduce_cognome">Tipo di utente</label>
				<div class="col-md-8">
					'.$HTMLusrtype.'
				</div>
			</div>
		
			<div class="form-group">
				<label class="col-md-4 control-label" for="conduce_cognome">Utente (Cognome Nome)</label>
				<div class="col-md-4">
					<input type="text" id="conduce_cognome" name="conduce_cognome" class="form-control" placeholder="Es: Rossi" value="'.$conduce_cognome.'">
				</div>
				<div class="col-md-4">
					<input type="text" id="conduce_nome" name="conduce_nome" class="form-control" placeholder="Es: Mario" value="'.$conduce_nome.'">
				</div>
			</div>
		
			<div class="form-group">
				<label class="col-md-4 control-label" for="conduce_intestatario">Intestatario</label>
				<div class="col-md-4">
					<input type="text" id="conduce_intestatario" name="conduce_intestatario" class="form-control" placeholder="Es: Verdi Antonio" value="'.$conduce_intestatario.'">
				</div>
				<div class="col-md-4">
				</div>
			</div>
		
			<div class="form-group">
				<label class="col-md-4 control-label" for="conduce_indirizzo">Indirizzo</label>
				<div class="col-md-3">
					<input type="text" id="conduce_indirizzo" name="conduce_indirizzo" class="form-control" placeholder="Es: Via XXIV Maggio" value="'.$conduce_indirizzo.'">
				</div>
				<div class="col-md-3">
					<input type="text" id="conduce_citta" name="conduce_citta" class="form-control" placeholder="Es: Frascati" value="'.$conduce_citta.'">
				</div>
				<div class="col-md-2">
					<input type="text" id="conduce_provincia" name="conduce_provincia" class="form-control" placeholder="Es: RM" value="'.$conduce_provincia.'">
				</div>
			</div>
		
			<div class="form-group">
				<label class="col-md-4 control-label" for="conduce_telefono_fix">Telefono (fisso e cell)</label>
				<div class="col-md-4">
					<input type="text" id="conduce_telefono_fix" name="conduce_telefono_fix" class="form-control" placeholder="Es: 06 4567 3210" value="'.$conduce_telefono_fix.'">
				</div>
				<div class="col-md-4">
					<input type="text" id="conduce_telefono_cel" name="conduce_telefono_cel" class="form-control" placeholder="Es: 320 1234 567" value="'.$conduce_telefono_cel.'">
				</div>
			</div>
		
			<div class="form-group">
				<label class="col-md-4 control-label" for="conduce_telefono_fix">Targa</label>
				<div class="col-md-4">
					<input type="text" id="conduce_targa" name="conduce_targa" class="form-control" placeholder="Es: AB123AA" value="'.$conduce_targa.'">
				</div>
				<div class="col-md-4">
				</div>
			</div>
		
			<div class="form-group">
				<label class="col-md-4 control-label" for="conduce_telefono_fix">Note</label>
				<div class="col-md-8">
					<textarea class="form-control ckeditor" name="conduce_note">'.$conduce_note.'</textarea>
				</div>
			</div>


		</div><!-- /modal-body -->
		<div class="modal-footer">
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
				<button type="button" class="btn btn-primary" id="submit">Salva</button>
			</div>
		</div><!-- /modal-footer -->
	</form>
</body>
</html>';
	return $HTML;
}

function get_AdminNameFromAdminID($aAdmin=Array(), $id=0) {
	foreach($aAdmin as $admin) {
		if ($admin['user_id']==$id) return $admin;
	}
}

// LastUpdate 2017.02.22
function HTML__ModalNotice($id=0, $title='', $aEl=Array(), $aNotifiche=Array()) {
	GLOBAL $m, $CONF;

	$aAdmin = GDB__Get_AllAdmin(); // tutti gli admin
	$uId    = GLOBAL_GetUser('user_id'); // l'admin loggato
	
	$aAlert = DB__Get_Alert($id);
	$ALERT  = '';
	if (count($aAlert)>=1) {
		$ALERT .= '<tr style="background-color:#ffeeee;">
					<td><span class="label label-danger">ALERT</span></td>
					<td><strong>Messaggio:</strong><br>'.$aAlert['alert_messaggio'].'</td>
					<td colspan="2">
						<em>Scadenza:</em><br>
						'.date("d/m/Y H:i", strtotime($aAlert['alert_data_notifica'])).'</td>
					<td><a href="javascript:void(0)" class="msg-fav-btn"><i class="fa fa-trash-o"></i></a></td>
				</tr>';
	} else {
		$ALERT .= '<tr style="background-color:#ffeeee">
					<td><span class="label label-danger">ALERT</span></td>
					<td>
						<strong>Messaggio:</strong><br>
						<textarea class="form-control" name="notifica_messaggio"> Non attivo </textarea></td>
					<td>
						<em>Scadenza:</em><br>
						'.$aEl['alert_data_notifica'].'</td>
					<td><button type="submit" class="btn btn-primary">Invia</button></td>
				</tr>';
	}
	
	$MSG = '';
	$c   = 1;
	$ico = '';
	foreach($aNotifiche as $n) {
		$aFrom = get_AdminNameFromAdminID($aAdmin, $n['user_id_from']);
		$aTo   = get_AdminNameFromAdminID($aAdmin, $n['user_id_to']);
		$class = '';
		switch($n['notifica_stato']) {
			case '0' :
				$class = "text-muted";
				$ico   = "fa-star-o";
				break;
			case '1' :
				$class = "text-warning";
				$ico   = "fa-star";
				break;
			case '2' :
				$class = "text-success";
				$ico   = "fa-star";
				break;
		}
		// star
		$star = '';
		if($uId==$n['user_id_from']){
			$data = 'm='.$m.'&app=true&op=set-notice-1&pid='.$id.'&nid='.$n['notifica_id'].'&uid='.$uId;
			$star = '<a href="javascript:GlobalSend_Notice(\'#n'.$n['notifica_id'].'\', \''.GLB_qsCrypt($data).'\')" id="n'.$n['notifica_id'].'" class="'.$class.' msg-fav-btn"><i class="fa '.$ico.'"></i></a>';
			$star = '<a href="javascript:GlobalSend_Notice(\'#n'.$n['notifica_id'].'\', \''.($data).'\')" id="n'.$n['notifica_id'].'" class="'.$class.' msg-fav-btn"><i class="fa '.$ico.'"></i></a>';
			//$star = '<a href="javascript:alert(\''.$data.'\')" class="'.$class.' msg-fav-btn"><i class="fa '.$ico.'"></i></a>';
		}
		// del
		$del = '';
		if(count($aNotifiche)<=$c && $uId==$n['user_id_from'] && $n['notifica_stato']!=2)
			$del = '<a href="javascript:void(0)" class="msg-fav-btn"><i class="fa fa-trash-o"></i></a>';
		
			$MSG .= '<tr>
				<td><em class="label label-info">'.$aFrom['user_name'].':</em></td>
				<td>
					'.$n['notifica_messaggio'].'
				</td>
				<td><em>'.date("d/m/Y H:i", strtotime($n['notifica_data_creazione'])).'</em></td>
				<td class="text-center" style="width: 20px;">'.$star.'</td>
                <td class="text-center" style="width: 20px;">'.$del.'</td>
			</tr>';
		$c++;
	}
	
	$HTML ='<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<title>'.$title.'</title>
	<script>
	$(document).on("hidden.bs.modal", function (e) {
		$(e.target).removeData("bs.modal").find(".modal-content").empty();
	});
		
	$("#submit").click(function(){
    	$.ajax({
            url: "index.php?m='.$m.'&app=true", //this is the submit URL
            type: "POST",
            data: $("#formUtente").serialize(),
            success: function(data){
            	//alert("successfully submitted")
            	$("#cid").attr("value", data.cid);
    
            	var action = $("body").find("form#formPratica").attr("action")+"#tab-coinvolti"
				$("body").find("form#formPratica").attr("action", action);
            	$("body").find("form#formPratica").submit();

            }
        });
	});
	</script>
</head>
<body>
	<div class="modal-header text-center">
		<h2 class="modal-title"><i class="fa fa-search"></i> '.$title.' <strong></strong></h2>
	</div><!-- /modal-header -->

	<div class="table-responsive">
		<table class="table table-hover table-vcenter">
			<thead>
				'.$ALERT.'
				<tr style="background-color:#ddecec;">
					<td><span class="label label-warning">Note</span></td>
					<td colspan="4">'.$aEl['pratica_note'].'</td>
				</tr>
			</thead>
			<tbody>
				'.$MSG.'
			</tbody>
		</table>
	</div>
	<form action="?m='.$m.'" method="post" enctype="multipart/form-data" class="form-horizontal" id="formUtente">
		<div class="modal-body">

			<input type="hidden" name="op"  value="op-notice-add">
			<input type="hidden" id="id"  name="id" value="'.$id.'">

			
			<div class="form-group">
				<label class="col-sm-2 control-label" for="notifica_messaggio">Commenta</label>
				<div class="col-sm-8">
					<textarea class="form-control" name="notifica_messaggio"> Non attivo </textarea>
				</div>
				<div class="col-sm-2">
					<button type="submit" class="btn btn-primary">Invia</button>
				</div>
			</div>


		</div><!-- /modal-body -->
		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="CheckNotice()">Chiudi</button>
		</div><!-- /modal-footer -->
	</form>
</body>
</html>';
	return $HTML;
}




// LastUpdate 2017.02.22
function HTML__ModalFile($id=0, $title='', $aEl=Array()) {
	GLOBAL $m, $CONF, $SESSION_ID;

	$fid                  = $aEl['allegato_id'];
	$pratica_id           = $aEl['pratica_id'];
	$allegato_descrizione = $aEl['allegato_descrizione'];
	$allegato_file        = $aEl['allegato_file'];

	$aFiles = DB__Get_Files($id);

	$HTML ='<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<title>'.$title.'</title>
	<script>
	$(document).on("hidden.bs.modal", function (e) {
		$(e.target).removeData("bs.modal").find(".modal-content").empty();
	});
	
			
	$(function() {
	
		$("#submit").click(function(){
	    	var action = $("body").find("form#formAllegato").attr("action")+"#tab-allegati";
			$("body").find("form#formAllegato").attr("action", action);
			$("body").find("form#formAllegato").submit();
		});

	});
	
	</script>
</head>
<body>
	<div class="modal-header text-center">
		<h2 class="modal-title"><i class="fa fa-search"></i> '.$title.' <strong></strong></h2>
	</div><!-- /modal-header -->

	<form action="?m='.$m.'&id='.$id.'" method="post" enctype="multipart/form-data" class="form-horizontal" id="formAllegato">
		<div class="modal-body">

			<input type="hidden" name="op"  value="op-file-add">
			<input type="hidden" id="fid" name="fid" value="'.$fid.'">
			<input type="hidden" id="id" name="id" value="'.$id.'">
		
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="conduce_cognome">File</label>
				<div class="col-md-9">
					'.(($fid!='' && $fid!=0) ? $allegato_file : '<input type="file" id="allegato_file" name="allegato_file" class="form-control">').'
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label" for="allegato_descrizione">Descrizione</label>
				<div class="col-md-9">
					<input type="text" id="allegato_descrizione" name="allegato_descrizione" class="form-control" value="'.$allegato_descrizione.'">
				</div>
			</div>
							

		</div><!-- /modal-body -->
		<div class="modal-footer">
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
				<button type="submit" class="btn btn-primary" id="submit">Salva</button>
			</div>
		</div><!-- /modal-footer -->
	</form>
</body>
</html>';
	return $HTML;
}

// LastUpdate 2017.02.14
function HTML__FormUtenti($id=0, $aAssicurati=Array(), $aControparte=Array(), $aTestimoni=Array()) {
	GLOBAL $m;

	if (!isset($id) || $id==0) return false;

	$HTML = '<div class="table-responsive">
			<table id="general-table" class="table table-striped table-vcenter table-borderless">
				<thead>
					<tr>
						<th style="width: 80px;" class="text-center">Tipo</th>
						<th>Nome e Cognome</th>
						<th>Intestatario</th>
						<th>Targa</th>
						<th>Contatti</th>
						<th colspan="2" class="text-center">Azioni</th>
					</tr>
				</thead>
				<tbody>';

	foreach($aAssicurati as $ut) {
		$actions = '
			<td class="text-center"><a href="?m='.$m.'&op=op-user&id='.$id.'&cid='.$ut['conduce_id'].'&app=true&'.get_RandomString(10).'" title="Modifica" data-toggle="modal" data-target="#modal"><i data-toggle="tooltip" title="" class="fa fa-pencil" data-original-title="Modifica"></i></a></td>
			<td class="text-center"><a href="javascript:uDel('.$m.', '.$ut['conduce_id'].')" title="Delete"><i data-toggle="tooltip" title="" class="fa fa-trash-o" data-original-title="Cancella"></i></a></td>';
		$HTML .= '<tr class="tr_'.$ut['conduce_id'].'">
				<td><a href="javascript:void(0)" class="label label-warning">Assicurato</a></td>
				<td>'.$ut['conduce_cognome'].' '.$ut['conduce_nome'].'</td>
				<td>'.$ut['conduce_intestatario'].'</td>
				<td>'.$ut['conduce_targa'].'</td>
				<td><small>
					'.$ut['conduce_indirizzo'].'<br>
					'.$ut['conduce_telefono_fix'].'<br>
					'.$ut['conduce_telefono_cel'].'
				</small></td>
				'.$actions.'
			</tr>';
	}
	foreach($aControparte as $ut) {
		$actions = '
			<td class="text-center"><a href="?m='.$m.'&op=op-user&id='.$id.'&cid='.$ut['conduce_id'].'&app=true&'.get_RandomString(10).'" title="Modifica" data-toggle="modal" data-target="#modal"><i data-toggle="tooltip" title="" class="fa fa-pencil" data-original-title="Modifica"></i></a></td>
			<td class="text-center"><a href="javascript:uDel('.$m.', '.$ut['conduce_id'].')" title="Delete"><i data-toggle="tooltip" title="" class="fa fa-trash-o" data-original-title="Cancella"></i></a></td>';
		$HTML .= '<tr class="tr_'.$ut['conduce_id'].'">
				<td><a href="javascript:void(0)" class="label label-info">Controparte</a></td>
				<td>'.$ut['conduce_cognome'].' '.$ut['conduce_nome'].'</td>
				<td>'.$ut['conduce_intestatario'].'</td>
				<td>'.$ut['conduce_targa'].'</td>
				<td><small>
					'.$ut['conduce_indirizzo'].'<br>
					'.$ut['conduce_telefono_fix'].'<br>
					'.$ut['conduce_telefono_cel'].'
				</small></td>
				'.$actions.'
			</tr>';
	}
	foreach($aTestimoni as $ut) {
		$actions = '
			<td class="text-center"><a href="?m='.$m.'&op=op-user&id='.$id.'&cid='.$ut['conduce_id'].'&app=true&'.get_RandomString(10).'" title="Modifica" data-toggle="modal" data-target="#modal"><i data-toggle="tooltip" title="" class="fa fa-pencil" data-original-title="Modifica"></i></a></td>
			<td class="text-center"><a href="javascript:uDel('.$m.', '.$ut['conduce_id'].')" title="Delete"><i data-toggle="tooltip" title="" class="fa fa-trash-o" data-original-title="Cancella"></i></a></td>';
		$HTML .= '<tr class="tr_'.$ut['conduce_id'].'">
				<td><a href="javascript:void(0)" class="label label-success">Testimone</a></td>
				<td>'.$ut['conduce_cognome'].' '.$ut['conduce_nome'].'</td>
				<td>'.$ut['conduce_intestatario'].'</td>
				<td>'.$ut['conduce_targa'].'</td>
				<td><small>
					'.$ut['conduce_indirizzo'].'<br>
					'.$ut['conduce_telefono_fix'].'<br>
					'.$ut['conduce_telefono_cel'].'
				</small></td>
				'.$actions.'
			</tr>';
	}

	$HTML .= '<tr>
			<td colspan="6"></td>
			<td colspan="2"  class="text-center">
				<a href="?m='.$m.'&op=op-user&id='.$id.'&cid=0&app=true" class="btn btn-alt btn-xs btn-primary btn-option btn_savePratica" data-toggle="modal" data-target="#modal"><i class="fa fa-user"></i> Aggiungi</a>
			</td>
		</tr>
		</tbody>
	</table>
	</div>';

	return $HTML;
}


// LastUpdate 2017.02.14
function HTML__FormFiles($id=0, $aFiles=Array()) {
	GLOBAL $m, $CONF;

	if (!isset($id) || $id==0) return false;

	$HTML = '<div class="table-responsive">
			<table id="general-table" class="table table-striped table-vcenter table-borderless">
				<thead>
					<tr>
						<th style="width: 80px;" class="text-center">Tipo</th>
						<th>File</th>
						<th>Descrizione</th>
						<th>Peso</th>
						<th colspan="2" class="text-center">Azioni</th>
					</tr>
				</thead>
				<tbody>';

	foreach($aFiles as $el) {
		$actions = '
			<td class="text-center"><a href="?m='.$m.'&op=op-file&id='.$id.'&fid='.$el['allegato_id'].'&app=true&'.get_RandomString(10).'" title="Modifica" data-toggle="modal" data-target="#modal"><i data-toggle="tooltip" title="" class="fa fa-pencil" data-original-title="Modifica"></i></a></td>
			<td class="text-center"><a href="javascript:fDel('.$m.', '.$el['allegato_id'].')" title="Delete"><i data-toggle="tooltip" title="" class="fa fa-trash-o" data-original-title="Cancella"></i></a></td>';
		$HTML .= '<tr class="tr_'.$el['allegato_id'].'">
				<td><i class="fa-2x fi fi-doc"></i></td>
				<td><a href="'.APPLICATION_UPLOAD.$CONF['upload'].$el['allegato_file'].'" target="_blank">'.$el['allegato_file'].'</a></td>
				<td>'.$el['allegato_descrizione'].'</td>
				<td>'.$el['allegato_weight'].'</td>
				'.$actions.'
			</tr>';
	}

	$HTML .= '<tr>
			<td colspan="4"></td>
			<td colspan="2"  class="text-center">
				<a href="?m='.$m.'&op=op-file&id='.$id.'&fid=0&app=true" class="btn btn-alt btn-xs btn-primary btn-option btn_savePratica" data-toggle="modal" data-target="#modal"><i class="gi gi-paperclip"></i> Aggiungi</a>
			</td>
		</tr>
		</tbody>
	</table>
	</div>';

	return $HTML;
}


// LastUpdate 2017.01.10
function HTML__ModalDetails2($title='', $aEl=Array()) {
	GLOBAL $m, $CONF;

	$aPratica     = DB__Get_Element($aEl['pratica_id']);
	$aAssicurati  = DB__Get_UtentiCoinvolti($aEl['pratica_id'], 1);
	$aControparte = DB__Get_UtentiCoinvolti($aEl['pratica_id'], 0);
	$aTestimoni   = DB__Get_UtentiCoinvolti($aEl['pratica_id'], 2);

	$HTMLassicurato  = HTML__ModalDetails_SchedaUtente($aAssicurati);
	$HTMLcontroparte = HTML__ModalDetails_SchedaUtente($aControparte);
	$HTMLtestimoni   = HTML__ModalDetails_SchedaUtente($aTestimoni);

	
	
	$aEl['pratica_agenzia_id'] = isset($aEl['pratica_agenzia_id']) ? $aEl['pratica_agenzia_id'] : 0;
	$aAg = DB__Get_Agenzia($aEl['pratica_agenzia_id']);
	//$aAg['agenzia_nome']       = isset($aAg['agenzia_nome']) ? $aAg['agenzia_nome'] : 'N.D.';
	if(empty($aAg)) $aAg['agenzia_nome'] = 'N.D.';
	
	
	if ($aPratica['pratica_status'] == 1) {
		$status = '<i class="fa fa-flag" data-toggle="tooltip" title="" data-original-title="Pratica archiviata"></i>';
		$stato  = 'Pratica archiviata';
	} else if ($aPratica['pratica_status'] == 2) {
		$status = '<i class="fa fa-flag text-success" data-toggle="tooltip" title="" data-original-title="Pratica aperta"></i>';
		$stato  = 'Pratica aperta';
	} else if ($aPratica['pratica_status'] == 3) {
		$status = '<i class="fa fa-flag text-warning" data-toggle="tooltip" title="" data-original-title="Pratica sollecitata"></i>';
		$stato  = 'Pratica sollecitata';
	} else {
		$status = '<i class="fa fa-flag text-muted" data-toggle="tooltip" title="" data-original-title="Pratica annullata"></i>';
		$stato  = 'Pratica annullata';
	}

	
	$HTML ='
	<div class="modal-body">
		<div class="table-responsive">
			<div class="block">
				<div class="block-title">
					<h2>Informazioni sulla pratica</h2>
				</div>
				<dl class="dl-horizontal">
					<dt>Anno</dt>
                    <dd>'.$aEl['pratica_anno'].'</dd>

                    <dt>Stato</dt>
                    <dd>'.$stato.' '.$status.'</dd>

                    <dt>Compagnia</dt>
                    <dd>'.$aAg['agenzia_nome'].'</dd>

                    <dt>Data apertura pratica</dt>
                    <dd>'.(($aEl['data_open'] != '0000-00-00' && $aEl['data_open'] != '') ? date("d/m/Y", strtotime($aEl['data_open'])) : 'N.D.').'</dd>

                    <dt>Data chiusura pratica</dt>
                    <dd>'.(($aEl['data_close'] != '0000-00-00' && $aEl['data_close'] != '') ? date("d/m/Y", strtotime($aEl['data_close'])) : 'N.D.').'</dd>

                    <dt>Protocollo</dt>
                    <dd>'.(($aEl['pratica_codice'] != '') ? $aEl['pratica_codice'] : 'N.D.').'</dd>

				</dl>
			</div>


			<div class="block">
				<div class="block-title">
					<h2>Informazioni sul sinistro</h2>
				</div>
				<dl class="dl-horizontal">
					<dt>Data sinistro</dt>
                    <dd>'.(($aEl['data_sinistro'] != '0000-00-00' && $aEl['data_sinistro'] != '') ? date("d/m/Y", strtotime($aEl['data_sinistro'])) : 'N.D.').'</dd>

                    <dt>Ispettorato</dt>
                    <dd>'.(isset($aEl['pratica_ispettorato']) && ($aEl['pratica_ispettorato'] != '') ? $aEl['pratica_ispettorato'] : 'N.D.').'</dd>

                    <dt>Oggetto</dt>
                    <dd>'.(isset($aEl['pratica_oggetto']) && ($aEl['pratica_oggetto'] != '') ? $aEl['pratica_oggetto'] : 'N.D.').'</dd>

                    <dt>Autorità</dt>
                    <dd>'.(isset($aEl['pratica_autorita']) && ($aEl['pratica_autorita'] != '') ? $aEl['pratica_autorita'] : 'N.D.').'</dd>

				</dl>
			</div>

			<div class="block">
				<div class="block-title">
					<h2>Informazioni sul collaboratore</h2>
				</div>
				<dl class="dl-horizontal">
					<dt>Collaboratore</dt>
                    <dd>'.(($aEl['collaboratore_nome'] == $aEl['collaboratore_nome']) ? $aEl['collaboratore_nome'] : 'N.D.').'</dd>

                    <dt>Data inizio collaboratore</dt>
                    <dd>'.(($aEl['collaboratore_data_open'] != '0000-00-00' && $aEl['collaboratore_data_open'] != '') ? date("d/m/Y", strtotime($aEl['collaboratore_data_open'])) : 'N.D.').'</dd>

                    <dt>Data fine collaboratore</dt>
                    <dd>'.(($aEl['collaboratore_data_close'] != '0000-00-00' && $aEl['collaboratore_data_close'] != '') ? date("d/m/Y", strtotime($aEl['collaboratore_data_close'])) : 'N.D.').'</dd>

				</dl>
			</div>';

	if(count($aAssicurati)>=1) {
		$HTML .= '<table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
				<tbody>
					<tr><th colspan="2" style="background-color: #f9f9f9;">Assicurato/i</th></tr>
					<tr>
						<td width="20%">Info</td>
						<td>'.$HTMLassicurato.'</td>
					</tr>
				</tbody>
			</table>';
	}

	if(count($aControparte)>=1) {
		$HTML .= '<table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
				<tbody>
					<tr><th colspan="2" style="background-color: #f9f9f9;">Controparte</th></tr>
					<tr>
						<td width="20%">Info</td>
						<td>'.$HTMLcontroparte.'</td>
					</tr>
				</tbody>
			</table>';
	}

	if(count($aTestimoni)>=1) {
		$HTML .= '<table id="example-datatable" class="table table-vcenter table-condensed table-bordered">
				<tbody>
					<tr><th colspan="2" style="background-color: #f9f9f9;">Testimoni</th></tr>
					<tr>
						<td width="20%">Info</td>
						<td>'.$HTMLtestimoni.'</td>
					</tr>
				</tbody>
			</table>';
	}

	$HTML .= '
		</div><!-- /modal-body -->';
	return $HTML;
}


// LastUpdate 2016.02.22
function HTML__ModalSearch($title='', $aP=Array()) {
	GLOBAL $m;
	
	// Select Anni
	$HTMLyears = '<select id="pratica_anno" name="pratica_anno" class="select-chosen" data-placeholder="..." >
			<option value="">...</option>';
	for($i=date('Y'); $i>=2006; $i--) {
		$selected   = (isset($aP['pratica_anno']) && (int)$aP['pratica_anno']==$i) ? 'selected' : '';
		$HTMLyears .= '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
	}
	$HTMLyears .= '</select>';
	
	// Select Collaboratori
	$aContributors = DB__Get_Contributors();
	$HTMLcontributors  = '<select id="collaboratore_nome" name="collaboratore_nome" class="select-chosen" data-placeholder="..." >
			<option value="">...</option>';
	foreach($aContributors as $contributor) {
		$HTMLcontributors .= '<option value="'.$contributor['collaboratore_nome'].'">'.ucfirst($contributor['collaboratore_nome']).'</option>';
	}
	$HTMLcontributors .= '</select>';
	
	// Select Status
	$aStatus    = get_PraticaStatus();
	$HTMLstatus = '<select id="pratica_status" name="pratica_status" class="select-chosen" data-placeholder="..." >
			<option value="">...</option>';
	foreach($aStatus as $status) {
		$HTMLstatus .= '<option value="'.$status[0].'">'.$status[1].'</option>';
	}
	$HTMLstatus .= '</select>';
	
	// Select Compagnie
	$aCompanies = DB__Get_Agenzie();
	$HTMLcompanies  = '<select id="agenzia_id" name="agenzia_id" class="select-chosen" data-placeholder="..." >
			<option value="">...</option>';
	foreach($aCompanies as $company) {
		$selected   = (isset($aP['agenzia_id']) && (int)$aP['agenzia_id']==$company['agenzia_id']) ? 'selected' : '';
		$HTMLcompanies .= '<option value="'.$company['agenzia_id'].'" '.$selected.'>'.($company['agenzia_nome']).'</option>';
	}
	$HTMLcompanies .= '</select>';
	
	
	$HTML ='<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<title>'.$title.'</title>
	<script>
	$("body").on("hidden.bs.modal", ".modal", function () {
	    $(this).removeData("bs.modal");
	});
	</script>
	<script src="js/app.js"></script>
</head>
<body>
	<div class="modal-header text-center">
		<h2 class="modal-title"><i class="fa fa-search"></i> '.$title.'</h2>
	</div><!-- /modal-header --><!-- /modal-header -->
	<div class="modal-body">
		<form action="?m='.$m.'" method="post" enctype="multipart/form-data" class="form-horizontal">
		<input type="hidden" name="op"  value="op-list">
			
			<div class="form-group">
				<label class="col-md-4 control-label" for="pratica_codice">Numero di sinistro</label>
				<div class="col-md-8">
					<input type="text" id="pratica_codice" name="pratica_codice" class="form-control" placeholder="">
				</div>
			</div>
				
			<div class="form-group">
				<label class="col-md-4 control-label" for="pratica_anno">Anno della pratica</label>
				<div class="col-md-8">
					'.$HTMLyears.'
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-4 control-label" for="data_sinistro">Data del sinistro</label>
				<div class="col-md-8">
					<input type="text" id="data_sinistro" name="data_sinistro" class="form-control input-datepicker-close" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"> 
				</div>
			</div>
				
			<div class="form-group">
				<label class="col-md-4 control-label" for="pratica_oggetto">Oggetto</label>
				<div class="col-md-8">
					<input type="text" id="pratica_oggetto" name="pratica_oggetto" class="form-control" placeholder="">
				</div>
			</div>
				
			<div class="form-group">
				<label class="col-md-4 control-label" for="agenzia_id">Compagnia</label>
				<div class="col-md-8">
					'.$HTMLcompanies.'
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-4 control-label" for="pratica_status">Stato</label>
				<div class="col-md-8">
					'.$HTMLstatus.'
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-4 control-label" for="collaboratore_nome">Collaboratore</label>
				<div class="col-md-8">
					'.$HTMLcontributors.'
				</div>
			</div>
				
			<div class="form-group">
				<label class="col-md-4 control-label" for="conduce_targa">Targa Utente</label>
				<div class="col-md-8">
					<input type="text" id="conduce_targa" name="conduce_targa" class="form-control" placeholder="Es: AA123BC">
				</div>
			</div>
				
			<div class="form-group">
				<label class="col-md-4 control-label" for="conduce_cognome">Utente (Cognome Nome)</label>
				<div class="col-md-4">
					<input type="text" id="conduce_cognome" name="conduce_cognome" class="form-control" placeholder="Es: Rossi">
				</div>
				<div class="col-md-4">
					<input type="text" id="conduce_nome" name="conduce_nome" class="form-control" placeholder="Es: Mario">
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Search</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div><!-- /modal-footer -->
		</form>
	</div><!-- /modal-body -->
</body>
</html>';
	return $HTML;
}



// LastUpdate 2016.12.05
function DB__Get_CountIspettorati($compName=0) {
	GLOBAL $CONF;

	$out  = 0;
	$q    = 'SELECT count(pratica_id) FROM '.DB_PREFIX.'pratiche WHERE pratica_ispettorato="'.$compName.'"';

	$CON    = GDB__Get_CoreSession();
	$r = mysqli_query($CON, $q) or LOG__Error("DB__Get_CountAdmin[]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		$out = mysqli_result($r, mysqli_affected_rows($CON), 0);
	}
	return $out;
}


// LastUpdate 2017.01.19
function DB__Get_UtentiCoinvolti($pId=0, $typeId=0) {
	GLOBAL $CONF;

	$q   = 'SELECT * FROM `'.DB_PREFIX.'pratiche_conducenti`
			WHERE pratica_id="'.(int)$pId.'" AND conduce_tipo="'.(int)$typeId.'" ORDER BY conduce_id ASC';
	$CON = GDB__Get_CoreSession();
	$aEl = DB__QueryN($CON, $q);

	return $aEl;
}


// LastUpdate 2017.01.19
function DB__Get_Files($id=0) {
	GLOBAL $CONF;

	$q   = 'SELECT * FROM `'.DB_PREFIX.'pratiche_allegati`
			WHERE pratica_id="'.(int)$id.'" ORDER BY allegato_id ASC';
	$CON = GDB__Get_CoreSession();
	$aEl = DB__QueryN($CON, $q);

	return $aEl;
}


// LastUpdate 2017.01.19
function DB__Get_UtenteCoinvolto($uId=0) {
	GLOBAL $CONF;

	$q   = 'SELECT * FROM `'.DB_PREFIX.'pratiche_conducenti`
			WHERE conduce_id="'.(int)$uId.'"';
	$CON = GDB__Get_CoreSession();
	$aEl = DB__Query1($CON, $q);

	if(empty($aEl)){
		$aEl = Array(
				'conduce_id' => '0',
				'pratica_id' => '0',
				'conduce_tipo' => '',
				'conduce_nome' => '',
				'conduce_cognome' => '',
				'conduce_intestatario' => '',
				'conduce_indirizzo' => '',
				'conduce_citta' => '',
				'conduce_provincia' => '',
				'conduce_telefono_fix' => '',
				'conduce_telefono_cel' => '',
				'conduce_targa' => '',
				'conduce_note' => ''
				);
	}

	return $aEl;
}


// LastUpdate 2017.01.19
function DB__Get_File($id=0) {
	GLOBAL $CONF;

	$q   = 'SELECT * FROM `'.DB_PREFIX.'pratiche_allegati`
			WHERE allegato_id="'.(int)$id.'"';
	$CON = GDB__Get_CoreSession();
	$aEl = DB__Query1($CON, $q);

	if(empty($aEl)){
		$aEl = Array(
				'allegato_id' => '0',
				'pratica_id' => '0',
				'allegato_descrizione' => '',
				'allegato_file' => '',
				'allegato_weight' => '',
				'allegato_ip_creazione' => '',
				'allegato_data_creazione' => ''
			);
	}

	return $aEl;
}

function get_PraticaStatus() {
	// l'array contenente gli status
	$a   = Array();
	$a[0] = Array(1, 'Archiviata');
	$a[1] = Array(2, 'Aperta');
	$a[2] = Array(0, 'Annullata');
	$a[3] = Array(3, 'Sollecitata');
	return $a;
}


function get_UserType() {
	// l'array contenente i tipi di utente
	
	$a   = Array();
	$a[0] = Array(0, 'Controparte');
	$a[1] = Array(1, 'Assicurato');
	$a[2] = Array(2, 'Testimone');
	return $a;
}


function HTML__Widget_Inbox() {
	GLOBAL $m;
	
	$aFilter = Array();
	$aFilter['pratica_status'] = 1;
	$aElements = DB__Get_Elements($aFilter);
	
	$HTMLp = '';
	foreach($aElements as $el){
		$HTMLp .= '<tr class="">
					    <td class="text-center" style="width: 30px;">
					        <a href="javascript:void(0)" class="text-muted msg-fav-btn"><i class="fa fa-star-o"></i></a>
					    </td>
					    <td class="text-center" style="width: 30px;">
					        <a href="javascript:myAlert();" class="text-muted msg-read-btn"><i class="hi hi-envelope"></i></a>
					    </td>
					    <td style="width: 20%;">
							'.$el['collaboratore_nome'].'<br>
							'.$el['collaboratore_data_open'].'
					    </td>
					    <td>
							<span class="label label-primary">'.$el['pratica_anno'].' / '.$el['pratica_anno_id'].'</span>
					        <span class="text-muted">'.$el['pratica_oggetto'].'</span>
					    </td>

					    <td class="text-right" style="width: 90px;">
					    	<em data-toggle="tooltip" data-original-title="Data di ultima modifica">'.$el['pratica_data_modifica'].'</em>
					    </td>
					    <td class="text-center" style="width: 30px;">
					        <a href="?m='.$m.'&op=op-edit&id='.$el['pratica_id'].'"><i class="hi hi-briefcase" data-toggle="tooltip" data-original-title="Vai alla pratica"></i></a>
					    </td>
					</tr>';
	}
	
	$OUT = '<div class="widget">
	<script>
		function myAlert(){
			//alert("asdasdad");
		}
	</script>
	<div class="block">
		<!-- Messages List Title -->
			<div class="block-title">
				<div class="block-options pull-right">
					<a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="" data-original-title="Settings"><i class="fa fa-cog"></i></a>
				</div>
				<h2>Inbox</h2>
			</div>
		<!-- END Messages List Title -->
	
		<!-- Messages List Content -->
		<div class="table-responsive">
		    <table class="table table-hover table-vcenter">
		        <!--<thead>
		            <tr>
		                <td colspan="3">
		                    <div class="btn-group btn-group-sm">
		                        <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="" data-original-title="Star Selected"><i class="fa fa-star"></i></a>
		                        <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="" data-original-title="Delete Selected"><i class="hi hi-envelope"></i></a>
		                    </div>
		                </td>
		                <td class="text-right" colspan="3">
						</td>
		            </tr>
		        </thead>-->
		        <tbody>
		            '.$HTMLp.'
		        </tbody>
		    </table>
		</div>
		<!-- END Messages List Content -->
		</div>
	</div>';
	return $OUT;
}


function HTML__Form($aEl=Array()) {
	GLOBAL $m;

	/*$aCountries = GDB__Get_Countries();
	 $HTMLoption = '';
	 foreach($aCountries as $el) {
		$selected    = ($el['country_name']==$aEl['u_country']) ? 'selected="selected"' : '';
		$HTMLoption .= '<option value="'.$el['country_name'].'" '.$selected.'>'.$el['country_code'].' - '.$el['country_name'].'</option>';
		}*/

	$id = $aEl['pratica_id'];
	$aCompanies   = DB__Get_Agenzie();
	$aStatus      = get_PraticaStatus();
	$aAutorita    = DB__Get_AutoritaN();
	$aIspettorati = DB__Get_Ispettorati();
	
	$aAssicurati  = DB__Get_UtentiCoinvolti($aEl['pratica_id'], 1);
	$aControparte = DB__Get_UtentiCoinvolti($aEl['pratica_id'], 0);
	$aTestimoni   = DB__Get_UtentiCoinvolti($aEl['pratica_id'], 2);
	$aFiles       = DB__Get_Files($aEl['pratica_id']);
	
	
	// Gli anni dall'accensione della piattaforma
	if(isset($id) && $id!=0) {
		$HTMLyear = '<p class="form-control-static">'.$aEl['pratica_anno'].'</p>';
	} else {
		$HTMLyear = '<select id="pratica_anno" name="pratica_anno" class="form-control select-chosen  col-md-5" size="1" onchange="setNumeroRiga('.$m.', this.value, '.$id.', true)">';
		for($i=date('Y'); $i>=2006; $i--) {
			$sel = ($i==$aEl['pratica_anno']) ? ' selected' : '';
			$HTMLyear .= '<option '.$sel.'>'.$i.'</option>';
		}
		$HTMLyear .= '</select>';
	}
	

	// Le compagnie assicurative
	$HTMLcomp = '<select id="pratica_agenzia_id" name="pratica_agenzia_id" class="form-control selectized" size="1">';
	$HTMLcomp .= '<option value="">Seleziona un elemento</option>';
	foreach($aCompanies as $comp) {
		$sel = ($comp['agenzia_id']==$aEl['pratica_agenzia_id']) ? ' selected' : '';
		$HTMLcomp .= '<option value="'.$comp['agenzia_id'].'" '.$sel.'>'.$comp['agenzia_nome'].'</option>';
	}
	$HTMLcomp .= '</select>';

	// ispettorato
	$HTMLispettorato = '<select id="pratica_ispettorato_id" name="pratica_ispettorato_id" class="form-control selectized" size="1">';
	$HTMLispettorato .= '<option value="">Seleziona un elemento</option>';
	foreach($aIspettorati as $el) {
		$sel = ($el['ispettorato_nome']==$aEl['pratica_ispettorato']) ? ' selected' : '';
		$sel = ($el['ispettorato_id']==$aEl['pratica_ispettorato_id']) ? ' selected' : $sel;
		$HTMLispettorato .= '<option value="'.$el['ispettorato_id'].'" '.$sel.'>'.$el['ispettorato_nome'].'</option>';
	}
	$HTMLispettorato .= '</select>';
	
	// autorita
	$HTMLautorita    = '<select id="pratica_autorita_id" name="pratica_autorita_id" class="form-control selectized" size="1">';
	$HTMLautorita .= '<option value="">Seleziona un elemento</option>';
	foreach($aAutorita as $el) {
		$sel = ($el['autorita_id']==$aEl['pratica_autorita_id']) ? ' selected' : '';
		$HTMLautorita .= '<option value="'.$el['autorita_id'].'" '.$sel.'>'.$el['autorita_nome'].'</option>';
	}
	$HTMLautorita .= '</select>';
	
	// Lo Status della pratica
	$HTMLstatus = '<select id="pratica_status" name="pratica_status" class="form-control select-chosen" size="1">';
	foreach($aStatus as $el) {
		$sel = ($el[0]==$aEl['pratica_status']) ? ' selected' : '';
		$HTMLstatus .= '<option value="'.$el[0].'" '.$sel.'>'.$el[1].'</option>';
	}
	$HTMLstatus .= '</select>';

	// Utenti coinvolti
	$HTMLutenti = HTML__FormUtenti($id, $aAssicurati, $aControparte, $aTestimoni);
	
	// File
	$HTMLfiles  = HTML__FormFiles($id, $aFiles);
	
	
	// Le date in formato italiano
	$aEl['data_open']  = (isset($aEl['data_open'])  && $aEl['data_open']!=''  && $aEl['data_open']!='0000-00-00')  ? date("d-m-Y", strtotime($aEl['data_open']))  : '';
	$aEl['data_close'] = (isset($aEl['data_close']) && $aEl['data_close']!='' && $aEl['data_close']!='0000-00-00') ? date("d-m-Y", strtotime($aEl['data_close'])) : '';
	$aEl['data_sinistro']            = (isset($aEl['data_sinistro'])            && $aEl['data_sinistro']!=''            && $aEl['data_sinistro']!='0000-00-00')            ? date("d-m-Y", strtotime($aEl['data_sinistro'])) : '';
	$aEl['collaboratore_data_open']  = (isset($aEl['collaboratore_data_open'])  && $aEl['collaboratore_data_open']!=''  && $aEl['collaboratore_data_open']!='0000-00-00')  ? date("d-m-Y", strtotime($aEl['collaboratore_data_open'])) : '';
	$aEl['collaboratore_data_close'] = (isset($aEl['collaboratore_data_close']) && $aEl['collaboratore_data_close']!='' && $aEl['collaboratore_data_close']!='0000-00-00') ? date("d-m-Y", strtotime($aEl['collaboratore_data_close'])) : '';
	
	
	
	
	$hiddenClass = ((isset($id) && $id!=0) ? '' : 'hidden');
	
	$HTML = '
	<iframe width="100%" height="10" style="display:none;" name="iframe_save"></iframe>
	<div class="block full">
		<div class="block-title">
			<h2>Scheda Pratica '.(($aEl['pratica_codice']!='') ? '[ '.$aEl['pratica_codice'].' ]' : '').'</h2>
		</div>
		
		<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog"><div class="modal-content"></div><!-- /.modal-content --></div>
		</div>
					
		<div class="row">		
			<ul class="nav nav-tabs push" data-toggle="tabs">
				<li class="active"><a href="#tab-pratica"><i class="hi hi-briefcase"></i> Info Pratica</a></li>
				<li><a href="#tab-sinistro"><i class="gi gi-car"></i> Info Sinistro</a></li>
				<li class="'.$hiddenClass.'"><a href="#tab-coinvolti"><i class="gi gi-group"></i> Utenti Coinvolti</a></li>
				<li class="'.$hiddenClass.'"><a href="#tab-allegati"><i class="gi gi-paperclip"></i> Allegati</a></li>
				<li class="'.$hiddenClass.'"><a href="#tab-riepilogo" onclick="refreshRiepilogo()"><i class="gi gi-notes_2"></i> Riepilogo</a></li>
			</ul>
		</div>

		<form name="formPratica" id="formPratica" action="?m='.$m.'" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" novalidate="novalidate">
		<input type="hidden" id="op" name="op" value="op-set">
		<input type="hidden" id="id" name="id" value="'.$id.'">
		<input type="hidden" id="m"  name="m"  value="'.$m.'">
		
		<div class="form-validation-message">
			<div class="error"></div>
		</div>		
		
		
			<div class="tab-content">
				<div class="tab-pane active" id="tab-pratica">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<div class="row">
				                    <div class="col-md-6">
										<div class="form-group">
											<label class="col-xs-4 control-label" for="pratica_anno_id">Numero pratica</label>
											<div class="col-xs-3"><input type="text" class="form-control" name="pratica_anno_id" id="pratica_anno_id" readonly="readonly" > </div>
											<div class="col-xs-5">'.$HTMLyear.'</div>
										</div>
									</div>
									<div class="col-md-6">
									</div>
								</div>
								<div class="row">				
									<div class="col-md-6">
										<label class="col-xs-4 control-label" for="pratica_codice">Numero di sinistro</label>
										<div class="col-xs-8"><input type="text" class="form-control" id="pratica_codice" name="pratica_codice" placeholder="1234567890" value="'.$aEl['pratica_codice'].'"></div>
									</div>
								</div>
								<div class="row">
				                    <div class="col-md-6">
										<label class="col-md-4 control-label" for="data_open">Data apertura</label>
										<div class="col-md-8"><input type="text" id="data_open" name="data_open" value="'.$aEl['data_open'].'" class="form-control input-datepicker-close" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"></div>
									</div>
									<div class="col-md-6">
										<label class="col-md-4 control-label" for="data_close">Data chiusura</label>
										<div class="col-md-8"><input type="text" id="data_close" name="data_close" value="'.$aEl['data_close'].'" class="form-control input-datepicker-close" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"></div>
									</div>
								</div>
				
								<div class="row">
				                    <div class="col-md-6">
										<label class="col-xs-4 control-label" for="pratica_agenzia_id">Compagnia</label>
										<div class="col-xs-8">'.$HTMLcomp.'</div>
									</div>
									<div class="col-md-6">
										<label class="col-xs-4 control-label" for="pratica_status">Status</label>
										<div class="col-xs-8">'.$HTMLstatus.'</div>
									</div>
								</div>
							</div>
							
						</div>
												
					</div>
				</div>
				
											
				<div class="tab-pane" id="tab-sinistro">
					
					<div class="form-group">

						<div class="row">

							<div class="col-md-6">
								<label class="col-md-4 control-label" for="data_sinistro">Data Sinistro</label>
								<div class="col-md-4"><input type="text" id="data_sinistro" name="data_sinistro" class="form-control input-datepicker" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="'.$aEl['data_sinistro'].'"></div>
								<div class="col-md-4">
									<div class="input-group bootstrap-timepicker">
										<input type="text" id="example-timepicker24" name="example-timepicker24" class="form-control input-timepicker24">
		                                <span class="input-group-btn">
											<a href="javascript:void(0)" class="btn btn-primary"><i class="fa fa-clock-o"></i></a>
										</span>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<!-- ex.pratica_codice -->
							</div>
						</div>												
												
						<div class="row">
		                    <div class="col-md-6">
								<label class="col-md-4 control-label" for="pratica_oggetto">Pratica Oggetto</label>
								<div class="col-md-8"><input type="text" class="form-control" id="pratica_oggetto" name="pratica_oggetto" placeholder="Es: 1234567890" value="'.$aEl['pratica_oggetto'].'"></div>
							</div>
							<div class="col-md-6"></div>
						</div>			
		
						<div class="row">
		                    <div class="col-md-6">
								<label class="col-md-4 control-label" for="pratica_ispettorato">Ispettorato</label>
								<div class="col-md-8">'.$HTMLispettorato.'</div>
							</div>
							<div class="col-md-6">
								<label class="col-md-4 control-label" for="pratica_autorita">Autorità</label>
								<div class="col-md-8">'.$HTMLautorita.'</div>
							</div>
						</div>
		
			
					
					
		            
					<div class="form-group">
		
		                <div class="row">
		                    <div class="col-md-6">
								<label class="col-md-4 control-label" for="pratica_indirizzo_via">Indirizzo</label>
								<div class="col-md-8"><input type="text" class="form-control" id="pratica_indirizzo_via" name="pratica_indirizzo_via" placeholder="Es: Via delle valli" value="'.$aEl['pratica_indirizzo_via'].'"></div>
							</div>
							<div class="col-md-6">
								<label class="col-md-4 control-label" for="pratica_indirizzo_num">Numero</label>
								<div class="col-md-8"><input type="text" class="form-control" id="pratica_indirizzo_num" name="pratica_indirizzo_num" placeholder="Es: 7" value="'.$aEl['pratica_indirizzo_num'].'"></div>
							</div>
						</div>
						
						
		
						<div class="row">
		                    <div class="col-md-6">
								<label class="col-md-4 control-label" for="pratica_indirizzo_cap">Cap</label>
								<div class="col-md-8"><input type="text" class="form-control" id="pratica_indirizzo_cap" name="pratica_indirizzo_cap" placeholder="Es: 00100" value="'.$aEl['pratica_indirizzo_cap'].'"></div>
							</div>
							<div class="col-md-6">
								<label class="col-md-4 control-label" for="pratica_indirizzo_citta">Città</label>
								<div class="col-md-8"><input type="text" class="form-control" id="pratica_indirizzo_citta" name="pratica_indirizzo_citta" placeholder="Es: Roma" value="'.$aEl['pratica_indirizzo_citta'].'"></div>
							</div>
						</div>
		
						<div class="row">
		                    <div class="col-md-6">
								<label class="col-md-4 control-label" for="pratica_indirizzo_prov">Provincia</label>
								<div class="col-md-8"><input type="text" class="form-control" id="pratica_indirizzo_prov" name="pratica_indirizzo_prov" placeholder="Es: RM" value="'.$aEl['pratica_indirizzo_prov'].'"></div>
							</div>
							<div class="col-md-6"></div>
						</div>
		
					</div>
										
					<div class="form-group">
			                <div class="row">
			                    <div class="col-md-6">
									<label class="col-xs-4 control-label" for="collaboratore_nome">Collaboratore</label>
									<div class="col-xs-8"><input type="text" class="form-control" id="collaboratore_nome" name="collaboratore_nome" placeholder="Es: Mario Rossi" value="'.$aEl['collaboratore_nome'].'"></div>
								</div>
								<div class="col-md-6"> </div>
							</div>
							<div class="row">
			                    <div class="col-md-6">
									<label class="col-md-4 control-label" for="collaboratore_data_open">Data inizio collaboratore</label>
									<div class="col-md-8"><input type="text" id="collaboratore_data_open" name="collaboratore_data_open" value="'.$aEl['collaboratore_data_open'].'" class="form-control input-datepicker-close" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"></div>
								</div>
								<div class="col-md-6">
									<label class="col-md-4 control-label" for="collaboratore_data_close">Data fine collaboratore</label>
									<div class="col-md-8"><input type="text" id="collaboratore_data_close" name="collaboratore_data_close" value="'.$aEl['collaboratore_data_close'].'" class="form-control input-datepicker-close" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"></div>
								</div>
							</div>
						</div>
					</div>					
				
					<div class="form-group">
						<div class="row">
							<div class="col-md-12">
								<label class="col-md-2 control-label" for="pratica_note">Note</label>
		                    	<div class="col-md-10"><textarea class="form-control ckeditor" name="pratica_note">'.$aEl['pratica_note'].'</textarea></div>
							</div>
		                </div>				
					</div>
					
				</div>
				
											
											
				<div class="tab-pane" id="tab-coinvolti">
					<div class="form-group">
						<div class="col-md-12">'.$HTMLutenti.'</div>		
					</div>
				</div>
											
				<div class="tab-pane " id="tab-allegati">
					<div class="form-group">
						<div class="col-md-12">'.$HTMLfiles.'</div>
					</div>
				</div>
				
				<div class="tab-pane " id="tab-riepilogo">
					'.HTML__ModalDetails2('', $aEl).'
				</div>
				
			</div>
		
									
		
			<div class="form-group form-actions">
				<div class="col-md-6 text-center">
					<button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
				</div>
				<div class="col-md-6 text-center">
					<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
				</div>
			</div>
		</form>
	</div>
	<script src="js/ckeditor/ckeditor.js"></script>';
	
	if(isset($id) && $id!=0) {
		$HTML .= '<script>
		$(function() {
			displayNumeroRiga('.$aEl['pratica_anno_id'].');
		});
	</script>';
	} else {
		$HTML .= '<script>
		$(function() {
			setNumeroRiga("'.$m.'", '.$aEl['pratica_anno'].',"'.$id.'");
		});
	</script>';
	}
	
	
	return $HTML;
}




?>