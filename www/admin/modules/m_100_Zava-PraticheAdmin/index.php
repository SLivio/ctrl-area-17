<?php /*
Version:     v7 2017.01.10
Module:      Zava16.PraticheAdmin
Author:      SLivio
*/



//--
//----------------------------------------------------------> [CONFIG]
if(file_exists($CONF['path_module'].'this.lib.php')) {
	require_once($CONF['path_module'].'this.lib.php');
}
$id  = (isset($_REQUEST['id'])    && $_REQUEST['id']    != '' && $_REQUEST['id'] != 0)     ? $_REQUEST['id']    : 0;
$opp = (isset($_REQUEST['opp'])   && $_REQUEST['opp']   != '' && $_REQUEST['opp'] != '')   ? $_REQUEST['opp'] : '';
$aP  = $_REQUEST;
$aFilter = Array();
//----------------------------------------------------------> [/CONFIG]

$CONF['upload']            = 'pratiche/';
$CONF['accepted_files']    = Array('jpg', 'png', 'pdf', 'doc', 'docx', 'ppt', 'xls', 'xlsx');
$CONF['accepted_mimetype'] = Array('application/msword', 'application/rtf', 'application/x-rtf', 'text/richtext', 'application/powerpoint', 'application/mspowerpoint', 'application/vnd.ms-powerpoint', 'application/x-mspowerpoint', 'text/html', 'text/plain', 'application/excel', 'application/vnd.ms-excel', 'application/x-excel', 'application/x-msexcel', 'image/png', 'image/gif', 'image/jpeg', 'image/jpeg', 'application/pdf', 'application/x-compressed', 'application/x-zip-compressed', 'application/zip', 'multipart/x-zip');



switch($op) {
	case 'del' : // servizio: eliminazione della pratica
		$out = DB__Del_Element($id);
		if($out==true) $aR  = Array('op'=>'1', 'msg'=>'Ok', 'random'=>get_RandomString());
		else           $aR  = Array('op'=>'0', 'msg'=>'Ko :: '.$out, 'random'=>get_RandomString());
		$json = json_encode($aR);
		die($json);
		break;
	case 'del-user' :  // servizio: eliminazione di un utente coinvolto
		$out = DB__Del_UtenteCoinvolto($id);
		if($out==true) $aR  = Array('op'=>'1', 'msg'=>'Ok', 'random'=>get_RandomString());
		else           $aR  = Array('op'=>'0', 'msg'=>'Ko :: '.$out, 'random'=>get_RandomString());
		$json = json_encode($aR);
		die($json);
		break;
	case 'del-file' : // servizio: eliminazione di un file
		$fid  = (isset($_REQUEST['fid']) && $_REQUEST['fid'] != '' && $_REQUEST['fid'] != 0) ? $_REQUEST['fid'] : 0;
		$out = DB__Del_File($fid);
		if($out==true) $aR  = Array('op'=>'1', 'msg'=>'Ok', 'random'=>get_RandomString());
		else           $aR  = Array('op'=>'0', 'msg'=>'Ko :: '.$out, 'random'=>get_RandomString());
		$json = json_encode($aR);
		die($json);
		break;
	case 'op-user-add' : // servizio: aggiungo un utente
		if($id==0) {
			$aR   = Array('op'=>'0', 'msg'=>'Ko :: Pratica non assegnata', 'random'=>get_RandomString());
			$json = json_encode($aR);
			die($json);
		}
		$aP['pratica_id'] = $id;
	
		$cid  = (isset($_REQUEST['cid']) && $_REQUEST['cid'] != '' && $_REQUEST['cid'] != 0) ? $_REQUEST['cid'] : 0;
	
		if($cid!=0) {
			$aP['conduce_id'] = $cid;
			$cId = DB__Set_UtenteCoinvolto($aP);
			if ($cId) { $aR  = Array('op'=>'1', 'cid'=>$cId, 'msg'=>'Ok', 'random'=>get_RandomString()); } // ok
			else      { $aR  = Array('op'=>'0', 'msg'=>'Ko.1 :: '.$cId.'|'.$id, 'random'=>get_RandomString()); } // ko
		} else {
			$cId = DB__Add_UtenteCoinvolto($aP);
			if ($cId) { $aR  = Array('op'=>'1', 'cid'=>$cId, 'msg'=>'Ok', 'random'=>get_RandomString()); } // ok
			else      { $aR  = Array('op'=>'0', 'msg'=>'Ko.2 :: '.$cId.'|'.$id, 'random'=>get_RandomString()); } // ko
		}
		$json = json_encode($aR);
		die($json);
		break;
	case 'get_riga' : // servizio: restituisce il numero di pratica per l'anno corrente
		$pratica_id = $id;
		$pratica_anno = (isset($_REQUEST['pratica_anno'])  ? trim($_REQUEST['pratica_anno']) : '');
		$out = DB_get_NumeroRiga($pratica_anno, $pratica_id);
		die(' '.$out);
		break;
	case 'get_pratica_bycode' : // servizio: restituisce la pratica partendo dal codice
		$pratica_codice = (isset($_REQUEST['pratica_codice'])  ? trim($_REQUEST['pratica_codice']) : '');
		$out = DB__Get_ElementByCode($pratica_codice);
		die(json_encode($out));
		break;
	case 'get_notice' : // servizio: restituisce informazioni sulle notifiche presenti nelle pratiche
		$ids  = (isset($_REQUEST['ids']) && $_REQUEST['ids'] != '' && $_REQUEST['ids'] != 0) ? $_REQUEST['ids'] : 0;
		$aSum = DB__Get_SummaryNotice($ids);
		$json = json_encode($aSum);
		die($json);
		break;
		case 'set-notice-1' :
			// $data = 'm='.$m.'&op=set-notice-1&pid='.$id.'&nid='.$n['notifica_id'].'&uid'.$uId; + status
			$pid    = (isset($_REQUEST['pid']) && $_REQUEST['pid'] != '' && $_REQUEST['pid'] != 0) ? $_REQUEST['pid'] : 0;
			$nid    = (isset($_REQUEST['nid']) && $_REQUEST['nid'] != '' && $_REQUEST['nid'] != 0) ? $_REQUEST['nid'] : 0;
			$status = (isset($_REQUEST['status']) && $_REQUEST['status'] != '' && $_REQUEST['status'] != 0) ? $_REQUEST['status'] : 0;
			$status = DB__Get_SetNotice($pid, $nid, $status);
			$aR   = Array('op'=>'1', 'msg'=>'Ok', 'status'=>$status, 'random'=>get_RandomString());
			$json = json_encode($aR);
			die($json);
			break;
	case 'op-new'  : // form per inserimento/modifica della pratica
	case 'op-edit' :
		$HTML = '';
		if($id!=0) {
			$label = 'Modifica '.$id;
			$aEl   = DB__Get_Element($id);
		} else {
			$aEl   = DB__Get_Element(0);
			$label = 'Nuova ';
		}
		$HTML .= GHTML__Get_ModuleHeader('Gestione Pratiche', $label);
		$HTML .= HTML__Form($aEl);
		echo $HTML;
		break;
	case 'op-set' : // azione di modifica della pratica
		$HTML = '';
		
		$HTML .= GHTML__Get_ModuleHeader('Gestione Pratiche', 'Modifica');
		
		if($id!=0) {
			$aP['pratica_id'] = $id;
			$dbId = DB__Set_Element($aP);
			if ($dbId) $HTML .= GHTML__Get_StatusMessage('success', 'Modifica Pratica', 'Modifica completata con successo');
			else       $HTML .= GHTML__Get_StatusMessage('error', 'Modifica Pratica', 'Si sono verificati degli errori [set.'.$id.']');
			
		} else {
			$id = DB__Add_Element($aP);
			if ($id) $HTML .= GHTML__Get_StatusMessage('success', 'Nuova Pratica', 'Inserimento completato con successo.');
			else     $HTML .= GHTML__Get_StatusMessage('error', 'Modifica Pratica', 'Si sono verificati degli errori [add]');
		}
		
		$aEl   = DB__Get_Element($id);
		$HTML .= HTML__Form($aEl);
		echo $HTML;
		break;
	case 'op-details' : // modale: dettagli della pratica
		$aEl= DB__Get_Element($id);
		$HTML = HTML__ModalDetails('Dettagli della pratica', $aEl);
		echo $HTML;
		break;
	case 'op-notice' : // le notifiche della singola pratica
		$aEl     = DB__Get_Element($id);
		$aNotice = DB__Get_Notifiche($id);
		$HTML    = HTML__ModalNotice($id, 'Notifiche', $aEl, $aNotice);
		echo $HTML;
		break;
	case 'op-file-add' : // form: aggiungo un file
		if($id==0) {
			$aR   = Array('op'=>'0', 'msg'=>'Ko :: Pratica non assegnata', 'random'=>get_RandomString());
			$json = json_encode($aR);
			die($json);
		}
		$aP['pratica_id'] = $id;
	
		$fid  = (isset($_REQUEST['fid']) && $_REQUEST['fid'] != '' && $_REQUEST['fid'] != 0) ? $_REQUEST['fid'] : 0;
	
		if($fid!=0) {
			$aP['allegato_id'] = $fid;
			$fid = DB__Set_File($aP, $_FILES);
			if ($fid) { $aR  = Array('op'=>'1', 'fid'=>$fid, 'msg'=>'Ok.1', 'random'=>get_RandomString()); } // ok
			else      { $aR  = Array('op'=>'0', 'msg'=>'Ko.1 :: '.$fid.'|'.$id, 'random'=>get_RandomString()); } // ko
		} else {
				
			$fid = DB__Add_File($aP, $_FILES);
			if ($fid) { $aR  = Array('op'=>'1', 'fid'=>$fid, 'msg'=>'Ok.2', 'random'=>get_RandomString()); } // ok
			else      { $aR  = Array('op'=>'0', 'msg'=>'Ko.2 :: '.$fid.'|'.$id, 'random'=>get_RandomString()); } // ko
		}
	
		$aEl   = DB__Get_Element($id);
		$label = 'Modifica '.$id;
		$HTML  = '';
		$HTML .= GHTML__Get_ModuleHeader('Gestione Pratiche', $label);
		$HTML .= HTML__Form($aEl);
		echo $HTML;
		break;
	case 'op-user' : // modale: aggiunge/modifica utenti
		$cid  = (isset($_REQUEST['cid']) && $_REQUEST['cid'] != '' && $_REQUEST['cid'] != 0) ? $_REQUEST['cid'] : 0;
		$aU   = DB__Get_UtenteCoinvolto($cid);
		$HTML = HTML__ModalUser($id, 'Persona coinvolta', $aU);
		echo $HTML;
		break;
	case 'op-file' : // modale: aggiunge/modifica file
		$fid  = (isset($_REQUEST['fid']) && $_REQUEST['fid'] != '' && $_REQUEST['fid'] != 0) ? $_REQUEST['fid'] : 0;
		$aU   = DB__Get_File($fid);
		$HTML = HTML__ModalFile($id, 'File allegato', $aU);
		echo $HTML;
		break;
	case 'op-search' : // modale: ricerca
		$HTML = HTML__ModalSearch('Cerca la pratica', $aP);
		echo $HTML;
		break;
	case 'inbox' : // widget: ultime pratiche aperte
		$HTML = HTML__Widget_Inbox();
		echo $HTML;
		break;
	default : // default: mostro le pratiche
	case 'op-list' :
		$aFilter['op_page']    = (isset($_REQUEST['op_page'])    ? $_REQUEST['op_page']         : 1);
		$aFilter['op_order']   = (isset($_REQUEST['op_order'])   ? $_REQUEST['op_order']        : ' pratica_id DESC ');
		$aFilter['pratica_codice']   = (isset($_REQUEST['pratica_codice'])  ? trim($_REQUEST['pratica_codice']) : '');
		$aFilter['pratica_status']   = (isset($_REQUEST['pratica_status'])  ? $_REQUEST['pratica_status'] : '');
		$aFilter['pratica_anno']     = (isset($_REQUEST['pratica_anno'])  ? trim($_REQUEST['pratica_anno']) : '');
		$aFilter['pratica_oggetto']  = (isset($_REQUEST['pratica_oggetto'])  ? trim($_REQUEST['pratica_oggetto']) : '');
		$aFilter['agenzia_id']       = (isset($_REQUEST['agenzia_id'])  ? trim($_REQUEST['agenzia_id']) : '');
		$aFilter['collaboratore_nome']  = (isset($_REQUEST['collaboratore_nome'])  ? trim($_REQUEST['collaboratore_nome']) : '');
		$aFilter['data_sinistro']    = (isset($_REQUEST['data_sinistro'])  ? trim($_REQUEST['data_sinistro'])  : '');
	
		$aFilter['conduce_targa']    = (isset($_REQUEST['conduce_targa'])   ? trim($_REQUEST['conduce_targa'])   : '');
		$aFilter['conduce_nome']     = (isset($_REQUEST['conduce_nome'])    ? trim($_REQUEST['conduce_nome'])    : '');
		$aFilter['conduce_cognome']  = (isset($_REQUEST['conduce_cognome']) ? trim($_REQUEST['conduce_cognome']) : '');
	
		$aFilter['filter_notice']    = (isset($_REQUEST['filter_notice'])  ? (int)($_REQUEST['filter_notice']) : 0);
		$aFilter['filter_alert']     = (isset($_REQUEST['filter_alert'])   ? (int)($_REQUEST['filter_alert'])  : 0);
		
	
		$aCount    = DB__Get_Count_Elements($aFilter);
		$aElements = DB__Get_Elements($aFilter);
	
		$HTML      = GHTML__Get_ModuleHeader('Gestione', 'Pratiche');
		$HTML     .= HTML__List($aFilter, $aElements, $aCount);
		echo $HTML;
		break;
}


?>