/*
Version:     v7 2017.01.10
Module:      Zava16.PraticheAdmin
Author:      SLivio
*/

function refreshRiepilogo() {
	//alert("asdasd");
}

// salva la notifica
function GlobalSend_Notice(obj, hex) {
	if($(obj).hasClass('text-muted')) {
		var status='1';
	} else {
		var status='0';
	}
	$.ajax({
		url:  "index.php",
		type: 'POST',
		cache: false,
		dataType: 'json',
		//data: 'app=true&hex='+hex+'&status='+status,
		data: 'app=true&'+hex+'&status='+status,
		timeout: 3000,
		async: false
	}).done(function(data) {
		if(data.op=='1' && status=='1') {
			$(obj).removeClass('text-muted').addClass('text-warning');
			$('i', obj).removeClass('fa-star-o').addClass('fa-star');
		} else {
			$(obj).removeClass('text-warning').addClass('text-muted');
			$('i', obj).removeClass('fa-star').addClass('fa-star-o');
		}
	});
}


function setNumeroRiga(m, anno, pid, cambioanno) {
	var my_html = $.ajax({
		url:  "index.php",
		type: 'POST',
		cache: false,
		dataType: 'text',
		data: 'app=true&m='+m+'&op=get_riga&pratica_anno='+anno+'&id='+pid,
		timeout: 3000,
		async: false
	}).responseText;
	if (cambioanno==true && pid != 0) {
		my_html=parseInt(my_html);
	}
	$("#pratica_anno_id").val(my_html);
}

function displayNumeroRiga(paid) {
	$("#pratica_anno_id").val(paid);
}

function gotoPage(lnk) {
	document.location.href=lnk;
}

function elDel(m, pId) {
	alert("Funzione non abilitata.");
}

function fDel(m, fid) {
	if (confirm("Eliminare file?")) {
		var my_html = $.ajax({
			url:  "index.php",
			type: 'POST',
			cache: false,
			dataType: 'text',
			data: 'app=true&m='+m+'&op=del-file&fid='+fid,
			timeout: 3000,
			async: false
		}).done(function(data) {
			var k = ".tr_"+fid
			//alert(k);
			$(k).addClass("text-danger").attr("style", "text-decoration: line-through;");
			$(k).find("a").attr("href", "javascript:;").attr("data-toggle", "");
			$(k).find("i").addClass("text-danger").tooltip('destroy');
			
		});
	}
}

function uDel(m, uId) {
	if (confirm("Eliminare utente?")) {
		var my_html = $.ajax({
			url:  "index.php",
			type: 'POST',
			cache: false,
			dataType: 'text',
			data: 'app=true&m='+m+'&op=del-user&id='+uId,
			timeout: 3000,
			async: false
		}).done(function(data) {
			var k = ".tr_"+uId
			//alert(k);
			$(k).addClass("text-danger").attr("style", "text-decoration: line-through;");
			$(k).find("a").attr("href", "javascript:;").attr("data-toggle", "");
			$(k).find("i").addClass("text-danger").tooltip('destroy');
			
		});
	}
}


function CheckNotice(){
	var count  = $(".notice").length;
	var params = "";
	var m      = "";
	var data;
	$(".notice").each(function() {
		params += "ids[]="+$(this).attr("data-ctrl-id")+"&";
		m       = "m="+$(this).attr("data-ctrl-m");
		console.log(count +" :: "+ params);
		if(!--count) {
			console.log(count +" :: ");
			$.ajax({
				url:  "index.php",
				type: 'GET',
				cache: false,
				dataType: 'json',
				data: 'app=true&op=get_notice&'+params+m,
				timeout: 3000,
				async: false
			}).done(function(data) {
				if(data.length >= 1) {
					$(data).each(function(i, item){
						var k = "#notice_"+item.pratica_id;
						if(item.WHO_NOTICE==1) {
							$(k).find("sup").remove();
							$(k).append('<sup><span class="badge label-primary">'+item.TOT+'</span></sup>')
							$(k).find('i').removeClass("text-muted").addClass("themed-color");
						} else {
							$(k).find("sup").remove();
							$(k).append('<sup><span class="badge">'+item.TOT+'</span></sup>');
							$(k).find('i').removeClass("themed-color").addClass("text-muted");
						}
						$(".debugDiv").prepend(k+"<br>");
					})
				}
				//if(data.pratica_id!=undefined && data.pratica_id!=0) {
				//$(".debugDiv").prepend(count.lenght+" > "+data.msg);
				//}
			});
		}
	});
}

$(function() {
	$('.form-horizontal .control-label').css('margin-bottom','15px');

	$('.selectized').selectize({
	    create: true,
	    sortField: 'text'
	});
	
	// quando aggiungo un file, invio il form della pratica al frame iframesend
	$(".btn_savePratica").click(function() {
		$("#formPratica").attr("target", "iframe_save");
		$("#formPratica").submit(function() {
			//$("#formPratica").removeAttr("target");
		});
		
	});
	
	$("#pratica_codice").focusout(function() {
		var pId   = $("#id").val();
		var m     = $("#m").val();
		var pCode = $("#pratica_codice").val();
		if(pId==0) {
			$.ajax({
				url:  "index.php",
				type: 'POST',
				cache: false,
				dataType: 'json',
				data: 'app=true&m='+m+'&op=get_pratica_bycode&pratica_codice='+pCode,
				timeout: 3000,
				async: false
			}).done(function(data) {
				if(data.pratica_id!=undefined && data.pratica_id!=0) {
					if(confirm("Già inserita. Vuoi andare a vederla?")) {
						// redirect
						// blocca
					}
				} else {
					// sblocca
				}
				
			});
		}
	});
	
	CheckNotice();
	
	if($('.ckeditor').length>=1) {
		CKEDITOR.replace( 'pratica_note', {
			height: 100,
			width: '100%',
			toolbarGroups: [
				{"name":"basicstyles","groups":["basicstyles"]}
			],
			// Remove the redundant buttons from toolbar groups defined above.
			removeButtons: 'Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
		} );
	}

});


// apertura di una specifica tab
$(function() {
    // Javascript to enable link to tab
    var hash = document.location.hash;
    if (hash) {
      console.log(hash);
      $('.nav-tabs a[href='+hash+']').tab('show');
    }

    // Change hash for page-reload
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
      window.location.hash = e.target.hash;
    });
  });





