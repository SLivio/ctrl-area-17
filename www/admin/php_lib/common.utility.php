<?php
/*
  File:        common.utility.php
  Version:     3.201111.11
  Author:      SLivio
  Contact:     silvio.coco@gmail.com
*/


//--------------------------------------------------------[STATIC_GENERIC_FUNCTION]


function Array_MyMultiSort($array, $on, $order=SORT_ASC) {
	// Simple function to sort an array by a specific key. Maintains index association.
	// print_r(array_sort($array, 'key', SORT_DESC));
	// http://php.net/manual/en/function.sort.php
    $new_array = array();
    $sortable_array = array();
    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }
        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
            break;
            case SORT_DESC:
                arsort($sortable_array);
            break;
        }
        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }
    return $new_array;
}


function JS_Header($url) {
	$url = str_replace('Location: ', '', $url);
	$url = str_replace('location: ', '', $url);
	$url = str_replace(' ',          '', $url);
	return '<script type="text/javascript">
	document.location.href = "'.$url.'";
	</script>';
}


function JS_Redirect($url='', $time=0) {
	$ran = rand(11, 99);
	return '<script type="text/javascript">
	function JS_DoRedirect_'.$ran.'() {
		document.location.href = "'.$url.'";
	}
	window.setTimeout("JS_DoRedirect_'.$ran.'()", '.(int)$time.');
	</script>';
}


function cleanWord($word) {
	return urlencode($word);
}


function nbsp($multiplier=2) {
	return str_repeat(' &nbsp;', $multiplier);
}


function txt2html($text='') {
	$text = str_replace('"',   "&quot;",   $text);
	$text = str_replace("à",   "&agrave;", $text);
	$text = str_replace("è",   "&egrave;", $text);
	$text = str_replace("é",   "&aacute;", $text);
	$text = str_replace("ì",   "&igrave;", $text);
	$text = str_replace("ò",   "&ograve;", $text);
	$text = str_replace("ù",   "&ugrave;", $text);
	$text = str_replace("'",   "&rsquo;",  $text);
	$text = str_replace("’",   "&rsquo;",  $text);
	$text = str_replace("‘",   "&lsquo;",  $text);

	//$text = str_replace("\n",   "",  $text);
	//$text = str_replace("\r",   "",  $text);
	//$text = str_replace("\"",   "'", $text);

	return $text;
}


function html2txt($text='') {
	$text = str_replace('&quot;',    '"',  $text);
	$text = str_replace("&agrave;",  "à",  $text);
	$text = str_replace("&egrave;",  "è",  $text);
	$text = str_replace("&eacute;",  "é",  $text);
	$text = str_replace("&igrave;",  "ì",  $text);
	$text = str_replace("&ograve;",  "ò",  $text);
	$text = str_replace("&ugrave;",  "ù",  $text);
	$text = str_replace("&rsquo;",   "'",  $text);
	$text = str_replace("&lsquo;",   "'",  $text);
	return $text;
}


function clean4Json($str) {
	$str = str_replace("\n", " ", $str);
	$str = str_replace("\r", " ", $str);
	$str = str_replace("\t", " ", $str);
	$str = str_replace("  ", " ", $str);
	$str = str_replace("  ", " ", $str);
	return $str;
}


function clean4Filename($word) {
	// define array replace
	$aPun = Array('?', '!', ',', ':', ';'); // 5
	$aQuo = Array('"', "'", '’', '‘'); //4
	$aOth = Array('|', '\\', '£', '$', '%', '&', '(', ')', '=', '^', '+', '*', '§', '°', '#', '@', 'ç', '[', ']');
	// clean name file
	$word = str_replace($aPun, '', $word);
	$word = str_replace($aQuo, '', $word);
	$word = str_replace($aOth, '', $word);
	// other replace
	$word = str_replace(' ',   "-", $word);
	// cleaned
	return $word;
}


function cleanMysqlInjection($word) {
	$word = str_replace(" ",      "", $word);
	$word = str_replace("\"",     "", $word);
	$word = str_replace("'",      "", $word);
	$word = str_replace(" OR ",   "", $word);
	$word = str_replace(" Or ",   "", $word);
	$word = str_replace(" oR ",   "", $word);
	$word = str_replace(" or ",   "", $word);
	$word = str_replace(" ",      "", $word);
	return $word;
}


function logError($key, $error) { LOG__Error($key, $error); }

function LOG__Error($key, $error) {
	GLOBAL $SESSION_ID;
	if (!isset($_SESSION[$SESSION_ID]['error'])) {
		$_SESSION[$SESSION_ID]['error'][0] = '<strong> '. $key . ' :: </strong>' . $error;
	} else {
		$tmp_id = count($_SESSION[$SESSION_ID]['error']);
		$_SESSION[$SESSION_ID]['error'][$tmp_id] = '<strong> '. $key . ' :: </strong>' . $error;
	}
	//printArray($_SESSION[$SESSION_ID]['error']);
}


function printArray($a=Array(), $r=false) {
	if ($r==false) {
		print "<pre>";
		print_r($a);
		print "</pre>";
	} else {
		$p = '<pre>'.print_r($a, true).'</pre>';
		return $p;
	}
}


function fillArrayWithKeys($a=Array()) {
	$ar = Array();
	foreach($a as $el) {
		$ar[$el] = '';
	}
	return $ar;
}


function get_Header($filetype='',$mimetype='', $content='') {
	$filename = date('Ymd_his');
	if ($filetype != '') {
		switch($filetype) {
			case 'xls' :
				header ("Content-Type: application/vnd.ms-excel");
				header ("Content-Disposition: inline; filename=".$filename.".xls");
				echo $content;
			break;
			case 'txt' :
			break;
			case 'csv' :
			break;
		}
	} else if ($mimetype != ''){
	}
}


function get_FileExtension($str) {
	$i = strrpos($str,".");
	if (!$i) { return ""; }
	$l = strlen($str) - $i;
	$ext = substr($str,$i+1,$l);
	$end = strtolower($ext);
	return $end;
}


function get_RandomString($length=10) {  
	$string = "";  
	// genera una stringa casuale che ha lunghezza  
	// uguale al multiplo di 32 successivo a $length  
	for ($i = 0; $i <= ($length/32); $i++)  
	  $string .= md5(time()+rand(0,99));  
	// indice di partenza limite  
	$max_start_index = (32*$i)-$length;  
	// seleziona la stringa, utilizzando come indice iniziale  
	// un valore tra 0 e $max_start_point  
	$random_string = substr($string, rand(0, $max_start_index), $length);  
	return $random_string;  
}


function PHP_scandir($dir, $sort=0, $filter=0, $complete=0) {
	$aFilter = Array('.', '..', 'Thumbs.db', '.DS_Store');
	if (PHP_VERSION >= '5') {
		$dirmap = scandir($dir, $sort);
		if($filter!=0) $dirmap = array_diff($dirmap, $aFilter);
		//return $dirmap;
	}
	$dirmap = array();
	if(!is_dir($dir)) {
		trigger_error("lib::scandir($dir): failed to open dir: Invalid argument", E_USER_WARNING);
		return false;
	}
	$r_dir = opendir($dir);
	while (false !== ($file = readdir($r_dir)))
		$dirmap[] = $file;
	closedir($r_dir);
	($sort == 1) ? rsort($dirmap) : sort($dirmap);
	if($filter!=0) $dirmap = array_diff($dirmap, $aFilter);
	if($complete!=0) {
		$aComp = Array();
		$c     = 0;
		foreach($dirmap as $el) {
			$tmp_info               = stat($dir.$el);
			$aComp[$c][0]           = $el;
			$aComp[$c]['f_name']    = $el;
			$aComp[$c]['f_size']    = $tmp_info['size'];
			$aComp[$c]['m_time']    = date('Y.m.d', $tmp_info['mtime']); //date('Y.m.d', strtotime($tmp_info['mtime']));;
			$aComp[$c]['f_size_kb'] = ($aComp[$c]['f_size'] > 0) ? (int)($aComp[$c]['f_size']/1024) : $aComp[$c]['f_size'];
			$aComp[$c]['f_est']     = get_FileExtension($el);
			$c++;
		}
		$dirmap = Array();
		$dirmap = $aComp;
	}
	return $dirmap;
}


function UPLOAD_GetPermission($what='web_img', $file='') {
	$what = (isset($what) && $what != '') ? $what : '';
	$file = (isset($file) && $file != '') ? $file : '';
	if ($what == '' || $file == '') {
		return false;
	} else {
		switch($what) {
			case 'web_img' :
				$a = Array('gif', 'jpg', 'jpeg', 'png');
			break;
			case 'doc' :
				$a = Array('pdf', 'doc', 'docx', 'xls', 'odt');
			break;
		}
		if (in_array(strtolower($file), $a)) return true;
		else                                 return false;
	}
}


function cleanArrayFromEmptyElements($aEl) {
	foreach($aEl as $chiave=>$valore) {
		if($aEl[$chiave] =='' || empty($aEl[$chiave]))
			unset($aEl[$chiave]);
	}
	return $aEl;
}


// LastUpdate 3.201111.11 [OK]
function FILE_ForceDownload($filename) {
	if (file_exists($filename)) {
		// required for IE, otherwise Content-disposition is ignored
		if(ini_get('zlib.output_compression')) ini_set('zlib.output_compression', 'Off');
		// addition by Jorg Weske
		$file_extension = get_FileExtension($filename);
		switch( $file_extension )
		{
		  case "pdf": $ctype="application/pdf"; break;
		  case "exe": $ctype="application/octet-stream"; break;
		  case "zip": $ctype="application/zip"; break;
		  case "doc": $ctype="application/msword"; break;
		  case "xls": $ctype="application/vnd.ms-excel"; break;
		  case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
		  case "gif": $ctype="image/gif"; break;
		  case "png": $ctype="image/png"; break;
		  case "jpeg":
		  case "jpg": $ctype="image/jpg"; break;
		  default: $ctype="application/force-download";
		}
		header("Pragma: public"); // required
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false); // required for certain browsers 
		header("Content-Type: $ctype");
		// change, added quotes to allow spaces in filenames, by Rajkumar Singh
		header("Content-Disposition: attachment; filename=\"".basename($filename)."\";" );
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".filesize($filename));
		readfile("$filename");
	} else {
		// 
	}
}
// LastUpdate 4.201203.25
function MyEscape($str='') {
	if (get_magic_quotes_gpc()==1) {
		return  $str;
	} else {
		return addslashes($str);
	}
}

?>