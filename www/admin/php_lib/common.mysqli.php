<?php



// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- [NEW FUNCTIONS]




function DB__Query1($CON, $q="") {
	$a = Array();
	$r = mysqli_query($CON, $q) or LOG__Error("DB__Query1[A]", mysqli_error($CON)."\n".$q);
	//$a = mysqli_fetch_array($r, MYSQLI_ASSOC) or LOG__Error("DB__Query1[B]", mysqli_error($CON)."\n".$q);
	if(mysqli_num_rows($r))
		$a = mysqli_fetch_assoc($r) or LOG__Error("DB__Query1[B]", mysqli_error($CON)."\n".$q);
		return $a;
}

function DB__QueryAdd($CON, $q="") {
	$result = mysqli_query($CON, $q) or LOG__Error("DB__Query1[]", mysqli_error($CON)."\n".$q);
	return mysqli_insert_id($CON);
}


function DB__QueryN($CON, $q="") {
	$a = Array();
	$c = 0;
	$r = mysqli_query($CON, $q) or LOG__Error("DB__QueryN[]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		while($rec = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
			$a[$c] = $rec;
			$c++;
		}
	}
	return $a;
}


// ----- ----- ----- ----- ----- ----- ----- ----- ----- ----- [OLD FUNCTIONS]




function DB__Connect($HOST, $DB, $USER, $PSWD) {
	$CON = mysqli_connect($HOST, $USER, $PSWD, $DB) or die("Database error: Could not connect to "  . $HOST);
	mysqli_set_charset($CON, 'utf8');
	return $CON;
}


function DB__Close($CON) {
	if ($CON) return mysqli_close($CON);
	else return true;
}


function DB__Query($CON, $q="") {
	$result = mysqli_query($CON, $q);
	if(strpos(strtolower($q), 'insert')!==false) {
		$result = mysqli_insert_id($CON);
	}
	return $result;
}


function DB__Get_numRecord($CON, $table, $id, $condition='') {
	$q = "SELECT count(".$id.") FROM ".$table." ".$condition;
	$r = mysqli_query($CON, $q) or LOG__Error("countRecord[]", mysqli_error($CON)."\n".$q);
	//echo $q;
	if (mysqli_affected_rows($CON) >= 1) {
		$out = mysqli_result($r, mysqli_affected_rows($CON), 0);
	} else {
		$out = 0;
	}
	return $out;
}


function DB__Get_allRecords($CON, $table, $condition='') {
	$a = Array();
	$c = 0;
	$q = "SELECT * FROM ".$table." ".$condition;
	$r = mysqli_query($CON, $q) or LOG__Error("get_AllRecord[]", mysqli_error($CON)."\n".$q);
	// echo $q;
	if (mysqli_affected_rows($CON) >= 1) {
		while($rec = mysqli_fetch_array($r)) {
			$a[$c] = $rec;
			$c++;
		}
	}
	return $a;
}


function DB__Get_infoRecord($CON, $table='', $condition='') {
	$a = Array();
	$q = "SELECT * FROM ".$table." ".$condition;
	//echo $q;
	$r = mysqli_query($CON, $q) or LOG__Error("get_InfoRecord[]", mysqli_error($CON)."\n".$q);
	if(mysqli_affected_rows($CON) == 1) {
		$a = mysqli_fetch_array($r);
	}
	return $a;
}


function DB__Get_searchInRecord($CON, $table, $record, $key,$printQuery=0) {
	$a = Array();
	$c = 0;
	$q = "SELECT * FROM ".$table." WHERE ".$record." LIKE '%".$key."%'";
	$r = mysqli_query($CON, $q) or LOG__Error("search_in_record[]", mysqli_error($CON)."\n".$q);
	// echo $q;
	while($rec = mysqli_fetch_array($r)) {
		$a[$c] = $rec;
		$c++;
	}
	return $a;
}


function DB__Del_records($CON, $table, $elementId, $id) {
	// ATTENZIONE: METTERE UN CONTROLLO... POTREBBE CANCELLARE TUTTI I RECORD SE LA CONDIZIONE E' SEMPRE VERA
	$q = "DELETE FROM ".$table." WHERE ".$elementId." = '".$id."'";
	$r = mysqli_query($CON, $q) or LOG__Error("remove_Record[]", mysqli_error($CON)."\n".$q);
	//echo $q;
}


function DB__updateRecord($CON, $table, $el_name, $el_value, $condition) {
	// ATTENZIONE: METTERE UN CONTROLLO... POTREBBE AGGIORNARE TUTTI I RECORD SE LA CONDIZIONE E' SEMPRE VERA
	$q = "UPDATE ".$table." SET ".$el_name." = '".$el_value."' ".$condition;
	$r = mysqli_query($CON, $q) or LOG__Error("remove_Record[]", mysqli_error($CON)."\n".$q);
}


function mysqli_result($res, $row, $field=0) {
	$res->data_seek($row);
	$datarow = $res->fetch_array();
	return $datarow[$field];
}
