<?php
/*
  Lib:         ctrl_CheckForm Function
  Version:     0.1.20090806
  Author:      SLivio & MiniOwl
  Contact:     silvio.coco@gmail.com
  Copiryght:   http://www.ctrl-area.com/copyright
*/


function checkField_Email($email=''){
    $email = trim($email);
    if(!$email) {
        return false;
    }
    $num_at = count(explode( '@', $email )) - 1;
    if($num_at != 1) {
        return false;
    }
    if(strpos($email,';') || strpos($email,',') || strpos($email,' ')) {
        return false;
    }
    if(!preg_match( '/^[\w\.\-]+@\w+[\w\.\-]*?\.\w{1,4}$/', $email)) {
        return false;
    }
    return true;
}



function checkField_Empty($field='') {
	$OUT   = false;
	$field = strip_tags($field);
	$field = trim($field);
	if ($field!='')	$OUT = true;
	return $OUT;
}

// restituisce true se � minorenne
function checkField_Minorenne($anno, $mese, $giorno) {
	// inizializzazione delle date
	$Yy = date('Y');
	$Mm = date('m');
	$Dd = date('j');
	// check
	$return = true;
	if(($Yy-$anno)<=17) { // minorenne
		$return = true;
		//alert('minorenne 1? '+_return);
	} else if(($Yy-$anno)==18) { // 18 nell'anno in corso
		if (($Mm)-$mese >= 1) {
			$return = false;
			//alert('minorenne 2? '+_return);
		} else if (($Dd)-$giorno >= 1) {
			$return = false;
			//alert('minorenne 3? '+_return);
		} else {
			$return = true;
			//alert('minorenne 4? '+_return);
		}
	} else {
		$return = false;
	}
	//alert('minorenne? '+_return);
	return $return; 
}

?>