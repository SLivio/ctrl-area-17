<?php
/*
  Version:	2.0100421
*/

function getDate_MonthDays($mese, $anno) {
	$keyM  = (int)($mese-1);
	$aDayM = Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	if ($keyM == 1 && (bcmod($anno, 4) ==0)) $return = 29;
	else                                     $return = $aDayM[$keyM];
	return $return;
}

function getDate_MonthIt($mese) {
	$aMesi = Array("" , "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre");
	return $aMesi[(int)$mese];
}

function getDate_It($data) {
	$a = explode("-", $data);
	if (count($a) == 3) {
		$aMesi = Array("" , "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre");
		$return = $a[2]." ".$aMesi[((int)$a[1])]." ".$a[0];
	} else {
		$return = date('d/m/Y');
	}
	return $return;
}

function getDate_Year($data) {
	$aData = explode('-', $data);
	return $aData[0];
}
function getDate_Month($data) {
	$aData = explode('-', $data);
	return $aData[1];
}
function getDate_Day($data) {
	$aData = explode('-', $data);
	return substr($aData[2], 0, 2);
}

function convert_FromDatetimeToTimestamp($str) {
	list($date, $time) = explode(' ', $str);
	list($year, $month, $day) = explode('-', $date);
	list($hour, $minute, $second) = explode(':', $time);
	$timestamp = mktime($hour, $minute, $second, $month, $day, $year);
	return $timestamp;
}


?>