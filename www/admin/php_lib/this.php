<?php
/*
  Version:     6.2014.05.20
  Module:      Common Lib
  Author:      SLivio
*/



//--
//-------------------------------------> [DB_LIBS]


// LastUpdate 2014.11.20
function GLOBAL_GetUser($what='') {
	GLOBAL $SESSION_ID;
	
	$out = false;
	switch($what) {
		case 'c_id' :
		case 'country_id' :
			$out = $_SESSION[$SESSION_ID]['info']['country_id'];
		break;
		case 'c_label' :
		case 'c_name' :
		case 'country_label' :
		case 'country_name' :
			$out = $_SESSION[$SESSION_ID]['info']['country_name'];
		break;
		case 'u_id' :
		case 'user_id' :
			$out = $_SESSION[$SESSION_ID]['admin'];
		break;
		case 'u_email' :
		case 'user_email' :
			$out = $_SESSION[$SESSION_ID]['admin_email'];
		break;
		case 'user_half_email' :
			$aTmp = explode("@", $_SESSION[$SESSION_ID]['admin_email']);
			$out  = $aTmp[0];
		break;
	}
	return $out;
}




//-------------------------------------> [UTILITY_LIBS]

// LastUpdate 2014.11.20
function GLOBAL_AdminSetUp($id=0) {
	GLOBAL $SESSION_ID;
	$CON = $_SESSION[$SESSION_ID]['db'];
	
	if ($id==0) return false;
	//
	// recupero gruppi dell'utente
	// (?) recupero ultimo accesso
	//
	$aOut = Array(); // return array
	$aU   = DB__Get_infoRecord($CON, DB_PREFIX.'core_admin', 'WHERE user_id="'.$id.'"', 0);
		// country
		$aCountry = DB__Get_infoRecord($CON, DB_PREFIX.'core_geo_country', 'WHERE country_id="'.$aU['country_id'].'"', 0);
		$aOut['country_id']    = isset($aCountry['country_id'])    ? $aCountry['country_id']    : '0';
		$aOut['country_name']  = isset($aCountry['country_name'])  ? $aCountry['country_name']  : '';
		$aOut['country_flag']  = isset($aCountry['country_flag'])  ? $aCountry['country_flag']  : '';
		$aOut['country_code']  = isset($aCountry['country_code'])  ? $aCountry['country_code']  : '';
		// area
		$aArea = DB__Get_infoRecord($CON, DB_PREFIX.'core_geo_area', 'WHERE area_id="'.$aU['area_id'].'"', 0);
		$aOut['area_id']   = isset($aArea['area_id'])   ? $aArea['area_id']   : '0';
		$aOut['area_name'] = isset($aArea['area_name']) ? $aArea['area_name'] : '';
		// city
		$aCity = DB__Get_infoRecord($CON, DB_PREFIX.'core_geo_city', 'WHERE city_id="'.$aU['city_id'].'"', 0);
		$aOut['city_id']   = isset($aCity['city_id'])   ? $aCity['city_id']   : '0';
		$aOut['city_name'] = isset($aCity['city_name']) ? $aCity['city_name'] : '';
	// get groups information
	$aG = Array();
	$c  = 0;
	$q  = "SELECT ".DB_PREFIX."core_groups.group_id,".DB_PREFIX."core_groups.group_label FROM ".DB_PREFIX."core_admin_groups,".DB_PREFIX."core_groups,".DB_PREFIX."core_admin
	WHERE ".DB_PREFIX."core_admin_groups.group_id=".DB_PREFIX."core_groups.group_id and
	".DB_PREFIX."core_admin_groups.user_id = ".DB_PREFIX."core_admin.user_id and
	".DB_PREFIX."core_admin.user_id=".$id."
	 ORDER BY group_weight DESC";
	$r = mysqli_query($CON, $q) or LOG__Error("GLOBAL_AdminSetUp[]", mysqli_error()."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		while($rec = mysqli_fetch_array($r)) {
			$aG[$c]['group_id']    = $rec['group_id'];
			$aG[$c]['group_label'] = $rec['group_label'];
			$c++;
		}
	}
	$aOut['groups']= $aG;
	//
	return $aOut;
	
}

//-------------------------------------> [OTHER_LIBS]


// LastUpdate 2014.11.20
function GLOBAL_AdminModules_ArrayDiff($aModules=Array(), $aUserModules=Array()) {
	$aOut = Array();
	if(count($aUserModules)==0) { // L'utente non ha permessi... restituisco tutto
		$aOut = $aModules;
	} else {
		foreach($aModules as $mod) { // scansione dell'array pi˘ pieno
			$aOut[] = $mod; // nizialmente lo inserisco tra i non presenti
			foreach($aUserModules as $umod) {
				if ($mod['module_id'] == $umod['module_id']) {
					array_pop($aOut);
				}
			}
		}
	}
	return $aOut;
}


// LastUpdate 2014.11.20
function GLOBAL_Get_GlobalQuery() {
	GLOBAL $CONF, $aModules, $aModule, $op, $m, $p, $opp;
	$aP = Array('op' => $op, 'm' => $m, 'p' => $p, 'opp' => $opp);
	return $aP;
}


// LastUpdate 2014.11.20
function LOCAL_Parser_ModuleOp($str='') {
	$OUT = Array();
	if ($str=='') { 
		return $OUT; 
	} // Ë vuota... non ci sono limitazioni
	//
	$str = str_replace(' ', '', $str); // eliminamo gli spazi
	$OUT = explode(',', $str);
	$OUT = array_unique($OUT);
	return $OUT;
}


// LastUpdate 3.201111.28 [dev]
function GLOBAL_Email2Group($aEmail=Array(), $gid=0) {
	if (empty($aEmail)) return false;
	if ($gid==0) return false;
	$aU = GDB__Get_UserGroup($gid);
	foreach($aU as $u) {
		//
		$mail = new PHPMailer();
		$mail->From     	 = EM_FROM_EMAIL;
		$mail->FromName 	 = EM_FROM_NAME;
		//$mail->Host     	 = 'localhost';
		//$mail->Mailer   	 = 'localhost';
		//$mail->IsSMTP();
		//$mail->SMTPAuth 	 = false;
		$mail->ContentType = "text/html";
		$mail->Body    	   = $aEmail['html'];
		$mail->AltBody 	   = $aEmail['txt'];
		$mail->Subject 	   = $aEmail['subject'];
		$mail->AddAddress($u['user_email'], $u['user_name']);
		//
		if(!$mail->Send()) $ok = false;
		else $ok = true;
		//
		$mail->ClearAddresses();
		$mail->ClearReplyTos();
	}
}


// LastUpdate 3.201112.05 [dev]
function GLOBAL_Email2Address($aEmail=Array()) {
	if (empty($aEmail)) return false;
	if (!isset($aEmail['user_email']) || checkField_Email($aEmail['user_email'])==false) return false;
	// from 
	$aEmail['from_email'] = isset($aEmail['from_email']) ? $aEmail['from_email'] : EM_FROM_EMAIL;
	$aEmail['from_name']  = isset($aEmail['from_name'])  ? $aEmail['from_name']  : EM_FROM_NAME;
	// to
	$aEmail['user_name']  = isset($aEmail['user_name']) ? $aEmail['user_name'] : $aEmail['user_email'];
	// send
	$mail = new PHPMailer();
	$mail->From     	 = $aEmail['from_email'];
	$mail->FromName 	 = $aEmail['from_name'];
	$mail->ContentType = "text/html";
	$mail->Body    	   = $aEmail['html'];
	$mail->AltBody 	   = $aEmail['txt'];
	$mail->Subject 	   = $aEmail['subject'];
	$mail->AddAddress($aEmail['user_email'], $aEmail['user_name']);
	// return 
	if(!$mail->Send()) $ok = false;
	else $ok = true;
	// clean memory
	$mail->ClearAddresses();
	$mail->ClearReplyTos();
	//
	return $ok;
}

//-------------------------------------> [JSON/JS_LIBS]


// LastUpdate 4.201112.16 [ok]
function JSON_FAQ($op=0, $msg='Default error.') {
	return '{ "op" : "'.$op.'", "msg" : "'.$msg.'", "random" : "'.get_RandomString().'"  }';
}


// LastUpdate 4.2012.01.10 [test]
function JSON_FromDbRecord($ar=Array(), $single=false) {
	$aTmpR = Array(); // Temp record
	$out   = '';
	if ($single==true) {
		foreach($ar as $key=>$val) {
			$aTmpF[] = '"'.$key.'":"'.$val.'"';
		}
		$out  = '{'.implode(',', $aTmpF).'}';
	} else {
		foreach($ar as $el) {
			$aTmpF = Array(); // Temp field
			$tmp   = '{';
			
			foreach($el as $key=>$val) {
				$aTmpF[] = '"'.$key.'":"'.$val.'"';
			}
			$tmp  .= implode(',', $aTmpF);
			$tmp  .= '}';
			$aTmpR[] = $tmp;
		}
		$out = '['.implode(',', $aTmpR).']';
	}
	return $out;
}


// LastUpdate 4.201112.16 [ok]
function JS_Goto_EditPage($m,$op,$id) {
	// This function go to edit page after 3 second 
	// 
	return '<p>
	You will be redirected to the edit page in <span id="crono">3</span> seconds.
	</p><script>
	window.setTimeout("document.location.href=\'?m='.$m.'&op='.$op.'&id='.$id.'\'", 3000);
	window.setTimeout("$(\'#crono\').html(\'2\')", 1000);
	window.setTimeout("$(\'#crono\').html(\'1\')", 2000);
	</script>';
}


/*
// LastUpdate 4.201112.16 [test]
function get_App() {
	GLOBAL $CONF, $aModules, $aModule;
	$m  = isset($_REQUEST['m'])  ? $_REQUEST['m']  : '';
	$op = isset($_REQUEST['op']) ? $_REQUEST['op'] : '';
	if ($m != '') {
			$aModule   = GDB__Get_InfoModule($m);
			$tmp_conf  = $CONF['path_modules'].'/'.$aModule['module_dir'].'/conf.php';
			$tmp_index = $CONF['path_modules'].'/'.$aModule['module_dir'].'/index.php';
			include($tmp_conf);
			include_once($tmp_index);
			die();
	}
}
*/

function GLB_qsCrypt($str='') {
	$crypt = bin2hex($str);
	return $crypt;
}

function GLB_qsDecrypt($crypt=''){
	$str = hex2bin($crypt);
	//echo $str;
	return $str;
}

// LastUpdate 4.201704.02 [test]
function get_App($crypt='') {
	GLOBAL $CONF, $aModules, $aModule;
	if($crypt!='') {
		$str = GLB_qsDecrypt($crypt);
		parse_str($str, $aOut);
		$m  = isset($aOut['m'])  ? $aOut['m']  : '';
		$op = isset($aOut['op']) ? $aOut['op'] : '';
		$_REQUEST = array_merge($aOut, $_REQUEST);
	} else {
		$m  = isset($_REQUEST['m'])  ? $_REQUEST['m']  : '';
		$op = isset($_REQUEST['op']) ? $_REQUEST['op'] : '';
	}
	//echo '<br>m:'.$m.' | op:'.$op;
	
	if ($m != '') {
		$aModule   = GDB__Get_InfoModule($m);
		//echo '<pre>'.print_r($aModule, 1).'</pre>';
		//echo '<pre>'.print_r($_REQUEST, 1).'</pre>';
		$tmp_conf  = $CONF['path_modules'].'/'.$aModule['module_dir'].'/conf.php';
		$tmp_index = $CONF['path_modules'].'/'.$aModule['module_dir'].'/index.php';
		//echo '<br>includo: '.$tmp_conf;
		//echo '<br>includo: '.$tmp_index;
		include($tmp_conf);
		include_once($tmp_index);
	}
	die(" ");
}

?>