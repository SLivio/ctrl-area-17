<?php
/*
  Version:       1.201309.24
  Author:        SLivio - silvio.coco@gmail.com
  Description:   Form builder in php with JS/HTML5/CSS generated code

  Mandatory lib: 
  	Jquery - http://jquery.com/
  	Jquery UI - http://jqueryui.com/
  	Timepicker - http://trentrichardson.com/examples/timepicker/
  	CKeditor - http://ckeditor.com/
  	

Create a webForm with this coe: 
	// just one line
	echo SLi_FormBuilder_Init($aForm);

Set default value with this code:
	// just one line 
	$aForm = SLi_FormBuilder_SetValue($aForm, $aFVal);

Get basic css class (and just one ajavascript funciont)
	// just one line... for the last time
	echo HTML__Get_Head_SLiFormBuilder();
	
Use this kind of array for default value and form fields (it's not a single line.. I know):

	// Array for default value
	$aFVal = Array(
		0 => Array('name'=>'test-text', 'value'=>'testo di test'),
		1 => Array('name'=>'test-textarea-html', 'value'=>'testo di test per la textarea in html'),
		2 => Array('name'=>'test-textarea-simple', 'value'=>'testo di test per la textarea semplice'),
		3 => Array('name'=>'test-select', 'value'=>'val-2'),
		3 => Array('name'=>'test-checkbox', 'value'=>'1'),
	);

	// Array for a form
	$aForm = Array();
	$aForm['form'] = Array(
		'name' => 'test',
		'id'   => 'fid1',
		'method' => 'post',
		'action' => '');
	$aForm['fields'] = Array(
		0 => Array(
			'label' => 'Test text',
			'name'  => 'test-text',
			'type'  => 'text',
			'validation'  => Array('empty'),
			'placeholder' => 'asd',
			'attribute' => ''),
		1 => Array(
			'label' => 'Test data',
			'name'  => 'test-data',
			'type'  => 'date',
			'validation'  => Array('empty'),
			'placeholder' => 'dd/mm/yyyy',
			'attribute' => Array('readonly'=>'readonly')),
		2 => Array(
			'label' => 'Test datatime',
			'name'  => 'test-datatime',
			'type'  => 'datetime',
			'placeholder' => 'dd/mm/yyyy hh:mm',
			'validation'  => Array('empty'),
			'attribute' => Array('readonly'=>'readonly')),
		3 => Array(
			'name'  => 'text-hidden',
			'type'  => 'hidden',
			'attribute' => Array('value'=>'4')),
		4 => Array(
			'label' => 'Test textarea',
			'name'  => 'test-textarea-simple',
			'type'  => 'textarea',
			'validation'  => Array('empty'),
			'placeholder' => '',
			'attribute' => ''),
		5 => Array(
			'label' => 'Test textarea',
			'name'  => 'test-textarea-html',
			'type'  => 'textarea',
			'validation'  => Array('empty'),
			'placeholder' => '',
			'attribute' => Array('class'=>'ckeditor')),
		6 => Array(
			'label' => 'Test checkbox',
			'name'  => 'test-checkbox',
			'type'  => 'checkbox',
			'value' => 1, // ok
			'validation'  => Array('checked'),
			'attribute' => ''),
		7 => Array(
			'label' => 'Select',
			'name'  => 'test-select',
			'id' => 'selectec-test',
			'type'  => 'select',
			'option' => Array(Array('val', 'label'), Array('val-2', 'label 2'), Array('val-3', 'label 3'), Array('val-4')),
			'value' => 'val-3', // ok
			'call' => 'Seleziona una voce...',
			'validation'  => Array('selected'),
			'placeholder' => '',
			'attribute' => ''),
		8 => Array(
			'label' => 'Test file light',
			'name'  => 'test-file',
			'type'  => 'file',
			'validation'  => Array(),
			'upload' => Array('path'=>'./tmp/', 'delete'=>'SLi_DeleteFile', 'maxsize'=>2048),
			'attribute' => Array('readonly'=>'readonly')),
		9 => Array(
			'label' => 'Bottone Submit ',
			'name'  => 'btn',
			'type'  => 'submit',
			'attribute' => Array('class'=>'btn-class', 'id'=>'btn-send'))
);


*/

if (!defined('DEBUG')) {
	define('DEBUG', 0);
}

function SLi_FormBuilder_HTMLfield($aForm=Array(), $f=Array()) {
	/*
	 * Last Update: 2014.04.16 13.26
	 * Questa funzione genera l'html del form.
	 * Ha in ingresso il form (tutto) e il campo da generare 
	 * */
	// controllo le propriet� di base del campo
	// genero il tipo di campo
	$name        = ($f['name']!='')        ? 'name="'.$f['name'].'"' : '';
	$id          = ($f['id']!='')          ? 'id="'.$f['id'].'"' : '';
	$value       = ($f['value']!='')       ? 'value="'.$f['value'].'"' : ''; // non usate per textarea e checkbox
	$placeholder = ($f['placeholder']!='') ? 'placeholder="'.$f['placeholder'].'"' : '';
	// aggiunta dei class
	if (isset($f['attribute']) && is_array($f['attribute'])) {
		$class       = 'class="';
		foreach($f['attribute'] as $key => $attr) {
			if (strtolower($key)=='class') {
				$class .= ' '.$attr;
			}
		}
		$class      .= ' form-control"';
	} else {
		$class = ' class="form-control" ';
	}
	// aggiunta dell'asterisco per i campi obbligatori
	$mandatory = (isset($f['validation']) && is_array($f['validation']) && in_array('empty', $f['validation'])) ? '*' : '';
	// agiunta del placeholder
	//$placeholder = (isset($f['placeholder']) && $f['placeholder']!='') ? ' placeholder="'.$placeholder.'" ' : '';
	// aggiunta dei tips
	// html di base
	$OUT = '<div class="form-group">';
	switch($f['type']) {
		case 'text' :
			$OUT .= '<label class="col-md-2 control-label">'.$f['label'].':<sup><i>'.$mandatory.'</i></sup></label>
			<div class="col-md-8"><input type="text" '.$name.' '.$id.' '.$placeholder.' '.$value.' '.$class.'></div>';
		break;
		case 'file' : // file need: simple, advansed search, simple or advanced with delete
			$f['upload'] = isset($f['upload']) ? $f['upload'] : Array();
			$delete      = (isset($f['upload']['delete']) && $f['upload']['delete']!='') ? $f['upload']['delete'] : false;
			$path        = (isset($f['upload']['path']) && $f['upload']['path']!='') ? $f['upload']['path'] : '';
			$OUT .= '<label class="col-md-2 control-label">'.$f['label'].':<sup><i>'.$mandatory.'</i></sup></label>';
			if ($value!='' && $delete!='') {
				$OUT .= '<input type="hidden" '.$name.' '.$value.' id="old-'.$f['id'].'">
				<span style="display:none"><input type="file" '.$name.' '.$id.' '.$class.'></span>';
				$OUT .= ' <a href="'.$path.$f['value'].'" target="_blank" id="view-'.$f['id'].'">view</a>';
				$OUT .= ' <a href="javascript:;" onClick="SLi_DelConfirm(\''.$f['id'].'\');" id="del-'.$f['id'].'">delete</a>';
			} else {
				$OUT .= '<div class="col-md-8"><input type="file" '.$name.' '.$id.' '.$class.'></div>';
			}
		break;
		case 'date' :
			$class = str_replace('class="', 'class="datepicker', $class);
			$OUT .= '<label class="col-md-2 control-label">'.$f['label'].':<sup><i>'.$mandatory.'</i></sup></label>
			<div class="col-md-4"><input type="text" '.$name.' '.$id.' '.$placeholder.' '.$value.' '.$class.'></div>';
		break;
		case 'datetime' :
			$class = str_replace('class="', 'class="datetimepicker', $class);
			$OUT .= '<label class="col-md-2 control-label">'.$f['label'].':<sup><i>'.$mandatory.'</i></sup></label>
			<div class="col-md-8"><input type="text" '.$name.' '.$id.' '.$placeholder.' '.$value.' '.$class.'></div>';
		break;
		case 'textarea' :
			$OUT .= '<label class="col-md-2 control-label">'.$f['label'].':<sup><i>'.$mandatory.'</i></sup></label>
			<div class="col-md-8"><textarea '.$name.' '.$id.' '.$placeholder.' '.$class.'>'.$f['value'].'</textarea></div>';
		break;
		case 'select' :
			$OUT .= '<label class="col-md-2 control-label">'.$f['label'].':<sup><i>'.$mandatory.'</i></sup></label>
			<div class="col-md-8"><select '.$name.' '.$id.' '.$class.'>';
			if(isset($f['call']) && $f['call']!='') {
				$OUT .= '<option value="">'.$f['call'].'</option>';
			}
			if(is_array($f['option'])) {
				foreach($f['option'] as $option) {
					$tmpVal   = (isset($option[0])) ? $option[0] : '';
					$tmpLabel = (isset($option[1])) ? $option[1] : $option[0];
					$selected = (isset($f['value']) && $f['value']==$option[0]) ? ' selected="selected" ' : '';
					$OUT .= '<option value="'.$tmpVal.'" '.$selected.'>'.$tmpLabel.'</option>';
				}
			}
			$OUT .= '</select></div>';
		break;
		case 'checkbox' :
			$class   = str_replace('form-control', '', $class);
			$checked = ($f['value']=='1') ? 'checked="checked"' : '';
			$OUT .= '<label class="col-md-2 control-label">'.$f['label'].':<sup><i>'.$mandatory.'</i></sup></label>
			 <input type="checkbox" '.$name.' '.$id.' '.$value.' '.$class.' '.$checked.' >';
		break;
	}
	$OUT .= '</div>';
	// hidden, senza formattazione
	switch($f['type']) {
		case 'hidden' :
			$OUT = '<input type="hidden" '.$name.' '.$id.' '.$value.'>';
		break;
		case 'submit' :
			$OUT = '<div class="btn">
			<input type="submit" value="'.$f['label'].'" '.$name.' '.$id.' '.$value.' '.$class.'>
			</div>';
		break;
	}

	return $OUT;
}


function SLi_FormBuilder_JSvalidation($aForm=Array(), $aField=Array()) {
	/*
	 * Last Update: 2013.09.13 00.32
	 * Questa funzione genera il javascript per la validazione
	 * Ha in ingresso il form (tutto) e il campo da generare 
	 * */
	$JS = 'tmpJQ = "#'.$aField['id'].'"; ';
	if(isset($aField['validation']) && is_array($aField['validation'])) {
		// TEXT,TEXTARE -> Avalidazione campo vuoto
		if(in_array('empty', $aField['validation'])) {
			$JS .= ' if($(tmpJQ).val()=="") { 
			$(tmpJQ).addClass("has-error");
			$(tmpJQ).closest(".form-group").addClass("has-error")
			valid=false; } 
			SLI_FormBuilder_JSdebug("Valide after "+tmpJQ+" > " + valid); ';
		}
		// SELECT -> validazione voce selezionata
		if($aField['type']=="select" && in_array('selected', $aField['validation'])) {
			$JS .= ' var tmpJQoption = tmpJQ+" option:selected"; 
			if($(tmpJQoption).val()=="") { 
			//$(tmpJQoption).addClass("has-error");
			$(tmpJQoption).closest(".form-group").addClass("has-error")
			valid=false; } 
			SLI_FormBuilder_JSdebug("Valide after "+tmpJQ+" > " + valid + " > " + tmpJQoption + ": "+$(tmpJQoption).val()); ';
		} 
		// OPTION -> validazione obbligo checked
		if($aField['type']=="checkbox" && in_array('checked', $aField['validation'])) {
			$JS .= ' var tmpJQckbox = tmpJQ+":checked";
			if($(tmpJQckbox).length==0) { 
			$(tmpJQckbox).addClass("has-error");
			//$(tmpJQckbox).closest(".form-group").addClass("has-error")
			valid=false; } 
			SLI_FormBuilder_JSdebug("Valide after "+tmpJQ+" > " + valid + " > " + tmpJQckbox + ": "+$(tmpJQckbox).length); ';
		}
	} else {
		$JS .= ' SLI_FormBuilder_JSdebug("No validation for: "+tmpJQ); ';
	}
	return $JS;
}


function SLi_FormBuilder_Analyze($aF=Array()) {
	/*
	 * Last Update: 2013.09.13 01.16
	 * Questa funzione analizza il form e i campi... se trova anomalie le sistema. 
	 */
	// analisi del form
	if(!isset($aF['form'])) {
		$aF['form'] = Array(
			'name' => 'test',
			'id'   => 'fid1',
			'method' => 'post',
			'action' => ''
		);
	}
	$aF['form']['name']   = (!isset($aF['form']['name']))   ? 'form' : $aF['form']['name'];
	$aF['form']['id']     = (!isset($aF['form']['id']))     ? 'fid'  : $aF['form']['id'];
	$aF['form']['method'] = (!isset($aF['form']['method'])) ? 'post' : $aF['form']['method'];
	$aF['form']['action'] = (!isset($aF['form']['action'])) ? ''     : $aF['form']['action'];
	// analisi dei campi
	if(!isset($aF['fields']) || !is_array($aF['fields'])) {
		$aF['fields'] = Array(
			0 => Array(
				'label' => 'Errore ',
				'id'    => 'err-id',
				'name'  => 'test-text',
				'type'  => 'text'
			)
		);
	}
	// campo per campo
	$c = 0;
	$aNewF['fields'] = Array();
	foreach($aF['fields'] as $f) {
		// check sull'id
		if (!isset($f['id'])) {
			if(isset($f['attribute']) && is_array($f['attribute'])) {
				foreach($f['attribute'] as $key => $attr) {
					if ($key=="id" && $attr!="") {
						$f['id'] = $attr;
					}
				}
			}
			if (!isset($f['id'])) {
				$f['id'] = $aF['form']['id'].'-'.rand(111,999);
			}
		}
		// check sul valore
		if (!isset($f['value'])) {
			if(isset($f['attribute']) && is_array($f['attribute'])) {
				foreach($f['attribute'] as $key => $attr) {
					if ($key=="value" && $attr!="") {
						$f['value'] = $attr;
					}
				}
			}
			if (!isset($f['value'])) {
				$f['value'] = '';
			}
		}
		// check sugli altri campi
		$f['name']        = (!isset($f['name']))        ? 'null-'.rand(0,999) : $f['name'];
		$f['type']        = (!isset($f['type']))        ? 'text'              : $f['type'];
		$f['label']       = (!isset($f['label']))       ? $f['name']          : $f['label'];
		$f['placeholder'] = (!isset($f['placeholder'])) ? ''                  : $f['placeholder'];
		// reset e new set
		$aNewF['fields'][$c] = $f;
		$c++;
	}
	unset($aF['fields']);
	$aF['fields'] = $aNewF['fields'];
	// restituisco il form pulito
	return $aF;
}


function SLI_FormBuilder_JSdebug($str='') {
	if (DEBUG==1) {
		return ' var debugK; $(function() { 
			if($("#SLi_debug").length > 0) { debugK="#SLi_debug"; } 
			else if($(".SLi_debug").length > 0) { debugK=".SLi_debug"; } 
			else { $("body").append("<div id=\'SLi_debug\'></div>"); debugK="#SLi_debug"; }
			//$(debugK).append("<a href=\'javascript:;\' onClick=\'$(debugK).html(\"\")\'>clear</a>");
			SLi_FormBuilder_JSdebugClear();
		});
		function SLi_FormBuilder_JSdebugClear() {
			$(debugK).append("<a href=\'javascript:;\' onClick=\'$(debugK).html(\"\");SLi_FormBuilder_JSdebugClear();\'>clear</a>");
		}
		function SLI_FormBuilder_JSdebug(str) {
			$(debugK).append("<br>").append(document.createTextNode(str));
		} SLI_FormBuilder_JSdebug(""); ';
	} else {
		return ' function SLI_FormBuilder_JSdebug(v) { } ';
	}
	
}


function SLi_FormBuilder_Init($aF=Array(), $aTemplate=Array()) {
	// inizializzazione delle variabili
	$FORM      = '';
	$FIELDS    = '';
	$VAL       = '';
	// controllo input
	$aTemplate = (isset($aTemplate) && is_array($aTemplate) && !empty($aTemplate)) ? 	$aTemplate : Array('<!-- [Form Start] -->','<!-- Form End-->'); 
	if(!is_array($aF)) return false;
	$aF = SLi_FormBuilder_Analyze($aF); // analyze and repair
	// JS validation
	$validation     = false;
	$datepicker     = false;
	$datetimepicker = false;
	$ckeditor       = false;
	$tinymce        = false;
	foreach($aF['fields'] as $f) {
		if(isset($f['validation'])) $validation = true;
		if($f['type']=='date') $datepicker = true;
		if($f['type']=='datetime') $datetimepicker = true;
		if(isset($f['attribute']) && is_array($f['attribute']) && in_array('ckeditor', $f['attribute'])) $ckeditor = true;
		if(isset($f['attribute']) && is_array($f['attribute']) && in_array('tinymce', $f['attribute'])) $tinymce = true;
	}
	if($validation==true) {
		$VAL .= ' '.SLI_FormBuilder_JSdebug().' ';
		$VAL .= 'function SLi_FormBuilder_Validation() { ';
		$VAL .= ' $("#'.$aF['form']['id'].' *").removeClass("has-error"); ';
		$VAL .= ' var valid=true; ';
		// for each field
		foreach($aF['fields'] as $f) {
			// get html
			$FIELDS .= SLi_FormBuilder_HTMLfield($aF, $f);
			// get js validation
			$VAL .= SLi_FormBuilder_JSvalidation($aF, $f);
		}
		$VAL .= ' //return false;
		if(valid==true) { return true; } else { SLi_scrollToAnchor("a-'.$aF['form']['name'].'"); return false; } }';
	} else {
		foreach($aF['fields'] as $f) {
			// get html
			$FIELDS .= SLi_FormBuilder_HTMLfield($aF, $f);
		}
	}
	// plugin: ckeditor, date/datetime picker, ...
	$VAL .= ' $(function() { ';
	if($datepicker==true) $VAL .= ' $(".datepicker").datepicker({ format: "yyyy-mm-dd" }); ';
	if($datetimepicker==true) $VAL .= ' $(".datetimepicker").datetimepicker({ dateFormat: "yy-mm-dd", timeFormat: "HH:mm:ss" }); ';
	if($ckeditor==true) $VAL .= ' $("textarea.ckeditor").ckeditor({ "width":660 }); ';
	if($tinymce==true) $VAL .= ' $("textarea.tinymce").tinymce({ script_url : "lib/tinymce/tinymce.min.js", theme : "modern" }); ';
	$VAL .= ' }); ';
	
	// HTML form
	//foreach($aF[fields] as $f) { }
	
	$FORM .= ($VAL!='') ? '<script>'.$VAL.'</script>' : '';
	$FORM .= (isset($aTemplate[0])) ? $aTemplate[0] : '';
	$FORM .= '<a name="a-'.$aF['form']['name'].'"></a>';
	$FORM .= '<form name="'.$aF['form']['name'].'" id="'.$aF['form']['id'].'" method="'.$aF['form']['method'].'" class="form-horizontal" ';
	$FORM .= ' enctype="multipart/form-data" ';
	if(isset($aF['form']['stop']) && $aF['form']['stop']==true) {
		$FORM .= ($VAL!='') ? 'onSubmit="return false;"' : '';
	} else {
		$FORM .= ($VAL!='') ? 'onSubmit="return SLi_FormBuilder_Validation(this);"' : ''; 
	}
	$FORM .= ' action="'.$aF['form']['action'].'">';
	$FORM .= '<fieldset class="formBuilder">';
	$FORM .= $FIELDS;
	$FORM .= '</fieldset></form>';
	$FORM .= (isset($aTemplate[1])) ? $aTemplate[1] : '';
	return $FORM;
}


function SLi_FormBuilder_SetValue($aF=Array(), $aFVal=Array()) {
	$aTmp = Array();
	foreach($aF['fields'] as $key => $field) {
		//echo $aF['fields'] .' - '. $key .' - '. $field;
		if(isset($field['name'])) {
			//echo '<br><b>'.$field['name'].'</b>';
			//foreach($aFVal as $aVal) {
			foreach($aFVal as $k => $aVal) {
				//echo '<br>- '. $k .' - '.' -- '.$field['name'].' -	'.$aVal;
				if($field['name']==$k) {
					//echo '<br>->'.$aVal['name'].' = '.$aVal['value'];
					$field['value'] = $aVal;
				}
			}
		}
		$aTmp[$key] = $field;
	}
	$aF['fields'] = Array();
	$aF['fields'] = $aTmp;
	return $aF;
}

?>