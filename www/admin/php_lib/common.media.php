<?php
/*
  Lib:         ctrl_Media Function
  Version:     0.1.20090806
  Author:      SLivio & MiniOwl
  Contact:     silvio.coco@gmail.com
  Copiryght:   http://www.ctrl-area.com/copyright
*/


function getMedia_Mov($file, $w, $h) {
	$OUT = '<object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab" height="'.$h.'" width="'.$w.'">'.
	       '<param name="src" value="'.$file.'">'.
	       '<param name="autoplay" value="true">'.
	       '<param name="controller" value="true">'.
	       '<param name="bgcolor" value="#fff">'.
	       '<embed type="video/quicktime" src="'.$file.'" autoplay="true" controller="true" bgcolor="#fff" height="'.$h.'" width="'.$w.'">'.
	       '</object>';
	return $OUT;
}

function getMedia_Wmv($file, $w, $h) {
	$OUT = '<object id="MediaPlayer" classid="CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95" type="application/x-oleobject" height="'.$h.'" width="'.$w.'">'.
	       '<param name="FileName" value="'.$file.'">'.                                                              
	       '<param name="autostart" value="1">'.
	       '<param name="showcontrols" value="1">'.
	       '<param name="bgcolor" value="#fff">'.
	       '<embed type="application/x-mplayer2" src="'.$file.'" autostart="1" showcontrols="1" bgcolor="#fff" height="'.$h.'" width="'.$w.'">'.
	       '</object>';
	return $OUT;
}

function getMedia_Mp3($file, $w='120', $h='16') {
	$OUT = '<embed src="'.$file.'" height="'.$h.'" width="'.$w.'"></embed>';
	return $OUT;
}

function getMedia_Wma($file, $w='120', $h='16') {
	$OUT = '<object data="'.$file.'" autostart="true" height="'.$h.'" width="'.$w.'">'.
	       '<embed src="'.$file.'" autostart="true" height="'.$h.'" width="'.$w.'">'.
	       '</object>';
	return $OUT;
}

function getUrl_Embed($code, $site='youtube') {
	$OUT = '';
	switch($site) {
		case 'youtube' :
			$code = str_replace('\'', '"', $code);
			$pos1 = stripos($code, 'src="');
			$pos2 = stripos($code, '"', ($pos1+5));
			$OUT  = substr($code, ($pos1+5), ($pos2-$pos1-5));
		break;
	}
	return $OUT;
}

function img_resize($fileName, $fileWidth, $fileHeight, $filePercent){
	$filePercent = $filePercent / 100;
	$fileNameOut = $fileName;
	$ok          = false;
	//
	if(file_exists($fileNameOut)) {
		// Get Image size ant File Type
		list($width, $height, $fileType) = getimagesize($fileName);
		// Create and Set new sizes
		if($filePercent != ''){
			$newWidth  = $width * $filePercent;
			$newHeight = $height * $filePercent;
		} else {
			if ($fileHeight == '' || $fileHeight == 0) {
				// Calcolo il ridimensionamento percentuale solo per l'altezza
				if($width > $fileWidth){
					$newWidth  = $fileWidth;
					$newHeight = (int)(($fileWidth * $height) / $width);
					$ok        = true;
				}
			}
			if ($fileWidth == '' || $fileWidth == 0) {
				// Calcolo il ridimensionamento percentuale solo per la larghezza
				if($height > $fileHeight){
					$newHeight = $fileHeight;
					$newWidth  = (int)(($fileHeight * $width) / $height);
					$ok        = true;
				}
			}
		}
		//
		if($ok == true){
			//Check if GIF or JPEG
			if($fileType == 1) {
				// Load
				$thumb = imagecreatetruecolor($newWidth, $newHeight);
				$source = imagecreatefromgif($fileName);
				// Resize
				imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
				// Output
				imagegif($thumb,$fileNameOut);
			} elseif($fileType == 2) {
				// Load
				$thumb = imagecreatetruecolor($newWidth, $newHeight);
				$source = imagecreatefromjpeg($fileName);
				// Resize
				imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
				// Output
				imagejpeg($thumb,$fileNameOut);
			} else if($fileType == 3) {
				// Load
				$thumb       = imagecreatetruecolor($newWidth, $newHeight);
				$source      = imagecreatefrompng($fileName);
				$trnprt_indx = imagecolortransparent($source);
				// If we have a specific transparent color
				if ($trnprt_indx >= 0) {
					// Get the original image's transparent color's RGB values
					$trnprt_color    = imagecolorsforindex($source, $trnprt_indx);
					// Allocate the same color in the new image resource
					$trnprt_indx    = imagecolorallocate($thumb, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
					// Completely fill the background of the new image with allocated color.
					imagefill($thumb, 0, 0, $trnprt_indx);
					// Set the background color for new image to transparent
					imagecolortransparent($thumb, $trnprt_indx);
				} else {
					// Turn off transparency blending (temporarily)
					imagealphablending($thumb, false);
					// Create a new transparent color for image
					$color = imagecolorallocatealpha($thumb, 0, 0, 0, 127);
					// Completely fill the background of the new image with allocated color.
					imagefill($thumb, 0, 0, $color);
					// Restore transparency blending
					imagesavealpha($thumb, true);
				}
				// Resize
				imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
				// Output
				imagepng($thumb,$fileNameOut);
			}
		}
	}
	return $fileNameOut;
}

/*
function img_resize($fileName, $fileWidth, $fileHeight, $filePercent){
	$filePercent = $filePercent / 100;
	$fileNameOut = $fileName;

	if(file_exists($fileNameOut)) {
	
		// Get Image size ant File Type
		list($width, $height, $fileType) = getimagesize($fileName);

		// Create and Set new sizes
		if($filePercent != ''){
			$newWidth = $width * $filePercent;
			$newHeight = $height * $filePercent;
		} else {
			$newWidth = $fileWidth;
			$newHeight = $fileHeight;
		}

		if ($fileHeight == '' || $fileHeight == 0) {
			// Get Image size ant File Type
			list($width, $height, $fileType) = getimagesize($fileName);
			// Calcolo il ridimensionamento percentuale solo per l'altezza
			if ($fileHeight == '' || $fileHeight == 0)
				$newHeight = (int)(($fileWidth * $height) / $width);
		}

		//Check if GIF or JPEG
		if($fileType == 1) {
			// Load
			$thumb = imagecreatetruecolor($newWidth, $newHeight);
			$source = imagecreatefromgif($fileName);
			// Resize
			imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
			// Output
			imagegif($thumb,$fileNameOut);
		} elseif($fileType == 2) {
			// Load
			$thumb = imagecreatetruecolor($newWidth, $newHeight);
			$source = imagecreatefromjpeg($fileName);
			// Resize
			imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
			// Output
			imagejpeg($thumb,$fileNameOut);
		} elseif($fileType == 3) {
			// Load
			$thumb = imagecreatetruecolor($newWidth, $newHeight);
			$source = imagecreatefrompng ($fileName);
			// Resize
			imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
			// Output
			imagepng($thumb,$fileNameOut);
		}
	}
  return $fileNameOut;
}
*/

function img_setSize($fileName, $resFileName, $fileWidth, $fileHeight, $filePercent){
  //$newPath = $path . "/thumbs";
  //if($filePercent != ''){
  //  $fileNameOut = $newPath . '/' . $filePercent . 'pct_' . $fileName;
  //}else{
  //  $fileNameOut = $newPath . '/' . $fileWidth . 'x' . $fileHeight . '_' . $fileName;
  //}
  $fileName    = $fileName;
  $fileNameOut = $resFileName;
  $filePercent = $filePercent / 100;



  if(!file_exists($fileNameOut)){

		// Get Image size ant File Type
		list($width, $height, $fileType) = getimagesize($fileName);
		
		// Calcolo il ridimensionamento percentuale solo per l'altezza
		if ($fileHeight == '' || $fileHeight == 0)
			$fileHeight = (int)(($fileWidth * $height) / $width);


		// Create and Set new sizes
		if($filePercent != ''){
			$newWidth = $width * $filePercent;
			$newHeight = $height * $filePercent;
		}else{
			$newWidth = $fileWidth;
			$newHeight = $fileHeight;
		}

/*
	//Check if GIF or JPEG
		if($fileType == 1){
			// Load
			$thumb = imagecreatetruecolor($newWidth, $newHeight);
			$source = imagecreatefromgif($fileName);
			// Resize
			imagecopyresized($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
			// Output
			imagegif($thumb,$fileNameOut);
		}elseif($fileType == 2){
			// Load
			$thumb = imagecreatetruecolor($newWidth, $newHeight);
			$source = imagecreatefromjpeg($fileName);
			// Resize
			imagecopyresized($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
			// Output
			imagejpeg($thumb,$fileNameOut);
		}
*/
			// Load
			$thumb = imagecreatetruecolor($newWidth, $newHeight);

		if($fileType == 1){
			$source = imagecreatefromgif($fileName);
		}elseif($fileType == 2){
			$source = imagecreatefromjpeg($fileName);
		}elseif($fileType == 3){
			$source = imagecreatefrompng($fileName);
		}
		// Resize
		imagecopyresampled ($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
		// Output
		imagejpeg($thumb,$fileNameOut, 75);
  }
  return $fileNameOut;
}
?>