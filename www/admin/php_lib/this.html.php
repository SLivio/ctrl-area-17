<?php


//-------------------------------------> [HTML_LIBS]
// GHTML__Get_ModuleHeader
// GHTML__Get_StatusMessage


// LastUpdate 2014.03.12
function GHTML__Get_StatusMessage($type='error', $title='', $text='') {
	// type: error, warning, information, success
	if ($type=='error') $type='danger';
	$rand = rand();
	$OUT = '<div class="alert alert-'.$type.' alert-dismissable" id="'.$rand.'">
		<button class="close" aria-hidden="true" type="button"> x </button>
		<h4><i class="fa fa-check-circle"></i> '.$title.'</h4>
		'.$text.'
	</div>';
	return $OUT;
}


function GHTML__Get_Pagination($PAGE_TOT, $PAGE_CURRENT, $aP) {
	$OUT = '';
	if ($PAGE_TOT > 1) {
		$OUT = '<div class="row">
			<div class="col-sm-12">
				<div id="example-datatable_length" class="dataTables_length text-right">
				<form name="f_page"><label>
					<select name="page_number" class="form-control" onChange="location.href=this.value" size="1">';
		for($i=0; $i< $PAGE_TOT; $i++) {
			$class = ($i==$PAGE_CURRENT) ? 'selected="selected"' : '';
			$OUT .= NL.'<option '.$class.' value="?'.http_build_query($aP).'&p='.$i.'">'.(int)($i+1).'</option> ';
		}
		$OUT .= '</select></label>
				</form>

				</div>
			</div>
		</div>';
	}
	return $OUT;
}


// LastUpload v7 2014.11.21
function GHTML__Get_Ico($ico='', $tooltip='', $size='') {
	$tt = ($tooltip!='') ? 'data-toggle="tooltip" title="'.$tooltip.'"' : '';
	switch ($ico) {
		case 'lock'                : $i = '<i '.$tt.' class="fa fa-lock"></i>';            break;
		case 'unlock'              : $i = '<i '.$tt.' class="fa fa-unlock"></i>';          break;
		case 'refresh'             : $i = '<i '.$tt.' class="fa fa-refresh"></i>';         break;
		case 'download'            : $i = '<i '.$tt.' class="fa fa-download" ></i>';       break;
		case 'zoom+'               : $i = '<i '.$tt.' class="fa fa-search-plus"></i>';     break;
		case 'zoom-'               : $i = '<i '.$tt.' class="fa fa-search-minus"></i>';    break;
		case '+'                   : $i = '<i '.$tt.' class="fa fa-plus-square"></i>';     break;
		case '-'                   : $i = '<i '.$tt.' class="fa fa-minus-square"></i>';    break;
		case 'ok-v'                : $i = '<i '.$tt.' class="fa fa-check-square-o"></i>';  break;
		case 'del'                 : $i = '<i '.$tt.' class="fa fa-trash-o"></i>';         break;
		case 'delete'              : $i = '<i '.$tt.' class="fa fa-trash-o"></i>';         break;
		case 'cancel'              : $i = '<i '.$tt.' class="fa fa-times"></i>';           break;
		case 'accept'              : $i = '<i '.$tt.' class="fa fa-check"></i>';           break;
		case 'ok'                  : $i = '<i '.$tt.' class="fa fa-check"></i>';           break;
		case 'add'                 : $i = '<i '.$tt.' class="fa fa-plus-square"></i>';     break;
		case 'connect'             : $i = '<i '.$tt.' class="fa fa-eye"></i>';             break;
		case 'disconnect'          : $i = '<i '.$tt.' class="fa fa-eye-slash"></i>';       break;
		case 'engine'              : $i = '<i '.$tt.' class="fa fa-gears"></i>';           break;
		case 'list'                : $i = '<i '.$tt.' class="fa fa-list-alt"></i>';        break;
		case 'table'               : $i = '<i '.$tt.' class="fa fa-table"></i>';           break;
		case 'public'              : $i = '<i '.$tt.' class="gi gi-eye_open"></i>';        break;
		case 'private'             : $i = '<i '.$tt.' class="gi gi-eye_close"></i>';       break;
		case 'ban'                 : $i = '<i '.$tt.' class="fa fa-ban"></i>';             break;
		case 'disconnect-orange'   : $i = 'disconnect-orange.png';   break;
		case 'bullet-black'        : $i = 'bullet_black.png';        break;
		case 'bullet-green'        : $i = '<i '.$tt.' class="fa fa-check-circle"></i>';     break;
		case 'bullet-yellow'       : $i = 'bullet_yellow.png';       break;
		case 'bullet-orange'       : $i = 'bullet_orange.png';       break;
		case 'bullet-red'          : $i = 'bullet_red.png';          break;
		case 'bullet-err'          : $i = '<i '.$tt.' class="fa fa-times-circle"></i>';    break;
		case 'bullet-add'          : $i = '<i '.$tt.' class="fa fa-plus-circle"></i>';     break;
		case 'bullet-delete'       : $i = '<i '.$tt.' class="fa fa-minus-circle"></i>';    break;
		case 'info'                : $i = '<i '.$tt.' class="gi gi-circle_info"></i>';     break;
		case 'i'                   : $i = '<i '.$tt.' class="gi gi-circle_info"></i>';     break;
		case '!'                   : $i = '<i '.$tt.' class="gi gi-circle_exclamation_mark"></i>'; break;
		case 'ko'                  : $i = '<i '.$tt.' class="gi gi-circle_exclamation_mark"></i>'; break;
		case 'conf'                : $i = '<i '.$tt.' class="fa fa-cog"></i>';             break;
		case 'conf-edit'           : $i = '<i data-toggle="tooltip" title="Config edit" class="fa fa-cog"></i>';            break;
		case 'user'                : $i = '<i '.$tt.' class="fa fa-user"></i>';           break;
		case 'users'               : $i = '<i '.$tt.' class="fa fa-users"></i>';          break;
		case 'user-edit'           : $i = '<i data-toggle="tooltip" title="User edit" class="fa fa-user"></i>';           break;
		case 'user-group'          : $i = '<i '.$tt.' class="fa fa-users"></i>';          break;
		case 'page-go'             : $i = '<i '.$tt.' class="fa fa-external-link"></i>';  break;
		case 'page-edit'           : $i = '<i '.$tt.' class="fa fa-pencil-square"></i>';  break;
		case 'edit'                : $i = '<i '.$tt.' class="fa fa-pencil"></i>';         break;
		case 'arrow-left'          : $i = '<i '.$tt.' class="fa fa-angle-left"></i>';     break;
		case 'arrow-right'         : $i = '<i '.$tt.' class="fa fa-angle-right"></i>';    break;
		case 'arrow-up'            : $i = '<i '.$tt.' class="fa fa-angle-up"></i>';       break;
		case 'arrow-down'          : $i = '<i '.$tt.' class="fa fa-angle-down"></i>';     break;
		case 'tree'                : $i = '<i '.$tt.' class="gi gi-tree_conifer"></i>';   break;
		case 'zoom-in'             : $i = '<i '.$tt.' class="fa fa-search-plus"></i>';    break;
		case 'zoom-out'            : $i = '<i '.$tt.' class="fa fa-search-minus"></i>';   break;
		case 'reply'               : $i = '<i '.$tt.' class="fa fa-comments-o"></i>';     break;
		case 'like'                : $i = '<i '.$tt.' class="fa fa-thumbs-o-up"></i>';    break;
		case 'like-mini'           : $i = '<i '.$tt.' class="fa fa-thumbs-o-up"></i>';    break;
		case 'loading'             : $i = 'loading.gif';             break;
		case 'loading-mini'        : $i = 'loading-mini.gif';        break;
		case 'directory'           : $i = '<i '.$tt.' class="fa fa-folder-o"></i>';    break;
		case 'save'                : $i = '<i '.$tt.' class="fa fa-floppy-o"></i>';    break;
		case 'file-php'            : $i = '<i '.$tt.' class="fi fi-php"></i>';         break;
		case 'file-txt'            : $i = '<i '.$tt.' class="fi fi-txt"></i>';         break;
		case 'file-gif'            : $i = '<i '.$tt.' class="fi fi-gif"></i>';         break;
		case 'file-js'             : $i = '<i '.$tt.' class="fi fi-js"></i>';          break;
		case 'file-text'           : $i = 'files/txt.png';           break;
		case 'file-doc'            : $i = 'files/doc.png';           break;
		case 'file-docx'           : $i = 'files/doc.png';           break;
		case 'file-xls'            : $i = 'files/xls.png';           break;
		case 'file-xlsx'           : $i = 'files/xls.png';           break;
		case 'file-csv'            : $i = 'files/xls.png';           break;
		case 'file-sql'            : $i = 'files/sql.png';           break;
		case 'file-rar'            : $i = 'files/zip.png';           break;
		case 'file-zip'            : $i = 'files/zip.png';           break;
		case 'file-tgz'            : $i = 'files/zip.png';           break;
		case 'file-pdf'            : $i = 'files/pdf.png';           break;
		case 'file-bmp'            : $i = 'files/gif.png';           break;
		case 'file-jpeg'           : $i = 'files/gif.png';           break;
		case 'file-jpg'            : $i = 'files/gif.png';           break;
		case 'file-png'            : $i = 'files/gif.png';           break;
		case 'file-tiff'           : $i = 'files/other_image.png';   break;
		case 'file-psd'            : $i = 'files/other_image.png';   break;
		case 'file-mp3'            : $i = 'files/mp3.png';           break;
		case 'file-mp4'            : $i = 'files/other_movie.png';   break;
		case 'file-mp5'            : $i = 'files/other_movie.png';   break;
		case 'file-mov'            : $i = 'files/other_movie.png';   break;
		case 'file-wmv'            : $i = 'files/other_movie.png';   break;
		case 'file-mpg'            : $i = 'files/other_movie.png';   break;
		case 'file-ppt'            : $i = 'files/ppt.png';           break;
		case 'flag'                : $i = 'flag/';                   break;
		default                    : $i = 'wall-break.png';          break;
	}
	if($size!='') {
		$i = str_replace("fa ", "fa-".$size." fa ", $i);
		$i = str_replace("fi ", "fa-".$size." fi ", $i);
	}
	return $i;
}
//function FILE_Ico($ico='', $path='') { return GHTML__Get_Ico($ico, $path); }


// LastUpload 4.201201.26 [ok]
function FILE_Ico($ico='', $path='') {
	$p = ($path=='') ? './img/ico/' : $path;
	$i = '';
	switch ($ico) {
		case 'lock'                : $i = 'lock.png';                break;
		case 'unlock'              : $i = 'unlock.png';              break;
		case 'refresh'             : $i = 'arrow_refresh.png';       break;
		case 'download'            : $i = 'download.png';            break;
		case 'zoom+'               : $i = 'magnifier_zoom_in.png';   break;
		case 'zoom-'               : $i = 'magnifier_zoom_out.png';  break;
		case '+'                   : $i = 'bullet_toggle_plus.png';  break;
		case '-'                   : $i = 'bullet_toggle_minus.png'; break;
		case 'ok-v'                : $i = 'tick.png';                break;
		case 'del'                 : $i = 'delete.png';              break;
		case 'delete'              : $i = 'delete.png';              break;
		case 'cancel'              : $i = 'cancel.png';              break;
		case 'accept'              : $i = 'accept.png';              break;
		case 'ok'                  : $i = 'accept.png';              break;
		case 'add'                 : $i = 'add.png';                 break;
		case 'connect'             : $i = 'connect.png';             break;
		case 'disconnect'          : $i = 'disconnect.png';          break;
		case 'disconnect-orange'   : $i = 'disconnect-orange.png';   break;
		case 'bullet-black'        : $i = 'bullet_black.png';        break;
		case 'bullet-green'        : $i = 'bullet_green.png';        break;
		case 'bullet-yellow'       : $i = 'bullet_yellow.png';       break;
		case 'bullet-orange'       : $i = 'bullet_orange.png';       break;
		case 'bullet-red'          : $i = 'bullet_red.png';          break;
		case 'bullet-err'          : $i = 'bullet_error.png';        break;
		case 'bullet-add'          : $i = 'bullet_add.png';          break;
		case 'bullet-delete'       : $i = 'bullet_delete.png';       break;
		case 'info'                : $i = 'information.png';         break;
		case 'i'                   : $i = 'information.png';         break;
		case '!'                   : $i = 'exclamation.png';         break;
		case 'ko'                  : $i = 'exclamation.png';         break;
		case 'conf'                : $i = 'cog.png';                 break;
		case 'conf-edit'           : $i = 'cog_edit.png';            break;
		case 'user-edit'           : $i = 'user_edit.png';           break;
		case 'user-group'          : $i = 'user_group.png';          break;
		case 'page-go'             : $i = 'page_go.png';             break;
		case 'page-edit'           : $i = 'page_edit.png';           break;
		case 'edit'                : $i = 'page_edit.png';           break;
		case 'arrow-up'            : $i = 'bullet_arrow_up.png';     break;
		case 'arrow-down'          : $i = 'bullet_arrow_down.png';   break;
		case 'tree'                : $i = 'chart_organisation.png';  break;
		case 'zoom-in'             : $i = 'magnifier_zoom_in.png';   break;
		case 'zoom-out'            : $i = 'magnifier_zoom_out.png';  break;
		case 'reply'               : $i = 'reply.png';               break;
		case 'like'                : $i = 'like.png';                break;
		case 'like-mini'           : $i = 'like-mini.png';           break;
		case 'loading'             : $i = 'loading.gif';             break;
		case 'loading-mini'        : $i = 'loading-mini.gif';        break;
		case 'file-txt'            : $i = 'files/txt.png';           break;
		case 'file-text'           : $i = 'files/txt.png';           break;
		case 'file-doc'            : $i = 'files/doc.png';           break;
		case 'file-docx'           : $i = 'files/doc.png';           break;
		case 'file-xls'            : $i = 'files/xls.png';           break;
		case 'file-xlsx'           : $i = 'files/xls.png';           break;
		case 'file-csv'            : $i = 'files/xls.png';           break;
		case 'file-sql'            : $i = 'files/sql.png';           break;
		case 'file-rar'            : $i = 'files/zip.png';           break;
		case 'file-zip'            : $i = 'files/zip.png';           break;
		case 'file-tgz'            : $i = 'files/zip.png';           break;
		case 'file-pdf'            : $i = 'files/pdf.png';           break;
		case 'file-bmp'            : $i = 'files/gif.png';           break;
		case 'file-gif'            : $i = 'files/gif.png';           break;
		case 'file-jpeg'           : $i = 'files/gif.png';           break;
		case 'file-jpg'            : $i = 'files/gif.png';           break;
		case 'file-png'            : $i = 'files/gif.png';           break;
		case 'file-tiff'           : $i = 'files/other_image.png';   break;
		case 'file-psd'            : $i = 'files/other_image.png';   break;
		case 'file-mp3'            : $i = 'files/mp3.png';           break;
		case 'file-mp4'            : $i = 'files/other_movie.png';   break;
		case 'file-mp5'            : $i = 'files/other_movie.png';   break;
		case 'file-mov'            : $i = 'files/other_movie.png';   break;
		case 'file-wmv'            : $i = 'files/other_movie.png';   break;
		case 'file-mpg'            : $i = 'files/other_movie.png';   break;
		case 'file-ppt'            : $i = 'files/ppt.png';           break;
		case 'flag'                : $i = 'flag/';                   break;
		default                    : $i = 'wall-break.png';          break;
	}
	return $p.$i;
}



//-------------------------------------> [CORE_LIBS]

// LastUpdate 2017.04.02 [test.17]
function GHTML__Get_Header($html1='', $html2='', $html3='', $html4='') {
	$TEMPLATE_START = '<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta name="author" content="SLivio">
	<meta name="generator" content="'.((defined('APP_NAME')) ? APP_NAME : 'Ctrl-Area').' '.((defined('APP_VERSION')) ? APP_VERSION : '').'">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
	<title>'.((defined('APP_NAME')) ? APP_NAME : 'Ctrl-Area').'</title>
	'.$html1.'
	<link rel="shortcut icon" href="img/favicon.ico">
	<link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
	<link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
	<link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
	<link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
	<link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
	<link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
	<link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/plugins.css">
	'.((defined('INTRANET') && (INTRANET==true)) ? '' : '<link rel="stylesheet" href="css/font.css">').'
	<link rel="stylesheet" href="css/main.css">
	'.$html2.'
	'.((defined('APP_THEME')) ? '<link id="theme-link" rel="stylesheet" href="css/themes/'.APP_THEME.'.css">' : '').'
	'.$html3.'
	<link rel="stylesheet" href="css/themes.css">
	<script src="js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
	<script src="'.((defined('INTRANET') && (INTRANET==true)) ? 'js/vendor/jquery-1.11.0.min.js' : 'http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js').'"></script>
	<script>!window.jQuery && document.write(unescape(\'%3Cscript src="js/vendor/jquery-1.11.0.min.js"%3E%3C/script%3E\'));</script>

	<link href="css/selectize.css" rel="stylesheet">
	<link href="css/selectize.bootstrap3.css" rel="stylesheet">
	
	<link href="css/validform.css" rel="stylesheet">
	<script src="js/validform.js"></script>
	<script src="js/formBuilder.js"></script>
	<script src="js/vendor/selectize.js"></script>
	'.$html4.'
</head>
<body id="homepage">
	<div id="page-container" class="header-fixed-top sidebar-visible-lg">';
	echo $TEMPLATE_START;
}


// NeedUpdate
function GHTML__Get_Header_Pop($html1='', $html2='', $html3='', $html4='') {
	echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="generator" content="'.((defined('APP_NAME')) ? APP_NAME : 'Ctrl-Area').' '.((defined('APP_VERSION')) ? APP_VERSION : '').'" />
		<title>'.((defined('APP_NAME')) ? APP_NAME : 'Ctrl-Area').'</title>
		'.$html1.'
		<link href="media/css/layout_popup.css" rel="stylesheet" type="text/css" />
		<link href="media/css/common.css" rel="stylesheet" type="text/css" />
		<link type="text/css" href="media/js/jquery/css/ui-lightness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
		'.$html2.'
		<script type="text/javascript" src="media/js/enhance.js"></script>
		<script type="text/javascript" src="media/js/excanvas.js"></script>
		<script type="text/javascript" src="media/js/jquery/js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="media/js/jquery/js/jquery-ui-1.8.18.custom.min.js"></script>
		<!--[if IE 6]>
		<script type="text/javascript" src="media/js/png_fix.js"></script>
		<script type="text/javascript">
		  DD_belatedPNG.fix("img, .notifycount, .selected");
		</script>
		<![endif]-->
		<script src="js/formBuilder.js"></script>
		'.$html3.'
	</head>
	<body  id="homepage">'.$html4;
}

function GHTML__Get_Header_Mod($html1 = '', $html2 = '', $html3 = '', $html4 = '') {
	$TEMPLATE_START = '<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta name="author" content="SLivio">
	<meta name="generator" content="'.((defined('APP_NAME')) ? APP_NAME : 'Ctrl-Area').' '.((defined('APP_VERSION')) ? APP_VERSION : '').'">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
	<title>'.((defined('APP_NAME')) ? APP_NAME : 'Ctrl-Area').'</title>
	'.$html1.'
	<link rel="shortcut icon" href="img/favicon.ico">
	<link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
	<link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
	<link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
	<link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
	<link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
	<link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
	<link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/plugins.css">
	'.((defined('INTRANET') && (INTRANET==true)) ? '' : '<link rel="stylesheet" href="css/font.css">').'
	<link rel="stylesheet" href="css/main.css">
	'.$html2.'
	'.((defined('APP_THEME')) ? '<link id="theme-link" rel="stylesheet" href="css/themes/'.APP_THEME.'.css">' : '').'
	'.$html3.'
	<link rel="stylesheet" href="css/themes.css">
	<script src="js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
	<script src="'.((defined('INTRANET') && (INTRANET==true)) ? 'js/vendor/jquery-1.11.0.min.js' : 'http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js').'"></script>
	<script>!window.jQuery && document.write(unescape(\'%3Cscript src="js/vendor/jquery-1.11.0.min.js"%3E%3C/script%3E\'));</script>

	<link href="css/validform.css" rel="stylesheet">
	<script src="js/validform.js"></script>
	<script src="js/formBuilder.js"></script>
	'.$html4.'
</head>
<body id="homepage">
	<div id="page-container" style="background-color:#ebf2f2;">';
	echo $TEMPLATE_START;
}

// LastUpdate 2014.02.28 [test.6]
function GHTML__Get_Footer($html1='') {
	GLOBAL $CONF, $aModule, $aModules, $op, $m;
	//
	
	$js = '';
	if(count($aModule)>=1) {
		$tmp_conf  = $CONF['path_modules'].'/'.$aModule['module_dir'].'/conf.php';
		include($tmp_conf);
		if(isset($LOCAL_CONF['local_js'])) {
            $js .= '
            <script src="'.$CONF['path_modules'].'/'.$aModule['module_dir'].'/'.$LOCAL_CONF['local_js'].'"></script>';
        }
	}
	echo $html1.'</div>
	'.$js.'
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/app.js"></script>
</body>
</html>';
}


// LastUpdate 4.201112.16 [ok]
function GHTML__Get_Footer_Pop($html1='') {
	GLOBAL $CONF, $aModules, $op, $m;
	//
	echo $html1.'
		</div>
	</body>
	</html>';
}


// LastUpdate 2014.03.02 [test.6]
function GHTML__Get_Left() {
	GLOBAL $aModules, $CONF, $m, $op;
	$HTML_ERR = '';
	echo '<div id="sidebar">
	<div class="sidebar-scroll">
		<div class="sidebar-content">
		'.GHTML__Get_Profile().'
			<ul class="sidebar-nav" id="nav">
				<li class="sidebar-header"><span class="sidebar-header-title">MODULI</span></li>';


	$LOCAL_CONF = Array();
	foreach($aModules as $mod) {
		$tmpCheck = false; // CHECK_PERMESSI [1/3]
		$tmpCheck = GDB__Check_AdminModules('', $mod['module_id'], ''); // CHECK_PERMESSI [2/3]
		if ($tmpCheck == true) { // CHECK_PERMESSI [3/3]
			$tmp_conf = $CONF['path_modules'].'/'.$mod['module_dir'].'/conf.php';
			if(file_exists($tmp_conf)) {

				require($tmp_conf);
				//$aTmpConf = $LOCAL_CONF['module_op'];
				$aTmpConf = (isset($LOCAL_CONF['module_op'])) ? $LOCAL_CONF['module_op'] : '';
				$aTmpMenu = (isset($LOCAL_CONF['menu']))      ? $LOCAL_CONF['menu']      : '';
				if(is_array($aTmpConf) && ((is_array($aTmpMenu) && isset($aTmpMenu['visible']) && $aTmpMenu['visible']==true) || $aTmpMenu=='')) {
						
						
					if ($mod['module_open']==1) {

						echo '<li>
								<a class="sidebar-nav-menu open"><i class="gi gi-show_big_thumbnails sidebar-nav-icon"></i><i class="fa fa-angle-left sidebar-nav-indicator"></i>'.$mod['module_label'].'</a>';

					} else if ($m != $mod['module_id']) {

						echo '<li>
								<a class="sidebar-nav-menu"><i class="gi gi-show_big_thumbnails sidebar-nav-icon"></i><i class="fa fa-angle-left sidebar-nav-indicator"></i>'.$mod['module_label'].'</a>';
							
					} else if ($m == $mod['module_id']) {

						echo '<li class="active">
								<a class="sidebar-nav-menu"><i class="gi gi-show_big_thumbnails sidebar-nav-icon"></i><i class="fa fa-angle-left sidebar-nav-indicator"></i>'.$mod['module_label'].'</a>';

					}
					if ($mod['module_open']==1) {
						echo '<ul class="navigation" style="display:block">';
					} else {
						echo '<ul class="navigation">';
					}
						
					foreach($aTmpConf as $conf) {
						$tmpCheck = false; // CHECK_PERMESSI [1/3]
						$tmpCheck = GDB__Check_AdminModules('', $mod['module_id'], $conf['op']); // CHECK_PERMESSI [2/3]
						if ($tmpCheck == true) { // CHECK_PERMESSI [3/3]
							$a_class = '';
							if(isset($_GET['op']) && !isset($_GET['id'])){
								$_SESSION['sess_op'] = $_GET['op'];
							}
							if($conf['op'] == $_SESSION['sess_op'] && $m == $mod['module_id']) $a_class = 'class="active"';
							echo '<li id="li-'.$mod['module_id'].'-'.$conf['op'].'"><a href="?m='.$mod['module_id'].'&op='.$conf['op'].'" '.$a_class.'>'.$conf['label'].'</a></li>';
						}
					}
					echo '</ul>';
						
						
				}
			} else {
				$HTML_ERR .= '<div class="alert alert-danger alert-alt"><i class="fa fa-bug fa-fw"></i><small>Errore nel file '.$tmp_conf.'.</small></div>';
			}
		}
		$LOCAL_CONF = Array();
	}
	echo '</ul>
			<div class="sidebar-section">
				'.$HTML_ERR.'
			</div>
			</div>
		</div>
	</div>';
}


// NeedUpdate
function GHTML__Get_Profile() {
	GLOBAL $SESSION_ID;
	$OUT = '<!-- User Info -->
	<div style="text-align:center;background:none repeat scroll 0 0 rgba(255, 255, 255, 0.1);">
		<img src="css/themes/'.APP_THEME.'/logo.png" alt="logo">
	</div>
	<div class="sidebar-section sidebar-user clearfix text-center" style="padding-left:0px; overflow:hidden;">
		<!--<div class="sidebar-user-avatar">
			<a href="page_ready_user_profile.html"><img src="img/placeholders/avatars/avatar2.jpg" alt="avatar"></a>
		</div>-->
		<div class="sidebar-user-name">'.GLOBAL_GetUser('user_half_email').'</div>
		<div class="sidebar-user-links">
			<a href="?m=3&op=u_form" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>
			<!--<a href="#modal-user-settings" data-toggle="modal" class="enable-tooltip" data-placement="bottom" title="Settings"><i class="gi gi-cogwheel"></i></a>-->
			<a href="javascript:;" class="enable-tooltip" title="'.$_SESSION[$SESSION_ID]['info']['country_name'].'"><img alt="'.$_SESSION[$SESSION_ID]['info']['country_name'].'" src="img/flag/'.$_SESSION[$SESSION_ID]['info']['country_flag'].'" /></a>
			<a href="?op=LOGOUT" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
		</div>
	</div>

	<!-- END User Info -->';
	return $OUT;
}


// LastUpdate v7 2014.11.21
function GHTML__Get_Right() {
	GLOBAL $CONF, $aModules, $aModule, $SESSION_ID, $m, $op;

	echo '
	<div id="main-container">
		<header class="navbar navbar-default navbar-fixed-top">
			<ul class="nav navbar-nav-custom">
            	<!-- Main Sidebar Toggle Button -->
                <li>
                	<a href="javascript:void(0)" onclick="App.sidebar(\'toggle-sidebar\');">
                    	<i class="fa fa-bars fa-fw"></i>
                    </a>
				</li>
				<li>
                    <a href="?ref=top"><i class="fa fa-home"></i></a>
				</li>
			</ul>
			<ul class="nav navbar-nav-custom pull-right"><li></li></ul>
			<!-- END Main Sidebar Toggle Button -->
		</header>
		<div id="page-content">';
	if ($m != '') {

		$aModule   = GDB__Get_InfoModule($m);
		$tmp_conf  = $CONF['path_modules'].'/'.$aModule['module_dir'].'/conf.php';
		$tmp_index = $CONF['path_modules'].'/'.$aModule['module_dir'].'/index.php';
		include($tmp_conf);
		include($tmp_index);
	} else {
		echo GHTML__Get_DashboardHeader();
		echo GHTML__Get_Dashboard();
	}
	echo '</div>
	</div>';
}

function GHTML__Get_Right_Mod() {
	GLOBAL $CONF, $aModules, $aModule, $SESSION_ID, $m, $op;

	echo '
	<div id="main-container">
		<div id="page-content">';
	if ($m != '') {

		$aModule   = GDB__Get_InfoModule($m);
		$tmp_conf  = $CONF['path_modules'].'/'.$aModule['module_dir'].'/conf.php';
		$tmp_index = $CONF['path_modules'].'/'.$aModule['module_dir'].'/index.php';
		include($tmp_conf);
		include($tmp_index);
	} else {
		echo GHTML__Get_DashboardHeader();
		echo GHTML__Get_Dashboard();
	}
	echo '</div>
	</div>';
}


// LastUpdate 5.201304.14 [OK]
function GHTML__Get_Debug() {
	GLOBAL $DEBUG, $HOST, $DB, $SESSION_ID;
	$DEBUG = (!isset($DEBUG)) ? '' : "\n[".$DEBUG."]\n";
	
	if (DEBUG==1) {
		$_SESSION[$SESSION_ID]['error'] = isset($_SESSION[$SESSION_ID]['error']) ? $_SESSION[$SESSION_ID]['error'] : Array();
		echo '<style>
		.debugDiv  { position:absolute; top:50px; left:0px; width:100%; height:600px; z-index:1000; overflow:scroll; display:none; background-color:#000; color:#3f0; font-size:11px; }
		.debugDiv pre { overflow:auto; width:98%; margin:0 auto; color:#3f0; font-size:11px; }
		.debugToggle { font-size:11px; color:#000; }
		.debugToggle:hover { font-size:11px; color:#3f0; }
		</style>
		<a href="javascript:;" class="debugToggle">
			<i class="gi gi-bug"></i>
			<span class="label label-primary label-indicator animation-floating">'.count($_SESSION[$SESSION_ID]['error']).'</span>
		</a>
		
		<div class="debugDiv">
			<pre>Database: '.$HOST.' / '.$DB.' :: Debug: '.DEBUG.'</pre><br>
			<pre>SESSION: '.print_r($_SESSION[$SESSION_ID], 1).'</pre><br>
			<pre>REQUEST: '.print_r($_REQUEST, 1).'</pre>
			<pre>FILES: '.print_r($_FILES, 1).'</pre>
		</div>
		<script>
		
		$(document).ready(function(){
			$(".debugToggle").click(function() {
				$(".debugDiv").toggle();
			});
			$(".debugToggle").appendTo(".navbar .nav.pull-right li")
		});
		</script>';
	}
}

function GHTML__Get_DashboardHeader() {
	return '<div class="content-header content-header-media">
		<div class="header-section">
			<div class="row">
				<!-- Main Title (hidden on small devices for the statistics to fit) -->
				<div class="col-md-4 col-lg-6 hidden-xs hidden-sm">
					<h1>Welcome<br><small>You Look Awesome!</small></h1>
				</div>
				<!-- END Main Title -->
			</div>
		</div>
		<!-- For best results use an image with a resolution of 2560x248 pixels (You can also use a blurred image with ratio 10:1 - eg: 1000x100 pixels - it will adjust and look great!) -->
		<img src="css/themes/'.APP_THEME.'/dashboard_header.jpg" alt="header image" class="animation-pulseSlow">
	</div>';
}


// LastUpdate 2016.12.11
function GHTML__Get_ModuleHeader($title='', $description='', $pagetype='FORM') {
	switch($pagetype) {
		default :
		case 'FORM'   : $ico = GHTML__Get_Ico('table');   break;
		case 'LIST'   : $ico = GHTML__Get_Ico('list');    break;
		case 'USER'   : $ico = GHTML__Get_Ico('user');    break;
		case 'USERS'  : $ico = GHTML__Get_Ico('users');   break;
		case 'ENGINE' : $ico = GHTML__Get_Ico('engine');  break;
	}
	return '<div class="content-header">
		<div class="header-section">
	    	<h1>'.$ico.' '.$title.'<br><small>'.$description.'</small></h1>
		</div>
	</div>';
}


// LastUpdate 3.201110.21 [OK]
function GHTML__Get_Dashboard() {
	GLOBAL $aModules, $CONF, $m, $op;
	$HTML = '';
	$JS   = '
	function JS_Widget__(wmodule, wop, where) {
		var idwhere = "#"+where;
		var wdata   = "m="+wmodule+"&op="+wop+"&app=true";
		$.get("index.php?"+wdata, function(result){
	    	$result = $(result);
			$resultHTML = $result.find(".block");
			if ($resultHTML) {
				$(idwhere).html("");
			    $result.find(".block").appendTo(idwhere);
			    $result.find("script").appendTo(idwhere);
			}
		}, "html");
	}
	';

	// Get Widget in conf.php
	$aW_fs = Array();
	foreach ($aModules as $module) {
		// CONF
		$LOCAL_CONF = '';
		$tmp_conf   = $CONF['path_modules'].'/'.$module['module_dir'].'/conf.php';
		$tmp_main   = $CONF['path_modules'].'/'.$module['module_dir'].'/index.php';
		if (file_exists($tmp_conf)) {
			include($tmp_conf);
			// LIB
			if (isset($LOCAL_CONF['local_widget']) && !empty($LOCAL_CONF['local_widget'])) {
				$c = 0;
				foreach($LOCAL_CONF['local_widget'] as $widget) {
					$rand         = get_RandomString();
					$tmpK         = $module['module_id'].'.'.$c;
					$class        = (isset($widget['class']) && $widget['class']!='') ? $widget['class'] : 'col-sm-6 col-lg-3';
					$aW_fs[$tmpK] = '
						<div id="'.$rand.'" class="'.$class.' m-'.$module['module_id'].'"><p class="ac"><i class="fa fa-spinner fa-2x fa-spin"></i></p></div>';
					$JS          .= '
						JS_Widget__("'.$module['module_id'].'", "'.$widget['op'].'", "'.$rand.'"); ';
					$c++;
				}
			}
		}
	}

	// Get Widget in DB... and print
	$aW_db  = GDB__Get_AllWidgets();
	$tmpRow = 0;
	$c      = 0;
	foreach($aW_db as $w) {
		if ($w['widget_row']!=$tmpRow) {
			if ($c>=1) $HTML .= '</div>';
			$HTML .= '<div class="row">';
		}

		$tmpK = $w['widget_key'];
		if (isset($aW_fs[$tmpK])) {
			$HTML .= $aW_fs[$tmpK];
			unset($aW_fs[$tmpK]);
		}
			
		$tmpRow = $w['widget_row'];
		$c++;
	}

	// print the others...
	foreach($aW_fs as $wf) {
		$HTML .= $wf;
	}

	$out = $HTML.'
		<script>
			'.$JS.'
			
			var t = setTimeout(function() {
				$(\'[data-toggle="lightbox-image"]\').magnificPopup({type: \'image\', image: {titleSrc: \'title\'}});
				$(\'[data-toggle="tooltip"]\').tooltip(); 
			}, 1000);
		</script>';

	return $out;
}
