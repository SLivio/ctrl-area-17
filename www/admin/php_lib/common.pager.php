<?php
/*
  Lib:         ctrl_Pager Function
  Version:     0.1.20090821
  Author:      SLivio & MiniOwl
  Contact:     silvio.coco@gmail.com
  Copiryght:   http://www.ctrl-area.com/copyright
*/

function pager_getInputHidden($name='text', $value='') {
	return "<input type='hidden' name='".$name."' value='".$value."' class='txt'>";
}

function pager_getInputText($name='text', $value='') {
	$value = str_replace('\'', '&rsquo;', $value);
	return "<input type='text' name='".$name."' value='".$value."' class='txt'>";
}

function pager_getInputPassword($name='text', $value='') {
	$value = str_replace('\'', '&rsquo;', $value);
	return "<input type='password' name='".$name."' value='".$value."' class='txt'>";
}

function pager_getReadonlyText($name='text', $value='') {
	return "<input type='text' name='".$name."' value='".$value."' class='txt' readonly >";
}

function pager_getInputLink($name='text', $value='') {
	return "<input type='text' name='".$name."' value='".$value."' class='txt'>";
}

function pager_getInputData($name='text', $value='', $format='yy-mm-dd') {
	$tmp_id = mt_rand();
	if ($value != '') {
		$js_value = ",
			setDate:\"".$value."\"";
	}
	$return = "<script type=\"text/javascript\"> 
	$(function() {
		$('#datepicker_".$tmp_id."').datepicker({
			//changeMonth: true,
			//changeYear: true,
			dateFormat: '".$format."'
		});
	});
	</script>".'<input type="text" name="'.$name.'" value="'.$value.'" id="datepicker_'.$tmp_id.'" class="txt mediumbox">';	
	return $return;
}

function pager_getTextarea($name='textarea', $value='', $type='Basic', $w='', $h='') {
	switch($type) {
		case 'Mini' :
			$h = ($h=='') ? '100px' : $h;
			$w = ($w=='') ? '300px' : $w;
			$HTML = "
			<script type='text/javascript'>
			<!--
			var oFCKeditor = new FCKeditor( '".$name."' ) ;
			oFCKeditor.Config['ToolbarStartExpanded'] = false ;
			oFCKeditor.BasePath = './lib_js/fckeditor/' ;
			oFCKeditor.ToolbarSet	= 'Basic' ;
			oFCKeditor.Height	= '".$h."' ;
			oFCKeditor.Width	= '".$w."' ;
			oFCKeditor.Value		= \"".txt2html($value)."\" ;
			oFCKeditor.Create() ;
			//-->
			</script>	";
		break;
		case 'Basic' :
			$h = ($h=='') ? '100px' : $h;
			$w = ($w=='') ? '600px' : $w;
			$HTML = "
			<script type='text/javascript'>
			<!--
			var oFCKeditor = new FCKeditor( '".$name."' ) ;
			oFCKeditor.Config['ToolbarStartExpanded'] = false ;
			oFCKeditor.BasePath = './lib_js/fckeditor/' ;
			oFCKeditor.ToolbarSet	= 'Basic' ;
			oFCKeditor.Height	= '".$h."' ;
			oFCKeditor.Width	= '".$w."' ;
			oFCKeditor.Value		= \"".txt2html($value)."\" ;
			oFCKeditor.Create() ;
			//-->
			</script>	";
		break;
		case 'Medium' :
			$h = ($h=='') ? '200px' : $h;
			$w = ($w=='') ? '600px' : $w;
			$HTML = "
			<script type='text/javascript'>
			<!--
			var oFCKeditor = new FCKeditor( '".$name."' ) ;
			oFCKeditor.Config['ToolbarStartExpanded'] = false ;
			oFCKeditor.BasePath = './lib_js/fckeditor/' ;
			oFCKeditor.ToolbarSet	= 'Default' ;
			oFCKeditor.Height	= '".$h."' ;
			oFCKeditor.Width	= '".$w."' ;
			oFCKeditor.Value		= \"".txt2html($value)."\" ;
			oFCKeditor.Create() ;
			//-->
			</script>	";
		break;
		case 'Default' :
			$h = ($h=='') ? 'height:200px' : 'height:'.$h.'px';
			$w = ($w=='') ? 'width:600px'  : 'width:'.$w.'px';
			$HTML = "<style type='text/css'> textarea.texta { ".$w."; ".$h."; }</style>";
			$HTML .= "<textarea name='".$name."' class='texta'>".$value."</textarea>";
		break;
	}
	return $HTML;
}

function pager_getTextarea_Tinymce($name='textarea', $value='', $type='Basic', $w='', $h='') {
	$value = paget_TinymceTextTransform($value);
	$h = ($h=='') ? '100px' : $h;
	$w = ($w=='') ? '300px' : $w;
	$HTML = '';
	switch($type) {
		case 'Mini' :
			$h = ($h=='') ? '100px' : $h;
			$w = ($w=='') ? '300px' : $w;
			$HTML = "
			<textarea id='".$name."' name='".$name."' style='width:".$w.";height:".$h.";'>".$value."</textarea>
			<script type='text/javascript'>
			<!--
			tinyMCE.init({
				mode : 'textareas',
				theme : 'simple'
			});
			//-->
			</script>	";
		break;
		case 'Basic' :
			$h = ($h=='') ? '100px' : $h;
			$w = ($w=='') ? '600px' : $w;
			$HTML = "
			<textarea id='".$name."' name='".$name."' style='width:".$w.";height:".$h.";'>".$value."</textarea>
			<script type='text/javascript'>
			<!--
			tinyMCE.init({
				// General options
				mode : 'textareas',
				theme : 'advanced',
				plugins : 'pagebreak,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,wordcount,advlist',
		
				// Theme options
				theme_advanced_buttons1 : 'newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull',
				theme_advanced_buttons2 : '',
				theme_advanced_buttons3 : '',
				theme_advanced_toolbar_location : 'top',
				theme_advanced_toolbar_align : 'left',
				theme_advanced_resizing : true,
		
			});
			//-->
			</script>	";
		break;
		case 'Medium' :
			$h = ($h=='') ? '200px' : $h;
			$w = ($w=='') ? '600px' : $w;
			$HTML = "
			<textarea id='".$name."' name='".$name."' style='width:".$w.";height:".$h.";'>".$value."</textarea>
			<script type='text/javascript'>
			<!--
			tinyMCE.init({
				// General options
				mode : 'textareas',
				theme : 'advanced',
				plugins : 'pagebreak,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,wordcount,advlist',
		
				// Theme options
				theme_advanced_buttons1 : 'help,code,|,save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull',
				theme_advanced_buttons2 : 'cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup',
				theme_advanced_buttons3 : 'tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,iespell,media,advhr',
				theme_advanced_toolbar_location : 'top',
				theme_advanced_toolbar_align : 'left',
				theme_advanced_resizing : true,
		
			});
			//-->
			</script>	";
		break;
		case 'Default' :
			$h = ($h=='') ? 'height:200px' : 'height:'.$h.'px';
			$w = ($w=='') ? 'width:600px'  : 'width:'.$w.'px';
			$HTML = "<style type='text/css'> textarea.texta { ".$w."; ".$h."; }</style>";
			$HTML .= "<textarea name='".$name."' class='texta'>".$value."</textarea>";
		break;
	}
	return $HTML;
}

function paget_TinymceTextTransform($text='') {
	$text = str_replace('<', '&lt;', $text);
	$text = str_replace('>', '&gt;', $text);
	return $text;
}

function pager_getInputFile($name='text', $value='') {
	return "<input type='file' name='".$name."' class='file' value='".$value."'>";
}

function pager_getSubmit($name='button', $value=''){
	return "<input type='submit' name='".$name."' value='".$value."' class='btn'>";
}

function pager_getButton($name='button', $value='', $action){
	return "<input type='button' name='".$name."' value='".$value."' class='btn' ".$action.">";
}

function pager_getCheckbox($name='check', $value='0') {
	$checked = ($value=='1') ? 'checked' : '';
	return "<input type='checkbox' name='".$name."' class='cbox' ".$checked." value='1'>";
}

function pager_getSelect($array, $selectName, $recValue, $recLabel, $value) {
	$r  = '<select name="'.$selectName.'" class="vf__one">';
	foreach($array as $el) {
		//$s  = ($value == $el[$selectName]) ? 'selected' : ''; // > Modificato in ValSinistri
		$s  = ($value == $el[$recValue]) ? 'selected' : '';
		$r .= '<option value="'.$el[$recValue].'" '.$s.'>'.$el[$recLabel].'</option>'."\n";
	}
	$r .= '</select>';
	return $r;
}

// select con il primo elemento vuoto
function pager_getSelect_Da($array, $selectName, $recValue, $recLabel, $value) {
	$r  = '<select name="'.$selectName.'">';
	$r .= '<option value="0">---</option>'."\n";
	foreach($array as $el) {
		$s  = ($value == $el[$selectName]) ? 'selected' : '';
		$r .= '<option value="'.$el[$recValue].'" '.$s.'>'.$el[$recLabel].'</option>'."\n";
	}
	$r .= '</select>';
	return $r;
}


?>