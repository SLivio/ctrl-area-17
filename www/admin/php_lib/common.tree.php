<?php
/*
  Lib:         ctrl_TreeContent Function
  Version:     0.2.20100622
  Author:      SLivio & MiniOwl
  Contact:     silvio.coco@gmail.com
  Copiryght:   http://www.ctrl-area.com/copyright
*/



// [Framework - Content]

function TREE_getPath_Content($node_id) { // (?)
	// look up the parent of this node
	$result = mysql_query('SELECT node_parent FROM core_tree WHERE node_id="'.$node_id.'";');
	$row = mysql_fetch_array($result);
	// save the path in this array
	$path = array();
	// only continue if this $node isn't the root node
	// (that's the node with no parent)
	if ($row['node_parent']!='') {
		// the last part of the path to $node, is the name
		// of the parent of $node
		$path[] = $row['node_parent'];
		// we should add the path to the parent of this node
		// to the path
		$path = array_merge(TREE_getPath_Content($row['node_parent']), $path);
	}
	// return the path
	return $path;
}



// [CMS]

// MODULE FUNCTION >>>> DB_Set_TreeNode
function setTree_Node($node_id=0, $node_label='', $node_status='', $node_type='', $node_info='', $node_template='') {
	$OUT = false;
	$q = "UPDATE `".DB_PREFIX.'site_tree'."` SET ".
	     "`node_label`='".$node_label."',".
	     "`node_status`=".$node_status.",".
	     "`node_type`='".$node_type."',".
	     "`node_info`='".$node_info."',".
	     "`node_template`='".$node_template."' WHERE `node_id`=".$node_id;
	$r = mysql_query($q) or LOG__Error("setTree_Node[]", mysql_error()."\n".$q);
	if(mysql_affected_rows() == 1) {
		$OUT = true;
	}
	return $OUT;
}

// $id_parent is the id_parent of the children we want to see
// $level is increased when we go deeper into the tree,
//        used to display a nice indented tree
$TREE = '';
function display_children($node_parent, $level, $node_status='') {
	GLOBAL $TREE;
	// retrieve all children of $parent
	$add_q  = ($node_status != '') ? "AND node_status = ".$node_status : '';
	$q      = 'SELECT node_id,node_label FROM '.DB_PREFIX.'site_tree'.' WHERE node_parent="'.$node_parent.'" '.$add_q.' ORDER BY node_id ASC';
	$result = mysql_query($q);// or LOG__Error("setTree_Node[]", mysql_error()."\n".$q);
	// display each child
	while ($row = mysql_fetch_array($result)) {
		// indent and display the title of this child
		$TREE .= str_repeat('#',$level).' '.$row['node_label'].','.$row['node_id']."\n";
		// call this function again to display this
		// child's children
		display_children($row['node_id'], $level+1);
	}
	return $TREE;
}

function getHTML_Children($node_parent, $level) {
	GLOBAL $TREE,$CLOSE;
	// retrieve all children of $parent
	$result = mysql_query('SELECT node_id,node_label FROM '.DB_PREFIX.'site_tree'.' WHERE node_parent="'.$node_parent.'" ORDER BY node_id ASC;');
	// display each child
	
	while ($row = mysql_fetch_array($result)) {
		// indent and display the title of this child
		$TREE .= "<a href='?id=".$row['node_id']."'>".$row['node_label'].','.$row['node_id']."</a> ";
		// call this function again to display this
		// child's children
		getHTML_Children($row['node_id'], $level+1);
	}
	//$TREE .= "</ul>";
	$CLOSE = true;
	return $TREE;
}

function get_NodeConfig($node_id=0) {
	$aInfo = get_InfoRecord(DB_PREFIX.'site_tree', 'WHERE node_id='.$node_id);
	parse_str($aInfo['node_info'], $aConf);
	return $aConf;
}

//$CLOSE = false;
//function getHTML_Children($node_parent, $level) {
//	GLOBAL $TREE,$CLOSE;
//	// retrieve all children of $parent
//	$result = mysql_query('SELECT node_id,node_label FROM tree WHERE node_parent="'.$node_parent.'" ORDER BY node_id ASC;');
//	// display each child
//	
//	if ($CLOSE==false) { 
//		$TREE .= "\n\t</ul>";
//	} else {
//		$TREE .= "\t<ul>\n";
//		$CLOSE = true;
//	}
//	while ($row = mysql_fetch_array($result)) {
//		// indent and display the title of this child
//		$TREE .= "\t\t<li>".$row['node_label'].','.$row['node_id']."</li>";
//		// call this function again to display this
//		// child's children
//		getHTML_Children($row['node_id'], $level+1);
//	}
//	//$TREE .= "</ul>";
//	$CLOSE = true;
//	return $TREE;
//}


// $node is the name of the node we want the path of
function get_Path($node_id) {
	// look up the parent of this node
	$result = mysql_query('SELECT node_parent,node_label FROM '.DB_PREFIX.'site_tree'.' WHERE node_id="'.$node_id.'";');
	$row = mysql_fetch_array($result);
	// save the path in this array
	$path = array();
	// only continue if this $node isn't the root node
	// (that's the node with no parent)
	if ($row['node_parent']!='') {
		// the last part of the path to $node, is the name
		// of the parent of $node
		$path[] = $row['node_label'];
		// we should add the path to the parent of this node
		// to the path
		$path = array_merge(get_Path($row['node_parent']), $path);
	}
	// return the path
	return $path;
}

// $node is the name of the node we want the path of
function get_Path_id($node_id) {
	// look up the parent of this node
	$result = mysql_query('SELECT node_parent FROM '.DB_PREFIX.'site_tree'.' WHERE node_id="'.$node_id.'";');
	$row = mysql_fetch_array($result);
	// save the path in this array
	$path = array();
	// only continue if this $node isn't the root node
	// (that's the node with no parent)
	if ($row['node_parent']!='') {
		// the last part of the path to $node, is the name
		// of the parent of $node
		$path[] = trim($row['node_parent']);
		// we should add the path to the parent of this node
		// to the path
		$path = array_merge(get_Path_id($row['node_parent']), $path);
	}
	// return the path
	return $path;
}

// $node is the name of the node we want the path of
function get_Path_idNode($node_id) {
	// look up the parent of this node
	$result = mysql_query('SELECT node_id FROM '.DB_PREFIX.'site_tree'.' WHERE node_id="'.$node_id.'";');
	$row = mysql_fetch_array($result);
	// save the path in this array
	$path = array();
	// only continue if this $node isn't the root node
	// (that's the node with no parent)
	if ($row['node_id']!='') {
		// the last part of the path to $node, is the name
		// of the parent of $node
		$path[] = trim($row['node_id']);
		// we should add the path to the parent of this node
		// to the path
		$path = array_merge(get_Path_id($row['node_id']), $path);
	}
	// return the path
	return $path;
}



?>