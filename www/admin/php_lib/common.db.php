<?php
/*
  Lib:         ctrl_DB Function
  Version:     0.1.20120101
  Author:      SLivio & MiniOwl
  Contact:     silvio.coco@gmail.com
  Copiryght:   http://www.ctrl-area.com/copyright
*/


$HOST     = (isset($HOST) && ($HOST!='')) ? $HOST : '';
$DB       = (isset($DB)   && ($DB!=''))   ? $DB   : '';
$USER     = (isset($USER) && ($USER!='')) ? $USER : '';
$PSWD     = (isset($PSWD) && ($PSWD!='')) ? $PSWD : '';


function myDB_Connect() {
  GLOBAL $HOST, $DB, $USER, $PSWD;
  $CON = mysql_connect($HOST, $USER, $PSWD) or die("Database error: Could not connect to "  . $HOST);
  mysql_set_charset('utf8',$CON);
  mysql_select_db($DB) or die("Database error: Could not select database " . $DB);
  return $CON;
}


function myDB_Close($CON) {
	if ($CON) return mysql_close($CON);
	else return true;
}


function countRecord($table, $id, $condition='', $printQuery='') {
	$q = "SELECT count(".$id.") FROM ".$table." ".$condition;
	$r = mysql_query($q) or LOG__Error("countRecord[]", mysql_error()."\n".$q);
	if ($printQuery=='1') print $q;
	if (mysql_affected_rows() >= 1) {
		$return = mysql_result($r, 0);
	}else{
		$return = 0;
	}
	return $return;
}


function get_AllRecord($table, $condition='', $printQuery='') {
	$a = Array();
	$c = 0;
	$q = "SELECT * FROM ".$table." ".$condition;
	$r = mysql_query($q) or LOG__Error("get_AllRecord[]", mysql_error()."\n".$q);
	if ($printQuery=='1') print $q;
	if (mysql_affected_rows() >= 1) {
		while($rec = mysql_fetch_array($r)) {
			$a[$c] = $rec;
			$c++;
		}
	}
	return $a;
}


function get_InfoRecord($table='', $condition='', $print='') {
	$a = Array();
	$q = "SELECT * FROM ".$table." ".$condition;
	$r = mysql_query($q) or LOG__Error("get_InfoRecord[]", mysql_error()."\n".$q);
	//echo $q;
	if(mysql_affected_rows() == 1) {
		$a = mysql_fetch_array($r);
	}
	if ($print!='') print $q;
	return $a;
}


function remove_Record($table, $elementId, $id) {
	// ATTENZIONE: METTERE UN CONTROLLO... POTREBBE CANCELLARE TUTTI I RECORD SE LA CONDIZIONE E' SEMPRE VERA
	$q = "DELETE FROM ".$table." WHERE ".$elementId." = '".$id."'";
	$r = mysql_query($q) or LOG__Error("remove_Record[]", mysql_error()."\n".$q);
	//print $q;
}


function search_in_record($table, $record, $key,$printQuery=0) {
	$a = Array();
	$c = 0;
	$q = "SELECT * FROM ".$table." WHERE ".$record." LIKE '%".$key."%'";
	$r = mysql_query($q) or LOG__Error("search_in_record[]", mysql_error()."\n".$q);
	if ($printQuery=='1') print $q;
	while($rec = mysql_fetch_array($r)) {
		$a[$c] = $rec;
		$c++;
	}
	return $a;
}


function update_Record($table, $el_name, $el_value, $condition) {
	// ATTENZIONE: METTERE UN CONTROLLO... POTREBBE CANCELLARE TUTTI I RECORD SE LA CONDIZIONE E' SEMPRE VERA
	$q = "UPDATE ".$table." SET ".$el_name." = '".$el_value."' ".$condition;
	$r = mysql_query($q) or LOG__Error("remove_Record[]", mysql_error()."\n".$q);
}


?>