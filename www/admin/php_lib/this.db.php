<?php


function GDB__Get_CoreSession() {
	GLOBAL $SESSION_ID;
	$CON = $_SESSION[$SESSION_ID]['db'];
	return $CON;
}


// LastUpdate v7 2014.11.20
function GDB__Add_Log($CON, $m_id=0, $m_op='', $info='') {
	GLOBAL $SESSION_ID;
	$CON = GDB__Get_CoreSession();

	$OUT = 0;
	//
	$user_id    = isset($_SESSION[$SESSION_ID]['admin'])              ? $_SESSION[$SESSION_ID]['admin']              : '0';
	$country_id = isset($_SESSION[$SESSION_ID]['info']['country_id']) ? $_SESSION[$SESSION_ID]['info']['country_id'] : '0';
	$area_id    = isset($_SESSION[$SESSION_ID]['info']['area_id'])    ? $_SESSION[$SESSION_ID]['info']['area_id']    : '0';
	$city_id    = isset($_SESSION[$SESSION_ID]['info']['city_id'])    ? $_SESSION[$SESSION_ID]['info']['city_id']    : '0';

	$q = "INSERT INTO `".DB_PREFIX.'core_log'."` (
	`log_id`,
	`log_module`,
	`log_op`,
	`log_info`,
	`user_id`,
	`country_id`,
	`area_id`,
	`city_id`,
	`record_date`,
	`record_ip`
	) VALUES (
	NULL,
	'".$m_id."',
	'".MyEscape($m_op)."',
	'".MyEscape($info)."',
	'".$user_id."',
	'".$country_id."',
	'".$area_id."',
	'".$city_id."',
	NOW(),
	'".$_SERVER["REMOTE_ADDR"]."'
	)";
	$r = mysqli_query($CON, $q) or LOG__Error("GLOBAL__DB__Add_Log", mysqli_error($CON)."\n".$q);
	$OUT = mysqli_insert_id($CON);
	return $OUT;
}


// LastUpdate v7 2014.11.20
function GDB__Check_AdminModules($user_id='', $module_id='', $module_op='') {
	GLOBAL $CONF, $aModules, $aModule, $op, $m;
	//
	$OUT = false;
	if ($CONF['admin_id']==0 && $CONF['admin_m'] == 0) { // non c'Ë login... sempre fuori
		$OUT = false;
	} else {
		// check del modulo (controllo utente in sessione)
		if($module_op=='' && $user_id=='' && $module_id!='') {
			foreach($CONF['admin_m'] as $tmpM) {
				if ($tmpM['module_id'] == $module_id) $OUT = true; // trovato modulo
			}
		}
		// check dell'operazione (controllo utente in sessione)
		if($module_op!='' && $user_id=='' && $module_id!='') {
			foreach($CONF['admin_m'] as $tmpM) {
				$tmpM['module_op_disable'] = '';
				$aTmpOp = LOCAL_Parser_ModuleOp($tmpM['module_op_disable']);
				if ($tmpM['module_id'] == $module_id) {
					if (!in_array($module_op, $aTmpOp)) $OUT = true; // trovato modulo
				}
			}
		}
	}
	return $OUT;
}


// LastUpdate v7 2014.11.20
function GDB__Count_Admin($enable=1) {
	$CON = GDB__Get_CoreSession();
	// enable=1 > Tutti gli attivi
	// enable=0 > Tutti i non attivi
	// enable=2 > Tutti
	$OUT = 0;
	switch($enable) {
		case 0 :
			$OUT = DB__Get_numRecord($CON, DB_PREFIX.'core_admin', 'user_id', 'WHERE `user_enable`="0"', 0);
			break;
		case 1 :
			$OUT = DB__Get_numRecord($CON, DB_PREFIX.'core_admin', 'user_id', 'WHERE `user_enable`="1"', 0);
			break;
		case 2 :
			$OUT = DB__Get_numRecord($CON, DB_PREFIX.'core_admin', 'user_id', ' ', 0);
			break;
	}
	return $OUT;
}


// LastUpdate v7 2014.11.20
function GDB__Count_Group($enable=1) {
	$CON = GDB__Get_CoreSession();

	// enable=1 > Tutti gli attivi
	// enable=0 > Tutti i non attivi
	// enable=2 > Tutti
	$OUT = 0;
	switch($enable) {
		case 0 :
			$OUT = DB__Get_numRecord($CON, DB_PREFIX.'core_groups', 'group_id', 'WHERE `group_enable`="0"', 0);
			break;
		case 1 :
			$OUT = DB__Get_numRecord($CON, DB_PREFIX.'core_groups', 'group_id', 'WHERE `group_enable`="1"', 0);
			break;
		case 2 :
			$OUT = DB__Get_numRecord($CON, DB_PREFIX.'core_groups', 'group_id', ' ', 0);
			break;
	}
	return $OUT;
}

// LastUpdate v7 2014.11.20
function GDB__Get_AllModules() {
	$CON = GDB__Get_CoreSession();

	$aR = Array();
	$aR = DB__Get_allRecords($CON, DB_PREFIX.'core_modules', 'WHERE `module_id`<>"" ORDER BY module_weight DESC, module_id ASC');
	return $aR;
}


// LastUpdate v7 2014.11.20
function GDB__Get_AllWidgets() {
	$CON = GDB__Get_CoreSession();

	$aR = Array();
	$aR = DB__Get_allRecords($CON, DB_PREFIX.'core_widgets', 'WHERE `widget_id`<>"" ORDER BY widget_row ASC, widget_weight DESC,  widget_id ASC');
	return $aR;
}


// LastUpdate v7 2014.11.20
function GDB__Get_AllAdmin($enable='') {
	$CON = GDB__Get_CoreSession();
	
	$aR = Array();
	$aR = DB__Get_allRecords($CON, DB_PREFIX.'core_admin', 'WHERE user_id<>0 '.(($enable!='') ? 'AND user_enable="'.(int)$enable.'"' : '').' ORDER BY user_alias ASC, user_name ASC');
	return $aR;
}

// LastUpdate v7 2014.11.20
function GDB__Get_Admin($user_id='', $user_email='', $user_password='') {
	$CON = GDB__Get_CoreSession();

	$aR = Array();
	if ($user_email != '' && $user_password != '')
		$aR = DB__Get_infoRecord($CON, DB_PREFIX.'core_admin', ' WHERE user_email="'.$user_email.'" &&  user_password="'.md5($user_password).'"', 0);
	else if($user_email != '')
		$aR = DB__Get_infoRecord($CON, DB_PREFIX.'core_admin', ' WHERE user_email="'.$user_email.'"', 0);
	else if($user_id != '')
		$aR = DB__Get_infoRecord($CON, DB_PREFIX.'core_admin', ' WHERE user_id="'.$user_id.'"', 0);
	return $aR;
}


// LastUpdate v7 2014.11.20
function GDB__Get_Groups() {
	$CON = GDB__Get_CoreSession();
	
	$a = Array();
	$c = 0;
	$q = "SELECT * FROM ".DB_PREFIX."core_groups
	 ORDER BY group_weight DESC, group_label ASC";
	$r = mysqli_query($CON, $q) or LOG__Error("GDB__Get_Group[]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		while($rec = mysqli_fetch_array($r)) {
			$a[$c] = $rec;
			$a[$c]['count'] = DB__Get_numRecord($CON, DB_PREFIX."core_admin_groups", 'group_id', 'where group_id="'.$rec['group_id'].'"', '');
			$c++;
		}
	}
	return $a;
}


// LastUpdate v7 2014.11.20
function GDB__Get_InfoGroup($group_id=0) {
	$CON = GDB__Get_CoreSession();
	$aR = Array();
	//
	if ($group_id != 0)
		$aR = DB__Get_infoRecord($CON, DB_PREFIX.'core_groups', 'WHERE `group_id`="'.$group_id.'"', 0);
	//
	return $aR;
}


// LastUpdate v7 2014.11.20
function GDB__Get_UserGroup($g_id=0) {
	$CON = GDB__Get_CoreSession();
	
	$a = Array();
	$c = 0;
	$q = "SELECT * FROM ".DB_PREFIX."core_admin_groups,".DB_PREFIX."core_groups,".DB_PREFIX."core_admin
	WHERE ".DB_PREFIX."core_admin_groups.group_id=".DB_PREFIX."core_groups.group_id and
	".DB_PREFIX."core_admin_groups.user_id = ".DB_PREFIX."core_admin.user_id and
	".DB_PREFIX."core_groups.group_id=".$g_id."
	 ORDER BY group_weight DESC";
	$r = mysqli_query($CON, $q) or LOG__Error("GDB__Get_UserGroup[]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		while($rec = mysqli_fetch_array($r)) {
			$a[$c] = $rec;
			$c++;
		}
	}
	return $a;
}


// LastUpdate v7 2014.11.20
function GDB__Get_GroupsForUser($user_id=0, $justId=false) {
	$CON = GDB__Get_CoreSession();
	
	$a = Array();
	$b = Array();
	$c = 0;
	$q = "SELECT * FROM ".DB_PREFIX."core_admin_groups,".DB_PREFIX."core_groups
	WHERE
	".DB_PREFIX."core_admin_groups.group_id=".DB_PREFIX."core_groups.group_id AND
	".DB_PREFIX."core_admin_groups.user_id=".$user_id." AND
	".DB_PREFIX."core_groups.group_enable='1'
	 ORDER BY group_weight DESC";
	$r = mysqli_query($CON, $q) or LOG__Error("GDB__Get_GroupsForUser[]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		while($rec = mysqli_fetch_array($r)) {
			$a[$c] = $rec;
			$b[$c] = $rec['group_id'];
			$c++;
		}
	}
	if ($justId!=false) return $b;
	else return $a;
}


// LastUpdate v7 2014.11.20
function GDB__Get_AdminModules($user_id, $group=true) {
	GLOBAL $SESSION_ID;
	$CON = GDB__Get_CoreSession();
	
	$a      = Array();
	$aId    = Array(0 => 0);
	$c      = 0;
	$qWhere = '';

	// all modules for user_id
	$q = 'SELECT * FROM '.DB_PREFIX.'core_modules,'.DB_PREFIX.'core_admin_modules
	      WHERE
	      ('.DB_PREFIX.'core_admin_modules.module_id='.DB_PREFIX.'core_modules.module_id
	      AND
	      '.DB_PREFIX.'core_admin_modules.user_id='.$user_id.')
	      AND module_enable = 1
	      ORDER BY module_weight DESC, '.DB_PREFIX.'core_modules.module_id ASC';
	$r = mysqli_query($CON, $q) or LOG__Error("GDB__Get_AdminModules[1]", mysqli_error($CON)."\n".$q);
	if (mysqli_affected_rows($CON) >= 1) {
		while($rec = mysqli_fetch_array($r)) {
			$a[$c] = $rec;
			$aId[] = $rec['module_id'];
			$c++;
		}
	}
	//echo '<!-- '.$q.' -->';

	if ($group) { // se group è falso non prendo i moduli ereditati dai gruppi
		// all modules for groups of user_id
		$aGroupsUser = GDB__Get_GroupsForUser($user_id, true); // all groups for $user_id
		array_push($aGroupsUser, 0);
		array_push($aId, 0);
		$q = 'SELECT DISTINCT('.DB_PREFIX.'core_groups_modules.module_id), '.DB_PREFIX.'core_modules.*  FROM '.DB_PREFIX.'core_modules, '.DB_PREFIX.'core_groups_modules
		      WHERE
				'.DB_PREFIX.'core_modules.module_id='.DB_PREFIX.'core_groups_modules.module_id AND
				'.DB_PREFIX.'core_groups_modules.group_id IN ('.implode(',', $aGroupsUser).') AND
				'.DB_PREFIX.'core_groups_modules.module_id NOT IN ('.implode(',', $aId).') AND
				'.DB_PREFIX.'core_modules.module_enable=1
		      ORDER BY module_weight DESC';
		$r = mysqli_query($CON, $q) or LOG__Error("GDB__Get_AdminModules[1]", mysqli_error($CON)."\n".$q);
		if (mysqli_affected_rows($CON) >= 1) {
			while($rec = mysqli_fetch_array($r)) {
				$a[$c] = $rec;
				$aId[] = $rec['module_id'];
				$c++;
			}
		}
		//echo '<!-- '.$q.' -->';
	}

	// devo filtrare i moduli ed evitare i doppioni...

	// all opened modules
	$q = 'SELECT * FROM '.DB_PREFIX.'core_modules
	      WHERE
	      '.DB_PREFIX.'core_modules.module_id NOT IN ('.implode(',', $aId).') AND
	      '.DB_PREFIX.'core_modules.module_open=1
	      ORDER BY module_weight DESC';
	$r = mysqli_query($CON, $q) or LOG__Error("GDB__Get_AdminModules[2]", mysqli_error($CON)."\n".$q);
	//echo '<!-- '.$q.' -->';
	if (mysqli_affected_rows($CON) >= 1) {
		while($rec = mysqli_fetch_array($r)) {
			$a[$c] = $rec;
			$a[$c]['module_op_disable']='';
			$aId[] = $rec['module_id'];
			$c++;
		}
	}
	$a = Array_MyMultiSort($a, 'module_weight', SORT_DESC);
	return $a;
}



// LastUpdate v7 2014.11.20
function GDB__Get_InfoModule($module_id='', $module_key='') {
	$CON = GDB__Get_CoreSession();
	$aR = Array();
	//
	if ($module_id != '')
		$aR = DB__Get_infoRecord($CON, DB_PREFIX.'core_modules', 'WHERE `module_id`="'.$module_id.'"', 0);
	else if ($module_key != '')
		$aR = DB__Get_infoRecord($CON, DB_PREFIX.'core_modules', 'WHERE `module_key`="'.$module_key.'"', 0);
	//
	return $aR;
}


// LastUpdate v7 2014.11.20
function GDB__Count_AdminModule($module_id=0) {
	$CON = GDB__Get_CoreSession();
	$OUT = 0;
	//
	if ($module_id != 0)
		$OUT = DB__Get_numRecord($CON, DB_PREFIX.'core_admin_modules', 'module_id', 'WHERE `module_id`="'.$module_id.'"', 0);
	//
	return $OUT;
}


// LastUpdate v7 2014.11.20
function GDB__Count_AdminGroup($group_id=0) {
	$CON = GDB__Get_CoreSession();
	$OUT = 0;
	//
	if ($group_id != 0)
		$OUT = DB__Get_numRecord($CON, DB_PREFIX.'core_admin_groups', 'group_id', 'WHERE `group_id`="'.$group_id.'"', 0);
	//
	return $OUT;
}


// LastUpdate v7 2014.11.20
function GDB__Count_GroupModule($module_id) {
	$CON = GDB__Get_CoreSession();
	
	$OUT = 0;
	//
	if ($module_id != '')
		$OUT = DB__Get_numRecord($CON, DB_PREFIX.'core_groups_modules', 'module_id', 'WHERE `module_id`="'.$module_id.'"', 0);
	//
	return $OUT;
}


?>