<?php
/*
  Project:     Ctrl-Area
  Version:     4.201112.16 b
  Author:      SLivio
  Contact:     silvio.coco@gmail.com
  Copyright:   http://www.ctrl-area.org
*/


require_once('inc.config.php');


//-------------------------------------> [CORE_CONF]
$CONF['path_modules'] = 'modules';
$CONF['def_modules']  = ''; // Default modules
$CONF['def_page']     = 20; // Default elements for lists page
$CONF['path_module']  = ''; // Active module path
$CONF['admin_id']     = (isset($_SESSION[$SESSION_ID]['admin']) && $_SESSION[$SESSION_ID]['admin'] != '' && $_SESSION[$SESSION_ID]['admin'] != 0) ? $_SESSION[$SESSION_ID]['admin'] : 0;
$CONF['admin_m']      = (isset($_SESSION[$SESSION_ID]['modules']) && is_array($_SESSION[$SESSION_ID]['modules'])) ? $_SESSION[$SESSION_ID]['modules'] : 0;
$CONF['admin_info']   = (isset($_SESSION[$SESSION_ID]['info'])    && is_array($_SESSION[$SESSION_ID]['info']))                                      ? $_SESSION[$SESSION_ID]['info']    : 0;


$op  = (isset($_REQUEST['op'])   && $_REQUEST['op']  != '')  ? $_REQUEST['op']   : '';
$m   = (isset($_REQUEST['m'])    && $_REQUEST['m']   != '')  ? $_REQUEST['m']    : $CONF['def_modules'];
$p   = (isset($_REQUEST['p'])    && $_REQUEST['p']   != '')  ? $_REQUEST['p']    : 0;
$pop = (isset($_REQUEST['pop'])  && $_REQUEST['pop'] != '')  ? true              : false;
$opp = (isset($_REQUEST['opp'])  && $_REQUEST['opp'] != '')  ? $_REQUEST['opp']  : '';
//$app = (isset($_REQUEST['app'])  && $_REQUEST['app'] != '')  ? true              : false;
$app = true;

$aModules = GDB__Get_AdminModules($CONF['admin_id']);
$aModule  = Array();

if ($m != '') {
	$aModule              = GDB__Get_InfoModule($m);
	$CONF['path_module']  = $CONF['path_modules'].'/'.$aModule['module_dir'].'/';
}


?>